/*
 * testclass.h
 *
 *  Created on: 2018/02/17
 *      Author: masah
 */

#ifndef TESTCLASS_H_
#define TESTCLASS_H_

#include <iostream>
using namespace std;
class testclass {
public:
	testclass();
	virtual ~testclass();

	void run();

};

#endif /* TESTCLASS_H_ */
