/*
 * Block.h
 *
 *  Created on: 2018/02/18
 *      Author: masah
 */

#ifndef BLOCK_H_
#define BLOCK_H_

#define BLOCK_BLACK 0
#define BLOCK_RED 1
#define BLOCK_YELLOW 2
#define BLOCK_BLUE 3
#define BLOCK_GREEN 4

#define BLOCK_MAX 5

//ブロッククラス
//ブロックの場所は黒ブロック位置番号で表される。

class Block {
public:
	Block();
	virtual ~Block();

	int	inputNum;		//初期位置コードからの位置番号
	int	startPosition;	//最初の位置番号（黒ブロック位置番号で）
	int color;			// ブロックの色

};

#endif /* BLOCK_H_ */
