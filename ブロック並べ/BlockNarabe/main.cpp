/* hello.c */

#include <iostream>
#include "Block.h"
#include "shape/ObjPentagon.h"

using namespace std;

int handInputCode(int *green);
int decodeInitial(int shoki, Block *mBlock);

char COLORNAME[5][7]={
	{"BLACK"},
	{"RED"},
	{"YELLOW"},
	{"BLUE"},
	{"GREEN"}
};


int main(){
	int i;
	int shoki,green;

    Block mBlock[5];
    ObjPentagon mPentagon;

    cout << "Start Block NARABE!!" << endl;


    /*初期位置コード生成*/
    shoki = handInputCode(&green);
    cout << "初期位置コードは" << shoki << "です。"<<endl;
//    cout << "GREEN "<< green << endl;

    //緑は手入力をそのまま入れる
	mBlock[BLOCK_GREEN].inputNum = green;

    /*初期位置コードから全カラーのブロック位置を解析する*/
	if(decodeInitial(shoki, mBlock)==-1){
		cout << "decodeInitial error!" <<endl;
	}

	//debug
    cout << "InputNum" <<endl;
    for (i=0;i<5;i++){
    	cout << "  Input No = " << mBlock[i].inputNum ;
       	cout << ", "<< COLORNAME[mBlock[i].color] << endl;

    }
    cout << "StartPosition" <<endl;
    for (i=0;i<5;i++){
    	cout << "  Start No = " << mBlock[i].startPosition;
       	cout << ", "<< COLORNAME[mBlock[i].color] << endl;

    }





	/*走行結果を算出する*/
		//①五角形
		//（ボーナス25秒　図形15秒＋有効移動 2*5=10秒）
		//の運搬方法を計算し、走行距離を出す

    mPentagon.makeScenario();

		//②三角形×２＋凹四角形＋センター　左右２パターン
		//（ボーナス25秒　図形12秒＋有効移動2*4+5=13秒）
		//の運搬方法を計算し、走行距離を出す


		//③四角形＋センター　４パターン
		//（ボーナス21秒　図形8秒＋有効移動2*4+5=13秒）
		//の運搬方法を計算し、走行距離を出す


		//④四角形＋三角形　８パターン
		//（ボーナス23秒　図形13秒＋有効移動2*5=10秒）
		//の運搬方法を計算し、走行距離を出す


		//結果を比較し、ベストコースを決定する。


	/*結果を表示する*/




}
#if 1
//初期位置コードからブロックの位置を割り出す関数
int decodeInitial(int shoki,Block *mBlock)
{
	int i;
	//ブロックの配置番号を、ブロック位置番号（黒を基準）に変換するためのテーブル
	int colortable[4][15]={
		{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15},	//黒
		{-1,1,2,3,4,5,-1,-1,6,7,8,9,10,-1,11},	//赤
		{1,2,-1,3,-1,4,5,6,7,8,9,10,-1,11,-1},	//黄
		{1,-1,2,-1,3,4,5,6,-1,7,8,-1,9,10,11}	//青
	};


	//初期位置コードから色番号算出
	mBlock[BLOCK_BLACK].inputNum=shoki/(11*11*11)+1;
	shoki -=(mBlock[BLOCK_BLACK].inputNum -1)*11*11*11;
	//	printf("\nDEBUG: shoki:%d\n",shoki);

	mBlock[BLOCK_RED].inputNum=shoki/(11*11)+1;
	shoki -=(mBlock[BLOCK_RED].inputNum -1)*11*11;
	//	printf("\nDEBUG: shoki:%d\n",shoki);

	mBlock[BLOCK_YELLOW].inputNum=shoki/(11)+1;
	shoki -=(mBlock[BLOCK_YELLOW].inputNum -1)*11;
	//	printf("\nDEBUG: shoki:%d\n",shoki);

	mBlock[BLOCK_BLUE].inputNum=shoki+1;

//入力した番号はカラーごとにブロック位置が違うので、
//黒ブロック位置番号に合わせて変換する。

	if(mBlock[BLOCK_GREEN].inputNum>15)
	 	return(-1);
	else {
		mBlock[BLOCK_GREEN].color=BLOCK_GREEN;
		mBlock[BLOCK_GREEN].startPosition = mBlock[BLOCK_GREEN].inputNum;	//緑はそのまま

	}
	if(mBlock[BLOCK_BLACK].inputNum>15)
	 	return(-1);
	else {
		mBlock[BLOCK_BLACK].color=BLOCK_BLACK;
		for(i=0;i<15;i++){
			if( colortable[BLOCK_BLACK][i]==mBlock[BLOCK_BLACK].inputNum)
				mBlock[BLOCK_BLACK].startPosition =i+1;
		}
	}


	if(mBlock[BLOCK_RED].inputNum>11)
	 	return(-1);
	else {
		mBlock[BLOCK_RED].color=BLOCK_RED;
		for(i=0;i<15;i++){
			if( colortable[BLOCK_RED][i]==mBlock[BLOCK_RED].inputNum)
				mBlock[BLOCK_RED].startPosition =i+1;
		}
	}


	if(mBlock[BLOCK_YELLOW].inputNum>11)
	 	return(-1);
	else {
		mBlock[BLOCK_YELLOW].color=BLOCK_YELLOW;
		for(i=0;i<15;i++){
			if( colortable[BLOCK_YELLOW][i]==mBlock[BLOCK_YELLOW].inputNum)
				mBlock[BLOCK_YELLOW].startPosition =i+1;
		}
	}

	if(mBlock[BLOCK_BLUE].inputNum>11)
	 	return(-1);
	else {
		mBlock[BLOCK_BLUE].color=BLOCK_BLUE;
		for(i=0;i<15;i++){
			if( colortable[BLOCK_BLUE][i]==mBlock[BLOCK_BLUE].inputNum)
				mBlock[BLOCK_BLUE].startPosition =i+1;
		}
	}


	return(1);
}

#endif


//手入力から初期位置コードを生成する
int handInputCode(int *gr){
	int shoki;
	int black,blue,red,yellow,green;

	//５色　手入力
	printf("Number of Green:");
	cin >> green;
	printf("Number of Black:");
	cin >> black;
	printf("Number of Red:");
	cin >> red;
	printf("Number of Yellow:");
	cin >> yellow;
	printf("Number of Blue:");
	cin >> blue;

	*gr = green;

	cout << "入力：緑 "<< green <<endl;
	cout << "入力：黒 "<< black<<endl;
	cout << "入力：赤 "<< red <<endl;
	cout << "入力：黄 "<< yellow <<endl;
	cout << "入力：青 "<< blue <<endl;


	//初期位置コード生成
	shoki = (black-1) *11 *11 *11
		+(red-1) *11 *11
		+(yellow-1) *11
		+(blue-1);

	return shoki;
}

