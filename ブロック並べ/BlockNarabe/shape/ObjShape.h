/*
 * ObjShape.h
 *
 *  Created on: 2018/02/18
 *      Author: masah
 */

#ifndef OBJSHAPE_H_
#define OBJSHAPE_H_

class ObjShape {
public:
	ObjShape();
	virtual ~ObjShape();

	int point[2][5];//目標図形の頂点番号

	void makeScenario();//目標図形シナリオを作成する。

};

#endif /* OBJSHAPE_H_ */
