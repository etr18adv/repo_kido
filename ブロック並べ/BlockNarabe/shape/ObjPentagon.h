/*
 * ObjPentagon.h
 *
 *  Created on: 2018/02/18
 *      Author: masah
 */

#ifndef SHAPE_OBJPENTAGON_H_
#define SHAPE_OBJPENTAGON_H_

#include "ObjShape.h"

class ObjPentagon: public ObjShape {
public:
	ObjPentagon();
	virtual ~ObjPentagon();

	void makeScenario();
};




#endif /* SHAPE_OBJPENTAGON_H_ */
