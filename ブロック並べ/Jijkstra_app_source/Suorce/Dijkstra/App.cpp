// Dijkstra.cpp : アプリケーションのエントリ ポイントを定義します。
//
#include "stdafx.h"
#include "resource.h"

#include <commdlg.h>

#include "dijkstra.h"

#define MAX_LOADSTRING 100

// グローバル変数:
TCHAR szTitle[MAX_LOADSTRING];					// タイトル バーのテキスト
TCHAR szWindowClass[MAX_LOADSTRING];			// メイン ウィンドウ クラス名

// このコード モジュールに含まれる関数の宣言を転送します:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

class MyApp
{
	HWND m_hWnd;

	Dijkstra m_dijkstra;//ダイクストラ法計算クラス

	Node*	m_select;//選択ノード（接続で使う）

	//コマンド
	enum Mode
	{
		Edit_Node,		//ノード追加、接続
		Select_Term,	//スタート・ゴール点指定
	};
	Mode	m_currMode;//現在のコマンド


	//コマンドの途中操作リセット
	void ResetCmd()
	{
		m_select = NULL;
	}
	
	//コマンド変更
	void SetMode( Mode mode)
	{
		ResetCmd();
		m_currMode = mode;
	}

public:
	MyApp()
	{
		m_hWnd = NULL;
		m_select = NULL;

		m_currMode = Edit_Node;

	}

	//ファイル開く
	void Open()
	{
		WCHAR strFile[MAX_PATH];
		ZeroMemory(strFile, sizeof( strFile ));
		wsprintf(strFile, _T("./dijkstra_save.txt") );

		OPENFILENAME ofn;
		ZeroMemory(&ofn, sizeof( ofn ));
		
		ofn.lStructSize		= sizeof (OPENFILENAME);
		ofn.hwndOwner		= m_hWnd;
		ofn.lpstrFilter		= _T("TXT files {*.txt}\0*.txt\0");
		ofn.lpstrDefExt		=_T("txt");
		ofn.nFilterIndex	= 0;
		ofn.lpstrFile		= strFile;
		ofn.nMaxFile		= _MAX_PATH;
		ofn.Flags			= OFN_FILEMUSTEXIST;
		
		if ( !GetOpenFileName( &ofn ) ) {
			return;
		}

		ResetCmd();
		
		if ( !m_dijkstra.Inport(strFile) ) {
			::MessageBox( m_hWnd,_T("ファイル情報が正しくありません。"),szTitle,MB_OK );
		}

		m_dijkstra.SearchRoot();
		::InvalidateRect( m_hWnd, NULL, true );
	}

	//保存
	void Save()
	{
		WCHAR strFile[MAX_PATH];
		ZeroMemory(strFile, sizeof( strFile ));
		wsprintf(strFile, _T("./dijkstra_save.txt") );

		OPENFILENAME ofn;
		ZeroMemory(&ofn, sizeof( ofn ));
		
		ofn.lStructSize		= sizeof (OPENFILENAME);
		ofn.hwndOwner		= m_hWnd;
		ofn.lpstrFilter		= _T("TXT files {*.txt}\0*.txt\0");
		ofn.lpstrDefExt		=_T("txt");
		ofn.nFilterIndex	= 0;
		ofn.lpstrFile		= strFile;
		ofn.nMaxFile		= _MAX_PATH;
		ofn.Flags			= OFN_FILEMUSTEXIST;
		
		if ( !GetSaveFileName( &ofn ) ) {
			return;
		}
		m_dijkstra.Export(strFile);
	}
	
	//ノード登録・接続
	void Cmd_Edit(){
		SetMode( Edit_Node );
		::InvalidateRect( m_hWnd, NULL, true );
	}

	//スタート・ゴール点指定
	void Cmd_SelectTerm(){
		SetMode( Select_Term );
		::InvalidateRect( m_hWnd, NULL, true );
	}

	//全ノードクリア
	void Clear()
	{
		ResetCmd();
		m_dijkstra.Clear();
		::InvalidateRect( m_hWnd, NULL, true );
	}

	//右クリックでの操作
	void OnRClick( int x, int y )
	{
		switch( m_currMode ) {
			case Edit_Node:
				{
					//ノードやノード接続を削除
					Node* n = m_dijkstra.Query(x,y);
					if ( n != NULL ) {
						m_dijkstra.Remove(n);
						m_dijkstra.SearchRoot();
						break;
					}
					
					NodePair pair;
					if ( m_dijkstra.QueryLineOnNodePair( &pair, x,y) ) {
						m_dijkstra.RemoveConnect( pair.n1, pair.n2 );
						m_dijkstra.SearchRoot();
						break;
					}
				}
				break;
			case Select_Term:
				{
					//ゴールノードの指定
					Node* n = m_dijkstra.Query(x,y);
					if ( n != NULL ) {
						m_dijkstra.SelctGoal(n);
						m_dijkstra.SearchRoot();
					}
				}
				break;
		}

		::InvalidateRect( m_hWnd, NULL, true );
	}
	
	//左クリックでの操作
	void OnLClick( int x, int y )
	{
		switch( m_currMode ) {
			case Edit_Node:
				{
					do {
						if ( !m_select ) {
							Node* n = m_dijkstra.Query(x,y);
							if ( n != NULL ) {
								//クリック位置のノードを選択
								m_select = n;
								break;
							}
						} else {
							Node* n = m_dijkstra.Query(x,y);
							
							if( n ) {
								//２こめのノード選択したら接続
								if ( n != m_select ) {
									m_dijkstra.ConnectNode(m_select, n);
									m_dijkstra.SearchRoot();
								}

							}

							//キャンセル
							m_select = NULL;
							break;

						}
						
						if ( m_currMode == Edit_Node ) {
							//それ以外はノード追加
							m_dijkstra.AddNode(x,y);
						}

					}while(0);
				}
				break;
			case Select_Term:
				{
					//スタートノードの指定
					Node* n = m_dijkstra.Query(x,y);
					if ( n != NULL ) {
						m_dijkstra.SelctStart(n);
						m_dijkstra.SearchRoot();
					}
				}
				break;
		}
		
		::InvalidateRect( m_hWnd, NULL, true );
	}

	//アプリ実行
	int Run()
	{
		MSG msg;
		HACCEL hAccelTable;

		// グローバル文字列を初期化しています。
		LoadString(::GetModuleHandle(NULL), IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
		LoadString(::GetModuleHandle(NULL), IDC_DIJKSTRA, szWindowClass, MAX_LOADSTRING);

		WNDCLASSEX wcex;

		wcex.cbSize = sizeof(WNDCLASSEX);

		wcex.style			= CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc	= WndProc;
		wcex.cbClsExtra		= 0;
		wcex.cbWndExtra		= 0;
		wcex.hInstance		= ::GetModuleHandle(NULL);
		wcex.hIcon			= LoadIcon(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_DIJKSTRA));
		wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
		wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
		wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_DIJKSTRA);
		wcex.lpszClassName	= szWindowClass;
		wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

		RegisterClassEx(&wcex);
   
		m_hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, 800, 800, NULL, NULL, ::GetModuleHandle(NULL), NULL);

		if (!m_hWnd)
		{
			return FALSE;
		}

		//サンプルファイルを読む
		if ( m_dijkstra.Inport(_T("./dijkstra_sample.txt")) ) {

			m_dijkstra.SearchRoot();
			::InvalidateRect( m_hWnd, NULL, true );

			//開始、終了コマンドに
			SetMode( Select_Term );
		}

		ShowWindow(m_hWnd, SW_SHOW);
		UpdateWindow(m_hWnd);


		hAccelTable = LoadAccelerators(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDC_DIJKSTRA));

		// メイン メッセージ ループ:
		while (GetMessage(&msg, NULL, 0, 0))
		{
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		return (int) msg.wParam;
	}

	void Draw(HDC hdc)
	{

		//ノード描画
		m_dijkstra.Draw(hdc);

		TCHAR text[1000];
		ZeroMemory(text, sizeof(text));

		//選択ノードを赤く
		if ( m_select ) {
			HBRUSH brs  = CreateSolidBrush(RGB(255,0,0));
			HBRUSH brsOld = (HBRUSH)SelectObject( hdc , brs );

			Ellipse( hdc, m_select->x - Circle_r, m_select->y - Circle_r, m_select->x + Circle_r, m_select->y + Circle_r );
			
			SelectObject( hdc , brsOld );
			DeleteObject(brs);
		}
	
		//説明書き
		{

			int linecount = 0;
			linecount++;
			int margin = 10;
			ZeroMemory(text, sizeof(text));

			switch( m_currMode ) {
				case Edit_Node:
			
					wsprintf(text, TEXT("ノード登録：何もないスペースをクリック") );
					TextOut(hdc, margin, 20*linecount, text, wcslen(text));
					linecount++;

					wsprintf(text, TEXT("ノード接続：ノードを２つクリック") );
					TextOut(hdc, margin, 20*linecount, text, wcslen(text));
					linecount++;

					wsprintf(text, TEXT("削除:ノードやラインを右クリック") );
					TextOut(hdc, margin, 20*linecount, text, wcslen(text));
					linecount++;
					linecount++;

					if ( m_select ) {
						wsprintf(text, TEXT("- 接続するノードの２つ目をクリックして下さい。- ") );
						TextOut(hdc, margin, 20*linecount, text, wcslen(text));
						linecount++;
					}
					break;
				case Select_Term:
					
					wsprintf(text, TEXT("スタート地点を左クリックで、ゴール地点を右クリックで指定できます。") );
					TextOut(hdc, margin, 20*linecount, text, wcslen(text));
					linecount++;				
					break;
			}
			
			
		}

	}
};


static MyApp myApp;


int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	myApp.Run();
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// 選択されたメニューの解析:
		switch (wmId)
		{
		case IDM_OPEN:
			myApp.Open();
			break;
		case IDM_SAVE:
			myApp.Save();
			break;
		case IDM_EDIT:
			myApp.Cmd_Edit();//ノード登録・接続
			break;
		case IDM_SELECTTERM:
			myApp.Cmd_SelectTerm();//スタート・ゴール点指定
			break;
		case IDM_CLEAR:
			myApp.Clear();
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_LBUTTONDOWN:
		{
			myApp.OnLClick( LOWORD(lParam), HIWORD(lParam) );
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_RBUTTONDOWN:
		{
			myApp.OnRClick( LOWORD(lParam), HIWORD(lParam) );
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: 描画コードをここに追加してください...
		myApp.Draw(hdc);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}