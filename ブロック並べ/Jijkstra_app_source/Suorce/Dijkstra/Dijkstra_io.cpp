
#include "stdafx.h"
#include "dijkstra.h"

struct IntPair
{
	int i1;
	int i2;

	IntPair(int _i1, int _i2 )
	{
		i1 = _i1;
		i2 = _i2;
	}
	bool ChkSame( IntPair* chk )
	{
		if(i1 == chk->i1 && i2 == chk->i2 ) {return true;}
		if(i1 == chk->i2 && i2 == chk->i1 ) {return true;}

		return false;
	}
};

//ファイル読み込み
bool Dijkstra::Inport(LPCWSTR filePath)
{
	bool ret = false;

	Clear();

	FILE* fp = NULL;

	try {

		_wfopen_s( &fp, filePath, _T("r") );

		if ( !fp ) { throw 0; }

		WCHAR s[256];
		if ( !fgetws( s, 256, fp ) ) {
			throw 0;
		}
		if ( wcscmp(s, _T("//NODE POINTS\n")) != 0 ) {
			throw 0;
		}

		while( fgetws( s, 256, fp ) ) {
			if ( wcscmp(s, _T("//START NODE\n")) == 0 ) {
				break;
			}
			int x,y;
			WCHAR *token, *nxtToken;
			token = wcstok_s( s, _T(","), &nxtToken );
			x = _wtoi(token);
			token = wcstok_s( NULL, _T(","), &nxtToken );
			if ( !token ) {
				throw 0;
			}
			y = _wtoi(token);

			AddNode(x,y);
		}
		
		//スタートノード
		{
			if ( !fgetws( s, 256, fp ) ) {
				throw 0;
			}
			Node* n = QueryFromIdx(_wtoi(s));
			if ( n ) { m_start = n; }
		}

		//ゴールノード
		{
			if ( !fgetws( s, 256, fp ) ) {
				throw 0;
			}
			if ( wcscmp(s, _T("//GOAL NODE\n")) != 0 ) {
				throw 0;
			}

			if ( !fgetws( s, 256, fp ) ) {
				throw 0;
			}

			Node* n = QueryFromIdx(_wtoi(s));
			if ( n ) { m_goal = n; }
		}

		//接続情報読み込み
		if ( !fgetws( s, 256, fp ) ) {
			throw 0;
		}
		if ( wcscmp(s, _T("//CONNECT INFO\n")) != 0 ) {
			throw 0;
		}
		while( fgetws( s, 256, fp ) ) {
			int i1,i2;
			WCHAR *token, *nxtToken;
			token = wcstok_s( s, _T(","), &nxtToken );
			i1 = _wtoi(token);
			token = wcstok_s( NULL, _T(","), &nxtToken );
			if ( !token ) {
				throw 0;
			}
			i2 = _wtoi(token);

			Node* n1 = QueryFromIdx(i1);
			Node* n2 = QueryFromIdx(i2);
			if ( !n1 || !n2 || ( n1 == n2 ) ) {
				throw 0;
			}
			
			ConnectNode( n1, n2 );
		}
		
		ret = true;

	} catch(...) {
		Clear();
	}
	
	if ( fp ) {
		fclose(fp);
	}

	return ret;

}

//ファイル書き込み
bool Dijkstra::Export( LPCWSTR filePath )
{
	FILE* fp;
	_wfopen_s( &fp, filePath, _T("w") );

	if ( fp ) {

		//ノード点
		fputws(_T("//NODE POINTS\n"),fp);
		TCHAR text[1000];
		ZeroMemory(text, sizeof(text));

		std::list<Node*>::iterator it = m_node.begin();
		for ( ; it != m_node.end(); it++ ) {		
			wsprintf(text, _T("%d,%d\n"), (*it)->x, (*it)->y );
			fputws(text,fp);
		}

		//スタートノード インデックス
		fputws(_T("//START NODE\n"),fp);
		wsprintf(text, _T("%d\n"), QueryIdx(m_start) );
		fputws(text,fp);

		//ゴールノード インデックス
		fputws(_T("//GOAL NODE\n"),fp);
		wsprintf(text, _T("%d\n"), QueryIdx(m_goal) );
		fputws(text,fp);

		//接続情報
		fputws(_T("//CONNECT INFO\n"),fp);
		
		//接続ノードの組み合わせを重複除外して列挙
		std::list<IntPair> addPair;
		it = m_node.begin();
		int currIdx = 0;
		for ( ; it != m_node.end(); it++, currIdx++ ) {

			std::list<NodeConnect>::iterator it_cnct = (*it)->m_connectNode.begin();
			for ( ; it_cnct != (*it)->m_connectNode.end(); it_cnct++ ) {
				int sideNodeIdx = QueryIdx(it_cnct->node);
				
				IntPair add(currIdx, sideNodeIdx);

				//重複なければ登録
				bool find = false;
				std::list<IntPair>::iterator it_pair = addPair.begin();
				for ( ; it_pair != addPair.end(); it_pair++ ) {
					if ( add.ChkSame(&( *it_pair) ) ) {
						find = true;
						break;
					}
				}
				if ( find ) {
					continue;
				}
				addPair.push_back(add);

				wsprintf(text, _T("%d,%d\n"), currIdx, sideNodeIdx );
				fputws(text,fp);
			}
		}

		fclose(fp);
	} else {
		return false;
	}

	return true;
}