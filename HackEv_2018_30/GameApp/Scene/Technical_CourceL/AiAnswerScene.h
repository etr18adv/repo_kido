#ifndef _AI_ANSWER_SCENE_
#define _AI_ANSWER_SCENE_

/** Include file *************************************************************/
#include "BaseScene.h"
#include "AiAnswerBody.h"
#include "PositionCalculator.h"

/** using�錾 ****************************************************************/


/* Class ******************************************************************/
class AiAnswerScene : public BaseScene
{
	public:
		AiAnswerScene( AiAnswerBody*, PositionCalculator* );
		~AiAnswerScene( );

		void Run( );

	private:
		AiAnswerBody* aiAnswerBody;
		PositionCalculator* positionCalculator;

		int RelativePotsitionId;

		void Start( );

		void GoToPosition( int Speed, int tx, int ty, int TargetRange, int id ){
			aiAnswerBody->GoToPosition( Speed, tx, ty, id );
			while( !positionCalculator->RangeJuge( tx, ty, TargetRange, id ) ){
				tslp_tsk( 4 );	/* 4ms�ҋ@ */
			}
		};
};

#endif 
