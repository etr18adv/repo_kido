/** Include file *************************************************************/
#include "AiAnswerScene.h"

/** using宣言 ****************************************************************/

/* Function ******************************************************************/
AiAnswerScene::AiAnswerScene( 
	AiAnswerBody* mAiAnswerBody, 
	PositionCalculator* mPositionCalculator):
	aiAnswerBody( mAiAnswerBody ),
	positionCalculator( mPositionCalculator )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
AiAnswerScene::~AiAnswerScene( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void AiAnswerScene::Start( )
/*****************************************************************************/
{
	aiAnswerBody->mStartRun( );
	const RGB_Checker* rgbC = aiAnswerBody->GetRgb( );

	aiAnswerBody->LineTrace( );
	while( !rgbC->ColorJude( RGB_Checker::Color::Green ) ){
		tslp_tsk( 4 );	/* 4ms待機 */
	}
	aiAnswerBody->Stop( );

	//aiAnswerBody->RelativeCheck( );
	//while( 1 ){
	//	tslp_tsk( 4 );	/* 4ms待機 */
	//}
	this->RelativePotsitionId = positionCalculator->CreateId();

	GoToPosition( 10,   10, 10, 2, this->RelativePotsitionId );
	GoToPosition( 10,   10, 50, 2, this->RelativePotsitionId );
	GoToPosition( 10,   20, 30, 2, this->RelativePotsitionId );
	GoToPosition( 10,  -20, 30, 2, this->RelativePotsitionId );
	GoToPosition( 10,  -10, 10, 2, this->RelativePotsitionId );
	GoToPosition( 10,  -10, 50, 2, this->RelativePotsitionId );

	aiAnswerBody->Stop( );
}

/* Function ******************************************************************/
void AiAnswerScene::Run( )
/*****************************************************************************/
{

}

