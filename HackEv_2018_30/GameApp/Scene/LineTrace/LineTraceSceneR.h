#ifndef _LINETRACE_R_SCENE_
#define _LINETRACE_R_SCENE_

/** Include file *************************************************************/
#include "LineTraceScene.h"
#include "LineTraceBody.h"

/** using�錾 ****************************************************************/


/* Class ******************************************************************/
class LineTraceSceneR : public LineTraceScene
{
	public:
		LineTraceSceneR( LineTraceBody*, PositionCalculator* );
		~LineTraceSceneR( );

		void Run( );

	private:
		void Start( );
		
};

#endif 
