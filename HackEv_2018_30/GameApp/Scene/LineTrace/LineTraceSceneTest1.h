#ifndef _LINETRACE_TEST1_SCENE_
#define _LINETRACE_TEST1_SCENE_

/** Include file *************************************************************/
#include "LineTraceScene.h"
#include "LineTraceBody.h"

/** using�錾 ****************************************************************/


/* Class ******************************************************************/
class LineTraceSceneTest1 : public LineTraceScene
{
	public:
		LineTraceSceneTest1( LineTraceBody*, PositionCalculator* );
		~LineTraceSceneTest1( );

		void Run( );

	private:
		void Start( );
		
};

#endif 
