/** Include file *************************************************************/
#include "BlockSortScene.h"

/** using宣言 ****************************************************************/


/* Function ******************************************************************/
BlockSortScene::BlockSortScene( 
	BlockSortBody* mBlockSortBody,
	PositionCalculator* mPositionCalculator,
	const BlockSortDataManager* mblockSortDataManager ):
	blockSortBody( mBlockSortBody ),
	positionCalculator( mPositionCalculator ),
	blockSortDataManager( mblockSortDataManager )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
BlockSortScene::~BlockSortScene( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void BlockSortScene::Start( )
/*****************************************************************************/
{
	blockSortBody->mStartRun( );						/* ←これは必須 */
	const RGB_Checker* rgbC = blockSortBody->GetRgb( );	/* RGBデータ取得用 */

	/* 初期位置コード取得 */
	int initCode = blockSortDataManager->GetInitPositionCode();
	/* 各ブロックの色情報取得 */
	BlockSortDataManager::ColorBlocks colorBlocks = blockSortDataManager->GetColorBlocks();

#if 0
	/* ライントレース */
	blockSortBody->LineTrace( );
	while( !rgbC->ColorJude( RGB_Checker::Color::Red ) ){
		/* 赤検知まで実行 */
		tslp_tsk( 4 );	/* 4ms待機 */
	}
	blockSortBody->Stop( );

	/* 相対座標の取得 */
	this->RelativePotsitionId = positionCalculator->CreateId();

	/* 指定座標まで移動 */
	GoToPosition( 30, -40,  0, 2, this->RelativePotsitionId );
	GoToPosition( 30, -40, 45, 2, this->RelativePotsitionId );
	GoToPosition( 30, -80, 90, 2, this->RelativePotsitionId );
	GoToPosition( 30,  40, 45, 2, this->RelativePotsitionId );
	GoToPosition( 30,   0,  0, 2, this->RelativePotsitionId );

#else
	/* 相対座標の取得 */
	this->RelativePotsitionId = positionCalculator->CreateId();

	/* バックする */
	blockSortBody->Back( 20, 50, RelativePotsitionId );
	while( blockSortBody->GetBusyState( ) ){
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	/* 左足中心に向きを変える */
	blockSortBody->TurnLeft( 20, -90, RelativePotsitionId );
	while( blockSortBody->GetBusyState( ) ){
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	/* バックする */
	blockSortBody->Back( 20, 50, RelativePotsitionId );
	while( blockSortBody->GetBusyState( ) ){
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	/* 右足中心に向きを変える */
	blockSortBody->TurnRight( 20, 90, RelativePotsitionId );
	while( blockSortBody->GetBusyState( ) ){
		tslp_tsk( 4 );	/* 4ms待機 */
	}

#endif
	/* 停止 */
	blockSortBody->Stop( );
}

/* Function ******************************************************************/
void BlockSortScene::Run( )
/*****************************************************************************/
{

}

