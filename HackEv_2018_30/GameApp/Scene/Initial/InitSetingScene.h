#ifndef _INIT_SETTING_SCENE_
#define _INIT_SETTING_SCENE_

/** Include file *************************************************************/
#include "BaseScene.h"

/** using�錾 ****************************************************************/

/* Class ******************************************************************/
class InitSetingScene : public BaseScene
{
	public:
		InitSetingScene( );
		~InitSetingScene( );

		void Run( );

	private:
		void Start( );
};

#endif 
