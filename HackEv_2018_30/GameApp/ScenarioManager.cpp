
/** Include file *************************************************************/
#include "ScenarioManager.h"

#include "InitSetingScene.h"
#include "ReadyScene.h"
#include "LineTraceSceneL.h"
#include "LineTraceSceneR.h"
#include "AiAnswerScene.h"
#include "BlockSortScene.h"
#include "L_ParkingScene.h"
#include "R_ParkingScene.h"
#include "Test1Scene.h"
#include "LineTraceSceneTest1.h"

/** using宣言 ****************************************************************/
using ev3api::TouchSensor;

std::vector<std::string> ScenarioManager::ScenarioNameList;

/* Function ******************************************************************/
ScenarioManager::ScenarioManager( 
	const BlockSortDataManager* blockSortDataManager,
	WaitBody* WaitBody,
	LineTraceBody* lineTraceBody,
	AiAnswerBody*  aiAnswerBody,
	BlockSortBody* blockSortBody,
	L_ParkingBody* l_ParkingBody,
	R_ParkingBody* r_ParkingBody,
	PositionCalculator* positionCalculator ):
	nowScenarioId( 0 )
/*****************************************************************************/
{
	/* スタートシナリオ作成 */
	InitScenario = new Scenario("InitScenario");
	InitScenario->AddScene( new InitSetingScene( ) );
	InitScenario->AddScene( new ReadyScene( ) );

	/* Lコースシナリオ作成 */
	Scenario *l_Cource = new Scenario("L-Cource");
	l_Cource->AddScene( new LineTraceSceneL( lineTraceBody, positionCalculator ) );
	l_Cource->AddScene( new AiAnswerScene( aiAnswerBody, positionCalculator ) );
	l_Cource->AddScene( new L_ParkingScene( l_ParkingBody ) );
	scenarioList.push_back( l_Cource );

	/* Rコースシナリオ作成 */
	Scenario *r_Cource = new Scenario("R-Cource");
	r_Cource->AddScene( new LineTraceSceneR( lineTraceBody, positionCalculator ) );
	l_Cource->AddScene( new BlockSortScene( blockSortBody, positionCalculator, blockSortDataManager ) );
	l_Cource->AddScene( new R_ParkingScene( r_ParkingBody ) );
	scenarioList.push_back( r_Cource );

	/* テスト1シナリオ作成 */
	Scenario *t1_Cource = new Scenario("TestScenario1");
	t1_Cource->AddScene( new BlockSortScene( blockSortBody, positionCalculator, blockSortDataManager ) );
	scenarioList.push_back( t1_Cource );

	/* テスト2シナリオ作成 */
	Scenario *t2_Cource = new Scenario("TestScenario2");
	t2_Cource->AddScene( new LineTraceSceneTest1( lineTraceBody, positionCalculator ) );
	scenarioList.push_back( t2_Cource );

	/* テスト3シナリオ作成 */
	Scenario *t3_Cource = new Scenario("TestScenario3");
	t3_Cource->AddScene( new AiAnswerScene( aiAnswerBody, positionCalculator ) );
	scenarioList.push_back( t3_Cource );

	{
		int i;
		for( i = 0; i < (int)scenarioList.size( ); i++ ){
			ScenarioNameList.push_back( scenarioList[ i ]->Name );
		}
	}
}

/* Function ******************************************************************/
std::vector<std::string> ScenarioManager::GetScenarioNameList()
/*****************************************************************************/
{
	return ScenarioNameList;
}

/* Function ******************************************************************/
void ScenarioManager::InitScenarioSatrt( )
/*****************************************************************************/
{
	int sceneNo = 0;

	while( sceneNo < (int)InitScenario->SceneList.size( ) ){
		SceneStart( InitScenario->SceneList[ sceneNo ] );
		sceneNo++;
	}
}

/* Function ******************************************************************/
void ScenarioManager::ScenarioSatrt( )
/*****************************************************************************/
{
	/* シナリオ開始 */
	nowSceneNo = 0;

	while( nowSceneNo < (int)scenarioList[ nowScenarioId ]->SceneList.size( ) ){
		SceneStart( scenarioList[ nowScenarioId ]->SceneList[ nowSceneNo ] );
		nowSceneNo++;
	}
}

/* Function ******************************************************************/
void ScenarioManager::SceneStart( BaseScene *scene )
/*****************************************************************************/
{
	nowScene = scene;

	/* シーン開始 */
	nowScene->SceneStart( );

	/* 完了待ち */
	while( !nowScene->Finished( ) ){
		tslp_tsk( 4 );	/* 4ms待機 */
	}
}

/* Function ******************************************************************/
void ScenarioManager::Thread( )
/*****************************************************************************/
{
	if( NULL != nowScene ){
		/* シーン定期処理 */
		nowScene->Run( );
	}
}

