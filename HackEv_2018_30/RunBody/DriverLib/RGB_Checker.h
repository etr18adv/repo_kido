#ifndef _RGB_CHECKER_H_
#define _RGB_CHECKER_H_

/** Include file *************************************************************/
#include "ColorSensor.h"

/** using宣言 ****************************************************************/
using ev3api::ColorSensor;

/** Class ********************************************************************/
class RGB_Checker {
public:
	/* 定期スレッド呼出し用 */
	void Thread();
	/* コンストラクタ */
	RGB_Checker(const ColorSensor&);

	/* 色定義 */
	enum Color {
		Red = 0,	/* 赤 */
		Green,		/* 緑 */
		Blue,		/* 青 */
		Yellow,		/* 黄 */
		Other		/* 判別不可 */
	};

	/* HSV構造体定義(H:色相 S:彩度 V:明度) */
	struct ST_HSV { int H; int S; int V; };
	/* HSVについて ******************************************************/
	/*  H: 0〜360で色を表現する。( 0:赤 〜 120:緑 〜 240:青 〜 360:赤 ) */
	/*  S: 鮮やかさと言われているが、要は色のはっきり具合。             */
	/*     小さいと白(灰色)、大きいとRGB(黒でないことに注意)            */
	/*     この値の大きさによりカラー上か白黒上かの判定に利用する。     */
	/*  V: 明るさ。この値が低いと黒くなる。                             */
	/********************************************************************/

	/* 利用可能 Function */
	rgb_raw_t GetRGB() const;				/* フィルタ後のRGB値取得 */
	struct ST_HSV GetHSV() const;			/* HSV値取得 */
	bool ColorJude() const;				/* カラー上かを判定する */
	bool ColorJude( enum Color ) const;		/* 指定カラー上かを判定する(色詳細ももらう) */
	int GetRefrectData() const;			/* ライントレース用データ */

private:
	ST_HSV nowHSV;

	static const int SaturationThreshold = 130;		/* カラー判定用閾値 */
	//static const int SaturationThreshold = 90;		/* カラー判定用閾値 */

	/* フィルター計算(移動平均)用変数 */
	rgb_raw_t CalData;						/* フィルター演算結果   */
	static const int FilterDataNo = 10;		/* フィルター数         */
	rgb_raw_t buffRGB[FilterDataNo];		/* 過去10回分の値保存用 */
	rgb_raw_t sumRGB;						/* フィルター合計数     */
	int RgbCalCount;						/* フィルター格納位置   */

	/* 使用するドライバ */
	const ColorSensor& gColorSensor;

	/* Function */
	struct ST_HSV RGB_to_HSV(rgb_raw_t rgbValue);	/* HSV計算 */
};

#endif  // EV3_APP_LINETRACER_H_
