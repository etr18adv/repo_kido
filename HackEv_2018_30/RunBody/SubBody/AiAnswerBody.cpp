/** Include file *************************************************************/
#include "AiAnswerBody.h"

/** using宣言 ****************************************************************/

static FILE	 *bt = NULL;
/* Function ******************************************************************/
AiAnswerBody::AiAnswerBody(
	/** 引数 **/
		PositionCalculator* mpositionCalculator ):
	/** 初期化 **/
		BodyBase( ),
		evt( Evt::Evt_Wait ),
		positionCalculator( mpositionCalculator )
/*****************************************************************************/
{
	bt = ev3_serial_open_file(EV3_SERIAL_BT);	/* ←本来はSceneの方も持っていて、戦略的な支持を出す。 */
}

/* Function ******************************************************************/
AiAnswerBody::~AiAnswerBody( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void AiAnswerBody::Start( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void AiAnswerBody::Run( Motor& m_leftWheel, Motor& m_rightWheel, ArmControl* m_armControl, RGB_Checker* m_rgb_Checker )
/*****************************************************************************/
{
	switch( this->evt ){
		case Evt::Evt_Wait:
			m_leftWheel.setPWM( 0 );
			m_rightWheel.setPWM( 0 );			
			break;

		case Evt::Evt_LineTrace:
			WheelControl( 
				15,
				(float)( PID_feedback( (m_rgb_Checker->GetRefrectData( ) - 60), 0.70F, 0.00F, 0.07F, 0.004F ) ),
				m_leftWheel, m_rightWheel );
			//{
			//	RGB_Checker::ST_HSV hsv = m_rgb_Checker->GetHSV( );
			//	fprintf( bt, "%d,%d,%d,%d,\r\n", hsv.H, hsv.S, hsv.V, m_rgb_Checker->GetRefrectData( ) );
			//}
			break;

		case Evt_GoToPosition:
			{
				double tA = positionCalculator->CalculateAngle( this->TargetPositionX, this->TargetPositionY, this->RelativeId );
				double dA = tA - positionCalculator->GetAngle( this->RelativeId );
				if( 90.0 < dA ){
					dA = 90.0;
				}else if( -90.0 > dA ){
					dA = -90.0;
				}else{
					
				}
				WheelControl( this->PositionSpeed, dA, m_leftWheel, m_rightWheel );

				//PositionCalculator::Position position = positionCalculator->GetPosition( this->RelativeId );
				//fprintf( bt, "%f,%f,%d,%d,\r\n", position.X, position.Y, tA, positionCalculator->GetAngle( this->RelativeId ) );
			}
			break;

		case Evt_RelativeCheck:
			WheelControl( 
				-10,
				(float)( PID_feedback( -(m_rgb_Checker->GetRefrectData( ) - 60), 0.70F, 0.00F, 0.07F, 0.004F ) ),
				m_leftWheel, m_rightWheel );
			break;

		default:
				
			break;
	}
		
}

/* Function ******************************************************************/
int AiAnswerBody::PID_feedback( float x, float Kp, float Ki, float Kd, float dt )
/*****************************************************************************/
{
	static float p = 0, i = 0, d = 0, integral = 0;
	static int prev_x = 0;

	p         = Kp * x;
	integral += ( prev_x + x ) / 2.0F * dt;
	d         = Kd * ( x - prev_x ) / dt;
	i         = Ki * integral;

	prev_x = x;
		
	return((int)( p + i + d ));
}

/* Function ******************************************************************/
void AiAnswerBody::WheelControl( int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel )
/*****************************************************************************/
{
	/* Turnが大きいほど右に曲がる */
	m_leftWheel.setPWM( (int)( front + ( turn * 0.50F ) ) );
	m_rightWheel.setPWM( (int)( front - ( turn * 0.50F ) ) );
}


/* Function ******************************************************************/
void AiAnswerBody::Stop( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Wait;
}

/* Function ******************************************************************/
void AiAnswerBody::LineTrace( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_LineTrace;
}

/* Function ******************************************************************/
void AiAnswerBody::RelativeCheck( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_RelativeCheck;
}

/* Function ******************************************************************/
void AiAnswerBody::GoToPosition( int speed, int x, int y, int id )
/*****************************************************************************/
{
	this->evt = Evt::Evt_GoToPosition;
	this->PositionSpeed = speed;
	this->TargetPositionX = x;
	this->TargetPositionY = y;
	this->RelativeId = id;
}

