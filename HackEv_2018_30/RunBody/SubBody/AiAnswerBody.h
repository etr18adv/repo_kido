#ifndef _AI_ANSWER_BODY_H_
#define _AI_ANSWER_BODY_H_

/** Include file *************************************************************/
#include "BodyBase.h"
#include "PositionCalculator.h"

/** using宣言 ****************************************************************/

/* Class ******************************************************************/
class AiAnswerBody : public BodyBase {
public:
	AiAnswerBody(PositionCalculator*);
	~AiAnswerBody();

private:
	enum Evt {
		Evt_Wait = 0,
		Evt_LineTrace,
		Evt_GoToPosition,
		Evt_RelativeCheck
	};
	Evt evt;

	PositionCalculator* positionCalculator;

	void Start();
	void Run(Motor&, Motor&, ArmControl*, RGB_Checker*);

	void WheelControl(int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel);
	int PID_feedback( float x, float Kp, float Ki, float Kd, float dt );

/* イベント定義 **********************************************************/
	/* Stop ************************************************/
	public:
		void Stop();
	private:
	/*******************************************************/

	/* LineTrace *******************************************/
	public:
		void LineTrace();
	private:
	/*******************************************************/

	/* RelativeCheck ***************************************/
	public:
		void RelativeCheck();
	private:
	/*******************************************************/

	/* GoToPosition ****************************************/
	public:
		void GoToPosition( int speed, int x, int y, int id );
	private:
		int PositionSpeed = 50;
		int TargetPositionX = 0;
		int TargetPositionY = 0;
		int RelativeId;
	/*******************************************************/

};

#endif 
