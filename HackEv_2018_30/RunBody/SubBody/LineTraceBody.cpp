/** Include file *************************************************************/
#include "LineTraceBody.h"

/** using宣言 ****************************************************************/

static FILE	 *bt = NULL;
/* Function ******************************************************************/
LineTraceBody::LineTraceBody(
	/** 引数 **/
		PositionCalculator* mpositionCalculator):
	/** 初期化 **/
		BodyBase( ),
		evt( Evt::Evt_Wait ),
		positionCalculator( mpositionCalculator )
/*****************************************************************************/
{
	bt = ev3_serial_open_file(EV3_SERIAL_BT);	/* ←本来はSceneの方も持っていて、戦略的な支持を出す。 */
}

/* Function ******************************************************************/
LineTraceBody::~LineTraceBody( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void LineTraceBody::Start( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void LineTraceBody::Run( Motor& m_leftWheel, Motor& m_rightWheel, ArmControl* m_armControl, RGB_Checker* m_rgb_Checker )
/*****************************************************************************/
{
	switch( this->evt ){
		case Evt::Evt_Wait:
			m_leftWheel.setPWM( 0 );
			m_rightWheel.setPWM( 0 );			
			break;

		case Evt::Evt_LineTrace:
			WheelControl( 
				this->LineTraceSpeed, 
				(float)( PID_feedback( (m_rgb_Checker->GetRefrectData( ) - this->LineTarget), 
				0.70F, 0.00F, 0.07F, 0.004F ) ),
					m_leftWheel, m_rightWheel );
			break;

		case Evt_GoToPosition:
			{
				double tA = positionCalculator->CalculateAngle( this->TargetPositionX, this->TargetPositionY );
				double dA = tA - positionCalculator->GetAngle();
				if( 60 < dA ){
					dA = 60;
				}else if( -60 > dA ){
					dA = -60;
				}else{
					
				}
				WheelControl( this->PositionSpeed, dA, m_leftWheel, m_rightWheel );

				//PositionCalculator::Position position = positionCalculator->GetPosition( );
				//fprintf( bt, "%f,%f,%d,%d,\r\n", position.X, position.Y, tA, positionCalculator->GetAngle() );
			}
			break;

		default:
				
			break;
	}
}

/* Function ******************************************************************/
void LineTraceBody::Stop( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Wait;
}

/* Function ******************************************************************/
void LineTraceBody::LineTrace( int speed, int Target )
/*****************************************************************************/
{
	this->evt = Evt::Evt_LineTrace;
	this->LineTraceSpeed = speed;
	this->LineTarget = Target;
}

/* Function ******************************************************************/
void LineTraceBody::GoToPosition( int speed, int x, int y )
/*****************************************************************************/
{
	this->evt = Evt::Evt_GoToPosition;
	this->PositionSpeed = speed;
	this->TargetPositionX = x;
	this->TargetPositionY = y;
}

/* Function ******************************************************************/
int LineTraceBody::PID_feedback( float x, float Kp, float Ki, float Kd, float dt )
/*****************************************************************************/
{
	static float p = 0, i = 0, d = 0, integral = 0;
	static int prev_x = 0;

	p         = Kp * x;
	integral += ( prev_x + x ) / 2.0F * dt;
	d         = Kd * ( x - prev_x ) / dt;
	i         = Ki * integral;

	prev_x = x;
		
	return((int)( p + i + d ));
}

/* Function ******************************************************************/
void LineTraceBody::WheelControl( int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel )
/*****************************************************************************/
{
	/* Turnが大きいほど右に曲がる */
	m_leftWheel.setPWM( (int)( front + ( turn * 0.50F ) ) );
	m_rightWheel.setPWM( (int)( front - ( turn * 0.50F ) ) );
}



