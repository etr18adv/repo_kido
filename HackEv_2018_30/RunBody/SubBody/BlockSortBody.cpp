/** Include file *************************************************************/
#include "BlockSortBody.h"

/** using宣言 ****************************************************************/

static FILE	 *bt = NULL;
/* Function ******************************************************************/
BlockSortBody::BlockSortBody(
	/** 引数 **/
		PositionCalculator* mpositionCalculator ):
	/** 初期化 **/
		BodyBase( ),
		evt( Evt::Evt_Wait ),
		positionCalculator( mpositionCalculator )
/*****************************************************************************/
{
	bt = ev3_serial_open_file(EV3_SERIAL_BT);	/* ←本来はSceneの方も持っていて、戦略的な支持を出す。 */
}

/* Function ******************************************************************/
BlockSortBody::~BlockSortBody( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void BlockSortBody::Start( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void BlockSortBody::Run( Motor& m_leftWheel, Motor& m_rightWheel, ArmControl* m_armControl, RGB_Checker* m_rgb_Checker )
/*****************************************************************************/
{
	switch( this->evt ){
		/* 待機 */
		case Evt::Evt_Wait:
			m_leftWheel.setPWM( 0 );
			m_rightWheel.setPWM( 0 );			
			break;

		/* ライントレース */
		case Evt::Evt_LineTrace:
			WheelControl( 
				15,
				(float)( PID_feedback( (m_rgb_Checker->GetRefrectData( ) - 60), 0.70F, 0.00F, 0.07F, 0.004F ) ),
				m_leftWheel, m_rightWheel );
			break;

		/* 指定座標へ向かう */
		case Evt::Evt_GoToPosition:
			{
				double tA = positionCalculator->CalculateAngle( this->TargetPositionX, this->TargetPositionY, this->RelativeId );
				double dA = tA - positionCalculator->GetAngle( this->RelativeId );
				if( 90.0 < dA ){
					dA = 90.0;
				}else if( -90.0 > dA ){
					dA = -90.0;
				}else{
					
				}
				WheelControl( this->PositionSpeed, dA, m_leftWheel, m_rightWheel );

				//PositionCalculator::Position position = positionCalculator->GetPosition( );
				//fprintf( bt, "%f,%f,%d,%d,\r\n", position.X, position.Y, tA, positionCalculator->GetAngle() );
			}
			break;

		/* 指定距離バックする */
		case Evt::Evt_Back:
			{
				int32_t NowDistance = positionCalculator->GetDistance( this->RelativeId );
				if( ( this->StartDistance - this->BackDistance ) <= NowDistance ){
					m_leftWheel.setPWM( -this->BackSpeed );
					m_rightWheel.setPWM( -this->BackSpeed );
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
					this->Busy = false;
				}
			}
			break;

		/* 左足を中心に方向を変える */
		case Evt::Evt_TurnLeft:
			{
				int NowAngle = (int)positionCalculator->GetAngle( this->RelativeId );
				if( ( this->TurnLeftAngle + this->LStartAngle )  <= NowAngle ){
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( this->TurnLeftSpeed );
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
					this->Busy = false;
				}
			}
			break;

		/* 右足を中心に方向を変える */
		case Evt::Evt_TurnRight:
			{
				int NowAngle = (int)positionCalculator->GetAngle( this->RelativeId );
				if( ( this->TurnRightAngle + this->RStartAngle ) >= NowAngle ){
					m_leftWheel.setPWM( this->TurnRightspeed );
					m_rightWheel.setPWM( 0 );
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
					this->Busy = false;
				}
			}
			break;

		/* フリー(検討用) */
		case Evt::Evt_Free:
			m_leftWheel.setPWM( this->LeftPower );
			m_rightWheel.setPWM( this->RightPower );
			break;

		default:
				
			break;
	}
	
}

/* Function ******************************************************************/
int BlockSortBody::PID_feedback( float x, float Kp, float Ki, float Kd, float dt )
/*****************************************************************************/
{
	float p = 0, i = 0, d = 0;

	p  = Kp * x;
	this->integral += ( this->prev_x + x ) / 2.0F * dt;
	d  = Kd * ( x - this->prev_x ) / dt;
	i  = Ki * this->integral;

	this->prev_x = x;
		
	return((int)( p + i + d ));
}

/* Function ******************************************************************/
void BlockSortBody::WheelControl( int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel )
/*****************************************************************************/
{
	/* Turnが大きいほど右に曲がる */
	m_leftWheel.setPWM( (int)( front + ( turn * 0.50F ) ) );
	m_rightWheel.setPWM( (int)( front - ( turn * 0.50F ) ) );
}


/* Function ******************************************************************/
void BlockSortBody::Stop( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Wait;
	this->Busy = false;
}

/* Function ******************************************************************/
void BlockSortBody::LineTrace( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_LineTrace;
	this->integral = 0;
	this->prev_x = 0;
}

/* Function ******************************************************************/
void BlockSortBody::GoToPosition( int speed, int x, int y, int id )
/*****************************************************************************/
{
	this->evt = Evt::Evt_GoToPosition;
	this->PositionSpeed = speed;
	this->TargetPositionX = x;
	this->TargetPositionY = y;
	this->RelativeId = id;
}

/* Function ******************************************************************/
void BlockSortBody::Back( int speed, int distance, int id )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Back;
	this->BackSpeed = speed;
	this->BackDistance = distance;
	this->RelativeId = id;

	this->StartDistance = positionCalculator->GetDistance( this->RelativeId );

	this->Busy = true;
}

/* Function ******************************************************************/
void BlockSortBody::TurnLeft( int speed, int Angle, int id )
/*****************************************************************************/
{
	this->evt = Evt::Evt_TurnLeft;
	this->TurnLeftSpeed = speed;
	this->TurnLeftAngle = Angle;
	this->RelativeId = id;

	this->LStartAngle = positionCalculator->GetAngle( this->RelativeId );

	this->Busy = true;
}

/* Function ******************************************************************/
void BlockSortBody::TurnRight( int speed, int Angle, int id )
/*****************************************************************************/
{
	this->evt = Evt::Evt_TurnRight;
	this->TurnRightspeed = speed;
	this->TurnRightAngle = Angle;
	this->RelativeId = id;

	this->RStartAngle = positionCalculator->GetAngle( this->RelativeId );

	this->Busy = true;
}


/* Function ******************************************************************/
void BlockSortBody::Free( int mLeftPower, int mRightPower )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Free;
	this->LeftPower  = mLeftPower;
	this->RightPower = mRightPower;
}

