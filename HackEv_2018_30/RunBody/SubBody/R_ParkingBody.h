#ifndef _R_PARKING_BODY_H_
#define _R_PARKING_BODY_H_

/** Include file *************************************************************/
#include "BodyBase.h"

/** using�錾 ****************************************************************/

/* Class ******************************************************************/
class R_ParkingBody : public BodyBase {
public:
	R_ParkingBody();
	~R_ParkingBody();

private:
	void Start();
	void Run(Motor&, Motor&, ArmControl*, RGB_Checker*);
};

#endif 
