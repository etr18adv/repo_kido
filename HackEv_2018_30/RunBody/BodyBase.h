#ifndef _BASE_BODY_
#define _BASE_BODY_

/** Include file *************************************************************/
#include "ev3api.h"
#include "Motor.h"

#include "ArmControl.h"
#include "RGB_Checker.h"

/** using�錾 ****************************************************************/
using ev3api::Motor;

/* Class ******************************************************************/
class BodyBase {
public:
	virtual ~BodyBase() { };

	static void Task();
	void mStartRun();

	static bool IsInitialSetting();
	static int32_t GetLeftWheelCount();
	static int32_t GetRightWheelCount();

	const RGB_Checker* GetRgb() { return rgb_Checker; };

	bool GetBusyState( ){ return Busy; };

private:
	static BodyBase*    nowbody;
	static Motor&	    LeftWheel;
	static Motor&	    RightWheel;
	static ArmControl*  armControl;
	static RGB_Checker* rgb_Checker;

	virtual void Start() = 0;
	virtual void Run(Motor&, Motor&, ArmControl*, RGB_Checker*) = 0;

protected:
	BodyBase();

	bool Busy = false;

};
#endif 
