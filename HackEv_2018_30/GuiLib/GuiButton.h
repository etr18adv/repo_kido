#ifndef _GUI_BUTTON_H_
#define _GUI_BUTTON_H_

/** Include file *************************************************************/
#include "ev3api.h"

/** using宣言 ****************************************************************/

/** Class ********************************************************************/
namespace GUI{
	class GuiButton
	{
		public:
			/* 定期スレッド呼出し用 */
			void Thread( );
			/* コンストラクタ */
    		GuiButton( );

			enum ButtonEvt{ ButtonEvt_None, ButtonEvt_Trg };
			enum ButtonSelect{ BtUp = 0, BtDown = 1, BtLeft = 2, BtRight = 3, BtEnter = 4, BtNone = 5 };

			enum ButtonEvt GetButtonEvt( enum ButtonSelect );
			enum ButtonSelect GetButtonEvt( );

		private:
			/* define ****************/
			enum ButtonState{ StateOff = 0,	StateOn	};

			struct StButton{ enum ButtonState State; unsigned char Buff; button_t TargetButton; };

			/* 変数 ******************/
			int ButtonCount;
			struct StButton ButtonData[ 5 ] ;
			enum ButtonEvt Answer[ 5 ] = { ButtonEvt_None, ButtonEvt_None, ButtonEvt_None, ButtonEvt_None, ButtonEvt_None };

			/* 使用するドライバ */
	};
}

#endif  // EV3_APP_LINETRACER_H_
