#ifndef _POSITION_CALCULATOR_H_
#define _POSITION_CALCULATOR_H_

/** Include file *************************************************************/
#include "Motor.h"

/** using宣言 ****************************************************************/
using ev3api::Motor;

/** Class ********************************************************************/
class PositionCalculator {
public:
	/* 定期スレッド呼出し用 */
	void Thread();
	/* コンストラクタ */
	PositionCalculator();

	/* 座標構造体定義 */
	struct Position { double X; double Y; };
	struct BodyLocation { int32_t Distance; double Angle; };

	/* 利用可能 Function */
	void StartCalculate();	/* 補正開始(ただし、補正区間はY座標を真直ぐ走ることとする。) */
	struct Position GetPosition();
	struct Position GetPosition( int );

	int32_t GetDistance( );
	int32_t GetDistance( int );
	double GetAngle( );
	double GetAngle( int );
	int CreateId();


	double CalculateAngle( int tx, int ty );
	double CalculateAngle( int tx, int ty, int id );
	bool RangeJuge( int tx, int ty, int range );
	bool RangeJuge( int tx, int ty, int range, int id );
	//struct BodyLocation SumLocation;
	//int32_t CalAngle;

private:
	/* define ****************/
	enum CalState {			/* 状態 */
		Def = 0,			/*   補正なしの走り */
		Init,				/*   補正開始 */
		InitCalculate,		/*   補正区間走行中 */
		Calculate			/*   補正完了 */
	};
	/* 整数以外の定義ができないので、0.1単位で記載 */
	static const int F_WIDTH = 146;	/* 左右のタイヤ間距離(14.6) */
	static const int F_DIAMETER = 100;	/* タイヤ直径(10.0) */

	static const int F_I_START_DISTANCE = 80;	/*  */
	static const int F_I_CAL_DISTANCE = 150;	/*  */

	/* 変数 ******************/
	struct AbsolutePosition{
		Position position;
		BodyLocation startLocation;
		int32_t startAngle;
	};
	struct AbsolutePosition aPosition[ 20 ];
	int AbsoluteId = 0;
	int AbsoluteCal( int id );
	
	enum CalState state;

	struct Position position;
	struct BodyLocation SumLocation;

	int32_t OldDistance;					/* 座標計算用距離 */
	double CalAngle;

	struct Position positionBuff[ 250 ];
	int Buffcount = 0;
	int SCount = 0;

	double MCalculateAngle( int tx, int ty, int bx, int by );

	int32_t LeftSatrtCount;
	int32_t RightStartCount;

	/* パラメータ */
	float WIDTH;
	float DIAMETER;
	float I_START_DISTANCE;
	float I_CAL_DISTANCE;

	/* 使用するドライバ */
};

#endif  // EV3_APP_LINETRACER_H_
