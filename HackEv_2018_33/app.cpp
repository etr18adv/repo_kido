/******************************************************************************
 *  app.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Implementation of the Task main_task
 *  Author: Kazuhiro.Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

/** Include file *************************************************************/
#include "app.h"

#include "PositionCalculator.h"
#include "ArmControl.h"
#include "GameApp.h"
#include "Gui.h"
#include "ScenarioManager.h"

#include "BlockSortDataManager.h"

#include "BodyBase.h"
#include "WaitBody.h"
#include "LineTraceBody.h"
#include "AiAnswerBody.h"
#include "BlockSortBody.h"
#include "L_ParkingBody.h"
#include "R_ParkingBody.h"

#include "algorithm"

/** using宣言 ****************************************************************/
using namespace GUI;
//using ev3api::GyroSensor;
//using ev3api::TouchSensor;
//using ev3api::SonarSensor;

/** オブジェクトを静的に確保する *********************************************/
//GyroSensor  gGyroSensor( PORT_4 );		/* ジャイロセンサ */
//SonarSensor gSonarSensor( PORT_3 );		/* 超音波センサ */
//Motor       gTailMoter( PORT_D );		/* 尻尾モーター   */

/** モジュールデータ *********************************************************/
static PositionCalculator	*gPositionCalculator;
static GameApp				*gGameApp;
static ScenarioManager		*gScenarioManager;
static BlockSortDataManager *gBlockSortDataManager;
static WaitBody             *gWaitBody;
static LineTraceBody		*gLineTraceBody;
static AiAnswerBody			*gAiAnswerBody;
static BlockSortBody		*gBlockSortBody;
static L_ParkingBody		*gL_ParkingBody;
static R_ParkingBody		*gR_ParkingBody;

/* Function ******************************************************************/
static void InitSet( )
/*****************************************************************************/
{
	tslp_tsk( 2 );
	
	gBlockSortDataManager = new BlockSortDataManager( );

	/* ドライバ、ミドル作成 */
	gPositionCalculator = new PositionCalculator( );

	gWaitBody           = new WaitBody( );
	gLineTraceBody		= new LineTraceBody( gPositionCalculator );
	gAiAnswerBody		= new AiAnswerBody( gPositionCalculator );
	gBlockSortBody		= new BlockSortBody( gPositionCalculator );
	gL_ParkingBody		= new L_ParkingBody( );
	gR_ParkingBody		= new R_ParkingBody( );
	gWaitBody->mStartRun( );	/* 走行体のタスク処理 */

	gScenarioManager	= new ScenarioManager( 
								gBlockSortDataManager, 
								gWaitBody,
								gLineTraceBody, 
								gAiAnswerBody,
								gBlockSortBody,
								gL_ParkingBody,
								gR_ParkingBody,
								gPositionCalculator );

	Gui::Create( gScenarioManager, gBlockSortDataManager );

	/* アプリ作成 */
	gGameApp			= new GameApp( gScenarioManager );
}

/* Function ******************************************************************/
static void EndSet( )
/*****************************************************************************/
{
	delete gPositionCalculator;

	delete gGameApp;

	delete gScenarioManager;

	delete gBlockSortDataManager;
};

/* Function ******************************************************************/
//static void DeviceReset()
/*****************************************************************************/
//{
	/* 本来、こういったデバイスの初期化は開始と同時におこなうものだが、  */
	/* 安定していない可能性があるため、初期化はシナリオ管理にタイミング  */
	/* を譲渡する。														 */
	//gGyroSensor.reset( );			/* ジャイロリセット */
	//gGyroSensor.setOffset( 0 );		/* ジャイロオフセット設定 */
	//gLeftWheel.reset( );			/* 左車輪角度リセット */
	//gRightWheel.reset( );			/* 右車輪角度リセット */
//}

/* Function ******************************************************************/
void main_task( 
	/** 引数 **/
		intptr_t unused ) 
/*****************************************************************************/
{	
	InitSet( );								/* 各インスタンスの生成 */
    
	/* 周期ハンドラ開始 */
    ev3_sta_cyc( EV3_CYC_MEASURE );			/* 計測タスク：開始 */
    ev3_sta_cyc( EV3_CYC_DRIVE_RUN );		/* 走行タスク：開始 */


	/* 開始 */
	gGameApp->GameMain( );

    /* 周期ハンドラ停止 */
    ev3_stp_cyc( EV3_CYC_MEASURE );			/* 計測タスク：終了 */
	ev3_stp_cyc( EV3_CYC_DRIVE_RUN );		/* 走行タスク：終了 */

	EndSet( );								/* 各インスタンスの削除 */

	/* 終了 */
    ext_tsk( );								/* タスクの終了 */
}

/* Function ******************************************************************/
void ev3_cyc_Measure( 
	/** 引数 **/
		intptr_t exinf ) 
/*****************************************************************************/
{
	gPositionCalculator->Thread( );
}

/* Function ******************************************************************/
void ev3_cyc_DriveRun( 
	/** 引数 **/
		intptr_t exinf ) 
/*****************************************************************************/
{
	gGameApp->Thread( );

	Gui* gGui = Gui::GetGui( );
	gGui->Thread( );

	BodyBase::Task( );
}

