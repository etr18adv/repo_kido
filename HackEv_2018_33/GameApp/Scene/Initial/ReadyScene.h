#ifndef _READY_SCENE_
#define _READY_SCENE_

/** Include file *************************************************************/
#include "BaseScene.h"

/** using�錾 ****************************************************************/
using ev3api::TouchSensor;

/* Class ******************************************************************/
class ReadyScene : public BaseScene
{
	public:
		ReadyScene( );
		~ReadyScene( );

		void Run( );

	private:
		static const int StartTime = 500 / 4;

		bool StartFix;
		int StartTimer;

		void Start( );
};

#endif 
