/** Include file *************************************************************/
#include "TouchSensor.h"

#include "ReadyScene.h"
#include "Gui.h"

/** using宣言 ****************************************************************/
using ev3api::TouchSensor;
using namespace GUI;

TouchSensor gTouchSensor( PORT_1 );		/* タッチセンサー */

/* Function ******************************************************************/
ReadyScene::ReadyScene(
 	/** 引数 **/
		):
	/** 初期化 **/
		BaseScene( ),
		StartFix( false ),StartTimer( ReadyScene::StartTime )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
ReadyScene::~ReadyScene( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void ReadyScene::Start( )
/*****************************************************************************/
{
	Gui* gui = Gui::GetGui( );

	StartFix = false;

	gui->ReadyDisplay( );
}

/* Function ******************************************************************/
void ReadyScene::Run( )
/*****************************************************************************/
{
	if( false == StartFix ){
		if( gTouchSensor.isPressed( ) ){
			this->StartFix = true;
			this->StartTimer = ReadyScene::StartTime;
		}
	}else{
		if( 0 != StartTimer ){
			--StartTimer;
		}else{
			SetFinish( );
			this->StartFix = false;
			this->StartTimer = ReadyScene::StartTime;
		}
	}
}

