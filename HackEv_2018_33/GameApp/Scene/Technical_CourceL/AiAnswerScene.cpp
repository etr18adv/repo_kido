/** Include file *************************************************************/
#include "AiAnswerScene.h"

/** using宣言 ****************************************************************/

/* Function ******************************************************************/
AiAnswerScene::AiAnswerScene( 
	AiAnswerBody* mAiAnswerBody, 
	PositionCalculator* mPositionCalculator):
	aiAnswerBody( mAiAnswerBody ),
	positionCalculator( mPositionCalculator )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
AiAnswerScene::~AiAnswerScene( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void AiAnswerScene::Start( )
/*****************************************************************************/
{
	/* AIアンサー用走行　許可 */
	aiAnswerBody->mStartRun( );
	/* RGB取得用ドライバ取得 */
	const RGB_Checker* rgbC = aiAnswerBody->GetRgb( );

	/* 緑検知までライントレース */
	aiAnswerBody->LineTrace( );
	while( !rgbC->ColorJude( RGB_Checker::Color::Green ) ){
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	/* 停止 */
	aiAnswerBody->Stop( );

	/* 相対座標作成 */
	this->RelativePotsitionId = positionCalculator->CreateId();

	/* デジタルデータ読取り */
	//GoToPosition( 10,   10, 45, 2, this->RelativePotsitionId );
	//GoToPosition( 10,   25, 30, 2, this->RelativePotsitionId );
	//GoToAngleRight( 20, 270 );
	//GoToPosition( 10,  -20, 30, 2, this->RelativePotsitionId );
	//GoToAngleLeft( 20, 135 );
	//GoToPosition( 10,  -5, 20, 2, this->RelativePotsitionId );
	//GoToAngleLeft( 20, 0 );
	//GoToPosition( 10,  -5, 45, 2, this->RelativePotsitionId );
	aiAnswerBody->DigitalDataScan( this->RelativePotsitionId );
	while( aiAnswerBody->GetBusyState( ) ){
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	{
		int DAnswer = 0;
		unsigned char Ddata = aiAnswerBody->GetDagitalData( );
		switch( Ddata ){
			case 11:
				DAnswer = 0;
				break;
			case 0:
				DAnswer = 1;
				break;
			case 7:
				DAnswer = 2;
				break;
			case 6:
				DAnswer = 3;
				break;
			case 12:
				DAnswer = 4;
				break;
			case 14:
				DAnswer = 5;
				break;
			case 15:
				DAnswer = 6;
				break;
			case 8:
				DAnswer = 7;
				break;
			default:

				break;
		}

		{
			int timer = 500 / 4;
			while(0 != DAnswer){
				ev3_speaker_play_tone(NOTE_A5,100);
				while(0 != timer){
					tslp_tsk( 4 );
					timer--;
				}
				timer = 500 / 4;
				DAnswer--;
			}
		}
	}

	/* 停止 */
	aiAnswerBody->Stop( );
}

/* Function ******************************************************************/
void AiAnswerScene::Run( )
/*****************************************************************************/
{

}

