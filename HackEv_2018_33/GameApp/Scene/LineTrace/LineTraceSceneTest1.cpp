/** Include file *************************************************************/
#include "LineTraceSceneTest1.h"

/** using�錾 ****************************************************************/


/* Function ******************************************************************/
LineTraceSceneTest1::LineTraceSceneTest1( 
	LineTraceBody* mlineTraceBody,
	PositionCalculator* mPositionCalculator ):
	LineTraceScene( mlineTraceBody, mPositionCalculator )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
LineTraceSceneTest1::~LineTraceSceneTest1( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void LineTraceSceneTest1::Start( )
/*****************************************************************************/
{
	lineTraceBody->mStartRun( );
	const RGB_Checker* rgbC = lineTraceBody->GetRgb( );

	lineTraceBody->LineTrace( 30, 60 );
	positionCalculator->StartCalculate( );

	while(positionCalculator->GetDistance() < 200){
		tslp_tsk( 4 );
	}

	GoToPosition( 50,    0, 320, 5 );
	GoToPosition( 50, -200, 340, 5 );
	GoToPosition( 50, -200, 420, 5 );
	GoToPosition( 50,  -10, 320, 5 );
	GoToPosition( 50,    0, 350, 5 );
	GoToPosition( 50,    0, 390, 5 );
	GoToPosition( 50,    0, 420, 5 );

	lineTraceBody->Stop( );
}

/* Function ******************************************************************/
void LineTraceSceneTest1::Run( )
/*****************************************************************************/
{

}

