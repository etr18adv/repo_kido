#ifndef _BLOCKSORT_DATA_MANAGER_H_
#define _BLOCKSORT_DATA_MANAGER_H_

/** Include file *************************************************************/


/** using宣言 ****************************************************************/


/** Class ********************************************************************/
class BlockSortDataManager
{
	public:
		/* 定期スレッド呼出し用 */
		void Thread( );
		/* コンストラクタ */
    	BlockSortDataManager( );

		int GetInitPositionCode( ) const;
		void SetInitPositionCode( int );

		
		enum ColorSet{ None, Red, Green, Blue, Yellow };
		struct ColorBlocks{
			enum ColorSet FirstColor;
			enum ColorSet SecoundColor;
			enum ColorSet ThurdColor;
			enum ColorSet FourthColor;		
		};
		struct ColorBlocks GetColorBlocks( ) const;
		void SetColorBlockColor( int, enum ColorSet );

	private:
		int InitPositionCode;
		struct ColorBlocks colorBlocks;
};

#endif  // EV3_APP_LINETRACER_H_
