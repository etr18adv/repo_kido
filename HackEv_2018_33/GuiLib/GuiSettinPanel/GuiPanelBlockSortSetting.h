#ifndef _GUI_PANEL_BLOCKSORT_SETTING_H_
#define _GUI_PANEL_BLOCKSORT_SETTING_H_

/** Include file *************************************************************/
#include "GuiPanelBase.h"
#include "GuiButton.h"
#include <string>

#include "BlockSortDataManager.h"
/** using宣言 ****************************************************************/


/** Class ********************************************************************/
namespace GUI{
	class GuiPanelBlockSortSetting : public GuiPanelBase{
		public:
			/* 定期スレッド呼出し用 */
			bool BtEvt( GuiButton* );
			/* コンストラクタ */
    		GuiPanelBlockSortSetting( BlockSortDataManager* );

			void Load( );
			void StartSet( );
			void Refresh( );

		private:
			int cursol;
			int pointColor[ 4 ];
			int InitialCode[ 5 ];
			bool InputMode = false;

			BlockSortDataManager* blockSortDataManager;

			void DrawTUp( int x, int y );
			void DrawTDown( int x, int y );
			void DrawMap( );
			void DrawOnMap( std::string, int );
			void DrawCursol( int x, int y );

			int InitialCodeChange( int InitialCode[] );
			void CodeChange( int code, int a[] );
			void CodeChangeValToHex( int inCodeA[], int HexCodeA[] );

			void draw_Color( int id, int c, int No  );
	};
}

#endif  // EV3_APP_LINETRACER_H_
