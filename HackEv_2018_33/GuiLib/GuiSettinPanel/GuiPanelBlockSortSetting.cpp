
/** Include file *************************************************************/
#include "GuiPanelBlockSortSetting.h"

namespace GUI{
	/* Function ******************************************************************/
	GuiPanelBlockSortSetting::GuiPanelBlockSortSetting( 
		/** 引数 **/
			BlockSortDataManager* mblockSortDataManager):
		/** 初期化 **/
			cursol( 0 ),
			pointColor( { 0, 0, 0, 0 } ),
			blockSortDataManager( mblockSortDataManager )
	/*****************************************************************************/
	{
		this->Name = "BlockSort";
		InitialCode[ 0 ] = 0;
		InitialCode[ 1 ] = 0;
		InitialCode[ 2 ] = 0;
		InitialCode[ 3 ] = 0;
		InitialCode[ 4 ] = 0;
	}

	/* Function ******************************************************************/
	bool GuiPanelBlockSortSetting::BtEvt( GuiButton* guiButton )
	/*****************************************************************************/
	{
		bool ans = false;
		GuiButton::ButtonSelect evtBt = guiButton->GetButtonEvt( );
		if( evtBt == GuiButton::ButtonSelect::BtEnter ){
			static const BlockSortDataManager::ColorSet ColorDef[ 5 ] = { 
				BlockSortDataManager::ColorSet::None, 
				BlockSortDataManager::ColorSet::Red, 
				BlockSortDataManager::ColorSet::Green, 
				BlockSortDataManager::ColorSet::Blue, 
				BlockSortDataManager::ColorSet::Yellow };
			blockSortDataManager->SetInitPositionCode( InitialCodeChange( this->InitialCode ) );

			blockSortDataManager->SetColorBlockColor( 0, ColorDef[ pointColor[ 0 ] ] );
			blockSortDataManager->SetColorBlockColor( 1, ColorDef[ pointColor[ 1 ] ] );
			blockSortDataManager->SetColorBlockColor( 2, ColorDef[ pointColor[ 2 ] ] );
			blockSortDataManager->SetColorBlockColor( 3, ColorDef[ pointColor[ 3 ] ] );

			CursolRightReturn( );
			ans = true;
		}else{
			if( !InputMode ){
				switch( evtBt ){
					case GuiButton::ButtonSelect::BtUp:
						if( 5 < cursol ){
							int cp = pointColor[ cursol - 6 ];
							if( 4 <= cp ){
								cp = 0;
							}else{
								cp++;
							}
							pointColor[ cursol - 6 ] = cp;
						}else if( 0 < cursol ){
							int v = InitialCode[ cursol - 1 ];
							if( 15 <= v ){
								v = 0;
							}else{
								v++;
							}
							InitialCode[ cursol - 1 ] = v;
						}else{
							InputMode = true;
						}
						ans = true;
						break;

					case GuiButton::ButtonSelect::BtDown:
						if( 5 < cursol ){
							int cp = pointColor[ cursol - 6 ];
							if( 0 == cp ){
								cp = 4;
							}else{
								cp--;
							}
							pointColor[ cursol - 6 ] = cp;
						}else if( 0 < cursol ){
							int v = InitialCode[ cursol - 1 ];
							if( 0 == v ){
								v = 15;
							}else{
								v--;
							}
							InitialCode[ cursol - 1 ] = v;
						}else{
							InputMode = true;
						}
						ans = true;
						break;

					case GuiButton::ButtonSelect::BtLeft:
						if( 0 == cursol ){
							cursol = 9;
						}else{
							--cursol;
						}
						ans = true;
						break;

					case GuiButton::ButtonSelect::BtRight:
						if( 9 <= cursol ){
							cursol = 0;
						}else{
							cursol++;
						}
						ans = true;
						break;

					default:

						break;
				}
			}else{
				int CodeArray[ 7 ];
				CodeChange( InitialCodeChange( this->InitialCode ), CodeArray );
				switch( evtBt ){
					case GuiButton::ButtonSelect::BtUp:
						if( 7 < cursol ){
							int cp = pointColor[ cursol - 8 ];
							if( 4 <= cp ){
								cp = 0;
							}else{
								cp++;
							}
							pointColor[ cursol - 8 ] = cp;
						}else if( 0 < cursol ){
							int v = CodeArray[ cursol - 1 ];
							if( 9 <= v ){
								v = 0;
							}else{
								v++;
							}
							CodeArray[ cursol - 1 ] = v;
							CodeChangeValToHex( CodeArray, this->InitialCode );
						}else{
							InputMode = false;
						}
						ans = true;
						break;

					case GuiButton::ButtonSelect::BtDown:
						if( 7 < cursol ){
							int cp = pointColor[ cursol - 8 ];
							if( 0 == cp ){
								cp = 4;
							}else{
								cp--;
							}
							pointColor[ cursol - 8 ] = cp;
						}else if( 0 < cursol ){
							int v = CodeArray[ cursol - 1 ];
							if( 0 == v ){
								v = 9;
							}else{
								v--;
							}
							CodeArray[ cursol - 1 ] = v;
							CodeChangeValToHex( CodeArray, this->InitialCode );
						}else{
							InputMode = false;
						}
						ans = true;
						break;

					case GuiButton::ButtonSelect::BtLeft:
						if( 0 == cursol ){
							cursol = 11;
						}else{
							--cursol;
						}
						ans = true;
						break;

					case GuiButton::ButtonSelect::BtRight:
						if( 11 <= cursol ){
							cursol = 0;
						}else{
							cursol++;
						}
						ans = true;
						break;

					default:

						break;
				}
			}
		}
		return ans;
	}

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::Load( )
	/*****************************************************************************/
	{

	};

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::StartSet( )
	/*****************************************************************************/
	{

	};

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::Refresh( )
	/*****************************************************************************/
	{
		if( !InputMode ){
			static const int cursolPointHex[ 10 ][ 2 ] = { { 0, 1 }, { 2, 1 }, { 3, 1 }, { 4, 1 }, { 5, 1 }, { 6, 1 }, { 1, 3 }, { 2, 3 }, { 3, 3 }, { 4, 3 } };
			static const std::string HexSt[ 16 ] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };
			DrawCursol( cursolPointHex[ cursol ][ 0 ], cursolPointHex[ cursol ][ 1 ] );

			draw_string(  "I", 0, 1 );

			draw_string(  HexSt[ this->InitialCode[ 0 ] ].c_str(), 2, 1 );
			draw_string(  HexSt[ this->InitialCode[ 1 ] ].c_str(), 3, 1 );
			draw_string(  HexSt[ this->InitialCode[ 2 ] ].c_str(), 4, 1 );
			draw_string(  HexSt[ this->InitialCode[ 3 ] ].c_str(), 5, 1 );
			draw_string(  HexSt[ this->InitialCode[ 4 ] ].c_str(), 6, 1 );
		}else{
			static const int cursolPoint[ 12 ][ 2 ] = { { 0, 1 }, { 2, 1 }, { 3, 1 }, { 4, 1 }, { 5, 1 }, { 6, 1 }, { 7, 1 }, { 8, 1 }, { 1, 3 }, { 2, 3 }, { 3, 3 }, { 4, 3 } };
			static const std::string ValSt[ 10 ] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
			int CodeArray[ 7 ];

			CodeChange( InitialCodeChange( this->InitialCode ), CodeArray );

			DrawCursol( cursolPoint[ cursol ][ 0 ], cursolPoint[ cursol ][ 1 ] );

			draw_string(  "C", 0, 1 );

			draw_string(  ValSt[ CodeArray[ 0 ] ].c_str(), 2, 1 );
			draw_string(  ValSt[ CodeArray[ 1 ] ].c_str(), 3, 1 );
			draw_string(  ValSt[ CodeArray[ 2 ] ].c_str(), 4, 1 );
			draw_string(  ValSt[ CodeArray[ 3 ] ].c_str(), 5, 1 );
			draw_string(  ValSt[ CodeArray[ 4 ] ].c_str(), 6, 1 );
			draw_string(  ValSt[ CodeArray[ 5 ] ].c_str(), 7, 1 );
			draw_string(  ValSt[ CodeArray[ 6 ] ].c_str(), 8, 1 );
		}

		{
			static const std::string ValSt[ 5 ] = { "N","R","G","B","Y" };
			draw_string(  ValSt[ pointColor[ 0 ] ].c_str(), 1, 3 );
			draw_string(  ValSt[ pointColor[ 1 ] ].c_str(), 2, 3 );
			draw_string(  ValSt[ pointColor[ 2 ] ].c_str(), 3, 3 );
			draw_string(  ValSt[ pointColor[ 3 ] ].c_str(), 4, 3 );
		}

		DrawMap( );

		{
			int i;
			for( i = 0; i < 4; i++){
				draw_Color( i, pointColor[ i ], this->InitialCode[ i ] );
			}
		}
	}

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::DrawMap()
	/*****************************************************************************/
	{
		int i;
		int StartXS = 10 * StrW;
		int StartYS = 1;
		int Witdh = StrW  * 2;
		for( i = 0; i < 4; i++ ){
			draw_line( StartXS + ( Witdh / 2 ),         ( ( StartYS + i ) * StrH ) + ( StrH / 2 ), StartXS + ( 3 * Witdh ) + ( Witdh / 2 ), ( ( StartYS + i ) * StrH ) + ( StrH / 2 ) );
			draw_line( StartXS + ( i * Witdh ) + ( Witdh / 2 ), ( StartYS * StrH ) + ( StrH / 2 ), StartXS + ( i * Witdh ) + ( Witdh / 2 ), ( ( StartYS + 3 ) * StrH ) + ( StrH / 2 ) );
		}
		draw_line( StartXS + ( Witdh / 2 ), ( ( StartYS + 2 ) * StrH ) + ( StrH / 2 ), StartXS - ( Witdh / 4 ), ( ( StartYS + 2 ) * StrH ) + ( StrH / 2 ) );
	}

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::draw_Color( int id, int c, int No )
	/*****************************************************************************/
	{
		static const std::string ValSt[ 5 ] = { "1","2","3","4" };
		static const std::string ColSt[ 5 ] = { "N","R","G","B","Y" };
		if( c == 0 ){
			DrawOnMap( ValSt[ id ], No );
		}else{
			DrawOnMap( ColSt[ c ], No );
		}
	}
	//draw_string(  ValSt[ CodeArray[ 6 ] ].c_str(), 8, 1 );

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::DrawOnMap( std::string moji, int No )
	/*****************************************************************************/
	{
		int x = No % 4;
		int y = No / 4;
		int StartXS = 10 * StrW;
	//	int StartYS = 1;
		int Witdh = StrW  * 2;
		draw_stringXFree( moji.c_str(), StartXS + ( Witdh / 4 ) + Witdh * x, y + 1 );
	}

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::DrawCursol( int x, int y )
	/*****************************************************************************/
	{
		DrawTUp( x, y - 1 );
		DrawTDown( x, y + 1 );
	}

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::DrawTUp( int x, int y )
	/*****************************************************************************/
	{
		draw_line(            ( x * StrW ) + 1, ( ( y + 1 ) * StrH ) - 1, ( x * StrW ) + ( StrW / 2 ),         ( y * StrH ) + 6 );
		draw_line( ( x * StrW ) + ( StrW / 2 ),         ( y * StrH ) + 6,    ( ( x + 1 ) * StrW ) - 2, ( ( y + 1 ) * StrH ) - 1 );
	}

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::DrawTDown( int x, int y )
	/*****************************************************************************/
	{
		draw_line(            ( x * StrW ) + 1,         ( y * StrH ) + 3, ( x * StrW ) + ( StrW / 2 ), ( ( y + 1 ) * StrH ) - 6 );
		draw_line( ( x * StrW ) + ( StrW / 2 ), ( ( y + 1 ) * StrH ) - 6,    ( ( x + 1 ) * StrW ) - 2,         ( y * StrH ) + 3 );
	}

	/* Function ******************************************************************/
	int GuiPanelBlockSortSetting::InitialCodeChange( int InitialCode[] )
	/*****************************************************************************/
	{
		return( ( InitialCode[ 0 ] * 16 * 16 * 16 * 16 ) +
				( InitialCode[ 1 ] * 16 * 16 * 16 ) +
				( InitialCode[ 2 ] * 16 * 16 ) +
				( InitialCode[ 3 ] * 16 ) +
				( InitialCode[ 4 ] ) );
	}

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::CodeChange( int code, int a[] )
	/*****************************************************************************/
	{
		int i;
		int cal = code;
		for( i = 0; i < 7; i++ ){
			a[ 6 - i ] = cal % 10;
			cal /= 10;
		}
	}

	/* Function ******************************************************************/
	void GuiPanelBlockSortSetting::CodeChangeValToHex( int inCodeA[], int HexCodeA[] )
	/*****************************************************************************/
	{
		int InCode =  ( ( inCodeA[ 0 ] * 1000000 ) +
						( inCodeA[ 1 ] * 100000 ) +
						( inCodeA[ 2 ] * 10000 ) +
						( inCodeA[ 3 ] * 1000 ) +
						( inCodeA[ 4 ] * 100 ) +
						( inCodeA[ 5 ] * 10 ) +
						( inCodeA[ 6 ] ) );

		{
			int i;
			for( i = 0; i < 5; i++ ){
				HexCodeA[ 4 - i ] = InCode % 16;
				InCode /= 16;
			}
		}
	}
}
