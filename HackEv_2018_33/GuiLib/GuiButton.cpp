
/** Include file *************************************************************/
#include "ev3api.h"
#include "GuiButton.h"

namespace GUI{
	/* Function ******************************************************************/
	GuiButton::GuiButton( 
		/** 引数 **/
			):
		/** 初期化 **/
			ButtonCount( 0 ),
			ButtonData( {
				{ StateOff, 0x00, UP_BUTTON   },
				{ StateOff, 0x00, DOWN_BUTTON  },
				{ StateOff, 0x00, LEFT_BUTTON  },
				{ StateOff, 0x00, RIGHT_BUTTON  },
				{ StateOff, 0x00, ENTER_BUTTON  }
			} )
	/*****************************************************************************/
	{
		;
	}

	/* Function ******************************************************************/
	void GuiButton::Thread( )
	/*****************************************************************************/
	{
		{
			int i;
			for( i = 0; i < 5; i++ ){
				Answer[ i ] = ButtonEvt::ButtonEvt_None;
			}
		}

		ButtonCount++;
		if( ( 40 / 4 ) <= ButtonCount ){
			ButtonCount = 0;
			{
				int i;
				for( i = 0; i < 5; i++ ){
					ButtonData[ i ].Buff <<= 1;
					if( ev3_button_is_pressed( ButtonData[ i ].TargetButton ) ){
						ButtonData[ i ].Buff |= 0x01;
					}else{
						ButtonData[ i ].Buff &= ~0x01;					
					}

					switch( ButtonData[ i ].State ){
						case StateOff:
							if( 0x03 == ( ButtonData[ i ].Buff & 0x03 ) ){
								ButtonData[ i ].State = StateOn;
							}
							break;
						case StateOn:
							if( 0x00 == ( ButtonData[ i ].Buff & 0x03 ) ){
								ButtonData[ i ].State = StateOff;
								Answer[ i ] = ButtonEvt::ButtonEvt_Trg;
							}						
							break;
						default:
							;	/* 異常 */
							break;
					}
				}
			}
		}
	}

	/* Function ******************************************************************/
	enum GuiButton::ButtonEvt GuiButton::GetButtonEvt( enum GuiButton::ButtonSelect bs)
	/*****************************************************************************/
	{
		if( ( bs == ButtonSelect::BtUp ) || 
			( bs == ButtonSelect::BtDown ) || 
			( bs == ButtonSelect::BtLeft ) || 
			( bs == ButtonSelect::BtRight ) || 
			( bs == ButtonSelect::BtEnter ) ){
			return( Answer[bs] );
		}else{
			return( ButtonEvt::ButtonEvt_None );
		}
	}

	/* Function ******************************************************************/
	enum GuiButton::ButtonSelect GuiButton::GetButtonEvt( )
	/*****************************************************************************/
	{
		ButtonSelect ans;

		if( ButtonEvt::ButtonEvt_Trg == Answer[ ButtonSelect::BtEnter ] ){
			ans = ButtonSelect::BtEnter;
		}else if( ButtonEvt::ButtonEvt_Trg == Answer[ ButtonSelect::BtUp ] ){
			ans = ButtonSelect::BtUp;
		}else if( ButtonEvt::ButtonEvt_Trg == Answer[ ButtonSelect::BtDown ] ){
			ans = ButtonSelect::BtDown;
		}else if( ButtonEvt::ButtonEvt_Trg == Answer[ ButtonSelect::BtLeft ] ){
			ans = ButtonSelect::BtLeft;
		}else if( ButtonEvt::ButtonEvt_Trg == Answer[ ButtonSelect::BtRight ] ){
			ans = ButtonSelect::BtRight;
		}else{
			ans = ButtonSelect::BtNone;
		}
		return( ans );
	}
}
