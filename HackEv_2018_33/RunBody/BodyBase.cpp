/** Include file *************************************************************/
#include "BodyBase.h"

/** using宣言 ****************************************************************/
using ev3api::ColorSensor;
using ev3api::Motor;

/* 静的であれば初期化で利用できる */
static ColorSensor gColorSensor( PORT_2 );	/* カラーセンサ   */
static Motor       gArmMoter( PORT_A );		/* アームモーター   */
static Motor       gLeftWheel( PORT_C );	/* 左モーター     */
static Motor       gRightWheel( PORT_B );	/* 右モーター     */

/* 静的変数の初期化 */
BodyBase*    BodyBase::nowbody = NULL;
RGB_Checker* BodyBase::rgb_Checker = new RGB_Checker( gColorSensor );
ArmControl*  BodyBase::armControl = new ArmControl( gArmMoter );
Motor&		 BodyBase::LeftWheel  = gLeftWheel;
Motor&		 BodyBase::RightWheel = gRightWheel;

/* Function ******************************************************************/
BodyBase::BodyBase(
	/** 引数 **/
		)
	/** 初期化 **/
			
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void BodyBase::mStartRun( )
/*****************************************************************************/
{
	this->Start( );
	BodyBase::nowbody = this;
}

/* Function ******************************************************************/
void BodyBase::Task( )		/* staticなしで書く */
/*****************************************************************************/
{
	rgb_Checker->Thread( );
	armControl->Thread( );

	if( NULL != nowbody ){ 
		nowbody->Run( LeftWheel, RightWheel, armControl, rgb_Checker); 
	}
}

/* Function ******************************************************************/
bool BodyBase::IsInitialSetting( )
/*****************************************************************************/
{
	//if( (BodyBase::armControl->IsInitial( )) && (BodyBase::armControl->IsAdjust( )) ){
	if( BodyBase::armControl->IsInitial( ) ){
		return true;
	}else{
		return false;
	}
}

/* Function ******************************************************************/
int32_t BodyBase::GetLeftWheelCount( )
/*****************************************************************************/
{
	return BodyBase::LeftWheel.getCount( );
}

/* Function ******************************************************************/
int32_t BodyBase::GetRightWheelCount( )
/*****************************************************************************/
{
	return BodyBase::RightWheel.getCount( );
}

