/** Include file *************************************************************/
#include "AiAnswerBody.h"

/** using宣言 ****************************************************************/

static FILE	 *bt = NULL;
/* Function ******************************************************************/
AiAnswerBody::AiAnswerBody(
	/** 引数 **/
		PositionCalculator* mpositionCalculator ):
	/** 初期化 **/
		BodyBase( ),
		evt( Evt::Evt_Wait ),
		positionCalculator( mpositionCalculator )
/*****************************************************************************/
{
	bt = ev3_serial_open_file(EV3_SERIAL_BT);	/* ←本来はSceneの方も持っていて、戦略的な支持を出す。 */
}

/* Function ******************************************************************/
AiAnswerBody::~AiAnswerBody( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void AiAnswerBody::Start( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void AiAnswerBody::Run( Motor& m_leftWheel, Motor& m_rightWheel, ArmControl* m_armControl, RGB_Checker* m_rgb_Checker )
/*****************************************************************************/
{
	switch( this->evt ){
		/* 待機 */
		case Evt::Evt_Wait:
			m_leftWheel.setPWM( 0 );
			m_rightWheel.setPWM( 0 );			
			break;

		/* ライントレース */
		case Evt::Evt_LineTrace:
			WheelControl( 
				15,
				(float)( PID_feedback( (m_rgb_Checker->GetRefrectData( ) - 60), 0.70F, 0.00F, 0.07F, 0.004F ) ),
				m_leftWheel, m_rightWheel );
			break;

		/* 指定座標へ移動 */
		case Evt_GoToPosition:
			{
				double tA = positionCalculator->CalculateAngle( this->TargetPositionX, this->TargetPositionY, this->RelativeId );
				double dA = tA - positionCalculator->GetAngle( this->RelativeId );
				if( 90.0 < dA ){
					dA = 90.0;
				}else if( -90.0 > dA ){
					dA = -90.0;
				}else{
					
				}
				WheelControl( this->PositionSpeed, dA, m_leftWheel, m_rightWheel );
			}
			break;

		/* 指定角度まで右回り方向転換 */
		case Evt_GotoAngleRight:
			{
				if( true == this->Busy ){
					double angle = positionCalculator->GetAngle( this->RelativeId );
					if( this->TargetAngle > angle ){
						WheelControl( 0, this->AngleSpeed, m_leftWheel, m_rightWheel );
					}else{
						this->Busy = false;
						m_leftWheel.setPWM( 0 );
						m_rightWheel.setPWM( 0 );
					}
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
				}
			}
			break;

		/* 指定角度まで左回り方向転換 */
		case Evt_GotoAngleLeft:
			{
				if( true == this->Busy ){
					double angle = positionCalculator->GetAngle( this->RelativeId );
					if( this->TargetAngle < angle ){
						WheelControl( 0, -this->AngleSpeed, m_leftWheel, m_rightWheel );
					}else{
						this->Busy = false;
						m_leftWheel.setPWM( 0 );
						m_rightWheel.setPWM( 0 );
					}
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
				}
			}
			break;

		/* バック */
		case Evt_RelativeCheck:
			WheelControl( 
				-10,
				(float)( PID_feedback( -(m_rgb_Checker->GetRefrectData( ) - 60), 0.70F, 0.00F, 0.07F, 0.004F ) ),
				m_leftWheel, m_rightWheel );
			break;

		case Evt_DigitalDataScan:
			digitalDataScanFuncetion( m_leftWheel, m_rightWheel, m_rgb_Checker, this->RelativeId);
			break;

		default:
				
			break;
	}
		
}

/* Function ******************************************************************/
int AiAnswerBody::PID_feedback( float x, float Kp, float Ki, float Kd, float dt )
/*****************************************************************************/
{
	static float p = 0, i = 0, d = 0, integral = 0;
	static int prev_x = 0;

	p         = Kp * x;
	integral += ( prev_x + x ) / 2.0F * dt;
	d         = Kd * ( x - prev_x ) / dt;
	i         = Ki * integral;

	prev_x = x;
		
	return((int)( p + i + d ));
}

/* Function ******************************************************************/
void AiAnswerBody::WheelControl( int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel )
/*****************************************************************************/
{
	/* Turnが大きいほど右に曲がる */
	m_leftWheel.setPWM( (int)( front + ( turn * 0.50F ) ) );
	m_rightWheel.setPWM( (int)( front - ( turn * 0.50F ) ) );
}


/* Function ******************************************************************/
void AiAnswerBody::Stop( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Wait;
}

/* Function ******************************************************************/
void AiAnswerBody::LineTrace( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_LineTrace;
}

/* Function ******************************************************************/
void AiAnswerBody::RelativeCheck( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_RelativeCheck;
}

/* Function ******************************************************************/
void AiAnswerBody::GotoAngleLeft( int speed, double angle )
/*****************************************************************************/
{
	this->evt = Evt::Evt_GotoAngleLeft;
	this->AngleSpeed  = speed;
	this->TargetAngle = angle;
	this->Busy = true;
}

/* Function ******************************************************************/
void AiAnswerBody::GotoAngleRight( int speed, double angle )
/*****************************************************************************/
{
	this->evt = Evt::Evt_GotoAngleRight;
	this->AngleSpeed  = speed;
	this->TargetAngle = angle;
	this->Busy = true;
}

/* Function ******************************************************************/
void AiAnswerBody::GoToPosition( int speed, int x, int y, int id )
/*****************************************************************************/
{
	this->evt = Evt::Evt_GoToPosition;
	this->PositionSpeed = speed;
	this->TargetPositionX = x;
	this->TargetPositionY = y;
	this->RelativeId = id;
}

/* Function ******************************************************************/
void AiAnswerBody::DigitalDataScan(int id)
/*****************************************************************************/
{
	this->evt = Evt::Evt_DigitalDataScan;
	this->DScanState = 0;
	this->RelativeId = id;
	this->Busy = true;
}

/* Function ******************************************************************/
void AiAnswerBody::digitalDataScanFuncetion(Motor& m_leftWheel, Motor& m_rightWheel, RGB_Checker* m_rgb_Checker, int id)
/*****************************************************************************/
{
	enum DScanRunState{ Wait, GoToP, GoToLA, GoToRA };
	static int mPositionSpeed = 10;
	static int mTargetPositionX = 0;
	static int mTargetPositionY = 0;
	static int mAngleSpeed = 20;
	static int mTargetAngle = 0;
	static DScanRunState mrunState= DScanRunState::Wait;
	static bool mRunBusy = false;
	static unsigned char Answer = 0x00;

	switch( this->DScanState ){
		case 0:
			Answer = 0x00;
			mPositionSpeed = 10;
			mTargetPositionX = 5 - 14;
			mTargetPositionY = 15;
			mrunState = DScanRunState::GoToP;
			mRunBusy = true;
			this->DScanState++;
			break;

		case 1:
			if( !mRunBusy ){
				mPositionSpeed = 10;
				mTargetPositionX = 5 - 14;
				mTargetPositionY = 45;
				mrunState = DScanRunState::GoToP;
				mRunBusy = true;
				this->DScanState++;
				fprintf( bt, "1st,\r\n" );
			}
			break;

		case 2:
			/* 第1線探索 */
			{
				static int sCount = 0;
				RGB_Checker::ST_HSV hsv = m_rgb_Checker->GetHSV( );
				PositionCalculator::Position p = positionCalculator->GetPosition( id );
				fprintf( bt, "%d,%d,%d,%f,%f,\r\n", hsv.H, hsv.S, hsv.V, p.X, p.Y );
				if( ( 30.0 <= p.Y ) && ( p.Y <= 35.0 ) ){
					if( hsv.V <= 100 ){
						sCount++;
						if( 10 <= sCount ){
							//ev3_speaker_play_tone(NOTE_A5,100);
							Answer |= 0x08;
						}
					}else{
						sCount = 0;
					}
				}
			}
			if( !mRunBusy ){
				mPositionSpeed = 10;
				mTargetPositionX = 25 - 14;
				mTargetPositionY = 30;
				mrunState = DScanRunState::GoToP;
				mRunBusy = true;
				this->DScanState++;
			}
			break;

		case 3:
			if( !mRunBusy ){
				mAngleSpeed = 20;
				mTargetAngle = 270;
				mrunState = DScanRunState::GoToRA;
				mRunBusy = true;
				this->DScanState++;
			}
			break;

		case 4:
			if( !mRunBusy ){
				mPositionSpeed = 10;
				mTargetPositionX = -20 - 14;
				mTargetPositionY = 30;
				mrunState = DScanRunState::GoToP;
				mRunBusy = true;
				this->DScanState++;
				fprintf( bt, "2nd,3rd,\r\n" );
			}
			break;

		case 5:
			/* 第2,3線探索 */
			{
				static int sCount1 = 0;
				static int sCount2 = 0;
				RGB_Checker::ST_HSV hsv = m_rgb_Checker->GetHSV( );
				PositionCalculator::Position p = positionCalculator->GetPosition( id );
				fprintf( bt, "%d,%d,%d,%f,%f,\r\n", hsv.H, hsv.S, hsv.V, p.X, p.Y );
				if( ( -18.0 <= p.X ) && ( p.X <= -10.0 ) ){
					if( hsv.V <= 100 ){
						sCount1++;
						if( 10 <= sCount1 ){
							Answer |= 0x04;
						}
					}else{
						sCount1 = 0;
					}
				}
				if( ( -28.0 <= p.X ) && ( p.X <= -23.0 ) ){
					if( hsv.V <= 100 ){
						sCount2++;
						if( 10 <= sCount2 ){
							Answer |= 0x02;
						}
					}else{
						sCount2 = 0;
					}
				}
			}
			if( !mRunBusy ){
				mAngleSpeed = 20;
				mTargetAngle = 135;
				mrunState = DScanRunState::GoToLA;
				mRunBusy = true;
				this->DScanState++;
			}
			break;

		case 6:
			if( !mRunBusy ){
				mPositionSpeed = 10;
				mTargetPositionX = -6 - 14;
				mTargetPositionY = 20;
				mrunState = DScanRunState::GoToP;
				mRunBusy = true;
				this->DScanState++;
			}
			break;

		case 7:
			if( !mRunBusy ){
				mAngleSpeed = 20;
				mTargetAngle = 0;
				mrunState = DScanRunState::GoToLA;
				mRunBusy = true;
				this->DScanState++;
			}
			break;

		case 8:
			if( !mRunBusy ){
				mPositionSpeed = 10;
				mTargetPositionX = -9 - 14;
				mTargetPositionY = 45;
				mrunState = DScanRunState::GoToP;
				mRunBusy = true;
				this->DScanState++;
				fprintf( bt, "4th,\r\n" );
			}
			break;

		case 9:
			/* 第4線探索 */
			{
				static int sCount = 0;
				RGB_Checker::ST_HSV hsv = m_rgb_Checker->GetHSV( );
				PositionCalculator::Position p = positionCalculator->GetPosition( id );
				fprintf( bt, "%d,%d,%d,%f,%f,\r\n", hsv.H, hsv.S, hsv.V, p.X, p.Y );
				if( ( 35.0 <= p.Y ) && ( p.Y <= 40.0 ) ){
					if( hsv.V <= 100 ){
						sCount++;
						if( 10 <= sCount ){
							Answer |= 0x01;
						}
					}else{
						sCount = 0;
					}
				}
			}
			if( !mRunBusy ){
				this->DagitalData = Answer;
				this->Busy = false;
			}

		default:
			
			break;
	}

	switch(mrunState){
		case DScanRunState::Wait:
			m_leftWheel.setPWM( 0 );
			m_rightWheel.setPWM( 0 );			
			break;

		case DScanRunState::GoToP:
			{
				/* 目標角度算出 */
				double tA = positionCalculator->CalculateAngle( mTargetPositionX, mTargetPositionY, id );
				double dA = tA - positionCalculator->GetAngle( id );
				if( 90.0 < dA ){
					dA = 90.0;
				}else if( -90.0 > dA ){
					dA = -90.0;
				}else{
					
				}

				/* 座標チェック */
				if( positionCalculator->RangeJuge( mTargetPositionX, mTargetPositionY, 2, id ) ){
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
					mRunBusy = false;
				}else{
					WheelControl( mPositionSpeed, dA, m_leftWheel, m_rightWheel );
				}
			}
			break;

		case DScanRunState::GoToLA:
			{
				if( true == mRunBusy ){
					double angle = positionCalculator->GetAngle( id );
					if( mTargetAngle < angle ){
						WheelControl( 0, -mAngleSpeed, m_leftWheel, m_rightWheel );
					}else{
						m_leftWheel.setPWM( 0 );
						m_rightWheel.setPWM( 0 );
						mRunBusy = false;
					}
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
				}
			}
			break;

		case DScanRunState::GoToRA:
			{
				if( true == mRunBusy ){
					double angle = positionCalculator->GetAngle( id );
					if( mTargetAngle > angle ){
						WheelControl( 0, mAngleSpeed, m_leftWheel, m_rightWheel );
					}else{
						mRunBusy = false;
						m_leftWheel.setPWM( 0 );
						m_rightWheel.setPWM( 0 );
					}
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
				}
			}
			break;

		default:
			
			break;
	}
}

