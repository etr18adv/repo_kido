#ifndef _L_PARKING_BODY_H_
#define _L_PARKING_BODY_H_

/** Include file *************************************************************/
#include "BodyBase.h"

/** using�錾 ****************************************************************/

/* Class ******************************************************************/

class L_ParkingBody : public BodyBase {
public:
	L_ParkingBody();
	~L_ParkingBody();

private:
	void Start();
	void Run(Motor&, Motor&, ArmControl*, RGB_Checker*);
};

#endif 
