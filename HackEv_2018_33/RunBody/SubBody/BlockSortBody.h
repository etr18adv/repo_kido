#ifndef _BLOCK_SORT_BODY_H_
#define _BLOCK_SORT_BODY_H_

/** Include file *************************************************************/
#include "BodyBase.h"
#include "PositionCalculator.h"

/** using宣言 ****************************************************************/

/* Class ******************************************************************/
class BlockSortBody : public BodyBase {
public:
	BlockSortBody(PositionCalculator*);
	~BlockSortBody();

private:
	enum Evt {
		Evt_Wait = 0,
		Evt_LineTrace,
		Evt_GoToPosition,
		Evt_Back,
		Evt_TurnLeft,
		Evt_TurnRight,
		Evt_Free
	};
	Evt evt;

	PositionCalculator* positionCalculator;
	void Start();
	void Run(Motor&, Motor&, ArmControl*, RGB_Checker*);

	void WheelControl(int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel);
	int PID_feedback( float x, float Kp, float Ki, float Kd, float dt );

/* イベント定義 **********************************************************/
	/* Stop * 待機 *****************************************/
	public:
		void Stop();
	private:
	/*******************************************************/

	/* LineTrace *ライントレース****************************/
	public:
		void LineTrace();
	private:
		float integral = 0;
		int prev_x = 0;
	/*******************************************************/

	/* GoToPosition *指定座標へ向かう***********************/
	public:
		void GoToPosition( int speed, int x, int y, int id );
	private:
		int PositionSpeed = 50;
		int TargetPositionX = 0;
		int TargetPositionY = 0;
		int RelativeId;
	/*******************************************************/

	/* Back *指定距離バックする*****************************/
	public:
		void Back( int speed, int distance, int id );
	private:
		int BackSpeed = 50;
		int BackDistance = 0;
		int32_t StartDistance = 0;
	/*******************************************************/

	/* TurnLeft *左足を中心に方向を変える*******************/
	public:
		void TurnLeft( int speed, int Angle, int id );
	private:
		int TurnLeftSpeed = 50;
		int TurnLeftAngle = 0;
		double LStartAngle = 0;
	/*******************************************************/

	/* TurnRight *右足を中心に方向を変える******************/
	public:
		void TurnRight( int speed, int Angle, int id );
	private:
		int TurnRightspeed = 50;
		int TurnRightAngle = 0;

		double RStartAngle = 0;
	/*******************************************************/

	/* Freet *フリー(検討用)********************************/
	public:
		void Free( int mLeftPower, int mRightPower );
	private:
		int LeftPower = 0;
		int RightPower = 0;
	/*******************************************************/

};

#endif 
