#ifndef _ARM_CONTROL_H_
#define _ARM_CONTROL_H_

/** Include file *************************************************************/
#include "Motor.h"

/** using宣言 ****************************************************************/
using ev3api::Motor;

/** Class ********************************************************************/
class ArmControl {
public:
	/* 定期スレッド呼出し用 */
	void Thread();
	/* コンストラクタ */
	ArmControl(Motor&);

	/* 利用可能 Function */
	void SetTargetAngle(int);
	bool IsAdjust();
	bool IsInitial();

private:
	/* define ****************/
	enum State { Init = 0, Play };

	/* 変数 ******************/
	enum State state;

	int TargetAngle;

	/* 使用するドライバ */
	Motor& gArmMoter;

	/* Function */
	bool InitAlignment();
};

#endif  // EV3_APP_LINETRACER_H_
