
/** Include file *************************************************************/
#include "RGB_Checker.h"

#include "math.h"


/* Function ******************************************************************/
RGB_Checker::RGB_Checker( 
	/** 引数 **/
		const ColorSensor& mColorSensor ):
	/** 初期化 **/
		CalData( { 0, 0, 0 } ),
		sumRGB( { 0, 0, 0 } ),
		gColorSensor( mColorSensor )
/*****************************************************************************/
{
	{
		/* バッファーの初期化。将来個数が変わる可能性があるので、ここで可変的に初期化。 */
		int i = 0;
		for( i = 0; i < RGB_Checker::FilterDataNo; i++ ){
			this->buffRGB[ i ].r = 0;
			this->buffRGB[ i ].g = 0;
			this->buffRGB[ i ].b = 0;
		}

		/* HSV計算 */
		this->nowHSV = RGB_Checker::RGB_to_HSV( this->CalData );
	}
}

/* Function ******************************************************************/
void RGB_Checker::Thread( )
/*****************************************************************************/
{
	{
		/* stack変数定義 */
		rgb_raw_t rgbValue;	/* 最新データ取得用変数 */

		/* RGBデータ取得(参照渡し) */
		gColorSensor.getRawColor( rgbValue );

		/* フィルターデータ更新 */
		{
			this->sumRGB.r -= this->buffRGB[ this->RgbCalCount ].r;
			this->sumRGB.g -= this->buffRGB[ this->RgbCalCount ].g;
			this->sumRGB.b -= this->buffRGB[ this->RgbCalCount ].b;

			this->buffRGB[ this->RgbCalCount ].r = rgbValue.r;
			this->buffRGB[ this->RgbCalCount ].g = rgbValue.g;
			this->buffRGB[ this->RgbCalCount ].b = rgbValue.b;

			this->sumRGB.r += this->buffRGB[ this->RgbCalCount ].r;
			this->sumRGB.g += this->buffRGB[ this->RgbCalCount ].g;
			this->sumRGB.b += this->buffRGB[ this->RgbCalCount ].b;

			this->RgbCalCount++;
			if( 10 <= this->RgbCalCount ){
				this->RgbCalCount = 0;
			}

			/* 計算結果格納 */
			this->CalData.r = this->sumRGB.r / RGB_Checker::FilterDataNo;
			this->CalData.g = this->sumRGB.g / RGB_Checker::FilterDataNo;
			this->CalData.b = this->sumRGB.b / RGB_Checker::FilterDataNo;
		}

		/* HSV計算 */
		this->nowHSV = RGB_Checker::RGB_to_HSV( this->CalData );
	}
}

#define Max(a,b)	( a > b ? a : b )
#define Min(a,b)	( a < b ? a : b )
/* Function ******************************************************************/
struct RGB_Checker::ST_HSV RGB_Checker::RGB_to_HSV( rgb_raw_t rgbValue )
/*****************************************************************************/
{
	struct ST_HSV mHSV;
	int MaxValue;
	int MinValue;

	MaxValue = Max( rgbValue.r, rgbValue.g );
	MaxValue = Max( MaxValue, rgbValue.b );

	MinValue = Min( rgbValue.r, rgbValue.g );
	MinValue = Min( MinValue, rgbValue.b );

	/* H演算 */
	if( ( rgbValue.r == rgbValue.g ) && ( rgbValue.g == rgbValue.b ) ){
		mHSV.H = 0;
	}else{
		if( ( rgbValue.r >= rgbValue.g ) && ( rgbValue.r >= rgbValue.b ) ){
			mHSV.H = ( 60 * ( rgbValue.g - rgbValue.b ) ) / ( MaxValue - MinValue );
		}else if( ( rgbValue.g >= rgbValue.r ) && ( rgbValue.g >= rgbValue.b ) ){
			mHSV.H = ( 60 * ( rgbValue.b - rgbValue.r ) ) / ( MaxValue - MinValue ) + 120;
		}else{
			mHSV.H = ( 60 * ( rgbValue.r - rgbValue.g ) ) / ( MaxValue - MinValue ) + 240;
		}
	}

	/* S演算 */
	mHSV.S = ( 255 * (MaxValue - MinValue) ) / MaxValue;

	/* V演算 */
	mHSV.V = MaxValue;

	return( mHSV );
}

/* Function ******************************************************************/
rgb_raw_t RGB_Checker::GetRGB() const
/*****************************************************************************/
{
	return( this->CalData );
}

/* Function ******************************************************************/
struct RGB_Checker::ST_HSV RGB_Checker::GetHSV() const
/*****************************************************************************/
{
	return( this->nowHSV );
}

/* Function ******************************************************************/
bool RGB_Checker::ColorJude( ) const
/*****************************************************************************/
{
	/* カラー判定用閾値より大きければカラー上と判定する */
	return ( RGB_Checker::SaturationThreshold <= this->nowHSV.S );
}

/* Function ******************************************************************/
bool RGB_Checker::ColorJude( enum RGB_Checker::Color color ) const
/*****************************************************************************/
{
	bool answer = false;
	enum RGB_Checker::Color ansColor = RGB_Checker::Color::Other;

	/* カラー判定用閾値より大きければカラー上と判定する */
	if( ( RGB_Checker::SaturationThreshold <= this->nowHSV.S ) &&
		( 60 <= this->nowHSV.V ) ){
		/* 色判定 */
		if( ( this->nowHSV.H <= 20 ) || ( 340 <= this->nowHSV.H ) ){
			ansColor = RGB_Checker::Red;
		}else if( ( 40 <= this->nowHSV.H ) && ( this->nowHSV.H <= 80 ) ){
			ansColor = RGB_Checker::Yellow;
		}else if( ( 100 <= this->nowHSV.H ) && ( this->nowHSV.H <= 140 ) ){
			ansColor = RGB_Checker::Green;
		}else if( ( 220 <= this->nowHSV.H ) && ( this->nowHSV.H <= 260 ) ){
			ansColor = RGB_Checker::Blue;
		}else{
			;		/* その他 */
		}
	}

	if( color == ansColor ){
		answer = true;
	}
	return answer;
}

/* Function ******************************************************************/
int RGB_Checker::GetRefrectData() const
/*****************************************************************************/
{
	return( this->CalData.r );
}
