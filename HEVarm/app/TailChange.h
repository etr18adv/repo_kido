/*
 * TailChange.h
 *
 *  Created on: 2018/05/06
 *      Author: Masahiro
 */

#ifndef MODELING_ETROBO_TR_EX3HEV_APP_TAILCHANGE_H_
#define MODELING_ETROBO_TR_EX3HEV_APP_TAILCHANGE_H_

#include "Motor.h"
#include "Clock.h"

using namespace ev3api;

class TailChange {
public:
//	TailChange(ev3api::Motor& tailMotor);
	TailChange(Motor& tailMotor);
	virtual ~TailChange();

	bool run(int32_t arc);

private:
	Motor& mTailMotor;
//	ev3api::Motor& mTailMotor;
	bool mIsInitialized;

	void tailInit();
	bool tailReset();
	bool tailSet(int32_t arc);
};

#endif /* MODELING_ETROBO_TR_EX3HEV_APP_TAILCHANGE_H_ */
