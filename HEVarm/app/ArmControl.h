/*
 * ArmControl.h
 *
 *  Created on: 2018/05/31
 *      Author: Masahiro
 */

#ifndef MODELING_HEVARM_APP_ARMCONTROL_H_
#define MODELING_HEVARM_APP_ARMCONTROL_H_

#include "Motor.h"
#include "Clock.h"

using namespace ev3api;

//アーム角度指定　尻尾角度0（NORMAL）
#define ARM_READY_NORMAL 3 //アーム辺先が地面と平行
#define ARM_LINEMONITOR_NORMAL 32	//カラーセンサが地面に垂直
#define ARM_FORWARD_NORMAL 70//アーム辺黒が地面と平行
#define ARM_UP_NORMAL 98	//アーム最大上げ状態

//アーム角度指定　尻尾角度1000（SONAR）
#define ARM_READY_SONAR 12 //アーム辺先が地面と平行
#define ARM_LINEMONITOR_SONAR 36	//カラーセンサが地面に垂直
#define ARM_FORWARD_SONAR 71//アーム辺黒が地面と平行

//アーム角度指定　尻尾角度2000（HIPUP）・・・使う予定なし
#define ARM_READY_HIPUP 54 //アーム辺先が地面と平行（タイヤが浮き使えない）
#define ARM_LINEMONITOR_HIPUP 54	//カラーセンサが地面に垂直
#define ARM_FORWARD_HIPUP 90//アーム辺黒が地面と平行（前に倒れ使えない）


class ArmControl {
public:
	ArmControl(Motor& armMotor);
	virtual ~ArmControl();

	bool run();

private:
	Motor& mArmMotor;
	bool mIsInitialized;

	bool armReset();
	bool armSet(int32_t arc, int pwm);

};

#endif /* MODELING_HEVARM_APP_ARMCONTROL_H_ */
