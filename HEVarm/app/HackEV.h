/*
 * HackEV.h
 *
 *  Created on: 2018/04/08
 *      Author: masah
 */

#ifndef SDK_MODELING_ETROBO_TR_EX3HEV_APP_HACKEV_H_
#define SDK_MODELING_ETROBO_TR_EX3HEV_APP_HACKEV_H_


#include "Starter.h"
#include "SimpleTimer.h"

#include "LineTracer.h"
#include "Spinning.h"
#include "TailChange.h"
#include "ArmControl.h"

class HackEV {
public:
    enum State {
        UNDEFINED,			//起動中
        WAITING_FOR_START,	//スタート待ち状態
        LINE_TRACING,		//ライントレース中
        SPINNING,	//スピン中
		TAIL_CHANGING,//尻尾テスト中
		ARM_TEST	//アームテスト中
    };

    int      bt_cmd=0;     /* Bluetoothコマンド 1:リモートスタート */
    FILE     *bt=NULL;     /* Bluetoothファイルハンドル */

	HackEV(LineTracer* lineTracer,
            Spinning* spinning,
            const Starter* starter,
            SimpleTimer* simpleTimer,
			TailChange* tailChange,
			ArmControl* armControl);
	virtual ~HackEV();
	void run();

private:

    LineTracer* mLineTracer;
    Spinning* mSpinning;
    const Starter* mStarter;
    SimpleTimer* mSimpleTimer;
    State mState;
    TailChange* mTailChange;
    ArmControl* mArmControl;

    void modeChangeAction(State st);
    void execUndefined();
    void execWaitingForStart();
    void execLineTracing();
    void execSpinning();
};

#endif /* SDK_MODELING_ETROBO_TR_EX3HEV_APP_HACKEV_H_ */
