/******************************************************************************
 *  LineMonitor.h (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Definition of the Class LineMonitor
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#ifndef EV3_UNIT_LINEMONITOR_H_
#define EV3_UNIT_LINEMONITOR_H_

#include <list>

#include "ColorSensor.h"

/*!
 * @class LineMonitor
 * @brief 反射光を計測するクラス
 */
class LineMonitor {
public:
	/**
	 * @param[in] colorSensor カラーセンサー
	 * @param[in] black 黒の反射光値
	 * @param[in] white 白の反射光値
	 */
    explicit LineMonitor(const ev3api::ColorSensor& colorSensor, int8_t black, int8_t white);

    void init();

    /**
     * @brief ライン上か否かを判定する
     * @retval true  ライン上
     * @retval false ライン外
     */
    bool isOnLine();

    /**
     * @brief ライン上か否かを判定する
     * @param[in] value 目標値
     * @retval true  ライン上
     * @retval false ライン外
     */
    bool isOnLine(int value);

    /*
     * @brief ラインを横切ったときの黒線中央にいるかを判定する
     * @param[in] value 白寄りの目標値
     * @retval true 中央
     * @retval false 中央でない
     * @attention 白寄りの目標値を発見 → カラー値が減る → 増えたらtrueを返す
     */
    bool crossLine(int value);

    /**
     * 黒の反射光を設定する
     * @param[in] value 測定値
     */
    void setBlack(int value);

    /**
     * 白の反射光を設定する
     * @param[in] value 測定値
     */
    void setWhite(int value);

    /*!
     * 目標値と現在値の差分を取得する
     */
    double getDiff();


    /*!
     * 目標値との差分に対する移動平均値を取得する
     */
    double getMovingDiff();

    /*!
     * 目標値を設定する
     * @param[in] target (0-100) 正規化後の目標値。値を小さくすると黒寄りとなり、大きくすると白よりになる
     */
    void setTarget(double target);

    /*
     * 目標値を設定する
     * @param[in] target 正規化前の値。正規化して目標値を設定する
     */
    void setTarget2(int target);

    /*
     * カラー目標値との差分を返す（現在値 - 目標値）
     */
    int getTarget();

    /*
     * num 回の平均値を返す
     */
    int getBrightness(int num);

    /*
     * num 回の正規値を返す
     */
    int getNormalBrightness(int num);

    /*
     * @brief 色番号を返す
     * @retval colorid_t 色番号
     * @attention 計測に時間がかかるので注意
     */
    colorid_t getColorNumber();

    void getRGB(rgb_raw_t *rgb);


    /*
     * @brief カラーのHSV値を返す。
     * by kidokoro for HackEV
     * */
    void getHSV(int *h,int *s,int *v);

    /*
         * @brief HSV値を見て色を判断する。
         * @retval int  色番号(colorid_t だとグレーがないので独自設定。common.h)
         * NONE 0
         * BLACK 1
         * BLUE 2
         * GREEN 3
         * YELLOW 4
         * RED 5
         * WHITE 6
         * GRAY 7
         * by kidokoro for HackEV
         * */
    int getColorHSV();


    bool checkColor(colorid_t color);

private:
    const ev3api::ColorSensor& mColorSensor;

    int8_t mBlack;
    int8_t mWhite;
    double mTarget;

    unsigned int crossStatus;
    double pre;

    /*
     * @param[in] value 入力値
     */
    double normalization(int value); //正規化


    std::list<int> colorList; 	//保持する値のリスト
    int colorSum;				//保持した値の総和
};

#endif  // EV3_UNIT_LINEMONITOR_H_
