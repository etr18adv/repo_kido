/** @file Sonar.h
*	@brief 前方ソナークラス
*   @author yamaguchi
*   @date 2017.06.18
*/

#ifndef SDK_WORKSPACE_ETRONO_TR2_UNIT_SONAR_H_
#define SDK_WORKSPACE_ETRONO_TR2_UNIT_SONAR_H_

#include <list>

#include "SonarSensor.h"

/*!
 * @class Sonar
 * @brief 超音波センサを用いて距離計測するクラス
 */
class Sonar {
public:
	Sonar(ev3api::SonarSensor& sonarSensor);
	virtual ~Sonar();

	/*!
	 * @brief 距離を保持する。周期ハンドラから呼び出されることを想定
	 */
	void doMeasure();

	/*!
	 * @brief 距離を返す
	 * @return getDistance 距離 [cm]
	 */
	double getDistance();

private:
//	int mDistance; /*!< 障害物までの距離 [cm] */
	//計測値
	std::list<int16_t> mDistance;
	int mSum;		//保持した値の総和

	ev3api::SonarSensor& mSonarSensor; //超音波センサー
};

#endif /* SDK_WORKSPACE_ETRONO_TR2_UNIT_SONAR_H_ */
