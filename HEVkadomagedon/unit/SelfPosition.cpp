/*
 * Measure.cpp
 *
 *  Created on: 2017/07/05
 *      Author: takeshi
 */

#include "SelfPosition.h"

#include <math.h>

#include "common.h"
#include "SonarSensor.h"


SelfPosition::SelfPosition(ev3api::Motor& leftWheel, ev3api::Motor& rightWheel)
 : mLeftWheel(leftWheel), mRightWheel(rightWheel)
{
	logID = -1;
	init();
}

SelfPosition::~SelfPosition() {

}

void SelfPosition::init(){
	setFlag = false;

	mPointX=0;
	mPointY=0;
	mLeftCount = -1;
	mRightCount = -1;

	cos0 = 1;
	sin0 = 0;

	cos1 = 1;
	sin1 = 0;

	theta = 0.0;
	logID++;

	dprintf("SelfPosition inited.\n");
}

void SelfPosition::doMeasure(){
	constexpr double CIRCUMFERENCE = (WHEEL_DIAMETER * M_PI)/360;

	//左
	if( mLeftCount != -1 ){
		mLeftCount2 = mLeftCount; //前回値
		mLeftCount  = mLeftWheel.getCount();
	} else {
		mLeftCount2 = mLeftWheel.getCount();
		mLeftCount  = mLeftCount2;
	}

	//右
	if( mRightCount != -1 ){
		mRightCount2 = mRightCount;
		mRightCount  = mRightWheel.getCount();
	} else {
		mRightCount2 = mRightWheel.getCount();
		mRightCount  = mRightCount2;
	}

	double L = (mLeftCount  - mLeftCount2)  * CIRCUMFERENCE;
	double R = (mRightCount - mRightCount2) * CIRCUMFERENCE;

	double d = (L + R)/2;
	theta += (R - L)/WHEEL_WIDTH;

	//dprintf("XX L:%ld,%ld, R:%ld,%ld\n", mLeftCount, mLeftCount2, mRightCount, mRightCount2);

//	double x = mPointX + (d * sin(theta) * -1);
//	double y = mPointY + (d * cos(theta));
//
//	mPointX = x;
//	mPointY = y;
//	mPointX = mPointX + (d * sin(theta) * -1);
	mPointX = mPointX - (d * sin(theta));
	mPointY = mPointY + (d * cos(theta));

}


int SelfPosition::getDistance(){
//	constexpr double PI     = 3.14159;  /* 円周率　170528 by kidokoro*/
//	constexpr double CIRCUMFERENCE_HALF = (WHEEL_DIAMETER * PI)/720;
	constexpr double CIRCUMFERENCE_HALF = (WHEEL_DIAMETER * M_PI)/720;

//    return ((mLeftCount + mRightCount)/2) * CIRCUMFERENCE;
    return (mLeftCount + mRightCount) * CIRCUMFERENCE_HALF;
}


void SelfPosition::printPosition(){
	double x,y;

	getPosition(&x, &y);

	dprintf("[POINT%d],%f,%f,%lf,%lf,%lf,%lf,%lf,%d\n",logID,mPointX,mPointY,mPointX1,mPointY1,mPointX2,mPointY2,theta,getDistance());
}


void SelfPosition::getPosition(double *x, double *y){
//	double theta0 = atan(mPointX0/mPointY0) * -1;
//	double x2 = (mPointX) * cos(theta0) - (mPointY * -1) * sin(theta0);
//	double y2 = -1.0 * ( (mPointX) * sin(theta0) + (mPointY * -1) * cos(theta0));

	//	if( mPointX0 == 0 && mPointY0 == 0){
//		*x = mPointX;
//		*y = mPointY;
//	} else {

	//スタート位置の置き方による座標位置補正
	*x = (mPointX * cos0) + (mPointY * sin0);
	*y = (mPointY * cos0) - (mPointX * sin0);

	mPointX1 = *x;
	mPointY1 = *y;

	if( setFlag == true ){
	//ゴール前のy座標補正
		*x = ((*x-mPointX0)*cos1) + ((*y-mPointY0)*sin1) + mPointX0;
		*y = ((mPointX0-*x)*sin1) + ((*y-mPointY0)*cos1) + mPointY0;
	}

	mPointX2 = *x;
	mPointY2 = *y;
}

void SelfPosition::setPositionX(double x, double y){
	mPointX = (x * cos0) - (y * sin0);
//	mPointX = x;
//
//	setFlag = true;

	dprintf("setPositionX called.\n");
}

void SelfPosition::setPositionY(double x, double y){
	//getPosition()で座標回転があるため、予め逆回転しておく
	mPointY = (x * sin0) + (y * cos0);
//	mPointY = y;
//
//	setFlag = true;

	dprintf("setPositionY called.\n");
}

void SelfPosition::ajustment(){
	//補正に使用する基準点を保持する。initの後の直線のどこかで呼ぶこと
//	mPointX0 = mPointX;
//	mPointY0 = mPointY;

	//補正に使用する基準点から座標の回転角を保持する
	//これはあくまで、スタート位置の走行体の向きの角度であり
	//左右モータの個体差を吸収できるわけではない
	double theta0 = atan(mPointX/mPointY) * -1;
	cos0 = cos(theta0);
	sin0 = sin(theta0);

	dprintf("SelfPosition ajusted. (cos0:%f, sin0:%f)\n", cos0, sin0);
}

void SelfPosition::rotate(double x0, double y0){
	double theta0 = atan((mPointY1-y0)/(mPointX1-x0));

	cos1 = cos(theta0);
	sin1 = sin(theta0);

	mPointX0 = x0;
	mPointY0 = y0;

	setFlag = true;

	dprintf("SelfPosition rotated. (%f,%f)-(%f,%f) theta0:%f \n", x0,y0,mPointX1,mPointY1,theta0);
}
