/** @file MainUnit.h
*	@brief EV3本体のLED、操作ボタン、液晶を制御するクラス
*   @author yamaguchi
*   @date 2017.06.16
*/

#ifndef SDK_WORKSPACE_ETRONO_TR2_UNIT_MAINUNIT_H_
#define SDK_WORKSPACE_ETRONO_TR2_UNIT_MAINUNIT_H_

/*!
 * @class MainUnit
 * @brief EV3本体を制御するクラス
 */
class MainUnit {
public:
	MainUnit();
	virtual ~MainUnit();

	/*
	 * @brief 次の状態へ遷移する
	 */
	void nextState();

	/*
	 * @brief 本体バックボタンが押下されるまで待機する
	 */
	void waitStop();

	/*
	 * 上下左右ボタンの次に決定を押されたら、ボタン番号を返す
	 * @return ボタン番号
	 */
	int waitButton();

	/*
	 * バックボタンが押されているか確認する
	 * @return true/false
	 */
	bool checkBackButton();

	/*
	 * LCD液晶に文字列を呼び出しの度に行を変えて表示する
	 * @param[in] str 文字列
	 */
	void display(const char *str);


	/*
	 * バッテリ残量を表示する
	 */
	void battery();

private:
	int mState; /*< 起動中、初期化完了、動作中、停止中など  */
};

#endif /* SDK_WORKSPACE_ETRONO_TR2_UNIT_MAINUNIT_H_ */
