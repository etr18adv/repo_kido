/******************************************************************************
 *  BalancingWalker.h (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Definition of the Class BalancingWalker
 *  Author: Kazuhiro.Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest

  20170528 by 城所
    calcDistance()を追加

 *****************************************************************************/

#ifndef EV3_UNIT_BALANCINGWALKER_H_
#define EV3_UNIT_BALANCINGWALKER_H_

#include <list>

#include "GyroSensor.h"
#include "Motor.h"
#include "BalancerCpp.h"
#include "Vibration.h"

class BalancingWalker {
public:

	/**
	 * コンストラクタ
	 * @param vibration  振動
	 * @param leftWheel  左モータ
	 * @param rightWheel 右モータ
	 * @param balancer   バランサ
	 */
    BalancingWalker(Vibration* vibration,
                    ev3api::Motor& leftWheel,
                    ev3api::Motor& rightWheel,
                    Balancer* balancer);

    /**
     * バランス走行に必要なものをリセットする
     */
    bool init();

    /**
     * バランス走行する
     */
    bool run();

    /*!
     * @brief スピン関連情報を初期化する
     */
    void spinInit();

    /*!
     * @brief その場でスピンする
     * @param[in] angle 終了角度 (>0)
     * @param[in] num 各モーターの回転周期 (num * 4 ms)
     * @retval true 動作中
     * @retval false 終了中
     * @attention setCommand()で設定したturnが正:右回り、負:左回り
     */
    bool spin(double angle, int num);

    /**
     * PWM値を設定する
     * @param[in] forward 前進値
     * @param[in] turn    旋回値
     */
    void setCommand(int forward, int turn);


#ifdef BATTERY_AVERAGE
    /*
     * バッテリ残量を計測する
     */
    void doMeasureBattery();
#endif

    /**
     * バッテリ残量の移動平均値を取得する
     */
    int getBattery();

    /*
     * ローパスフィルタ係数を設定する
     */
    void setA_R(float value);

    float getA_R();

private:
    Vibration* mVibration;
    ev3api::Motor& mLeftWheel;
    ev3api::Motor& mRightWheel;
    Balancer* mBalancer;
    int mForward;
    int mTurn;
    int mCount;

	int mNum;
	int32_t mRight0;
	int32_t mLeft0;

	int mBattery;
	std::list<int> batteryList;
	int batterySum;

	float A_R; /* ローパスフィルタ係数(左右車輪の目標平均回転角度用) */
};

#endif  // EV3_UNIT_BALANCINGWALKER_H_
