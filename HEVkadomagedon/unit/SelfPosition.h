/*
 * Measure.h
 *
 *  Created on: 2017/07/05
 *      Author: takeshi
 */

#ifndef SDK_WORKSPACE_ETRONO_TR2_UNIT_SELFPOSITION_H_
#define SDK_WORKSPACE_ETRONO_TR2_UNIT_SELFPOSITION_H_

#include "Motor.h"

//const unsigned char ROTATION_RIGHT  = (unsigned char)0;
//const unsigned char ROTATION_LEFT   = (unsigned char)1;


/*!
 * @class SelfPosition
 * @brief 自己位置を推定するクラス
 */
class SelfPosition {
public:
	SelfPosition(ev3api::Motor& leftWheel, ev3api::Motor& rightWheel);
	virtual ~SelfPosition();

	/*!
	 * @brief 内部パラメータを初期化する
	 */
	void init();

	/*!
	 * @brief モーター回転角を計測し、x,y座標を計算する
	 */
	void doMeasure();

	/*!
	 * @brief 走行距離を返す
	 * @return 走行距離 [mm]
	 */
	int getDistance();

	/*!
	 * @brief 現在のx,y座標をログ出力する
	 */
	void printPosition();


	/*!
	 * @brief 現在のx,y座標を取得する
	 * @param[in,out] x x座標
	 * @param[in,out] y y座標
	 */
	void getPosition(double *x, double *y);

	/*!
	 * x座標を変更する
	 */
	void setPositionX(double x, double y);

	/*!
	 * y座標を変更する
	 */
	void setPositionY(double x, double y);

	/*!
	 * @brief 補正に使用する回転角を保持する
	 */
	void ajustment();

	/*!
	 * @brief y軸補正に使用する回転角を保持する
	 */
	void rotate(double x0, double y0);


private:
	ev3api::Motor& mLeftWheel;
	ev3api::Motor& mRightWheel;

	int logID;

	//計測値
	double mPointX;
	double mPointY;
	int32_t mLeftCount;
	int32_t mRightCount;
	int32_t mLeftCount2;
	int32_t mRightCount2;

	//スタート位置補正後
	double mPointX1;
	double mPointY1;
	//ゴール前y軸補正後
	double mPointX2;
	double mPointY2;

	double sin0,cos0;
	double theta;

	bool setFlag;
	double sin1,cos1; //y軸回転補正用
	double mPointX0;
	double mPointY0;
};

#endif /* SDK_WORKSPACE_ETRONO_TR2_UNIT_SELFPOSITION_H_ */
