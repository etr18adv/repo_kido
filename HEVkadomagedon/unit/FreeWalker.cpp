/** @file FreeWalker.cpp
*	@brief バランス制御なし走行を制御するクラス
*   @author yamaguchi
*   @date 2017.06.17
*/


#include "common.h"
#include "FreeWalker.h"

FreeWalker::FreeWalker(ev3api::Motor& leftWheel, ev3api::Motor& rightWheel)
:  mLeftWheel(leftWheel), mRightWheel(rightWheel), mForward(0), mTurn(0)
{

}

FreeWalker::~FreeWalker() {

}


void FreeWalker::run(){
	//ER ev3_motor_steer()を参考に実装
	int leftPWM, rightPWM;
#if 0
	if( mTurn > 0 ){
		leftPWM  = mForward;
		rightPWM = mForward * (100-mTurn)/100;
	}
	else {
		leftPWM  = mForward * (100-mTurn)/100;
		rightPWM = mForward;
	}
#else// for HackEV by kidokoro	ライントレース動作修正が難しいため、一旦こちらにする
	if(mForward==0){//スピード0時は停止する
		leftPWM=0;
		rightPWM=0;
	}
	else{
		leftPWM  = mForward + mTurn;//左側ライントレースの時はこちらが＋
		rightPWM = mForward - mTurn;//右側ライントレースの時はこちらが＋
	}
#endif

//    dprintf("leftPWM = %d, rightPWM = %d\n", leftPWM, rightPWM);
	mLeftWheel.setPWM(leftPWM);
	mRightWheel.setPWM(rightPWM);
}

void FreeWalker::spinInit(){
	mRight0 = -1;
	mLeft0  = -1;
}

bool FreeWalker::spin(double angle){
	int leftPWM  = mForward;
	int rightPWM = mForward * -1;


	if( mRight0 == -1 )
		mRight0 = mRightWheel.getCount();
	if( mLeft0 == -1 )
		mLeft0 = mLeftWheel.getCount();

	//目標角度がマイナスのときは逆周りにするため-1を乗算
	if( angle < 0){
		leftPWM  *= -1;
		rightPWM *= -1;
	}

	mLeftWheel.setPWM(leftPWM);
	mRightWheel.setPWM(rightPWM);

	int32_t count,countR,countL;
	countR = mRightWheel.getCount() - mRight0;
	countL = mLeftWheel.getCount() - mLeft0;

	if( angle > 0 ){
		count = (countL - countR)/2;
	} else{
		count = (countR - countL)/2;
	}
	dprintf("YY count=%ld mRight0:%ld mLeft0:%ld angle:%f\n", count, mRight0, mLeft0, angle);

	// by kidokoro 逆回転ができなかったので、修正
	int uns_angle;
	if(angle<0)uns_angle=-angle;
	else uns_angle=angle;

	//if( (count * WHEEL_DIAMETER/WHEEL_WIDTH) > angle ){ //160mm:タイヤ間距離、80mm:タイヤ直径
	if( (count * WHEEL_DIAMETER/WHEEL_WIDTH) > uns_angle ){ //by kidokoro　逆回転ができなかったので修正
		dprintf("XX count=%ld mRight0:%ld mLeft0:%ld angle:%f\n", count, mRight0, mLeft0, angle);
			return false;

	}
	else
		 return true;

}

bool FreeWalker::turn(double angle){
	int leftPWM  = mForward;
	int rightPWM = mForward;

	//ターン開始時の初期値が入る（-1はモードが変わったときにspinInitで初期化された値）
	if( mRight0 == -1 )
		mRight0 = mRightWheel.getCount();
	if( mLeft0 == -1 )
		mLeft0 = mLeftWheel.getCount();


	//目標角度がマイナスのときは逆周りにするため-1を乗算
	if( angle < 0){
		leftPWM  *= 0;//左を支点にターン
	}
	else{
		rightPWM=0;//右を支点にターン
	}
	mLeftWheel.setPWM(leftPWM);
	mRightWheel.setPWM(rightPWM);

	int32_t count,countR,countL;
	countR = mRightWheel.getCount() - mRight0;
	countL = mLeftWheel.getCount() - mLeft0;

#if 0
	if( angle > 0 ){
		count = (countL - countR)/2;
	} else{
		count = (countR - countL)/2;
	}

	dprintf("YY count=%ld mRight0:%ld mLeft0:%ld angle:%f\n", count, mRight0, mLeft0, angle);

	// by kidokoro 逆回転ができなかったので、修正
	double uns_angle;
	if(angle<0)uns_angle=-angle;
	else uns_angle=angle;
#else
	double uns_angle;
	if( angle > 0){
		count =countL;
		uns_angle = angle;
	}
	else{
		count = countR;
		uns_angle = -1*angle;

	}
#endif
	//if( (count * WHEEL_DIAMETER/WHEEL_WIDTH) > angle ){ //160mm:タイヤ間距離、80mm:タイヤ直径
	if( (count * (WHEEL_DIAMETER/2)/WHEEL_WIDTH) > uns_angle ){ //by kidokoro　逆回転ができなかったので修正
		dprintf("XX count=%ld mRight0:%ld mLeft0:%ld uns_angle:%f\n", count, mRight0, mLeft0, uns_angle);
		return false;// 目標角度達成
	}
	else //目標角度未達成。逆の法が良いかも Todo
		 return true;

}



void FreeWalker::stop(){
    /* float停止設定 */
	mLeftWheel.setBrake(false);
	mRightWheel.setBrake(false);

	mLeftWheel.setPWM(0);
	mRightWheel.setPWM(0);

	//モーターエンコーダをリセット
	mLeftWheel.reset();
	mRightWheel.reset();

	dprintf("FreeWalker stopped.\n");
}


void FreeWalker::init(){
	// モータエンコーダをリセットする
	mLeftWheel.reset();
	mRightWheel.reset();

	dprintf("FreeWalker inted.\n");
}


/**
 * PWM値を設定する
 * @param forward 前進値
 * @param turn    旋回値
 */
void FreeWalker::setCommand(int forward, int turn) {
    mForward = forward;
    mTurn    = turn;
}


int FreeWalker::getLeftCount(){
	return (int)mLeftWheel.getCount();
}


int FreeWalker::getRightCount(){
	return (int)mRightWheel.getCount();
}
