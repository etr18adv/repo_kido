/*
 * Vibration.cpp
 *
 *  Created on: 2017/07/08
 *      Author: takeshi
 */

#include "common.h"
#include "Vibration.h"

Vibration::Vibration(const ev3api::GyroSensor& gyroSensor)
: mGyroSensor(gyroSensor)
{
	anglerList = {};
	anglerSum = 0;
}

Vibration::~Vibration() {

}

void Vibration::doMeasure(){
	int16_t value = mGyroSensor.getAnglerVelocity();

	currentAngler = value; //現在値を保存

	//絶対値に変換
	if(value < 0){
		value *= -1;
	}

	if(anglerList.size() < VIBLIST_MAX){
		anglerList.push_back(value);
		anglerSum += value;
	}
	else{
		anglerSum -= anglerList.front();
		anglerList.pop_front();
		anglerList.push_back(value);
		anglerSum += value;
	}
}

double Vibration::getAnglerAve(int num){
	int sum = 0;

	for(int i = 0; i< num; i++){
		sum += mGyroSensor.getAnglerVelocity();
	}

	//dprintf("sum=%d\n", sum);

	return sum/num; //平均
}

int16_t Vibration::getAngler(){
	return anglerSum/anglerList.size(); //移動平均
}

int16_t Vibration::getCurrentAngler(){
	return currentAngler;
}
