/******************************************************************************
 *  BalancerCpp.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Implementation of the Class Balancer
 *  Author: Kazuhiro.Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "balancer.h"
#include "BalancerCpp.h"

/**
 * @brief コンストラクタ
 */
Balancer::Balancer()
    : mForward(0),
      mTurn(0),
      mOffset(0),
      mRightPwm(0),
      mLeftPwm(0) {
}

/**
 * @brief バランサを初期化する
 * @param[in] offset ジャイロセンサオフセット値
 */
void Balancer::init(int offset) {
    mOffset = offset;
    balance_init();  // 倒立振子制御初期化
}

/**
 * @brief バランサの値を更新する
 * @param[in] angle   角速度
 * @param[in] rwEnc   右車輪エンコーダ値
 * @param[in] lwEnc   左車輪エンコーダ値
 * @param[in] battery バッテリ電圧値
 * @param[in] A_R ローパスフィルタ係数(左右車輪の目標平均回転角度用)
 */
void Balancer::update(int angle, int rwEnc, int lwEnc, int battery, float A_R) {
    // 倒立振子制御APIを呼び出し、倒立走行するための左右モータ出力値を得る
    balance_control(
        static_cast<float>(mForward),
        static_cast<float>(mTurn),
        static_cast<float>(angle),
        static_cast<float>(mOffset),
        static_cast<float>(lwEnc),
        static_cast<float>(rwEnc),
        static_cast<float>(battery),
        &mLeftPwm,
        &mRightPwm,
		A_R);
}

/**
 * @brief バランサの値を更新する
 * @param[in] angle   角速度
 * @param[in] rwEnc   右車輪エンコーダ値
 * @param[in] lwEnc   左車輪エンコーダ値
 * @param[in] battery バッテリ電圧値
 * @param[in] A_R ローパスフィルタ係数(左右車輪の目標平均回転角度用)
 * @param[in] vector 向き(1 or -1)
 */
void Balancer::update_spin(int angle, int rwEnc, int lwEnc, int battery, float A_R, int vector) {
	int8_t tmpL,tmpR;

    // 倒立振子制御APIを呼び出し、倒立走行するための左右モータ出力値を得る
    balance_control(
        static_cast<float>(mForward),
        static_cast<float>(mTurn),
        static_cast<float>(angle),
        static_cast<float>(mOffset),
        static_cast<float>(lwEnc),
        static_cast<float>(rwEnc),
        static_cast<float>(battery),
        &tmpL,
        &tmpR,
		A_R);

    mLeftPwm  = -1*vector*(tmpL+tmpR)/2;
    mRightPwm = vector*(tmpL+tmpR)/2;
}

/**
 * @brief PWM値を設定する
 * @param[in] forward 前進値
 * @param[in] turn    旋回値
 */
void Balancer::setCommand(int forward, int turn) {
    mForward = forward;
    mTurn    = turn;

//    printf("forward: %d, turn: %d\n", forward, turn);
}

/**
 * @brief 右車輪のPWM値を取得する
 * @return 右車輪のPWM値
 */
int8_t Balancer::getPwmRight() {
    return mRightPwm;
}

/**
 * @brief 左車輪のPWM値を取得する
 * @return 左車輪のPWM値
 */
int8_t Balancer::getPwmLeft() {
    return mLeftPwm;
}
