/******************************************************************************
 *  PidController.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/06/12
 *  Implementation of the Class BalancingWalker
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "common.h"
#include "PidController.h"

/**
 * コンストラクタ
 */
PidController::PidController(){
	init();
}

/**
 * デストラクタ
 */
PidController::~PidController() {
}

void PidController::init(){
	//前回値を初期化
	diff1 = -1000.0; //ありえない値を初期値としておく
	integral = 0.0;

	//	mDeviation = 0;
	//	mIntegral = 0;
}

void PidController::setKPID(double p, double i, double d){
	Kp = p;
	Ki = i;
	Kd = d;
}


double PidController::calc_pid_value(double deviation){
	double diff = diff1;
	double pid;

	diff1 = deviation; //現在値

	if( diff == -1000.0 ){
		//前回値が初期値の場合、P制御を行う
		pid = Kp * mDeviation;
	}
	else {
		//前回値がある場合、PID制御を行う
		//integral += (diff1 + diff)/2.0; //東海地区サンプルと計算式が異なる

		pid = (Kp * diff1) +
			  (Ki * integral) +
			  (Kd * (diff1 - diff));
	}

//	dprintf("[PID],%f,%f,%f\n", diff, diff1, pid);

	return (pid > 100) ? 100:((pid < -100) ? -100 : pid);

}
