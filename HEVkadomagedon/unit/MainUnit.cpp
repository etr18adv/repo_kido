/** @file MainUnit.cpp
*	@brief EV3本体のLED、操作ボタン、液晶を制御するクラス
*   @author yamaguchi
*   @date 2017.06.16
*/

#include "common.h"
#include "MainUnit.h"


MainUnit::MainUnit() {
	mState = 0;

	// フォントの設定と0行目の表示
	ev3_lcd_set_font(EV3_FONT_MEDIUM);

	// バッテリ残量を表示
	battery();
}

MainUnit::~MainUnit() {
	ev3_led_set_color(LED_OFF);
}


void MainUnit::nextState(){
	switch( ++mState % 4 ){
	case 0:
		ev3_led_set_color(LED_OFF);
		break;
	case 1:
		ev3_led_set_color(LED_ORANGE);
		break;
	case 2:
		ev3_led_set_color(LED_GREEN);
		break;
	case 3:
		ev3_led_set_color(LED_RED);
		break;
	default:
		break;
	}

//	dprintf("mState: %d\n", mState);
//	display("next State");
}


void MainUnit::waitStop(){

	// 尻尾を止めておくためにバックボタンが押されるまで待つ
	display("Press BACK_BUTTON..");

    while (1) {
    	if ( ev3_button_is_pressed(BACK_BUTTON) ){
    		dprintf("BACK_BUTTON pressed.\n");
    		break;
    	}

    	dly_tsk(100);
    }
}


int MainUnit::waitButton(){
	static int button = -1;

    while (1) {
    	for(int i=LEFT_BUTTON; i<=DOWN_BUTTON; i++){
    		if ( ev3_button_is_pressed((button_t)i) ){
    			//dprintf("BUTTON pressed: %d.\n", i);

    			if( button != i ){
    				display(cCourseName[i]);
    				button = i;
    			}
    		}
    	}

    	if ( ev3_button_is_pressed(ENTER_BUTTON) ){
    		//dprintf("ENTER_BUTTON pressed.\n");
    		if(button >= LEFT_BUTTON && button <= DOWN_BUTTON)
    			break;
    	}

    	dly_tsk(50);
    }

    return button;
}


bool MainUnit::checkBackButton(){
	return ev3_button_is_pressed(BACK_BUTTON);
}


void MainUnit::display(const char *str){
	const int8_t line_height = 20;
	const int maxLine = 6; //最大6行のラウンドロビン

	static unsigned int count = 0;
	int32_t line = count % maxLine;

	char s[32];
	//sprintf(s, "%02d: %s", count, str);
	sprintf(s, "%s", str);

	//行単位で文字列をLCD表示
	ev3_lcd_fill_rect(0, line * line_height, EV3_LCD_WIDTH, line_height, EV3_LCD_WHITE);
	ev3_lcd_draw_string(s, 0, line * line_height);

	if(line==0){
//		sprintf(s, "%s", str);

		for(int j=1; j<maxLine; j++){
			ev3_lcd_fill_rect(0, j * line_height, EV3_LCD_WIDTH, line_height, EV3_LCD_WHITE);
			ev3_lcd_draw_string("", 0, j * line_height);
		}

	}

	count++;

	dprintf("%s\n", str);
}


void MainUnit::battery(){
	char s[32];
	sprintf(s, "%d mV, %d mA", ev3_battery_voltage_mV(), ev3_battery_current_mA());

	dprintf("battery: %s\n",s);

	display(s);
}
