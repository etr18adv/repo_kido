/** @file FreeWalker.h
*	@brief バランス制御なし走行を制御するクラス
*   @author yamaguchi
*   @date 2017.06.17
*/


#ifndef SDK_WORKSPACE_ETRONO_TR2_UNIT_FREEWALKER_H_
#define SDK_WORKSPACE_ETRONO_TR2_UNIT_FREEWALKER_H_

#include "Motor.h"

/*!
 * @class FreeWalker
 * @brief 3点走行を制御するクラス
 */
class FreeWalker {
public:

	FreeWalker(ev3api::Motor& leftWheel, ev3api::Motor& rightWheel);
	virtual ~FreeWalker();

    void init();
    void run();

    /*!
     * @brief スピン関連情報を初期化する
     */
    void spinInit();

    bool spin(double angle);	//その場で回転する
    bool turn(double angle);	//片方の車輪を止めて回る
    void stop();
    void setCommand(int forward, int turn);
//    int getDistance();

    int getLeftCount();
    int getRightCount();

private:
    ev3api::Motor& mLeftWheel;
    ev3api::Motor& mRightWheel;
    int mForward;
    int mTurn;

    int32_t mRight0;
    int32_t mLeft0;
};

#endif /* SDK_WORKSPACE_ETRONO_TR2_UNIT_FREEWALKER_H_ */
