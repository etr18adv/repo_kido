/** @file Sonar.cpp
*	@brief 前方ソナークラス
*   @author yamaguchi
*   @date 2017.06.18
*/

#include "common.h"
#include "Sonar.h"

using ev3api::SonarSensor;

Sonar::Sonar(ev3api::SonarSensor& sonarSensor)
 : mSonarSensor(sonarSensor)
{
	mDistance = {};
	mSum = 0;
}

Sonar::~Sonar() {
}

void Sonar::doMeasure(){
	double value = mSonarSensor.getDistance() * 100;

	if(mDistance.size() < SONARLIST_MAX){
		mDistance.push_back((int)value);
		mSum += (int)value;
	}
	else{
		mSum -= mDistance.front();
		mDistance.pop_front();
		mDistance.push_back((int)value);
		mSum += (int)value;
	}

//	dprintf("[SONAR],%d,%f\n", mDistance.back(), getDistance());
}

double Sonar::getDistance(){
	//return mDistance.back();
	return mSum/mDistance.size()/100;
}

