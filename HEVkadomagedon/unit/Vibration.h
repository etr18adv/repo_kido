/*
 * Vibration.h
 *
 *  Created on: 2017/07/08
 *      Author: takeshi
 */

#ifndef SDK_WORKSPACE_ETRONO_TR2_UNIT_VIBRATION_H_
#define SDK_WORKSPACE_ETRONO_TR2_UNIT_VIBRATION_H_

#include <list>

#include "GyroSensor.h"

/*!
 * @class Vibration
 * @brief ジャイロセンサを用いて揺れを計測するクラス
 */
class Vibration {
public:
	Vibration(const ev3api::GyroSensor& gyroSensor);
	virtual ~Vibration();

	void doMeasure();

	/*
	 * @brief 計測
	 * @param[in] num 計測回数
	 * @return 平均値
	 */
	double getAnglerAve(int num);

	/*
	 * @brief 振動の移動平均値を返す
	 */
	int16_t getAngler();

	/*
	 * @brief 振動の最新値を返す
	 */
	int16_t getCurrentAngler();

private:
    const ev3api::GyroSensor& mGyroSensor;

    //計測値
    std::list<int16_t> anglerList; 	//保持する値のリスト
    int anglerSum;				//保持した値の総和

    int16_t currentAngler; //現在値
};

#endif /* SDK_WORKSPACE_ETRONO_TR2_UNIT_VIBRATION_H_ */
