/*
 * ArmControl.cpp
 *
 *  Created on: 2018/05/31
 *      Author: Masahiro
 */

#include "ArmControl.h"
#include "common.h"

ArmControl::ArmControl(Motor& armMotor)
		:mArmMotor(armMotor),mIsInitialized(false){
	// TODO 自動生成されたコンストラクター・スタブ
	char s[32];
   Clock clock;
	sprintf(s,"ARM INIT  %d",mArmMotor.getCount());
	ev3_lcd_draw_string(s,0,20);

	//アームを押し付ける
	mArmMotor.setPWM(-10);
	//0.3秒待機
	clock.sleep(300);

	//尻尾停止
	mArmMotor.stop();

// モータエンコーダをリセットする
	mArmMotor.reset();
	sprintf(s,"ARM INIT DONE %d",mArmMotor.getCount());
	ev3_lcd_draw_string(s,0,40);

}

ArmControl::~ArmControl() {
	// TODO Auto-generated destructor stub
}
// 試作で使っていた。参考。
bool ArmControl::run(){
	char s[32];
	static int st=0;
	//armSet()の動作確認

	if(st==0){
		if(setArm(ARM_LINEMONITOR_NORMAL,10)){
			st++;
			return false;
		}
		else{
			return false;
		}
	}
#if 0
	else if(st==1){
		if( setArm(ARM_READY_NORMAL,10) ){
			st++;
			return false;
		}
		else{
			return false;
		}
	}
	else if(st==2){
		if( setArm(ARM_LINEMONITOR_NORMAL,10) ){
			st++;
			return false;
		}
		else{
			return false;
		}
	}
#endif
	else{
		return true;
	}


}
//setArm 指定角度(arc)まで回ったらtrueを返す
bool ArmControl::setArm(int32_t arc,int pwm){
		char s[32];
		int32_t count;	//現在角度

		//arcが0から100の範囲以外はエラー終了
		if(arc<0||arc>100){
			printf("ERROR:armSet() arc<0\n");
			return true;
		}
		//pwmの値でも範囲でエラー判定
		if(pwm<0){
			printf("ERROR:armSet() arc<0\n");
			return true;
		}

		//現在角度を確認する。
		count = mArmMotor.getCount();

		//現在角度＜指定角度の場合→アームを前へ
		if(count<arc){
			mArmMotor.setPWM(pwm);/*スピード pwmで動かす*/

		}
		//現在角度＞指定角度の場合→アームを後ろへ
		else{
			mArmMotor.setPWM(-pwm);/*スピード-pwmで動かす*/
		}

		sprintf(s,"c %d: a %d",count,arc);
		ev3_lcd_draw_string(s,0,60);
		dprintf("%s\n",s);
		//printf("c %d: a %d\n",count,arc);// BTに出力

		if(count==arc){/*指定角度まできた？*/ //todo 動作が早すぎるとぴったりにならないかも。
			mArmMotor.stop();
			sprintf(s,"STOP count=%d:%d",count,arc);
			ev3_lcd_draw_string(s,0,80);
			return true;
		}
		else{
			return false;
		}

}


