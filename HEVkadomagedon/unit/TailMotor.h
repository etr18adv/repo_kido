/** @file TailMotor.h
*	@brief 後方モータクラス
*   @author yamaguchi
*   @date 2017.06.10
*/

#ifndef SDK_WORKSPACE_ETRONO_TR_APP_TAILMOTOR_H_
#define SDK_WORKSPACE_ETRONO_TR_APP_TAILMOTOR_H_

#include "Motor.h"
#include "PidController.h"

//extern int targetAngle; /* 後方モータの目標角度[度] */

/*!
 * @class TailMotor
 * @brief 後方モータを制御するクラス
 */
class TailMotor {
public:

	/*!
	 * 目標角度の初期化と、後方モータのオブジェクト生成するコンストラクタ
	 * @param[in] port ポート
	 */
	TailMotor(ePortM port);
	virtual ~TailMotor();

	/*!
	 * モータ目標角度の設定
	 * @param[in] angle 目標角度
	 */
	void setAngle(signed int angle);
	void setStandby();
	void setStart();
	void setStop();
	void setBack();
	void setWalk();
    void setGate();

	/*!
	 * モータ目標角度の取得
	 * @return 目標角度
	 */
	signed int getAngle();

	/*!
	 * 現在の角度が目標角度と一致しているか確認する
	 * @return 一致(true) / 不一致(false)
	 */
	bool checkStatus();

	/*!
	 * モーター制御を行う
	 */
	void run();

	/*!
	 * 目標角度を増加させる
	 */
	void standUp();

	/*!
	 * 目標角度を減少させる
	 */
	void standDown();

private:
	    const int TAIL_ANGLE_STAND    = 0; /* 完全停止時の角度[度] */
	    const int TAIL_ANGLE_WALK     =  0; /* バランス走行時の角度[度] */
	    const int TAIL_ANGLE_STOP     = 0; /* 静止時の角度[度] */
	    const int TAIL_ANGLE_START    = 0; /* スタートダッシュ時の角度 */
	//    static constexpr int TAIL_ANGLE_GATE     = 65; /* 3点走行時のモーター角度 */
	    const int TAIL_ANGLE_GATE     = 0; /* 3点走行時のモーター角度 */
	    const int TAIL_ANGLE_BACK     = 0; /* 3点バック走行時 */

#if 0
	/* getAngleメソッド呼び出し後の目標角度判定に使用するためpublicへ移動する   */
    const int TAIL_ANGLE_STAND    = 89; /* 完全停止時の角度[度] */
    const int TAIL_ANGLE_WALK     =  3; /* バランス走行時の角度[度] */
    const int TAIL_ANGLE_STOP     = 80; /* 静止時の角度[度] */
    const int TAIL_ANGLE_START    = 90; /* スタートダッシュ時の角度 */
//    static constexpr int TAIL_ANGLE_GATE     = 65; /* 3点走行時のモーター角度 */
    const int TAIL_ANGLE_GATE     = 60; /* 3点走行時のモーター角度 */
    const int TAIL_ANGLE_BACK     = 80; /* 3点バック走行時 */
#endif
	signed int targetAngle; /* 後方モータの目標角度[度] */

	ev3api::Motor *mTailMotor; /* 後方モータ */
	PidController*   mPidController;


	const double P_GAIN		= 1.0;  /* モータ制御比例係数 */
	const int PWM_ABS_MAX	= 10;  /* モータ制御PWM絶対最大値 */

};

#endif /* SDK_WORKSPACE_ETRONO_TR_APP_TAILMOTOR_H_ */
