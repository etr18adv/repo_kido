/******************************************************************************
 *  LineMonitor.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Definition of the Class LineMonitor
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "common.h"
#include "LineMonitor.h"

// 定数宣言
//const int8_t LineMonitor::INITIAL_THRESHOLD = 20;  // 黒色の光センサ値

/**
 * コンストラクタ
 * @param colorSensor カラーセンサ
 */
LineMonitor::LineMonitor(const ev3api::ColorSensor& colorSensor, int8_t black, int8_t white)
    : mColorSensor(colorSensor){
	mBlack = black;
	mWhite = white;
	mTarget = -1;
	init();
}

void LineMonitor::init(){
	pre = -1;
	colorList = {};
	colorSum = 0;
	crossStatus = 0;
	dprintf("LineMonitor inited.\n");
}


bool LineMonitor::isOnLine() {
    // 光センサからの取得値を見て
    // 黒以上であれば「true」を、
    // そうでなければ「false」を返す
	int color = normalization(mColorSensor.getBrightness());

    if (color < getTarget()) {
        return true;
    } else {
        return false;
    }
}

bool LineMonitor::isOnLine(int value) {
    // 光センサからの取得値を見て
    // value未満であれば「true」を
	// そうでなければ「false」を返す

	int color = normalization(mColorSensor.getBrightness());
	dprintf("normalize= %d,Brightness=%d\n",color,mColorSensor.getBrightness());// by kidokoro
    if (color < value) {
        return true;
    } else {
        return false;
    }
}

bool LineMonitor::crossLine(int value){
	int color = normalization(mColorSensor.getBrightness());

	//static unsigned int status = 0;

	switch(crossStatus){
	case 0:
		//まずはvalueを探す
		if( color < value ){
			crossStatus++;
		}

		break;
	case 1:
		//前回値よりも、現在値が大きくなったら中央と判定する
		if( color > pre ){
			crossStatus = 0;
			return true;
		}
		break;
	default:
		break;
	}

	pre = color;

	return false;
}

void LineMonitor::setBlack(int value) {
    mBlack = value;
    dprintf("mBlack=%d\n", mBlack);
}

void LineMonitor::setWhite(int value) {
    mWhite = value;
    dprintf("mWhite=%d\n", mWhite);
}

void LineMonitor::setTarget(double target){
	if( target != mTarget ){
		dprintf("mTarget: %f->%f, mWhite: %d, mBlack: %d\n", mTarget, target, mWhite, mBlack);
	}

	mTarget = target;
}

void LineMonitor::setTarget2(int target){
	mTarget = normalization(target);

	dprintf("XX mTarget: %f\n", mTarget);
}

int LineMonitor::getTarget(){
//	int value = (mWhite - mBlack) * 2 / 3;
//	return (value - mBlack) *100/ (mWhite - mBlack);
	return mTarget;
}

int LineMonitor::getBrightness(int num){
	int value = 0;

	for(int i=0; i<num; i++)
		value += mColorSensor.getBrightness();

	return value/num; //平均値
}

int LineMonitor::getNormalBrightness(int num){
	int value = 0;

	for(int i=0; i<num; i++)
		value += mColorSensor.getBrightness();

	return normalization(value/num); //平均値
}

double LineMonitor::getDiff(){
	const double LF = 0.9; //加重平均で用いる定数

	double now = pre;
	double value;

	pre = normalization(mColorSensor.getBrightness()); //正規化

	if( now == -1.0 ){
		value = pre;
	}
	else {
		//ローパスフィルタ
		value = pre * LF + now * (1.0-LF); //本部性能審査団の資料Young Master参考 p.6
	}

	return value - getTarget();
}




double LineMonitor::getMovingDiff(){
	int diff = normalization(mColorSensor.getBrightness()) - getTarget();

	if( COLORLIST_MAX < 2 ){
		return getDiff();
	}

	if(colorList.size() < COLORLIST_MAX){
		colorList.push_back(diff);
		colorSum += diff;
	}
	else{
		colorSum -= colorList.front();
		colorList.pop_front();
		colorList.push_back(diff);
		colorSum += diff;
	}

//	dprintf("XX diff:,%d, sum:,%d, num:,%d, \n", diff,colorSum,colorList.size());

	return (double)colorSum/colorList.size(); //移動平均
}


double LineMonitor::normalization(int value){
	return (value - mBlack) * 100 / (mWhite - mBlack);
}

colorid_t LineMonitor::getColorNumber(){
	return mColorSensor.getColorNumber();
}

void LineMonitor::getRGB(rgb_raw_t *rgb){
	mColorSensor.getRawColor(*rgb);
}
/*
 * @brief カラーのHSV値を返す。
 * by kidokoro for HackEV
 * */
void LineMonitor::getHSV(int *hsv_h,int *hsv_s, int *hsv_v){
	rgb_raw_t rgb;
//	char ss[32];

	//RGB値を取得する
	getRGB(&rgb);
//	sprintf(ss,"RGB %d,%d,%d            \n",rgb.r, rgb.g, rgb.b);
//	ev3_lcd_draw_string(ss,0,100);
	//dprintf("%s",ss);

	//HSVを算出する。
	//H 0-359, S 0-255,V 0-255とする。
	int r, g, b,max, min, h, s, v;
	r = rgb.r;
	g = rgb.g;
	b = rgb.b;
	max = r > g ? r : g;
	max = max > b ? max : b;
	min = r < g ? r : g;
	min = min < b ? min : b;
	v = max;
	s =(int)( 255*(max-min)/max);
	//Hの算出
	//Rがmax
	if(max == r){
		h= (int)(60*(g-b)/(max-min));
	}
	else if(max ==g){//Gがmax
		h= (int)(60*(b-r)/(max-min)) +120;
	}
	else if(max == b){//Bがmax
		h= (int)(60*(r-g)/(max-min)) +240;
	}
	else if(max == min){//max=min
		h=0;
	}
	else{
		dprintf("HSV error!!\n");
	}
	//0-360に納める
	while(1){
		if(h>=0){
			break;
		}
		else{
			h+=360;
		}
	}

//	sprintf(ss,"HSV %d,%d,%d            \n",h, s, v);
//	ev3_lcd_draw_string(ss,0,100);

	*hsv_h =h;
	*hsv_s =s;
	*hsv_v =v;

	return ;
}
/*
     * @brief HSV値を見て色を判断する。
     * @retval int  色番号(colorid_t だとグレーがないので独自設定。common.h)
     * NONE 0
     * BLACK 1
     * BLUE 2
     * GREEN 3
     * YELLOW 4
     * RED 5
     * WHITE 6
     * GRAY 7
     * by kidokoro for HackEV
     * */
int LineMonitor::getColorHSV(){
	int hsv_h,hsv_v,hsv_s;
	int color;
	//3回一致
	static int color1= HSV_NONE;
	static int color2 = HSV_NONE;

	this->getHSV(&hsv_h,&hsv_s,&hsv_v);

	char s[32];
//	sprintf(s,"HSV %d,%d,%d            \n",hsv_h, hsv_s, hsv_v);
//	ev3_lcd_draw_string(s,0,80);

	if(hsv_v < 20){//黒判断
		sprintf(s,"HSV Black !!!!!!!!!!\n");
		color = HSV_BLACK;
	}
	else if(85<hsv_s && hsv_s < 110){// グレー判断
		sprintf(s,"HSV Gray !!!!!!!!!!\n");
		color = HSV_GRAY;
	}
	else if (hsv_v >100){//白判断
		sprintf(s,"HSV White !!!!!!!!!!\n");
		color = HSV_WHITE;
	}
	else if(hsv_s >160){//Sが160以上じゃないと白黒以外の色判定しない
		if(hsv_h>340||hsv_h<20){
			sprintf(s,"HSV RED !!!!!!!!!!\n");
			color = HSV_RED;
		}
		else if(hsv_h>30&&hsv_h<70){
			sprintf(s,"HSV YELLOW !!!!!!!!!!\n");
			color = HSV_YELLOW;
		}
		else if(hsv_h>100&&hsv_h<140){
			sprintf(s,"HSV GREEN !!!!!!!!!!\n");
			color = HSV_GREEN;
		}
		else if(hsv_h>190&&hsv_h<230){
			sprintf(s,"HSV BLUE !!!!!!!!!!\n");
			color = HSV_BLUE;
		}
		else{
			sprintf(s,"HSV NONE !!!!!!!!!!\n");
			color = HSV_NONE;
		}
	}
	else{
		sprintf(s,"HSV NONE !!!!!!!!!!\n");
		color = HSV_NONE;
	}

	//ノイズ除去。2回一致しないとその値としない。
	//ちょっと黄色判定がむずかしくなった？
	if ((color == color1)){
		ev3_lcd_draw_string(s,0,60);
		return color;
	}
	else{
	//	color2 = color1;
		color1= color;
		return HSV_NONE;
	}



}

bool LineMonitor::checkColor(colorid_t color){
	bool ret = false;
	rgb_raw_t rgb;

	getRGB(&rgb);

	switch(color){
	case COLOR_GREEN:
		if( rgb.r < 15 && rgb.g > 20 && rgb.b > 20){
			ret = true;
		}
		break;
	case COLOR_BLACK:
		if( rgb.r < 15 && rgb.g < 15 && rgb.b < 15){
			ret = true;
		}
		break;
	default:
		dprintf("[ERR!] color is invalid.\n");
		break;
	}

	return ret;
}
