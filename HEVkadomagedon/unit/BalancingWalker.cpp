/******************************************************************************
 *  BalancingWalker.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Implementation of the Class BalancingWalker
 *  Author: Kazuhiro.Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest

  20170528 by 城所
    calcDistance()を追加

 *****************************************************************************/

#include "common.h"
#include "BalancingWalker.h"
#include "Vibration.h"

BalancingWalker::BalancingWalker(Vibration* vibration,
                                 ev3api::Motor& leftWheel,
                                 ev3api::Motor& rightWheel,
                                 Balancer* balancer)
    : mVibration(vibration),
      mLeftWheel(leftWheel),
      mRightWheel(rightWheel),
      mBalancer(balancer),
      mForward(0),
      mTurn(0),
	  mCount(0),
	  batterySum(0){

	init();
	spinInit();
}


bool BalancingWalker::run() {
    int16_t angle = mVibration->getCurrentAngler();  // ジャイロセンサ値
    int32_t rightWheelEnc = mRightWheel.getCount();       // 右モータ回転角度
    int32_t leftWheelEnc  = mLeftWheel.getCount();        // 左モータ回転角度

    static bool state = true;

    if(state == false)
    	return false;

    mBalancer->setCommand(mForward, mTurn);

    int battery = getBattery();
//    dprintf("[BATTERY],%d,\n", battery);
    mBalancer->update(angle, rightWheelEnc, leftWheelEnc, battery, A_R);

#ifndef VIB_SAFETYSTOP
    //転倒したときに安全停止
    if( (mBalancer->getPwmLeft() >=  100 && mBalancer->getPwmRight() >=  100) ||
    	(mBalancer->getPwmLeft() <= -100 && mBalancer->getPwmRight() <= -100) ){
    	if( mCount++ > 1100 ){ //30回だと7/22ライントレース走行時に超えた
    		mLeftWheel.setPWM(0);
    		mRightWheel.setPWM(0);
    		mCount = 0;
    		dprintf("safety stop L=%d R=%d\n", mBalancer->getPwmLeft(), mBalancer->getPwmRight());
    		wup_tsk(MAIN_TASK); //メインタスク起動させ、周期ハンドラ停止
    		state = false;
    		return false;
    	}
    } else {
    	mCount = 0;
    }
#else
	//TODO: 走行中に誤って安全停止にならないように要動作確認！
    const int SAFETYSTOP_COUNT = 1;

    if( mVibration->getAngler() > VIB_SAFETYSTOP && mCount++ > SAFETYSTOP_COUNT ){
    	mLeftWheel.setPWM(0);
    	mRightWheel.setPWM(0);
    	mCount = 0;
    	dprintf("safety stop L=%d R=%d, angle=%d\n", mBalancer->getPwmLeft(), mBalancer->getPwmRight(), angle);
    	wup_tsk(MAIN_TASK); //メインタスク起動させ、周期ハンドラ停止
    	state = false;
    	return false;
    }
    else {
    	mCount = 0;
    }
#endif

    mLeftWheel.setPWM( mBalancer->getPwmLeft());
    mRightWheel.setPWM(mBalancer->getPwmRight());

    return true;
}

#ifdef BATTERY_AVERAGE
void BalancingWalker::doMeasureBattery(){
	const int num = BATTERY_AVERAGE;

	mBattery = ev3_battery_voltage_mV();

	if( num == 1 ){
		return;
	}

	if(batteryList.size() < num){
		batteryList.push_back(mBattery);
		batterySum += mBattery;
	}
	else{
		batterySum -= batteryList.front();
		batteryList.pop_front();
		batteryList.push_back(mBattery);
		batterySum += mBattery;
	}
}
#endif

int BalancingWalker::getBattery(){
#ifndef BATTERY_AVERAGE
	return ev3_battery_voltage_mV();
#else
	const int num = BATTERY_AVERAGE;

	if( num == 1 ){
		return mBattery;
	}

//	dprintf("%d,%d,%d\n",mBattery,batteryList.size(),batterySum);

	return batterySum / batteryList.size();
#endif
}

void BalancingWalker::spinInit() {
	mNum    = 0;
	mRight0 = -1;
	mLeft0  = -1;
}

bool BalancingWalker::spin(double angle, int num){
	if( mRight0 == -1 )
		mRight0 = mRightWheel.getCount();
	if( mLeft0 == -1 )
		mLeft0 = mLeftWheel.getCount();

	if( mNum < num ){
		mNum++;
	} else if ( mNum < num*2-1){
		mForward *= -1;
		mNum++;
	} else {
		mForward *= -1;
		mNum = 0;
	}
	//run();

	int32_t count,countR,countL;
	countR = mRightWheel.getCount() - mRight0;
	countL = mLeftWheel.getCount() - mLeft0;
	if( mForward > 0 ){
		count = (countR - countL)/2;
	} else{
		count = (countL - countR)/2;
	}
	//dprintf("Forward=%d, Angle=%f count=%d\n", mForward, angle, count);


	//走行する
	run();

	if( (count * WHEEL_DIAMETER/WHEEL_WIDTH) > angle ){ //160mm:タイヤ間距離、80mm:タイヤ直径
		dprintf("XX count=%ld mRight0:%ld mLeft0:%ld angle:%f\n", count, mRight0, mLeft0, angle);

		return false;
	}
	else
		return true;
}

#if 0
bool BalancingWalker::spin(double targetAngle){
	/* API仕様書 p.15より
	ユーザー独自の旋回制御をおこなうには、倒立振子制御の実行関数(balance_control)の第 2 引数を
	0 に設定し(pwm_turn = 0)、戻り値の左右 PWM 出力に対して、独自の旋回制御に基づいた±方向の
	PWM 出力オフセットをそれぞれ与えることで実現できます。ここで重要なのは、旋回制御用 PWM
	出力オフセットの絶対値は等しくするということです。これは倒立振子の前後方向の制御に必要な
	力と旋回方向の制御に必要な力のベクトルを直交させることで、2 つの制御間の干渉を防ぐためです。
	*/

    int16_t angle = mGyroSensor.getCurrentAngler();  // ジャイロセンサ値
    int32_t rightWheelEnc = mRightWheel.getCount();   // 右モータ回転角度
    int32_t leftWheelEnc  = mLeftWheel.getCount();    // 左モータ回転角度

	int32_t count,countR,countL;
	countR = mRightWheel.getCount() - mRight0;
	countL = mLeftWheel.getCount() - mLeft0;
	if( mForward > 0 ){
		count = (countR - countL)/2;
	} else{
		count = (countL - countR)/2;
	}

    mBalancer->setCommand(mForward, 0);

    int battery = getBattery();

    mBalancer->update_spin(angle, rightWheelEnc, leftWheelEnc, battery, A_R, 1);

    mLeftWheel.setPWM( mBalancer->getPwmLeft()  * PWM_BIAS);
    mRightWheel.setPWM(mBalancer->getPwmRight() * PWM_BIAS);

    if( (count * WHEEL_DIAMETER/WHEEL_WIDTH) > angle ){ //160mm:タイヤ間距離、80mm:タイヤ直径
    	//dprintf("XX count=%ld mRight0:%ld mLeft0:%ld angle:%f\n", count, mRight0, mLeft0, angle);

    	return false;
    }
    else
    	return true;
}
#endif

bool BalancingWalker::init() {
	static double pre = -10000;
	static int num=1;
	const int max=2; //一定回数同じ値が続いたら、その値を採用する

	int offset = mVibration->getAnglerAve(10);

	//前回計測した平均値と一致するまで計測する
	//20回連続同じ値を取得しようとしたが、まれにノイズ？が入り困難
	//ノイズの入る周期があるならば、平均値の一致で問題ないはず
	if(pre != offset){
		printf("X");
		pre = offset;
		num = 1;
		return false;
	}else{
		printf("0");

		if(num++ < max){
			return false;
		}
	}
	printf(": offset\n");

    // モータエンコーダをリセットする
    mLeftWheel.reset();
    mRightWheel.reset();

    /* float停止設定 */
//   	mLeftWheel.setBrake(false);
//   	mRightWheel.setBrake(false);

    // 倒立振子制御初期化
    mBalancer->init(offset);

    //ローパスフィルタ係数を初期化
    A_R = DEFAULT_A_R;

    dprintf("mBalancer->init called.\n");
    return true;
}


void BalancingWalker::setCommand(int forward, int turn) {
    mForward = forward;
    mTurn    = turn;
}

void BalancingWalker::setA_R(float value){
	if( A_R != value ){
		dprintf("A_R value changed. (%f -> %f)\n", A_R, value);
	}

	A_R = value;
}

float BalancingWalker::getA_R(){
	return A_R;
}


