/** @file TailMotor.cpp
*	@brief 後方モータクラス
*   @author yamaguchi
*   @date 2017.06.10
*/

#include "common.h"
#include "Port.h"
#include "TailMotor.h"

using ev3api::Motor;


TailMotor::TailMotor(ePortM port)
{
	targetAngle = 0;
	mTailMotor  = new Motor(port);
	mPidController = new PidController();
}

TailMotor::~TailMotor() {
	delete mPidController;
	delete mTailMotor;
}

void TailMotor::setAngle(signed int angle){
	if( targetAngle != angle )
		dprintf("changed targetAngle: %d -> %d\n", targetAngle, angle);

	targetAngle = angle;
}


void TailMotor::setStandby(){
	setAngle(TAIL_ANGLE_STAND);
}
void TailMotor::setStart(){
	setAngle(TAIL_ANGLE_START);
}
void TailMotor::setStop(){
	setAngle(TAIL_ANGLE_STOP);
}
void TailMotor::setBack(){
	setAngle(TAIL_ANGLE_BACK);
}
void TailMotor::setWalk(){
	setAngle(TAIL_ANGLE_WALK);
}
void TailMotor::setGate(){
	setAngle(TAIL_ANGLE_GATE);
}



signed int TailMotor::getAngle(){
	return targetAngle;
}

bool TailMotor::checkStatus(){
	if( mTailMotor->getCount() == targetAngle ){
		return true;
	} else {
		return false;
	}
}


void TailMotor::run(){
	//double pwm = (targetAngle - mTailMotor->getCount()) * P_GAIN; //比例制御

	mPidController->setKPID(P_GAIN, 0, 0); //比例制御
	double pwm = mPidController->calc_pid_value(targetAngle - mTailMotor->getCount());

	//    dprintf("angle = %d pwm = %f\n", gTailMotor->getAngle(), pwm);

   /* PWM出力飽和処理 */
   if (pwm > PWM_ABS_MAX)
   {
       pwm = PWM_ABS_MAX;
   }
   else if (pwm < -PWM_ABS_MAX)
   {
       pwm = -PWM_ABS_MAX;
   }

//    dprintf("pwm = %f\n", pwm);

   mTailMotor->setPWM(pwm);


}

void TailMotor::standUp(){
	targetAngle++;
}

void TailMotor::standDown(){
	targetAngle--;
}
