/******************************************************************************
 *  PidController.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/06/12
 *  Implementation of the Class BalancingWalker
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#ifndef EV3_UNIT_PIDCONTROLLER_H_
#define EV3_UNIT_PIDCONTROLLER_H_

#include "ev3api.h"

/*!
 * @class PidController
 * @brief PID制御用の計算を行うクラス
 */
class PidController
{
public:
    PidController();
    virtual ~PidController();

    void init();

    /*!
     * @brief PIDパラメータを設定する
     * @param[in] p Kp値
     * @param[in] i Ki値
     * @param[in] d Kd値
     *
     */
    void setKPID(double p, double i, double d);

    /*!
     * @brief PID制御のための計算を行う
     * @param[in] diff 差分
     * @return PID制御値
     */
    double calc_pid_value(double diff);

private:
    int32_t mDeviation;
    int32_t mIntegral;

    double diff1;
    double integral;

    double Kp,Ki,Kd; //PID調整パラメータ


};

#endif  // EV3_UNIT_PIDCONTROLLER_H_
