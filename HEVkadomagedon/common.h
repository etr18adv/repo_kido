/** @file common.h
*	@brief 共用ヘッダファイル
*   @author yamaguchi
*   @date 2017.06.09
*/

#include "ev3api.h"

/*! 使用するロボットを選択  */
#define ROBOTNO (0)    /* 0:城所機体、2:幾野機体、6:山口機体  */
/*! 本番はコメントアウトすること    */
#define __DEBUG__ //デバッグフラグ

/*! @brief 関数名、行数を行頭につけてログ出力するマクロ */
#ifdef __DEBUG__
#define dprintf(fmt, ...)	printf("[%s:%d] " fmt, __func__,__LINE__,##__VA_ARGS__)
#else
#define dprintf(fmt, ...)	;
#endif

//#define VIB_SAFETYSTOP 50 //本体転倒を検知する振動値の閾値

#if (ROBOTNO == 0)
/* 走行体によって個体差のある定数宣言 */
//2点走行時における反射光のデフォルト値
const int DEFAULT_BLACK = 5;
const int DEFAULT_WHITE = 50;
//3点走行時における反射光のデフォルト値
const int DEFAULT_BLACK3 = 0;
const int DEFAULT_WHITE3 = 28;
//2点走行）通常ライントレース用
#if 1 // by kidokoro forHackEV Kpを倍、Kdを半分にしてみる
constexpr double PARAM_LINETRACE_KP	=	0.24; //Ku * 0.6
constexpr double PARAM_LINETRACE_KI	=	0.0;
constexpr double PARAM_LINETRACE_KD	=	(PARAM_LINETRACE_KP * 250 * 0.125 * 0.8996624)/2; //KP * 4ms周期 * 0.125 * 周期[sec]
//3点走行）通常ライントレース用 (SPEED > 40のとき)
constexpr double PARAM_LINETRACE_KP_MAX	=	0.24*4; //Ku * 0.6
constexpr double PARAM_LINETRACE_KI_MAX	=	0.0;
constexpr double PARAM_LINETRACE_KD_MAX	=	PARAM_LINETRACE_KP * 250 * 0.125 * 0.8996624 *10; //KP * 4ms周期 * 0.125 * 周期[sec]
#else
constexpr double PARAM_LINETRACE_KP	=	0.12; //Ku * 0.6
constexpr double PARAM_LINETRACE_KI	=	0.0;
constexpr double PARAM_LINETRACE_KD	=	PARAM_LINETRACE_KP * 250 * 0.125 * 0.8996624; //KP * 4ms周期 * 0.125 * 周期[sec]
//2点走行）通常ライントレース用 (SPEED > 100のとき)
constexpr double PARAM_LINETRACE_KP_MAX	=	0.12; //Ku * 0.6
constexpr double PARAM_LINETRACE_KI_MAX	=	0.0;
constexpr double PARAM_LINETRACE_KD_MAX	=	PARAM_LINETRACE_KP * 250 * 0.125 * 1.09; //KP * 4ms周期 * 0.125 * 周期[sec]
#endif
//2点走行）仮想直線ライントレース用
constexpr double PARAM_VIRTUALLINE_KP = 0.066; //Ku = 0.11 * 0.6
constexpr double PARAM_VIRTUALLINE_KI = 0.0;
constexpr double PARAM_VIRTUALLINE_KD = PARAM_VIRTUALLINE_KP * 250 * 0.125 * 5.52;
//2点走行）仮想円ライントレース用
constexpr double PARAM_VIRTUALCIRCLE_KP = PARAM_VIRTUALLINE_KP;
constexpr double PARAM_VIRTUALCIRCLE_KI = PARAM_VIRTUALLINE_KI;
constexpr double PARAM_VIRTUALCIRCLE_KD = PARAM_VIRTUALLINE_KD;
//3点走行）仮想直線ライントレース用
constexpr double PARAM_VIRTUALLTAIL_KP = 0.1;
constexpr double PARAM_VIRTUALLTAIL_KI = 0.0;
constexpr double PARAM_VIRTUALLTAIL_KD = 0.0; //周期 [sec]
#elif (ROBOTNO == 2)
/* 走行体によって個体差のある定数宣言 */
//2点走行時における反射光のデフォルト値
const int DEFAULT_BLACK = 5;
const int DEFAULT_WHITE = 40;
//3点走行時における反射光のデフォルト値
const int DEFAULT_BLACK3 = 0;
const int DEFAULT_WHITE3 = 28;
//2点走行）通常ライントレース用
constexpr double PARAM_LINETRACE_KP	=	0.24; //Ku * 0.6
constexpr double PARAM_LINETRACE_KI	=	0.0;
constexpr double PARAM_LINETRACE_KD	=	PARAM_LINETRACE_KP * 250 * 0.125 * 0.8996624; //KP * 4ms周期 * 0.125 * 周期[sec]
//2点走行）通常ライントレース用 (SPEED > 100のとき)
constexpr double PARAM_LINETRACE_KP_MAX	=	0.12; //Ku * 0.6
constexpr double PARAM_LINETRACE_KI_MAX	=	0.0;
constexpr double PARAM_LINETRACE_KD_MAX	=	PARAM_LINETRACE_KP * 250 * 0.125 * 1.09; //KP * 4ms周期 * 0.125 * 周期[sec]
//2点走行）仮想直線ライントレース用
constexpr double PARAM_VIRTUALLINE_KP = 0.066; //Ku = 0.11 * 0.6
constexpr double PARAM_VIRTUALLINE_KI = 0.0;
constexpr double PARAM_VIRTUALLINE_KD = PARAM_VIRTUALLINE_KP * 250 * 0.125 * 5.52;
//2点走行）仮想円ライントレース用
constexpr double PARAM_VIRTUALCIRCLE_KP = PARAM_VIRTUALLINE_KP;
constexpr double PARAM_VIRTUALCIRCLE_KI = PARAM_VIRTUALLINE_KI;
constexpr double PARAM_VIRTUALCIRCLE_KD = PARAM_VIRTUALLINE_KD;
//3点走行）仮想直線ライントレース用
constexpr double PARAM_VIRTUALLTAIL_KP = 0.1;
constexpr double PARAM_VIRTUALLTAIL_KI = 0.0;
constexpr double PARAM_VIRTUALLTAIL_KD = 0.0; //周期 [sec]
#elif (ROBOTNO == 6)
/* 走行体によって個体差のある定数宣言 */
//2点走行時における反射光のデフォルト値
const int DEFAULT_BLACK = 5;
const int DEFAULT_WHITE = 55;
//3点走行時における反射光のデフォルト値
const int DEFAULT_BLACK3 = 0;
const int DEFAULT_WHITE3 = 28;
//2点走行）通常ライントレース用
constexpr double PARAM_LINETRACE_KP	=	0.24; //Ku * 0.6
constexpr double PARAM_LINETRACE_KI	=	0.0;
constexpr double PARAM_LINETRACE_KD	=	PARAM_LINETRACE_KP * 250 * 0.125 * 0.75; //KP * 4ms周期 * 0.125 * 周期[sec]
//2点走行）通常ライントレース用 (SPEED > 100のとき)
constexpr double PARAM_LINETRACE_KP_MAX	=	0.09; //Ku * 0.6
constexpr double PARAM_LINETRACE_KI_MAX	=	0.0;
constexpr double PARAM_LINETRACE_KD_MAX	=	PARAM_LINETRACE_KP * 250 * 0.125 * 1.26; //KP * 4ms周期 * 0.125 * 周期[sec]
//2点走行）仮想直線ライントレース用
constexpr double PARAM_VIRTUALLINE_KP = 0.066; //Ku = 0.11 * 0.6
constexpr double PARAM_VIRTUALLINE_KI = 0.0;
constexpr double PARAM_VIRTUALLINE_KD = PARAM_VIRTUALLINE_KP * 250 * 0.125 * 5.52;
//2点走行）仮想円ライントレース用
constexpr double PARAM_VIRTUALCIRCLE_KP = PARAM_VIRTUALLINE_KP;
constexpr double PARAM_VIRTUALCIRCLE_KI = PARAM_VIRTUALLINE_KI;
constexpr double PARAM_VIRTUALCIRCLE_KD = PARAM_VIRTUALLINE_KD;
//3点走行）仮想直線ライントレース用
constexpr double PARAM_VIRTUALLTAIL_KP = 0.1;
constexpr double PARAM_VIRTUALLTAIL_KI = 0.0;
constexpr double PARAM_VIRTUALLTAIL_KD = 0.0; //周期 [sec]
#else
#endif


/* 定数宣言 */
const int SPEED_ZERO		=   0;
const int SPEED_SLOW		=   5;
const int SPEED_LOW1		=  10;
const int SPEED_LOW1_5		=  15;
const int SPEED_LOW2		=  20;
const int SPEED_LOW3		=  30;
const int SPEED_NORMAL1		=  40;
const int SPEED_NORMAL2		=  50;
const int SPEED_NORMAL3		=  60;
const int SPEED_HIGH1		=  70;
const int SPEED_HIGH2		=  80;
const int SPEED_HIGH3		=  90;
const int SPEED_HIGH4		=  99;
const int SPEED_MAX		    = 130;
const int SPEED_OVER		= 135; //150はスタート時に転倒

const int TURNMAX_SPEEDMAX = 30; //最高速度のときの旋回最大値。転倒防止
//const int TURNMAX_SPEEDMAX = 50; //最高速度のときの旋回最大値。転倒防止

const int COLORLIST_MAX  = 5;	//カラーセンサ値の移動平均用データ数上限
const int POSERRLIST_MAX = 5;  //位置ずれの移動平均用データ数上限
const int SONARLIST_MAX  = 3;  //超音波センサーで計測した距離用データ数上限
const int VIBLIST_MAX    = 5;  //振動の絶対値用データ数上限

const float DEFAULT_A_R = 0.996F;

//HSVによる色判定値　by kidokoro for HackEV
const int HSV_NONE = 0;
const int HSV_BLACK = 1;
const int HSV_BLUE = 2;
const int HSV_GREEN = 3;
const int HSV_YELLOW =4;
const int HSV_RED = 5;
const int HSV_WHITE = 6;
const int HSV_GRAY = 7;

/* 本体設定 */
const int WHEEL_DIAMETER =  100; /* タイヤ直径 100mm by Kidokoro for HackEV*/
const int WHEEL_WIDTH    = 140; /* タイヤ車輪幅 140mm by Kidokoro for HackEV*/

#define AJUSTMENT  //PIDパラメータ調整などの調整時。本番はコメントアウトすること

/* コース名 : 上から順に選択ボタンの左右上下に対応 */
constexpr char cCourseName[4][32] = {
#ifndef AJUSTMENT //本番用
	"L course (aggressive)",
	"R course (aggressive)",
	"L course (safety)",
	"R course (safety)",
	//"basicL",
	//"basicR"
#else //調整用
	"dataDriven",
    "dataBlock",
	"checkHSV",
	"testHackEV"
// "testGate"
//	"testBackLine",
//	"testUturn",
//	"testColorL",
//	"testVLine",
//	"testTailVLine",
//	"testTailColorL",
//	"testMotor",
//	"reduceBattery",
//	"testSpin"
#endif
};
