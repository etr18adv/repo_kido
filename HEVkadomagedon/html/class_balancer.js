var class_balancer =
[
    [ "Balancer", "class_balancer.html#a62ea099474d4e83d02a26666c55d3ebf", null ],
    [ "getPwmLeft", "class_balancer.html#ad4a936088f8271e1ab1c2b71fbbb453d", null ],
    [ "getPwmRight", "class_balancer.html#a87d7886caa0ae8060461916b1924ac2f", null ],
    [ "init", "class_balancer.html#ac525763d0503de6e032f44a82422580b", null ],
    [ "setCommand", "class_balancer.html#af3af50e0d486516c24610a49a6c6ae68", null ],
    [ "update", "class_balancer.html#a05978a893429d046a21f4c0ee92a9c62", null ],
    [ "update_spin", "class_balancer.html#ae06b37e05fc21e5ed4606775d521d851", null ]
];