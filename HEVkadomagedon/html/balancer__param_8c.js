var balancer__param_8c =
[
    [ "A_D", "balancer__param_8c.html#aa94509c8176c09180b5820a45e3b0154", null ],
    [ "BATTERY_GAIN", "balancer__param_8c.html#ac9a83fbf484ccc187c70692c9fbfb5a9", null ],
    [ "BATTERY_OFFSET", "balancer__param_8c.html#ac9f60d8c879cc59cb6f59e66ceff980c", null ],
    [ "K_F", "balancer__param_8c.html#afcef5e071a84c0210c13bab2be5b061e", null ],
    [ "K_I", "balancer__param_8c.html#a5c4fa6fc05c5c961b22321c556b05b46", null ],
    [ "K_PHIDOT", "balancer__param_8c.html#aca1e856409f5f21664ad3b054b11c36a", null ],
    [ "K_THETADOT", "balancer__param_8c.html#a0a15d43c3a8132989be36d9a7a5c702d", null ]
];