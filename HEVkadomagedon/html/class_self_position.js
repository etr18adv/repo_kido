var class_self_position =
[
    [ "SelfPosition", "class_self_position.html#a7f373f51124f9ad5264ccf82f732f0aa", null ],
    [ "~SelfPosition", "class_self_position.html#ab041bd0b72d62872d0b9e1d0b6414c49", null ],
    [ "ajustment", "class_self_position.html#a0e44d9028b5a329245f34622fc9ea6fc", null ],
    [ "doMeasure", "class_self_position.html#a4dcbb88dfc96603520035b8914dd6325", null ],
    [ "getDistance", "class_self_position.html#a3b87eb1d5824826fdef23f23da315eb9", null ],
    [ "getPosition", "class_self_position.html#a81d846a46b5125ac2ecfcc09e3a92881", null ],
    [ "init", "class_self_position.html#a6cb1f2e219060d2100850f352b7f8a5c", null ],
    [ "printPosition", "class_self_position.html#a3180409d77f81273556000e082e4195a", null ],
    [ "setPositionX", "class_self_position.html#a2c113e5a647cba3a027e6729421be28f", null ],
    [ "setPositionY", "class_self_position.html#a547136a0481d62a2e0bb86a249de162a", null ]
];