var balancer__private_8h =
[
    [ "A_D", "balancer__private_8h.html#aa94509c8176c09180b5820a45e3b0154", null ],
    [ "BATTERY_GAIN", "balancer__private_8h.html#ac9a83fbf484ccc187c70692c9fbfb5a9", null ],
    [ "BATTERY_OFFSET", "balancer__private_8h.html#ac9f60d8c879cc59cb6f59e66ceff980c", null ],
    [ "K_F", "balancer__private_8h.html#afcef5e071a84c0210c13bab2be5b061e", null ],
    [ "K_I", "balancer__private_8h.html#a5c4fa6fc05c5c961b22321c556b05b46", null ],
    [ "K_PHIDOT", "balancer__private_8h.html#aca1e856409f5f21664ad3b054b11c36a", null ],
    [ "K_THETADOT", "balancer__private_8h.html#a0a15d43c3a8132989be36d9a7a5c702d", null ]
];