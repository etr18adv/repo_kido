var class_balancing_walker =
[
    [ "BalancingWalker", "class_balancing_walker.html#a9b302196e975056326ca8b32cc9f14f4", null ],
    [ "getA_R", "class_balancing_walker.html#a768ebb2e2e9750f3a04db1e9b0011875", null ],
    [ "getBattery", "class_balancing_walker.html#a161de42ded527b172c1ad4fd7de7414b", null ],
    [ "init", "class_balancing_walker.html#a47ca3433145d192d40ffe57238715bb8", null ],
    [ "run", "class_balancing_walker.html#a29fa360dae9da84b1f4da3cb7503e8cf", null ],
    [ "setA_R", "class_balancing_walker.html#a183985832667ea2174e75bba9382f92a", null ],
    [ "setCommand", "class_balancing_walker.html#a5da14a4d26f2cad9c1cee44e046c80ab", null ],
    [ "spin", "class_balancing_walker.html#a3bd30954bc41d8db1411187ce1a92cde", null ],
    [ "spinInit", "class_balancing_walker.html#a760b66d97a58df0b90ce49978cec8a6d", null ]
];