var class_tail_motor =
[
    [ "TailMotor", "class_tail_motor.html#ac47e4be08c35dc4b9e3adbf2c224df6e", null ],
    [ "~TailMotor", "class_tail_motor.html#a77e1ddeabebb997848595633f7cfe90d", null ],
    [ "checkStatus", "class_tail_motor.html#a1873f1ee55793d996e210f251eab7c2b", null ],
    [ "getAngle", "class_tail_motor.html#aae64809306cfd2842c565de589bb28de", null ],
    [ "run", "class_tail_motor.html#ad2a366f0a5d560a6bb5dbdffa0769908", null ],
    [ "setAngle", "class_tail_motor.html#afe0203720557b39521a7088e3ef8d9dd", null ],
    [ "setBack", "class_tail_motor.html#a17d4a2e6470705dd0305ca3cb8066f21", null ],
    [ "setGate", "class_tail_motor.html#a935dfa46d4276f924938b2878353dd6c", null ],
    [ "setStandby", "class_tail_motor.html#a7788b6269bd67a897e6d23c5bb6710d2", null ],
    [ "setStart", "class_tail_motor.html#a51feea4fd510c1dbb827a3d9b9d627e7", null ],
    [ "setStop", "class_tail_motor.html#ad97e46d58ba6fa9bbd0a456cfad1c6f6", null ],
    [ "setWalk", "class_tail_motor.html#a5399faa9f57314e34f907f67e23ec482", null ],
    [ "standDown", "class_tail_motor.html#a198bf1ac994b0663d0f68a7fc9761433", null ],
    [ "standUp", "class_tail_motor.html#a50a9aa26b17ecf6619cf1bb4b02ebc26", null ]
];