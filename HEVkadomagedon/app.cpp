/******************************************************************************
 *  app.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Implementation of the Task main_task
 *  Author: Kazuhiro.Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "app.h"
#include "common.h"
#include "MainUnit.h"
#include "Course.h"

//#include <syssvc/serial.h>

// https://github.com/ETrobocon/etroboEV3/wiki/problem_and_coping
void *__dso_handle=0; /*!< デストラクタ問題の回避 */

// オブジェクトの定義
static MainUnit        *gMainUnit;
static Course          *gCourse;

//タスク間通信用データキュー
void* dtqmb;


/**
 * @brief EV3システム生成
 */
static void user_system_create() {
    // オブジェクトの作成
	gMainUnit 	= new MainUnit();

    gCourse		= new Course();

    // 初期化完了
    gMainUnit->nextState();

}

/**
 * @brief EV3システム破棄
 */
static void user_system_destroy() {
	//停止中へ
	gMainUnit->nextState();

    //本体バックボタンが押されるまで待機
    gMainUnit->waitStop();

    // 周期ハンドラ停止
    ev3_stp_cyc(EV3_CYC_TRACER);

    delete gCourse;

    gMainUnit->display("Finished.");

	//バッテリ残量を表示
	gMainUnit->battery();

	//停止完了へ
	gMainUnit->nextState();
	delete gMainUnit;
}

/**
 * @brief トレース実行タイミング
 * @param[in] exinf
 */
void ev3_cyc_tracer(intptr_t exinf) {
	ER ercd = act_tsk(TRACER_TASK);

    if( ercd != E_OK ){
    	dprintf("ERR! act_tsk ercd=%d\n", ercd );
    }
}

/**
 * @brief メインタスク
 */
void main_task(intptr_t unused) {
	dprintf("\n-----------------------------------------\n");
	dprintf("started.\n");

	// 初期化処理
    user_system_create();

	//本体ボタンによるコース選択
    gMainUnit->display("Select course..");

	// 左右ボタンの押下が決定されるまで待つ
	int button = gMainUnit->waitButton();

	if( (gCourse->setCourse((Course::ECource)button)) == false ){
		dprintf("[ERR!] setCourse failed.\n");
		ext_tsk();
	}

	gMainUnit->nextState();

    // 周期ハンドラ開始
    ev3_sta_cyc(EV3_CYC_TRACER);

    gMainUnit->display("press TOUCH...");

    // バックボタンが押されるかコース完走するまで待機
    slp_tsk();

	gMainUnit->display("corse end.");

    // 周期ハンドラ停止
    //ev3_stp_cyc(EV3_CYC_TRACER); //後方モータを止めておくために終了処理内で実施

    // 終了処理
    user_system_destroy();

    dprintf("finished.\n");

    ext_tsk();
}

/**
 * @brief ライントレースタスク
 * @param[in] exinf
 */
void tracer_task(intptr_t exinf) {
	if( gMainUnit->checkBackButton() ){
        wup_tsk(MAIN_TASK);  // バックボタン押下
    }
    else {
     	gCourse->run();

    }
    ext_tsk();
}


/*
 * @brief Bluetooth通信タスク
 */
#if 1
void bt_task(intptr_t exinf){
    static FILE     *bt = NULL;     /* Bluetoothファイルハンドル */

	// 接続状態を確認
	while(!ev3_bluetooth_is_connected()){
		tslp_tsk(100);
	}

	/* Open Bluetooth file */
	bt = ev3_serial_open_file(EV3_SERIAL_BT);

	dprintf("bluetooth connected.\n");
	dprintf("1:RemoteStart, 2:StandUP, 3:StandDown, 4:setWhite, 5:setBlack \n");

	while(1){
		uint8_t c = fgetc(bt); /* 受信 */
		dprintf("bt=%c (%x)\n", c, c); /* エコーバック */

		snd_dtq(DT_QID, (int)c-48);
	}

	//注意: Bluetooth未接続でアプリを実行すると正常動作しない
}
#else
void bt_task(intptr_t exinf){
	static char buf[1] = {0};

	while(1){
		int size = serial_rea_dat(SIO_PORT_BT, buf, sizeof(buf));

		if(size > 0){
			serial_wri_dat(SIO_PORT_BT, buf, size);
			snd_dtq(DT_QID, (int)buf[0]-48);
		}
	}
}
#endif

