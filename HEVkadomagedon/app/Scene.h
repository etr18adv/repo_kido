/** @file Scene.h
*	@brief コースを分割したシーンを制御する親クラス
*   @author yamaguchi
*   @date 2017.06.09
*/

#ifndef SDK_WORKSPACE_ETRONO_TR_SCENE_H_
#define SDK_WORKSPACE_ETRONO_TR_SCENE_H_

#include "TailMotor.h"
#include "BalancingWalker.h"
#include "FreeWalker.h"
#include "LineMonitor.h"

/*!
 * @class Scene
 * @brief シーンを制御するスーパークラス
 */
class Scene {
public:
	Scene();
	virtual ~Scene();

	/*!
	 * @brief シーン処理を実施する
	 * @retval true 動作中
	 * @retval false 停止中
	 */
	virtual bool run() = 0; //純粋仮想関数

	/*!
	 * @brief シーン終了処理を実施する
	 */
	virtual void stop();

	/*!
	 * 継承先のクラス名をログ表示する
	 */
	void printClassName();

    /*!
     * @brief ビープ音を鳴動する
     * @param[in] id 音ID
     */
    void outBeep(int id);
};

#endif /* SDK_WORKSPACE_ETRONO_TR_SCENE_H_ */
