/** @file Course.h
 *	@brief 走行コースの選択と走行開始を制御するクラス
 *   @author yamaguchi
 *   @date 2017.06.09
 */

#ifndef SDK_WORKSPACE_ETRONO_TR_COURSE_H_
#define SDK_WORKSPACE_ETRONO_TR_COURSE_H_

#include "SelfPosition.h"
#include "Scene.h"
#include "Sonar.h"
#include "Vibration.h"
#include "LineTraceInfo.h"
#include "ArmControl.h"
/*!
 * @class Course
 * @brief 走行コースの選択と走行開始を制御するクラス
 */
class Course {
public:
	/*
	 * @enum ECourse
	 * @brief 走行コース種別
	 */
	enum ECource {
		COURSE_LEFT,     /*!< 左ボタン対応コース */
		COURSE_RIGHT,    /*!< 右ボタン対応コース */
		COURSE_UP,       /*!< 上ボタン対応コース */
		COURSE_DOWN,     /*!< 下ボタン対応コース */
		COURSE_MAX,      /*!< 上限値 */
	};

	/*
	 * @enum EMode
	 * @brief 走行モード
	 */
	enum EMode {
		MODE_STARTDASH = 0, /*!< スタートダッシュ */
		MODE_COLORL, 	    /*!< 実ライン左トレース (加重平均) */
		MODE_COLORL1, 	    /*!< 実ライン左トレース (加重平均) 走行距離に応じた加速 */
		MODE_COLORL2,	    /*!< 実ライン左トレース (移動平均) */
		MODE_COLORR,	    /*!< 実ライン右トレース (加重平均) */
		MODE_VLINE,         /*!< 仮想直線 */
		MODE_VLINE2,	    /*!< 減速しながら仮想直線*/
		MODE_VCIRCLE,       /*!< 仮想円 */
		MODE_TURN,		    /*!< ライン無視走行 (速度0、角度0でその場に停止) */
		MODE_TURN3,		    /*!< 3点ターン */
		MODE_SPIN,		    /*!< 2点スピン */
		MODE_SPIN2,	   		/*!< 新2点スピン */
		MODE_SPIN3,		    /*!< 3点スピン */
		MODE_GOAL,	   		/*!< ゴール前直線走行 (回転ありy軸補正) */
		MODE_GRAY,		    /*!< 灰色クリア */
		MODE_BACKLINE,	    /*!< ライン復帰 */
		MODE_TAILCOLORL,    /*!  3点実ライン左トレース(加重平均) ~3点時は尻尾をなるべくたてらせた状態で使用するように~  */
		MODE_TAILRUN,	    /*!< 3点ライン無視走行 */
		MODE_TAILVLINE,	    /*!< 3点仮想直線*/
		MODE_TAILUP,        /*!< 3->2点走行切替えで用いる尻尾の角度変更 */
		MODE_TAILDOWN,	    /*!< 2->3点走行切替えで用いる尻尾の角度変更 */
		MODE_ARMSET,		/* アーム制御用　for HackEV by kidokoro 	*/
		CHECK_HSV,		/* HSV値確認用　for HackEV by kidokoro 	*/
		TEST_COLOR,         /*!< 実ライントレースの調整用 */
		TEST_VLINE,         /*!< 2点仮想直線の調整用 */
		TEST_TAILVLINE,     /*!< 3点仮想直線の調整用 */
		TEST_VCIRCLE,       /*!< 2点仮想円の調整用 */
		TEST_MOTOR,			/*!< 電圧とLRモーター回転数の確認用 */
		REDUSE_BATTERY,		/*!< 電池消耗用 */
        MODE_MAX
	};

	/*
	 * @enum EendTerms
	 * @brief 終了条件
	 */
	enum EendTerms {
		NONE = 0,	/*!< 終了条件固定の場合 */
		DIS0_GE,	/*!< 座標初期化位置からの距離 >= */
		DIS0_LT,	/*!<                          <  */
		DIS_GE,		/*!< 区間開始位置からの距離   >= */
		DIS_LT,		/*!<                          <  */
		XPOS_GE,	/*!< x座標 >= */
		XPOS_LT,	/*!<       <  */
		YPOS_GE,	/*!< y座標 >= */
		YPOS_LT,	/*!<       <  */
		ON_LINE,	/*!< ライン上 */
		ON_LINESPIN,/*!< スピン後のライン上 */
		SONAR_CLOSE,  /*!< 超音波で計測した距離がendValue(cm)以下かつ、最短になったとき */
		CROSSLINE,	/*!< ライン中央検知 */
		COUNTUP,	/*!< カウント数 (x4ms = 秒数) */
		VIB_GE,		/*!< 振動値 >= */
		SONAR_LT,	/*!< ソナー距離 < */
		GRAY_GE,	/*!< 黒を最後に検出した地点からの距離 >= */
		TAIL_ANGLE,	/*!< 尻尾角度 == */
		TURN_LT,	/*!< 旋回値 < */
		SPIN_ANGLE, /*スピン旋回値。正負どちらでも超えれば終了*/
		TURN_ANGLE,/*ターン旋回値。正負どちらでも超えれば終了*/
		BATTERY_LT,	/*!< バッテリ残量(mV) < */
		HSV_COLOR,/*getHSV()によるカラー判定*/
		ENDTERM_MAX /*!< ENDTERM数   */
	};

	/*
	 * @enum EPositionAjust
	 * @brief 自己位置座標の補正指示
	 */
	enum EPositionAjust {
		POS_INIT = 1,	/*!< 初期化 */
		POS_AJST,		/*!< 位置補正 */
		POS_END,		/*!< ガレージ停止 */
		POS_YINIT_BLOCK	/*ブロック並べ初期化用Bykidokoro*/
	};


	/*!
	 * @brief 選択コース・内部状態を示す内部パラメータを初期化するコンストラクタ
	 */
	Course();

	/*!
	 * @brief デストラクタ
	 */
	virtual ~Course();

	/*!
	 * @brief 選択コースを設定する。アプリ起動中に一度だけ実行すること
	 * @param[in] course 選択コース
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool setCourse(Course::ECource course);


	/*
	 * @brief 選択コース名に対応する処理を実行する
	 * @param[in] course 選択コース
	 */
	void setCourseData(Course::ECource course);

	/*!
	 * @brief 選択コースを取得する
	 * @return 選択コース
	 */
	Course::ECource getCourse();

	/*!
	 * @brief コースのシナリオ実行を開始する
	 */
	void run();

	/*!
	 * 次のシーンに切り替える
	 */
	void nextScene();


private:
	ECource mCourse; 	/*!< 選択コース */

	std::list<Scene*> mScenario; /* シーンはFIFOで管理。最初に格納されているシーンが現在走行中のシーンとなる */

	LineTraceInfo* mLineTraceInfo;

	TailMotor* mTailMotor;
	ArmControl* mArmControl;

	Sonar* mSonar;
	Vibration* mVibration;

	SelfPosition* mSelfPosition;
	BalancingWalker* mBalancingWalker;
	FreeWalker*      mFreeWalker;
	LineMonitor*     mLineMonitor;
	LineMonitor*     mLineMonitor3;
	Balancer*        mBalancer;

	/*!
	 * @brief シナリオにシーンを追加する
	 * @param[in] scene 追加シーン
	 */
	void addScene(Scene* scene);

	/*
	 * @brief ショートカットなしLコース
	 */
	//void basicL();

	/*
	 * @brief ショートカットなしRコース
	 */
	//void basicR();

	/*
	 * @brief ショートカットありLコース
	 * @param[in] firstSpin  1段目スピン有無
	 * @param[in] secondSpin 2段目スピン有無
	 */
	void aggressiveL(bool firstSpin, bool secondSpin);
	void safetyL(bool firstSpin, bool secondSpin);

	/*
	 * @brief ショートカットありRコース
	 * @param[in] gateDouble ゲート攻略ダブル有無
	 */
	void aggressiveR(bool gateDouble);
	void safetyR(bool gateDouble);

	/*
	 * @brief 階段攻略
	 * @param[in] firstSpin  1段目スピン有無
	 * @param[in] secondSpin 2段目スピン有無
	 */
	void testSteps(bool firstSpin, bool secondSpin);

	/*
	 * @brief ゲート攻略
	 */
	void testGate(bool gateDouble);

	/*
	 * @brief ガレージ停止
	 */
	void stopGarage();

	/*
	 * @brief スピンのテスト用
	 */
	void testSpin();

	/*
	 * @brief 紙ベースの練習コースライントレース用
	 */
	void paperCourse();

	/*
	 * @brief ライン復帰テスト
	 */
	void testBackLine();

	/*
	 * @brief Uturnテスト
	 */
    void testUturn();

	/*
	 * @brief 3点→2点バランス走行のテスト用
	 */
	void testWalker();

	/*
	 * @brief 調整用のシーンを実行する
	 * @param[in] id 調整メニューID
	 */
	void ajustment(int id);

	/*
	 * @brief その場で一時停止
	 * @param[in] time 停止時間(ms)
	 */
	void pause(int time);

	/*
	 * @brief 3点走行時の一時停止
	 * @param[in] time 停止時間(ms)
	 */
	void pause3(int time);

	/*
	 * @brief HackEV動作確認用 by kidokoro
	 */

	void testHackEV();

	/*
	 * @brief HackEV カラーセンサ HSV値確認用　by kidokoro
	 */
	void checkHSV();

	/*
	 * @brief HackEV データ読み込み用　by kidokoro
	 */
	void dataDriven();
	void block_only();
	void dataBlock();

};

#ifdef __DEBUG__
    /*
     * @brief モード修正時にこの文字列配列の修正を行う
     */
    constexpr char cModeStr[Course::MODE_MAX][16] = {       /*! 文字列配列(モード)          */
        "STARTDASH",
        "COLORL",
		"COLORL1",
        "COLORL2",
        "COLORR",
        "VLINE",
        "VLINE2",
        "VCIRCLE",
        "TURN",
		"TURN3",
        "SPIN",
        "SPIN2",
        "SPIN3",
        "GOAL",
        //"GOAL2",
        "GRAY",
        "BACKLINE",
		"TAILCOLORL",
        "TAILRUN",
        "TAILVLINE",
        "TAILUP",
        "TAILDOWN",
		"MODE_ARMSET",// by kidokoro for HackEV
		"CHECK_HSV",// by kidokoro for HackEV
        "TESTCOLOR",
        "TESTVLINE",
        "TESTTAILVLINE",
        "TESTVCIRCLE",
		"TEST_MOTOR",
		"REDUSE_BATTERY"
    };

    /*
     * @brief エンドターム修正時にこの文字列配列の修正を行う
     */
    constexpr char cEndTermStr[Course::ENDTERM_MAX][16] = { /*! 文字列配列(エンドターム)          */
        "NONE",
        "DIS0_GE",
        "DIS0_LT",
        "DIS_GE",
        "DIS_LT",
        "XPOS_GE",
        "XPOS_LT",
        "YPOS_GE",
        "YPOS_LT",
        "ON_LINE",
        "ON_LINESPIN",
        "SONAR_CLOSE",
        "CROSSLINE",
        "COUNTUP",
        "VIB_GE",
        "SONAR_LT",
        "GRAY_GE",
        "TAIL_ANGLE",
		"TURN_LT",
		"SPIN_ANGLE",
		"TURN_ANGLE",
		"BATTERY_LT"
    };
#endif  /* __DEBUG__    */

#endif /* SDK_WORKSPACE_ETRONO_TR_COURSE_H_ */
