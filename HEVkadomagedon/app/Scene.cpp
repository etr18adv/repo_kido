/** @file Scene.cpp
*	@brief コースを分割したシーンを制御する親クラス
*   @author yamaguchi
*   @date 2017.06.09
*/

#include <typeinfo>
#include <cxxabi.h>
#include <math.h>

#include "common.h"
#include "Scene.h"


Scene::Scene() {
}

Scene::~Scene() {
}


void Scene::stop(){
	outBeep(1);
}

void Scene::printClassName(){
#ifdef __DEBUG__
	char buf[64] = "unknown";
	size_t len = sizeof(buf);

	dprintf("%s\n", abi::__cxa_demangle(typeid(*this).name(),buf,&len,NULL));
#endif
}


void Scene::outBeep(int id){
#ifdef __DEBUG__
	//音を鳴らす

	switch(id){
	case 1:
		ev3_speaker_play_tone(NOTE_C4, 50);
		break;
	case 2:
		ev3_speaker_play_tone(NOTE_C6, 50);
		break;
	case 3:
		ev3_speaker_play_tone(NOTE_A5, 50);
		break;
	case 4:
		ev3_speaker_play_tone(NOTE_A6, 50);
		break;
	case 5:
		ev3_speaker_play_tone(NOTE_C5, 50);
		break;
	case 6:
		ev3_speaker_play_tone(NOTE_A5, 500);
		break;
	default:
		ev3_speaker_play_tone(NOTE_A4, 50);
		break;
	}

#endif
}
