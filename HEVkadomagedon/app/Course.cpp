/** @file Course.cpp
*	@brief 走行コースの選択と走行開始を制御するクラス
*   @author yamaguchi
*   @date 2017.06.09
*/


#include "common.h"
#include "Scene.h"
#include "SceneStandby.h"
#include "SceneLineTrace.h"
#include "Course.h"
#include "LineTraceInfo.h"
#include "SelfPosition.h"
#include "SonarSensor.h"
#include "Vibration.h"

 #include <string.h>

// using宣言
using ev3api::ColorSensor;
using ev3api::GyroSensor;
using ev3api::Motor;
using ev3api::SonarSensor;

// Device objects
// オブジェクトを静的に確保する
// HackEV用に変更　by kidokoro
// PORT_1 →タッチセンサ	SceneStandby.cpp
SonarSensor gSonarSensor(PORT_3);
ColorSensor gColorSensor(PORT_2);
GyroSensor  gGyroSensor(PORT_4);
// PORT_A →アームモータ　もともと無い
Motor 		gArmMotor(PORT_A);
Motor       gRightWheel(PORT_B);
Motor       gLeftWheel(PORT_C);
// PORT_D　→尻尾モータ 　以下のcourse のコンストラクタで宣言

static SceneStandby*    sceneStandby;
static SceneLineTrace*  sceneLineTrace;

Course::Course(){
	mCourse = COURSE_MAX;

	/* システム生成 */
	mSelfPosition = new SelfPosition(gLeftWheel, gRightWheel);

    mSonar		= new Sonar(gSonarSensor);
    mVibration  = new Vibration(gGyroSensor);

    mTailMotor	= new TailMotor(PORT_D);// by kidokoro for HackEV
   mArmControl = new ArmControl(gArmMotor);//by kidokoro for HackEV

	mFreeWalker		 = new FreeWalker(gLeftWheel, gRightWheel);

    mBalancer        = new Balancer();
    mBalancingWalker = new BalancingWalker(mVibration,
                                           gLeftWheel,
                                           gRightWheel,
                                           mBalancer);
    mLineMonitor     = new LineMonitor(gColorSensor, DEFAULT_BLACK,  DEFAULT_WHITE);  //2点走行時
    mLineMonitor3    = new LineMonitor(gColorSensor, DEFAULT_BLACK3, DEFAULT_WHITE3); //3点走行時

    mLineTraceInfo   = new LineTraceInfo();


	/* シーン */
	sceneStandby 	= new SceneStandby(   mTailMotor, mLineMonitor, mBalancingWalker, mVibration);
	sceneLineTrace  = new SceneLineTrace( mTailMotor, mLineMonitor, mLineMonitor3, mBalancingWalker, mFreeWalker, mSelfPosition, mLineTraceInfo, mVibration, mSonar, mArmControl);
	//by kidokoro for HackEV
}

Course::~Course() {
	delete sceneStandby;
	delete sceneLineTrace;

	delete mLineTraceInfo;
	delete mLineMonitor3;
	delete mLineMonitor;
	delete mBalancingWalker;
	delete mBalancer;
	delete mFreeWalker;
	delete mTailMotor;
	delete mVibration;
	delete mSonar;
	delete mSelfPosition;
}



bool Course::setCourse(Course::ECource course) {
	if( mCourse != COURSE_MAX ){
		dprintf("invalid call.\n");
		return false;
	}

	if ( course >= COURSE_MAX ){
		dprintf("cource not found. %d\n", (int)course);
		return false;
	}

	mCourse = course;

	//共通のスタンバイシーンを追加
	addScene(sceneStandby);

	//cCourseNameに従ってコース設定
	setCourseData(mCourse);

	return true;
}


void Course::setCourseData(Course::ECource course){
	int id = (int)course;

	if(strcmp(cCourseName[id],"L course (aggressive)")==0){
		aggressiveL(false, true); //Rコースに入るショートカット
	} else if(strcmp(cCourseName[id],"R course (aggressive)")==0){
		aggressiveR(true); //最後の直線でLコースに入るショートカット
	} else if(strcmp(cCourseName[id],"L course (safety)")==0){
		safetyL(false, true);
	} else if(strcmp(cCourseName[id],"R course (safety)")==0){
		safetyR(true);
	} else if(strcmp(cCourseName[id],"testGate")==0){
		testGate(true); //ゲート攻略
	} else if(strcmp(cCourseName[id],"testBackLine")==0){
		testBackLine(); //ライン復帰テスト用
	} else if(strcmp(cCourseName[id],"testSteps")==0){
		testSteps(true,true);  //階段テスト用（ゴール後の灰色後から開始）
	} else if(strcmp(cCourseName[id],"paperCourse")==0){
		paperCourse();  //紙ベースの練習コース走行用
	} else if(strcmp(cCourseName[id],"testColorL")==0){
		ajustment(0); //反射光ライントレースの調整用
	} else if(strcmp(cCourseName[id],"testVLine")==0){
		ajustment(1); //2点仮想直線の調整用
	} else if(strcmp(cCourseName[id],"testTailVLine")==0){
		ajustment(2); //3点仮想直線の調整用
	} else if(strcmp(cCourseName[id],"testTailColorL")==0){
		ajustment(3); //3点実ライントレースの調整用
	} else if(strcmp(cCourseName[id],"testMotor")==0){
		ajustment(4); //モーターの個体差調査用
	} else if(strcmp(cCourseName[id],"reduceBattery")==0){
		ajustment(5); //バッテリ消耗用
	} else if(strcmp(cCourseName[id],"testSpin")==0){
		testSpin();   //スピンのテスト用
	} else if(strcmp(cCourseName[id],"testWalker")==0){
		testWalker();
	} else if(strcmp(cCourseName[id],"testHackEV")==0){
		testHackEV(); //HackEV テスト用　by kidokoro
	} else if(strcmp(cCourseName[id],"checkHSV")==0){
		checkHSV(); //HackEV 用　by kidokoro
	} else if(strcmp(cCourseName[id],"dataDriven")==0){
		dataDriven(); //HackEV 用　by kidokoro
	} else if(strcmp(cCourseName[id],"dataBlock")==0){
		dataBlock(); //HackEV 用　by kidokoro
	} else {
		dprintf("[ERR!] course name is not found.\n");
	}
}

// HackEV データ駆動用　by kidokoro

void Course::dataDriven(){
	int i;
	const int data_num=10;
	int param[data_num][7]={
	//まず、簡単なやり方を考える。
			{MODE_ARMSET,NONE,0,0,ARM_LINEMONITOR_NORMAL, 30, POS_INIT},	//アーム角度設定 とりあえず版 POS_INIT はここだけで実施

			//Rコース走行箇所
			//Rコースを４つに分割する
			{MODE_TAILCOLORL, DIS0_GE, 1900,  SPEED_NORMAL2,  40,   0, 0},
			{MODE_TAILCOLORL, DIS_GE, 3000,  SPEED_LOW3,  70,   0, 0},
			{MODE_TAILCOLORL, DIS_GE, 2800,  SPEED_LOW2,  30,   0, 0},
			{MODE_TAILCOLORL, DIS_GE, 2200,  SPEED_NORMAL2,  40,   0, 0},
			//Rコースからブロック並べまで
			{MODE_TAILCOLORL, DIS_GE, 2000,  SPEED_LOW2,  70,   0, POS_INIT},
			//今のところ灰色検知は不要
			{MODE_MAX,0,0,0,0,0,0} //終了判定用
	};

	i=0;
	while(1){
		if(param[i][0]==MODE_MAX){
			break;
		}
		else{
			mLineTraceInfo->addData(param[i][0],param[i][1],param[i][2],param[i][3],param[i][4],param[i][5],param[i][6]);
			i++;
		}
	}

	//ブロック並べ以降を追加
	block_only();
}

//ブロック並べ試走会向け調整用
//ブロック並べ直前からの走行だけにしている。
void Course::dataBlock(){
	int i=0;
	int param[1][7]={
	//まず、簡単なやり方を考える。
			{MODE_ARMSET,NONE,0,0,ARM_LINEMONITOR_NORMAL, 30, POS_INIT},	//アーム角度設定 とりあえず版 POS_INIT はここだけで実施
	};
	i=0;
	mLineTraceInfo->addData(param[i][0],param[i][1],param[i][2],param[i][3],param[i][4],param[i][5],param[i][6]);

	//ブロック並べ以降を追加
	block_only();
}

//ブロック並べ走行データの追加
void Course::block_only(){
	int i;
	const int data_num=100;
	int param[data_num][7]={
			// Todo （座標原点は赤ブロック置き場の200mm手前。ToDO 赤ブロック置き場に来た時の補正方法）*/
#if 1 		//ブロック並べ走行箇所
			{MODE_TAILCOLORL, DIS0_GE, 100,  SPEED_LOW2,  10,   0, POS_AJST},			//ライントレース＆位置補正　速度落とし、反射光目標値を低め(10)にして黒寄りを走る
			// 青　８ー＞１０
			{MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  10,   0, 0},	//（キャッチ）最初のブロック置き場（赤）まで移動
			{MODE_TURN3, TURN_ANGLE, 75, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR,HSV_BLUE,  SPEED_LOW2,  30,   0, 0},//ノード１２に進む
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR,HSV_GREEN,  SPEED_LOW2,  30,   0, 0},//ノード１３に進む
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える
			{MODE_SPIN3, SPIN_ANGLE,-10, SPEED_LOW2, 0,20,0},//超えたあと首振り
			{MODE_TAILCOLORL, HSV_COLOR,HSV_RED,  SPEED_LOW2,  30,   0, 0},//ノード１４に進む
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR,HSV_BLUE,  SPEED_LOW2,  30,   0, 0},//ノード１０に進む
//			{MODE_TAILRUN,   XPOS_GE, 200, -SPEED_LOW1,  0,0, 0},//ノード35まで後退
			{MODE_TAILRUN,   DIS_LT, -120, -SPEED_LOW1,  0,0, 0},//12cm後退
			{MODE_SPIN3, SPIN_ANGLE,160, SPEED_LOW2, 0,20,0},		//スピンとりあえず９０度
			{MODE_SPIN3, ON_LINE,30, SPEED_LOW2, 0,20,0},		//スピン

			// 緑　９ー＞５
			{MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  30,   0, 0},	//
			{MODE_TURN3, TURN_ANGLE, 75, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR,HSV_GREEN,  SPEED_LOW2,  30,   0, 0},//ノード１３に進む
			{MODE_TURN3, TURN_ANGLE, 75, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR,HSV_YELLOW,  SPEED_LOW2,  10,   0, 0},//（キャッチ）ノード９に進む
//			{MODE_TAILVLINE, XPOS_LT, -100, SPEED_LOW2, -200,450+200,0},//ノード９を超える
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える
			{MODE_SPIN3, SPIN_ANGLE,-10, SPEED_LOW2, 0,20,0},//超えたあと首振り
			{MODE_TAILCOLORL, HSV_COLOR,HSV_GREEN,  SPEED_LOW2,  30,   0, 0},//ノード５に進む
//			{MODE_TAILRUN,   XPOS_GE, -250, -SPEED_LOW1,  0,0, 0},//ノード35まで後退
			{MODE_TAILRUN,   DIS_LT, -120, -SPEED_LOW1,  0,0, 0},//12cm後退
			{MODE_SPIN3, SPIN_ANGLE,160, SPEED_LOW2, 0,20,0},		//スピンとりあえず９０度
			{MODE_SPIN3, ON_LINE,30, SPEED_LOW2, 0,20,0},		//スピン　オンラインまで

			// 黄　６ー＞９
			{MODE_TAILCOLORL, HSV_COLOR, HSV_YELLOW,  SPEED_LOW2,  30,   0, 0},	//ノード９に進む
			{MODE_TURN3, TURN_ANGLE, 75, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  30,   0, 0},	//ノード８に進む
			{MODE_TURN3, TURN_ANGLE, 75, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR, HSV_BLUE,  SPEED_LOW2,  30,   0, 0},	//ノード４に進む
//			{MODE_TAILVLINE, XPOS_LT, -500, SPEED_LOW2, -600,0+200,0},//ノード4を超える
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える
			{MODE_SPIN3, SPIN_ANGLE,-10, SPEED_LOW2, 0,20,0},//超えたあと首振り
			{MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  30,   0, 0},	//ノード０に進む
			{MODE_TURN3, TURN_ANGLE, 75, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR, HSV_YELLOW,  SPEED_LOW2,  30,   0, 0},	//
//			{MODE_TAILVLINE, YPOS_GE, 500, SPEED_LOW2, -800,1350+200,0},//ノード１を超える
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える
			{MODE_SPIN3, SPIN_ANGLE,-10, SPEED_LOW2, 0,20,0},//超えたあと首振り
			{MODE_TAILCOLORL, HSV_COLOR, HSV_BLUE,  SPEED_LOW2,  30,   0, 0},	//
//			{MODE_TAILVLINE, YPOS_GE, 1000, SPEED_LOW2, -800,1350+200,0},//ノード２を超える
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える
			{MODE_SPIN3, SPIN_ANGLE,-10, SPEED_LOW2, 0,20,0},//超えたあと首振り
			{MODE_TAILCOLORL, HSV_COLOR, HSV_GREEN,  SPEED_LOW2,  30,   0, 0},	//ノード３に進む
			{MODE_TURN3, TURN_ANGLE, 75, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR, HSV_YELLOW,  SPEED_LOW2,  30,   0, 0},	//ノード７に進む
			{MODE_TURN3, TURN_ANGLE, 75, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  10,   0, 0},	//（キャッチ）ノード６に進む
			{MODE_TURN3, TURN_ANGLE, 75, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, HSV_COLOR, HSV_BLUE,  SPEED_LOW2,  30,   0, 0},	//ノード２に進む
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//（運搬）左ターンする。
			{MODE_TAILCOLORL, HSV_COLOR, HSV_YELLOW,SPEED_LOW2,  30,   0, 0},	//ノード１に進む
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える ノード１
			{MODE_SPIN3, SPIN_ANGLE,-10, SPEED_LOW2, 0,20,0},//超えたあと首振り
			{MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  10,   0, 0},//ノード０に進む
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//（運搬）左ターンする。
			{MODE_TAILCOLORL, HSV_COLOR, HSV_BLUE,SPEED_LOW2,  30,   0, 0},	//ノード４に進む
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える ノード４
			{MODE_SPIN3, SPIN_ANGLE,-10, SPEED_LOW2, 0,20,0},//超えたあと首振り
			{MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  30,   0, 0},//ノード８に向かう
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//（運搬）左ターンする。
			{MODE_TAILCOLORL, HSV_COLOR, HSV_YELLOW,SPEED_LOW2,  10,   0, 0},	//（リリース）ノード９に向かう
			{MODE_TAILRUN,   DIS_LT, -120, -SPEED_LOW1,  0,0, 0},//12cm後退
			{MODE_SPIN3, SPIN_ANGLE,160, SPEED_LOW2, 0,20,0},		//スピンとりあえず１６０度
			{MODE_SPIN3, ON_LINE,30, SPEED_LOW2, 0,20,0},		//スピン　オンラインまで

			// 赤　４ー＞６
			{MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  30,   0, 0},	//ノード８に進む
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//左ターンする。
			{MODE_TAILCOLORL, HSV_COLOR, HSV_BLUE,  SPEED_LOW2,  30,   0, 0},	//ノード１２に進む
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//左ターンする。
			{MODE_TAILCOLORL, HSV_COLOR,HSV_GREEN,  SPEED_LOW2,  30,   0, 0},//ノード１３に進む
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える
			{MODE_SPIN3, SPIN_ANGLE,-10, SPEED_LOW2, 0,20,0},//超えたあと首振り
			{MODE_TAILCOLORL, HSV_COLOR,HSV_RED,  SPEED_LOW2,  30,   0, 0},//ノード１４に進む
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える
			{MODE_SPIN3, SPIN_ANGLE,-10, SPEED_LOW2, 0,20,0},//超えたあと首振り
			{MODE_TAILCOLORL, HSV_COLOR, HSV_YELLOW,  SPEED_LOW2,  10,   0, 0},	//（キャッチ）ノード１５に進む
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//（運搬）左ターンする。
			{MODE_TAILCOLORL, HSV_COLOR,HSV_GREEN,  SPEED_LOW2,  30,   0, 0},//ノード１１に進む
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える
			{MODE_SPIN3, SPIN_ANGLE,-10, SPEED_LOW2, 0,20,0},//超えたあと首振り
			{MODE_TAILCOLORL, HSV_COLOR,HSV_YELLOW,  SPEED_LOW2,  30,   0, 0},//ノード７に進む
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//（運搬）左ターンする。
			{MODE_TAILCOLORL, HSV_COLOR, HSV_RED,SPEED_LOW2,  10,   0, 0},	//（リリース）ノード６に向かう
			{MODE_TAILRUN,   DIS_LT, -120, -SPEED_LOW1,  0,0, 0},//12cm後退
			{MODE_SPIN3, SPIN_ANGLE,160, SPEED_LOW2, 0,20,0},		//スピンとりあえず１６０度
			{MODE_SPIN3, ON_LINE,30, SPEED_LOW2, 0,20,0},		//スピン　オンラインまで
			{MODE_TAILRUN, DIS_GE, 100, SPEED_LOW2, 0,0,0},//ノード超える
			//ノード１１に進む
			{MODE_TAILCOLORL, HSV_COLOR,HSV_YELLOW,  SPEED_LOW2,  30,   0, 0},//ノード７に進む
			{MODE_TURN3, TURN_ANGLE, 75, SPEED_LOW1, 0,20,0},//右ターンする。
			{MODE_TAILCOLORL, HSV_COLOR,HSV_GREEN,  SPEED_LOW2,  30,   0, 0},//ノード１１に進む
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//左ターンする。
#endif
			//ガレージへ向かう
			{MODE_TAILRUN, ON_LINE, 30, SPEED_LOW3, 0,0,0},//ノード超える
			{MODE_SPIN3, SPIN_ANGLE,-90, SPEED_LOW2, 0,20,0},//左ターンする。
//			{MODE_TAILRUN,   DIS_LT, -120, -SPEED_LOW2,  0,0, 0},//12cm後退
			{MODE_TAILCOLORL, DIS_GE, 150,  SPEED_LOW1,  30,   0, 0},//オンライン走行
			{MODE_TAILRUN, DIS_GE, 450, SPEED_NORMAL1, 0,0,0},//ガレージイン
			{MODE_MAX,0,0,0,0,0,0} //終了判定用

//以下は別解法。仮想LTが多く、安定しないのであきらめ。
#if 0
			//青ブロック　８ー＞２
			{MODE_ARMSET,NONE,0,0,ARM_LINEMONITOR_NORMAL, 30, POS_INIT},	//アーム角度設定 とりあえず版 POS_INIT はここだけで実施
			{MODE_TAILCOLORL, DIS0_GE, 100,  SPEED_LOW2,  10,   0, POS_AJST},			//ライントレース＆位置補正　速度落とし、反射光目標値を低め(10)にして黒寄りを走る
			{MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  10,   0, 0},	//（キャッチ）最初のブロック置き場（赤）まで移動
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			{MODE_TAILCOLORL, XPOS_LT, -200,  SPEED_LOW2,  10,   0, 0},//ノード２６まで進む
			{MODE_SPIN3, SPIN_ANGLE, 45, SPEED_LOW2, 0,20,0},		//スピン//ToDo 目標角度の自動算出
			{MODE_TAILVLINE, ON_LINE, 30, SPEED_LOW2, -400,350+200,0},//ノード２３に進む　オンライン版
			{MODE_TAILVLINE, ON_LINE, 30, SPEED_LOW2, -600,450+200,0},//ノード２０に進む
			{MODE_TAILVLINE, ON_LINE, 30, SPEED_LOW2, -800,675+200,0},//ノード１７に進む
			{MODE_SPIN3, SPIN_ANGLE, 20, SPEED_LOW1, 0,20,0},		//スピン//ToDo 目標角度の自動算出
																									//Todo 次LTのとき右スピン抑えめ
			{MODE_TAILCOLORL, HSV_COLOR,HSV_BLUE,  SPEED_LOW2,  10,   0, 0},//ノード５にリリース
			{MODE_TAILRUN,   YPOS_LT, 675+200, -SPEED_LOW1,  0,0, 0},//ノード１７まで後退
										//ポイント　バックのときはturn値（param2）を０にする

			//緑ブロック　９ー＞３
			{MODE_SPIN3, SPIN_ANGLE, 135, SPEED_LOW1, 0,20,0},		//スピン//ToDo 目標角度の自動算出
			{MODE_TAILVLINE, YPOS_LT, 450+200, SPEED_LOW2, -600,450+200,0},//ノード２０に進む
			{MODE_SPIN3, SPIN_ANGLE, -30, SPEED_LOW2, 0,20,0},		//スピン//ToDo 目標角度の自動算出、ターン幅の調整
			{MODE_TAILVLINE, XPOS_GE, -200, SPEED_LOW2, -200,450+200,0},//ノード２７に進む
				//Todo　一気にノード５を超える　ノード２０ー＞２７
			{MODE_TAILCOLORL, HSV_COLOR, HSV_YELLOW,  SPEED_LOW2,  10,   0, 0},	//（キャッチ）ブロック置き場（黄色）９まで移動
			{MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0},//（運搬）ターンする。//ToDo 目標角度の自動算出
			//黄ブロック　６ー＞７
			//赤ブロック１５ー＞６
			{MODE_MAX,0,0,0,0,0,0} //終了判定用
#endif

	};

	i=0;
	while(1){
		if(param[i][0]==MODE_MAX){
			break;
		}
		else{
			mLineTraceInfo->addData(param[i][0],param[i][1],param[i][2],param[i][3],param[i][4],param[i][5],param[i][6]);
			i++;
		}
	}
#if 0
	for(i=0;i<data_num;i++){
		mLineTraceInfo->addData(param[i][0],param[i][1],param[i][2],param[i][3],param[i][4],param[i][5],param[i][6]);
	}
#endif

	//ストップ
	mLineTraceInfo->addData( MODE_TAILCOLORL, DIS0_GE, 300,  0,  20,   0, POS_INIT); //ストップ（仮）
	addScene(sceneLineTrace);

}
// HackEV テスト用　by kidokoro
void Course::testHackEV(){
/*仮想円はブロック並べには不要*/

	/*アーム角度設定 とりあえず版 POS_INIT はここだけで実施
	 Todo （座標原点は赤ブロック置き場の200mm手前。ToDO 赤ブロック置き場に来た時の補正方法）*/
	mLineTraceInfo->addData( MODE_ARMSET, NONE, 0, 0, ARM_LINEMONITOR_NORMAL, 30, POS_INIT); //アームセット

	///直線ライントレース＆位置補正　速度落とし、 反射光目標値を低め(10)にして黒寄りを走る
	mLineTraceInfo->addData( MODE_TAILCOLORL, DIS0_GE, 100,  SPEED_LOW2,  10,   0, POS_AJST);
	//（キャッチ）最初のブロック置き場（赤）まで移動
	mLineTraceInfo->addData( MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  10,   0, 0); //直線ライントレース＆位置補正
	//（運搬）ターンする。終了条件がなく、今のところparam1に目標角度が入る。ToDo 目標角度の自動算出
	mLineTraceInfo->addData( MODE_TURN3, TURN_ANGLE, -90, SPEED_LOW1, 0,20,0);
	//（運搬）目標のブロック置き場（青）まで
	mLineTraceInfo->addData( MODE_TAILCOLORL, HSV_COLOR, HSV_BLUE,  SPEED_LOW2,  10,   0, 0);
	//（リリース）バックする[距離を指定]
	mLineTraceInfo->addData( MODE_TAILRUN,   XPOS_GE, -200, -SPEED_LOW1,  0, 0, 0);
	//（リリース）スピンする（次の目標を向く）Todo スピン角度の算出
    mLineTraceInfo->addData( MODE_SPIN3, SPIN_ANGLE, 45, SPEED_LOW3, 0,20,0);
    //（受け取り）仮想LT　ToDo 目標座標を次のスピン・LTを考えて、ずらす？
    //（座標原点は赤ブロック置き場の200mm手前。ToDO 赤ブロック置き場に来た時の補正方法）
    //本当の目標座標は(-400,400)。2手先のライントレース安定化のため、LTを長くなる方向にずらす
    mLineTraceInfo->addData( MODE_TAILVLINE, XPOS_LT, -400, SPEED_LOW3, -400,350,0);
	//（受け取り）スピンする。本当は45だけど15にする　ToDo 次が左LTの場合、右回転は少なめ、左回転は多めにする？
    mLineTraceInfo->addData( MODE_SPIN3, SPIN_ANGLE, 15, SPEED_LOW2, 0,20,0);
	//（キャッチ）ライントレースでブロック置き場（緑）まで
	mLineTraceInfo->addData( MODE_TAILCOLORL, HSV_COLOR, HSV_GREEN,  SPEED_LOW2,  10,   0, 0);
	//（運搬）ターンする。本当は９０だけど８０にする
	mLineTraceInfo->addData( MODE_TURN3, TURN_ANGLE, 80, SPEED_LOW1, 0,20,0);
	//（運搬）ToDO 本来ラインの真ん中で止まる。
	mLineTraceInfo->addData( MODE_TAILCOLORL, HSV_COLOR, HSV_YELLOW,  SPEED_LOW2,  10,   0, 0);
	//（運搬）ターンする。本当は９０だけど８０にする
	mLineTraceInfo->addData( MODE_TURN3, TURN_ANGLE, 80, SPEED_LOW1, 0,20,0);
	//（運搬）Todo 本来ショートカットする
	mLineTraceInfo->addData( MODE_TAILCOLORL, HSV_COLOR, HSV_RED,  SPEED_LOW2,  10,   0, 0);
	//（運搬）
	//（運搬）
	//（リリース）バックする[距離を指定]
	mLineTraceInfo->addData( MODE_TAILRUN,   YPOS_GE, 400, -SPEED_LOW1,  0, 0, 0);

#if 0
	mLineTraceInfo->addData( MODE_TAILCOLORL, DIS0_GE, 130,  SPEED_LOW2,  40,   0, POS_AJST); //直線ライントレース＆位置補正
	//座標目指して仮想ライントレース。
	mLineTraceInfo->addData( MODE_TAILVLINE,   YPOS_GE, 425, SPEED_LOW2,  0,425, 0);
    mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW1, -45,20,0);

	mLineTraceInfo->addData( MODE_TAILVLINE,   YPOS_GE, 650, SPEED_LOW2,  -200, 650, 0);
    mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW1, -90,20,0);
	mLineTraceInfo->addData( MODE_TAILVLINE,   XPOS_LT, -400, SPEED_LOW2,  -400, 425, 0);
    mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW1, -90,20,0);
	mLineTraceInfo->addData( MODE_TAILVLINE,   XPOS_GE, 0, SPEED_LOW2,  0, 200, 0);
#endif
#if 0	//ブロック運搬確認
	mLineTraceInfo->addData( MODE_TAILVLINE,   YPOS_GE, 200, SPEED_LOW2,  0,200, 0);
	mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_NORMAL3, -90,20,0);
	mLineTraceInfo->addData( MODE_TAILVLINE,   XPOS_LT, -200, SPEED_NORMAL2,  -200, 200, 0);
	mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW3, -90,20,0);
	mLineTraceInfo->addData( MODE_TAILVLINE,   YPOS_LT, 0, SPEED_LOW1,  -200, 0, 0);
#endif
	//ストップ
	mLineTraceInfo->addData( MODE_TAILCOLORL, DIS0_GE, 300,  0,  20,   0, POS_INIT); //ストップ（仮）
	addScene(sceneLineTrace);
}

void Course::checkHSV(){

	/*アーム角度設定 とりあえず版 POS_INIT はここだけで実施
	 Todo （座標原点は赤ブロック置き場の200mm手前。ToDO 赤ブロック置き場に来た時の補正方法）*/
	mLineTraceInfo->addData( MODE_ARMSET, NONE, 0, 0, ARM_LINEMONITOR_NORMAL, 30, POS_INIT); //アームセット
	mLineTraceInfo->addData( CHECK_HSV, NONE, 0, 0, ARM_LINEMONITOR_NORMAL, 30, POS_INIT);

	//ストップ
	mLineTraceInfo->addData( MODE_TAILCOLORL, DIS0_GE, 300,  0,  20,   0, POS_INIT); //ストップ（仮）

	addScene(sceneLineTrace);
}


void Course::aggressiveL(bool firstSpin, bool secondSpin){
	//Lコースショートカット用
	sceneLineTrace->setShiftX(-400); //地図のx座標調整(-400mm)

	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
	mLineTraceInfo->addData( MODE_COLORL1, DIS0_GE,  100, SPEED_HIGH1, 50,   0, SPEED_MAX); //走行距離に応じて加速
	mLineTraceInfo->addData( MODE_COLORL2, DIS0_GE, 2000, SPEED_MAX,   50,   0, POS_AJST); //高速走行

	mLineTraceInfo->addData( MODE_VCIRCLE, XPOS_GE,  770, SPEED_MAX, 770, 1950, 280);
	mLineTraceInfo->addData( MODE_VCIRCLE, YPOS_LT, 2000, SPEED_MAX, 770, 1950, 280);
	mLineTraceInfo->addData( MODE_VLINE,   YPOS_LT, 1300, SPEED_MAX,  820,1036, 0);
	mLineTraceInfo->addData( MODE_VCIRCLE, XPOS_GE, 1100, SPEED_MAX, 1100,1000, -200);
//	mLineTraceInfo->addData( MODE_VCIRCLE, YPOS_GE,  700, SPEED_MAX, 1100,1000, -200);
//	mLineTraceInfo->addData( MODE_VLINE,   XPOS_GE, 2000, SPEED_MAX, 2000,1150, 0);
	mLineTraceInfo->addData( MODE_VCIRCLE, YPOS_GE,  620, SPEED_MAX, 1100,1000, -200);
#if (ROBOTNO == 0) //城所機体
#elif (ROBOTNO == 2) //幾野機体
	mLineTraceInfo->addData( MODE_VLINE,   XPOS_GE, 2200, SPEED_MAX, 2250,1200, 0);
	mLineTraceInfo->addData( MODE_VLINE, CROSSLINE, 60, SPEED_MAX, 2550, 1310, 0); //黒線中央まで仮想直線
	mLineTraceInfo->addData( MODE_GOAL, XPOS_GE, 3800, SPEED_MAX, 1310, 3400, 40); //ライン復帰
	mLineTraceInfo->addData( MODE_VLINE2, YPOS_GE, 1350, SPEED_MAX, 4500, 1400, SPEED_LOW3);  //減速して少し左へ仮想直線

#elif (ROBOTNO == 6) //山口機体
	mLineTraceInfo->addData( MODE_VLINE,   XPOS_GE, 2100, SPEED_MAX, 2100,1200, 0);
	mLineTraceInfo->addData( MODE_VLINE, CROSSLINE, 60, SPEED_MAX, 2550, 1310, 0); //黒線中央まで仮想直線
	mLineTraceInfo->addData( MODE_GOAL, XPOS_GE, 3800, SPEED_MAX, 1310, 3400, 40); //ライン復帰
	mLineTraceInfo->addData( MODE_VLINE2, DIS_GE, 300, SPEED_MAX, 4500, 1400, SPEED_LOW3);  //減速して少し左へ仮想直線
#else
#endif

	mLineTraceInfo->addData( MODE_VLINE2, CROSSLINE, 60, SPEED_HIGH1, 5000, 1500, SPEED_LOW3);//仮想直線の目標座標変更し、黒線またいだら終わる
	mLineTraceInfo->addData( MODE_TURN,   CROSSLINE, 60,-SPEED_LOW3, 0,0,0);//少し後退
	mLineTraceInfo->addData( MODE_TURN,   DIS_LT, -10,-SPEED_LOW2, 0,0,0);//少し後退
//	mLineTraceInfo->addData( MODE_TURN,   DIS_LT, -10,-SPEED_LOW2, -15,0,0);//ターンしながら少し後退  オンラインなしで
	mLineTraceInfo->addData( MODE_TURN,   ON_LINE, 40,-SPEED_SLOW, -15,0,0);//ターンしながら少し後退 オンラインまで
//	mLineTraceInfo->addData( MODE_COLORL, ON_LINE, 40, SPEED_SLOW,  40,  0, 0);//ゆっくりライン復帰
	mLineTraceInfo->addData( MODE_COLORL, DIS_GE, 50, SPEED_SLOW,  40,  0, 0);//ゆっくりライン復帰

	//ゴール後左に曲がる〜階段の手前
	mLineTraceInfo->addData( MODE_COLORL,  YPOS_GE, 1800, SPEED_LOW3,  50,  0, POS_INIT); //階段の方を向くまでライントレース
	mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,  180,SPEED_NORMAL1,42,   0, POS_AJST);
	mLineTraceInfo->addData( MODE_VLINE,   DIS_GE,  250, SPEED_LOW3,  0,1000, 0);

	testSteps(firstSpin, secondSpin); //階段攻略

	addScene(sceneLineTrace);
}

void Course::safetyL(bool firstSpin, bool secondSpin){
	//Lコースショートカット用　ルートL2
	sceneLineTrace->setShiftX(-400); //地図のx座標調整(-400mm)

	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
	mLineTraceInfo->addData( MODE_COLORL1,DIS0_GE,  100, SPEED_HIGH1, 50, 0, SPEED_MAX); //走行距離に応じて加速
	mLineTraceInfo->addData( MODE_COLORL2,DIS0_GE, 2000, SPEED_MAX, 50, 0, POS_AJST); //高速走行？
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 2150, SPEED_MAX, 50, 0, 0); //L0～L1: 直線

	// スタートの直線以降～Gate2までの仮想円&仮想直線 //TODO：コースからそれるため要修正！(2017/9/23)
	mLineTraceInfo->addData( MODE_VCIRCLE, XPOS_GE,  780, SPEED_MAX,  765, 2118,   366); //L1～L2
	mLineTraceInfo->addData( MODE_VCIRCLE, YPOS_LT, 1795, SPEED_MAX,  765, 2118,   366); //L1～L2続き
	mLineTraceInfo->addData( MODE_VCIRCLE, YPOS_LT, 1045, SPEED_MAX, 1564,  807, -1168); //L2左カーブ
	mLineTraceInfo->addData( MODE_VLINE,   YPOS_LT,  700, SPEED_MAX,  420,  600,     0); //L2直線
	mLineTraceInfo->addData( MODE_VCIRCLE, XPOS_GE, 1300, SPEED_MAX, 1106,  803,  -693); //L2左カーブ(Gate2通過)

	mLineTraceInfo->addData( MODE_VLINE,   XPOS_GE, 1950, SPEED_MAX, 2000,1150, 0);
	mLineTraceInfo->addData( MODE_VLINE, CROSSLINE, 60, SPEED_MAX, 2350, 1310, 0); //黒線中央まで仮想直線
	mLineTraceInfo->addData( MODE_GOAL, XPOS_GE, 3800, SPEED_MAX, 1310, 3400, 40); //ライン復帰

	mLineTraceInfo->addData( MODE_VLINE2, DIS_GE, 300, SPEED_MAX, 4500, 1400, SPEED_LOW3);  //減速して少し左へ仮想直線
	mLineTraceInfo->addData( MODE_VLINE2, CROSSLINE, 60, SPEED_HIGH1, 5000, 1500, SPEED_LOW3);//仮想直線の目標座標変更し、黒線またいだら終わる
	mLineTraceInfo->addData( MODE_TURN,   CROSSLINE, 60,-SPEED_LOW3, 0,0,0);//少し後退
	mLineTraceInfo->addData( MODE_TURN,   DIS_LT, -10,-SPEED_LOW3, 0,0,0);//少し後退
	mLineTraceInfo->addData( MODE_COLORL, ON_LINE, 40, SPEED_SLOW,  40,  0, 0);//ゆっくりライン復帰

	//ゴール後左に曲がる〜階段の手前
	mLineTraceInfo->addData( MODE_COLORL,  YPOS_GE, 1800, SPEED_LOW3,  50,  0, POS_INIT); //階段の方を向くまでライントレース
	mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,  180,SPEED_NORMAL1,42,   0, POS_AJST);
	mLineTraceInfo->addData( MODE_VLINE,   DIS_GE,  250, SPEED_LOW3,  0,1000, 0);

	testSteps(firstSpin, secondSpin); //階段攻略

	addScene(sceneLineTrace);
}

void Course::aggressiveR(bool gateDouble){
	//Rコースショートカット用　ルートR２
	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
	mLineTraceInfo->addData( MODE_COLORL1, DIS0_GE,  100, SPEED_HIGH1, 50,  0, SPEED_MAX); //走行距離に応じて加速
	mLineTraceInfo->addData( MODE_COLORL,  DIS0_GE, 2000, SPEED_MAX,   50,  0, POS_AJST); //L0～L1: 直線
	mLineTraceInfo->addData( MODE_COLORL,  DIS0_GE, 2010, SPEED_MAX,   50,  0, 0); //L0～L1: 直線
	mLineTraceInfo->addData( MODE_VCIRCLE, XPOS_GE,  764, SPEED_MAX,  764, 2107, 600);
	mLineTraceInfo->addData( MODE_VCIRCLE, YPOS_LT, 1600, SPEED_MAX,  764, 2107, 600);
	mLineTraceInfo->addData( MODE_VLINE,   YPOS_LT, 1300, SPEED_MAX,  720, 1036, 0);
	mLineTraceInfo->addData( MODE_VCIRCLE, XPOS_GE, 1100, SPEED_MAX, 1100, 1000, -200);
	mLineTraceInfo->addData( MODE_VCIRCLE, YPOS_GE,  700, SPEED_MAX, 1100, 1000, -200);
	mLineTraceInfo->addData( MODE_VLINE,   XPOS_GE, 2000, SPEED_MAX, 2000,1150, 0);
	//mLineTraceInfo->addData( MODE_GOAL,    XPOS_GE, 4200, SPEED_MAX, 2450,1310, 1310);
	mLineTraceInfo->addData( MODE_VLINE, CROSSLINE, 60, SPEED_MAX, 2350, 1310, 0);
	mLineTraceInfo->addData( MODE_GOAL, XPOS_GE, 3800, SPEED_MAX, 1310, 3400, 40);
	mLineTraceInfo->addData( MODE_VLINE2, DIS_GE, 300, SPEED_MAX, 4500, 1500, SPEED_NORMAL1);  //減速
//	mLineTraceInfo->addData( MODE_VLINE2, YPOS_GE, 60, SPEED_HIGH1, 5000, 2000, SPEED_NORMAL1);

	//ライン復帰
	mLineTraceInfo->addData( MODE_SPIN, NONE, 0, SPEED_HIGH4, -110,-20,2);
	//mLineTraceInfo->addData( MODE_VLINE, YPOS_GE, 2100, SPEED_LOW3, 4300, 2300, 0);
	mLineTraceInfo->addData( MODE_VLINE, CROSSLINE, 60, SPEED_LOW3, 4300, 2300, 0); //黒線検知するまで仮想直線
//	mLineTraceInfo->addData( MODE_TURN,  DIS_GE,  20, SPEED_SLOW, 0,0,0);//少し前進
	pause(1000);
	mLineTraceInfo->addData( MODE_TURN,  ON_LINE, 40,-SPEED_SLOW, 0,0,0);//後退してライン検知
	mLineTraceInfo->addData( MODE_TURN,  DIS_GE,  10, SPEED_SLOW, 0,0,0);//少し前進
	mLineTraceInfo->addData( MODE_SPIN,  ON_LINESPIN, 40, SPEED_NORMAL2, 30,20,2); //右に旋回
//	mLineTraceInfo->addData( MODE_TURN,  ON_LINE, 40,-SPEED_SLOW, 0,0,0);//後退してライン検知
//	mLineTraceInfo->addData( MODE_TURN,  DIS_LT,  10,-SPEED_SLOW, 0,0,0); //少し後退
	mLineTraceInfo->addData( MODE_COLORL,DIS_GE,  50, SPEED_SLOW, 50,  0, 0); //ライン復帰

	testGate(gateDouble); //ゲート攻略

	addScene(sceneLineTrace);
}

void Course::safetyR(bool gateDouble){
	//Rコースショートカット用
	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
	mLineTraceInfo->addData( MODE_COLORL1, DIS0_GE,  100, SPEED_HIGH1, 50,  0, SPEED_MAX); //走行距離に応じて加速
	mLineTraceInfo->addData( MODE_COLORL,  DIS0_GE, 2000, SPEED_MAX,   50,  0, POS_AJST); //L0～L1: 直線
	mLineTraceInfo->addData( MODE_COLORL,  DIS0_GE, 2010, SPEED_MAX,   50,  0, 0); //L0～L1: 直線
	mLineTraceInfo->addData( MODE_VCIRCLE, XPOS_GE,  764, SPEED_MAX,  764, 2107, 600);
	mLineTraceInfo->addData( MODE_VCIRCLE, YPOS_LT, 1600, SPEED_MAX,  764, 2107, 600);
	mLineTraceInfo->addData( MODE_VLINE,   YPOS_LT, 1300, SPEED_MAX,  720, 1036, 0);
	mLineTraceInfo->addData( MODE_VCIRCLE, XPOS_GE, 1100, SPEED_MAX, 1100, 1000, -200);
	mLineTraceInfo->addData( MODE_VCIRCLE, YPOS_GE,  700, SPEED_MAX, 1100, 1000, -200);

	mLineTraceInfo->addData( MODE_VLINE,   XPOS_GE, 1750, SPEED_MAX, 2000, 1713, 0);

	mLineTraceInfo->addData( MODE_VLINE, CROSSLINE, 60, SPEED_MAX, 2500, 1713, 0); //TODO: 目標x座標を2400にしてみる？
	mLineTraceInfo->addData( MODE_GOAL, XPOS_GE, 3800, SPEED_MAX, 1713, 3400, 40);
	mLineTraceInfo->addData( MODE_VLINE2, DIS_GE, 500, SPEED_MAX, 4500, 1750, SPEED_HIGH1);  //減速して仮想直線

	//ライン復帰
	mLineTraceInfo->addData( MODE_SPIN,  NONE,     0, SPEED_MAX, 110,-20,2);
	mLineTraceInfo->addData( MODE_VLINE, DIS_GE,  50, SPEED_LOW2, 4300, 2200, 0);
	//mLineTraceInfo->addData( MODE_VLINE, ON_LINE, 80, SPEED_LOW1, 4300, 2300, 0); //黒線検知するまで仮想直線
	mLineTraceInfo->addData( MODE_VLINE, CROSSLINE, 60, SPEED_LOW1, 4300, 2300, 0);
	mLineTraceInfo->addData( MODE_COLORL,DIS_GE,  50, SPEED_SLOW, 50,  0, 0); //ライン復帰

	testGate(gateDouble); //ゲート攻略

	addScene(sceneLineTrace);
}


void Course::testSteps(bool firstSpin, bool secondSpin){
#ifdef AJUSTMENT
		addScene(sceneLineTrace);

		//テスト時はゴール後灰色の後から開始すること
/*		mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
		mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,   150, SPEED_LOW3,  42,   0, POS_AJST);
//		mLineTraceInfo->addData( MODE_COLORL,  XPOS_GE, -450, SPEED_LOW3,  42,   0, POS_INIT); //GrayLineから移行
//		mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,   450,SPEED_NORMAL1,42,   0, POS_AJST);
		mLineTraceInfo->addData( MODE_COLORL,  XPOS_LT, -400, SPEED_LOW3,  42,   0, POS_INIT); //0913城所修正
		mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,   250,SPEED_NORMAL1,42,   0, POS_AJST);//0913城所修正
		mLineTraceInfo->addData( MODE_VLINE,   DIS_GE,   300, SPEED_NORMAL1,  0,1000, 0);
		//	mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,   100, SPEED_LOW2,  33,   0, 0);
*/
		//階段直前30cmくらいから（自宅環境用）
		mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
		mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,   250,SPEED_NORMAL1,42,   0, POS_AJST);

#endif
		ev3_speaker_play_tone(NOTE_C5, 50);
	//ここから階段用データ
	mLineTraceInfo->addData( MODE_COLORL,  VIB_GE,   50, SPEED_LOW3,  40,   0, 0); //SceneStepsから移行
	mLineTraceInfo->addData( MODE_TURN,    DIS_LT, -120,-SPEED_LOW3, 0,0,0);
	mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,   20, SPEED_LOW1,  40,   0, 0); //まっすぐにする

	if(firstSpin == true){
#if 1
		//1段ずつ登る
		mLineTraceInfo->addData( MODE_COLORL, COUNTUP,  250, SPEED_HIGH4,   40,   0, 0);// ここで１段目を登る
//		mLineTraceInfo->addData( MODE_COLORL,  COUNTUP,  10, -200,  40,   0, 0); //一瞬逆走
		pause(2000);
		mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,   100, SPEED_SLOW,  40,   0, 0); //ライン復帰
		pause(1500);
#else
		//1段ずつ登る　登った後前の２段目にぶつかってから後退
		mLineTraceInfo->addData( MODE_COLORL, COUNTUP,  250, SPEED_HIGH4,   40,   0, 0);
		mLineTraceInfo->addData( MODE_TURN,   COUNTUP, 1500,SPEED_ZERO, 0,0,0); //少し静止
		mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,   50, SPEED_SLOW,  40,   0, 0); //ライン復帰
		mLineTraceInfo->addData( MODE_COLORL,  VIB_GE,   50, SPEED_NORMAL3,  40,   0, 0);
//		pause(2000);
		mLineTraceInfo->addData( MODE_TURN,    DIS_LT, -120,-SPEED_LOW3, 0,0,0);
		pause(2000);
#endif

#if 1
		mLineTraceInfo->addData( MODE_SPIN, NONE, 0, SPEED_HIGH4,  30,-20,2); //360度以上スピンするため少し逆回転する。
		mLineTraceInfo->addData( MODE_TURN, COUNTUP, 1, 0, 0, 0, POS_INIT); //スピン2のための位置初期化
		pause(400); //少し静止 城所追加
		//mLineTraceInfo->addData( MODE_SPIN, ON_LINESPIN, 40, SPEED_HIGH4, 360,20,2); // 1段目スピン　城所修正

		mLineTraceInfo->addData( MODE_SPIN2, NONE, 1, SPEED_HIGH4, 90,20,2);
		pause(500);
		mLineTraceInfo->addData( MODE_SPIN2, NONE,-1,-SPEED_HIGH4, 90,20,2);
		pause(500);
		mLineTraceInfo->addData( MODE_SPIN2, NONE,-1, SPEED_HIGH4, 90,20,2);
		pause(500);
		mLineTraceInfo->addData( MODE_SPIN2, NONE, 1,-SPEED_HIGH4, 80,20,2);
		pause(500);
		mLineTraceInfo->addData( MODE_SPIN, ON_LINESPIN, 40, SPEED_HIGH4, 10,20,2);
#else
		// スピン
		mLineTraceInfo->addData( MODE_SPIN,   COUNTUP,  350, SPEED_HIGH4, 90,-20,2,0.9F);//
		pause(400); //少し静止 城所追加
		mLineTraceInfo->addData( MODE_SPIN,   ON_LINESPIN,  40, SPEED_HIGH4, 400,50,2,0.9F);//残りの回転
#endif
		mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,   50, SPEED_SLOW,  40,   0, 0); //ライン復帰
		mLineTraceInfo->addData( MODE_COLORL,  VIB_GE,   50, SPEED_LOW3,  40,   0, 0); //２段目の段差を検知する
		mLineTraceInfo->addData( MODE_TURN,    DIS_LT, -120,-SPEED_LOW3, 0,0,0);//２段目を登るための助走をとる
		mLineTraceInfo->addData( MODE_COLORL,  DIS_GE,   20, SPEED_LOW1,  40,   0, 0); //まっすぐにする
		mLineTraceInfo->addData( MODE_COLORL, COUNTUP,  250, SPEED_HIGH4,   40,   0, 0);//２段目を登る
	} else {
		//2段目まで一気に登る
		mLineTraceInfo->addData( MODE_COLORL, COUNTUP,  570, SPEED_HIGH4,   40,   0, 0);
	}

	pause(4000); //直角より先で少し静止
	mLineTraceInfo->addData( MODE_TURN,   ON_LINE,  40,-SPEED_LOW1, 0,0,0); //後退して黒線を探す TODO:黒線よりも右側にいたらコースアウト！
	mLineTraceInfo->addData( MODE_TURN,    DIS_GE,  20, SPEED_LOW1, 0,0,0); //スピンして脱輪しないよう先に位置調整

	if(secondSpin == true){
		//mLineTraceInfo->addData( MODE_SPIN,  ON_LINESPIN,  40, SPEED_MAX, 360+90-30,-20,2);//0927修正
		mLineTraceInfo->addData( MODE_TURN, COUNTUP, 1, 0, 0, 0, POS_INIT); //スピン2のための位置初期化
		mLineTraceInfo->addData( MODE_SPIN2, NONE,-1, SPEED_HIGH4, 90,-20,2);
		pause(700);
		mLineTraceInfo->addData( MODE_SPIN2, NONE, -1,-SPEED_HIGH4, 90,-20,2);
		pause(700);
		mLineTraceInfo->addData( MODE_SPIN2, NONE, 1, SPEED_HIGH4, 90,-20,2);
		pause(700);
		mLineTraceInfo->addData( MODE_SPIN2, NONE,1,-SPEED_HIGH4, 90,-20,2);
		pause(700);
		mLineTraceInfo->addData( MODE_SPIN, ON_LINESPIN, 40, SPEED_HIGH4, 60,-20,2);
	} else {
		mLineTraceInfo->addData( MODE_SPIN,   ON_LINESPIN,  40, SPEED_MAX, 90-10,-20,2,0.9F);//0927修正
	}

	mLineTraceInfo->addData( MODE_COLORR, ON_LINE,  40, SPEED_LOW1, 40,0,0); //ライン復帰するまで右側ライントレース
	mLineTraceInfo->addData( MODE_COLORR, DIS_GE,   30, SPEED_LOW1, 40,0,0);
	mLineTraceInfo->addData( MODE_COLORR, DIS_GE,  180,SPEED_HIGH1, 40,0,0); //階段から速度上げて降りる
	pause(6000); //少しその場で停止

	mLineTraceInfo->addData( MODE_BACKLINE, 0, 40,SPEED_LOW2, 10,90,0); //仮想円によるライン復帰

	mLineTraceInfo->addData( MODE_COLORL, DIS_GE, 100, SPEED_SLOW,  5,0,POS_INIT); //かなり黒よりで左側ライントレース
	mLineTraceInfo->addData( MODE_COLORL, DIS_GE, 320, SPEED_LOW2,  5,0,POS_AJST);
	mLineTraceInfo->addData( MODE_GRAY,  GRAY_GE, 325, SPEED_LOW2,60,180,1.2);

	stopGarage(); //ガレージ停止
}


void Course::testGate(bool doubleGate){
#ifdef AJUSTMENT
		//テスト時のみ
		mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
		addScene(sceneLineTrace);
#endif

	//ゲート攻略シングル
	mLineTraceInfo->addData( MODE_COLORL,  SONAR_LT, 80, SPEED_LOW3,   45,  0, POS_INIT);
	mLineTraceInfo->addData( MODE_COLORL,  SONAR_LT, 60, SPEED_LOW3,   45,  0, POS_AJST);
	mLineTraceInfo->addData( MODE_VLINE,   SONAR_LT, 15, SPEED_NORMAL1, 0, 3000, 0);
    mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, 15, 70, true, 0);
    mLineTraceInfo->addData( MODE_TAILVLINE,YPOS_GE, 1000, SPEED_LOW1, 0, 3000, 0);
    mLineTraceInfo->addData( MODE_TAILUP,  TAIL_ANGLE, 95, 15, 70, 0, 0);
    mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW1, 180,20,0);
    mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, 15, 70, false, 0);
    mLineTraceInfo->addData( MODE_TAILVLINE, YPOS_LT, 600 , SPEED_LOW1, 0, -3000, 0);//TODO
    mLineTraceInfo->addData( MODE_TAILUP,  TAIL_ANGLE, 95, 15, 70, 0, 0);
    mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW1, 180,20,0);

    mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, 15, 70, false, 0);
    mLineTraceInfo->addData( MODE_TAILVLINE, YPOS_GE, 1000, SPEED_LOW1, 0, 3000, 0);//TODO

    mLineTraceInfo->addData( MODE_TAILUP,  TAIL_ANGLE, 85, SPEED_LOW1_5, 375, 0, 0);

    /* 2点復帰  */
#if 0
	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 100, 0, 0, 0, 0); //スタートダッシュ
	mLineTraceInfo->addData( MODE_TURN, DIS_GE, 100, SPEED_HIGH1, 0,0,0, 0.5F); //低速度だと後退するため、ある程度速度出す
	mLineTraceInfo->addData( MODE_TURN, DIS_GE, 300, SPEED_NORMAL2, 0, 0, 0);

	mLineTraceInfo->addData( MODE_TURN, DIS_LT, -400, -SPEED_HIGH1, 0,0,0);

	/* ガレージイン */
	mLineTraceInfo->addData( MODE_BACKLINE, 0, 40,SPEED_LOW2, 10,90,0); //仮想円によるライン復帰
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE,   50, SPEED_SLOW, 40,0,0);
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 2000, SPEED_LOW2, 40,0,0);

	/* ガレージ停止 */
	stopGarage(); //ガレージ停止
#else
	//65cmを3点仮想直線
/*
	mLineTraceInfo->addData( MODE_TAILVLINE, DIS_GE, 300, SPEED_LOW3, 0, 3000, 1.5);
	mLineTraceInfo->addData( MODE_TAILVLINE, DIS_GE, 250, SPEED_LOW2, 0, 3000, 1.5);
	mLineTraceInfo->addData( MODE_TAILVLINE, DIS_GE, 100, SPEED_LOW1, 0, 3000, 1.5);
*/
    mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW1, 25,-20,0);
	mLineTraceInfo->addData( MODE_TAILVLINE, DIS_GE, 300, SPEED_LOW1, -5000, 1600, 1.5);
	mLineTraceInfo->addData( MODE_TAILVLINE, DIS_GE, 250, SPEED_LOW1, -5000, 1600, 1.5);
	mLineTraceInfo->addData( MODE_TAILVLINE, DIS_GE, 80, SPEED_LOW1, -5000, 1600, 1.5);

	mLineTraceInfo->addData( MODE_TAILVLINE, NONE, 0, SPEED_ZERO, 0, 3000, 0); //ガレージ停止
#endif

}

void Course::testSpin(){
	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 100, SPEED_HIGH4,   50,  0, POS_AJST);
	pause(4000);

#if 0 //右回り
	//mLineTraceInfo->addData( MODE_SPIN2,   ON_LINESPIN,  40, SPEED_HIGH4, 360+90-20,15,2,0.9F);
	mLineTraceInfo->addData( MODE_SPIN2, NONE, 1, SPEED_HIGH4, 90,20,2);
	mLineTraceInfo->addData( MODE_SPIN2, NONE,-1,-SPEED_HIGH4, 90,20,2);
	mLineTraceInfo->addData( MODE_SPIN2, NONE,-1, SPEED_HIGH4, 90,20,2);
	mLineTraceInfo->addData( MODE_SPIN2, NONE, 1,-SPEED_HIGH4, 90,20,2);
	//mLineTraceInfo->addData( MODE_SPIN, ON_LINESPIN, 40, SPEED_HIGH4, 90,20,2);
#else //左回り
	mLineTraceInfo->addData( MODE_SPIN2, NONE,-1, SPEED_HIGH4, 90,-20,2);
	mLineTraceInfo->addData( MODE_SPIN2, NONE, 1,-SPEED_HIGH4, 90,-20,2);
	mLineTraceInfo->addData( MODE_SPIN2, NONE, 1, SPEED_HIGH4, 90,-20,2);
	mLineTraceInfo->addData( MODE_SPIN2, NONE,-1,-SPEED_HIGH4, 90,-20,2);
	mLineTraceInfo->addData( MODE_SPIN2, NONE,-1, SPEED_HIGH4, 90,-20,2);
	//mLineTraceInfo->addData( MODE_SPIN, ON_LINESPIN, 40, SPEED_HIGH4, 60,-20,2);
#endif

	pause(4000);



	stopGarage(); //ガレージ停止

	addScene(sceneLineTrace);
}

void Course::stopGarage(){
	mLineTraceInfo->addData( MODE_TURN, TAIL_ANGLE, 80, SPEED_ZERO, 0,0,0); //尻尾が目標角度になるまで静止
	mLineTraceInfo->addData( MODE_TAILRUN, COUNTUP, 60, SPEED_LOW3, 0,0,POS_END); //若干前に出る
}

void Course::paperCourse(){
	//紙の練習コース走行用
#if 0
	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE,  100, SPEED_NORMAL2, 33, 0, 0);
	mLineTraceInfo->addData( MODE_COLORL2,DIS0_GE,  250, SPEED_NORMAL2, 33, 0, POS_AJST);
//	mLineTraceInfo->addData( MODE_COLORL2,DIS0_GE, 2000, SPEED_NORMAL2, 33, 0, 0);
//	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 3000, SPEED_NORMAL2, 33, 0, 0);
//	mLineTraceInfo->addData( MODE_COLORL2,DIS0_GE, 4000, SPEED_NORMAL2, 33, 0, 0);

	mLineTraceInfo->addData( MODE_VCIRCLE, XPOS_GE, 100, SPEED_NORMAL2,  100, 250, -100);
	mLineTraceInfo->addData( MODE_VCIRCLE, YPOS_LT, 250, SPEED_NORMAL2,  100, 250, -100);
	mLineTraceInfo->addData( MODE_VLINE,   DIS_GE, 500, SPEED_NORMAL2,  200, 0, 0);
#else//171222　デモ用シナリオ（布コース）
	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //0.スタートダッシュ
//	mLineTraceInfo->addData( MODE_COLORL1, DIS0_GE,  1000, SPEED_LOW3, 33, 0, SPEED_MAX);
//	mLineTraceInfo->addData( MODE_COLORL2,DIS0_GE,  250, SPEED_NORMAL2, 33, 0, POS_AJST);
	mLineTraceInfo->addData( MODE_COLORL,DIS0_GE, 50, SPEED_LOW1, 33, 0, 0); //1.出だし抑えめ
	ev3_speaker_play_tone(NOTE_A4, 50);
	mLineTraceInfo->addData( MODE_COLORL,DIS0_GE, 150, SPEED_NORMAL1, 33, 0, POS_AJST); //2.位置補正
	ev3_speaker_play_tone(NOTE_A4, 50);
	mLineTraceInfo->addData( MODE_COLORL,XPOS_GE, 450, SPEED_HIGH1, 33, 2.0, 0);//3.最初のカーブ
	ev3_speaker_play_tone(NOTE_C5, 50);
	mLineTraceInfo->addData( MODE_COLORL,YPOS_LT, 430, SPEED_LOW3, 33, 2.0, 0);//4. S字1 右
	ev3_speaker_play_tone(NOTE_C4, 50);
	mLineTraceInfo->addData( MODE_COLORL,YPOS_LT, 330, 15, 50, 0, 0);//4. S字2左
	ev3_speaker_play_tone(NOTE_C4, 50);
	mLineTraceInfo->addData( MODE_COLORL,YPOS_LT, 0, SPEED_NORMAL1, 33, 2.0, 0);//4. S字3 右
	ev3_speaker_play_tone(NOTE_C4, 50);
	mLineTraceInfo->addData( MODE_COLORL,YPOS_LT, -150, SPEED_HIGH1, 33, 0, 0);//5. 第3コーナー前　直線
	ev3_speaker_play_tone(NOTE_C5, 50);
	mLineTraceInfo->addData( MODE_COLORL,DIS_GE, 350, SPEED_NORMAL1, 33, 2.0, 0);//6. 第3コーナー　９０度カーブ
	ev3_speaker_play_tone(NOTE_C5, 500);
	mLineTraceInfo->addData( MODE_COLORL,XPOS_LT, 350, SPEED_HIGH3, 33, 0, 0);//7. 直線
	ev3_speaker_play_tone(NOTE_A4, 50);
	mLineTraceInfo->addData( MODE_COLORL,DIS_GE, 350, SPEED_NORMAL1, 33, 2.0, 0);//8. 第4コーナー
	ev3_speaker_play_tone(NOTE_A4, 50);
	mLineTraceInfo->addData( MODE_COLORL,YPOS_GE, 0, SPEED_HIGH3, 33, 0, 0);//9. 直線（スタートライン前）
	ev3_speaker_play_tone(NOTE_A4, 50);

	// ２周目
	mLineTraceInfo->addData( MODE_COLORL,XPOS_GE, 450, SPEED_HIGH1, 33, 2.0, 0);//3.最初のカーブ
	ev3_speaker_play_tone(NOTE_C5, 50);
	mLineTraceInfo->addData( MODE_COLORL,YPOS_LT, 430, SPEED_LOW3, 33, 2.0, 0);//4. S字1 右
	ev3_speaker_play_tone(NOTE_C4, 50);
	mLineTraceInfo->addData( MODE_COLORL,YPOS_LT, 330, 15, 50, 0, 0);//4. S字2左
	ev3_speaker_play_tone(NOTE_C4, 50);
	mLineTraceInfo->addData( MODE_COLORL,YPOS_LT, 0, SPEED_NORMAL1, 33, 2.0, 0);//4. S字3 右
	ev3_speaker_play_tone(NOTE_C4, 50);
	mLineTraceInfo->addData( MODE_COLORL,YPOS_LT, -150, SPEED_HIGH1, 33, 0, 0);//5. 第3コーナー前　直線
	ev3_speaker_play_tone(NOTE_C5, 50);
	mLineTraceInfo->addData( MODE_COLORL,DIS_GE, 350, SPEED_NORMAL1, 33, 2.0, 0);//6. 第3コーナー　９０度カーブ
	ev3_speaker_play_tone(NOTE_C5, 500);
	mLineTraceInfo->addData( MODE_COLORL,XPOS_LT, 350, SPEED_HIGH3, 33, 0, 0);//7. 直線
	ev3_speaker_play_tone(NOTE_A4, 50);
	mLineTraceInfo->addData( MODE_COLORL,DIS_GE, 350, SPEED_NORMAL1, 33, 2.0, 0);//8. 第4コーナー
	ev3_speaker_play_tone(NOTE_A4, 50);
	mLineTraceInfo->addData( MODE_COLORL,YPOS_GE, 0, SPEED_HIGH3, 33, 0, 0);//9. 直線（スタートライン前）
	ev3_speaker_play_tone(NOTE_A4, 50);


	#endif

	stopGarage(); //ガレージ停止

	addScene(sceneLineTrace);
}

void Course::testBackLine(){
	//ライン復帰テスト

	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
	pause(3000);
	mLineTraceInfo->addData( MODE_BACKLINE, 0, 40,SPEED_LOW2, 10,90,0); //仮想円によるライン復帰

	mLineTraceInfo->addData( MODE_COLORL, DIS_GE, 100, SPEED_SLOW,  5,0,POS_INIT); //かなり黒よりで左側ライントレース
	mLineTraceInfo->addData( MODE_COLORL, DIS_GE, 320, SPEED_LOW2,  5,0,POS_AJST);
	mLineTraceInfo->addData( MODE_GRAY,  GRAY_GE, 325, SPEED_LOW2,40,180,3);

	stopGarage(); //ガレージ停止

	addScene(sceneLineTrace);
}


void Course::testWalker(){
	//2点バランス走行
	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE,  250, SPEED_NORMAL2, 33, 0, POS_AJST);

	//3点に移行
	mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, SPEED_LOW1_5, 70, true, 0);
	mLineTraceInfo->addData( MODE_TAILUP,  TAIL_ANGLE, 85, SPEED_LOW1_5, 375, 0, 0);

	//2点に移行
	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, 0); //スタートダッシュ
	mLineTraceInfo->addData( MODE_TURN, DIS_GE, 100, SPEED_HIGH1, 0,0,0, 0.5F); //低速度だと後退するため、ある程度速度出す
	mLineTraceInfo->addData( MODE_COLORL, DIS_GE,  300, SPEED_NORMAL2, 40, 0, 0); //ライン復帰

	stopGarage();

	addScene(sceneLineTrace);

}

void Course::pause(int time){
	mLineTraceInfo->addData( MODE_TURN, COUNTUP, time/4, SPEED_ZERO, 0,0,0); //その場で停止
}

void Course::pause3(int time){
	mLineTraceInfo->addData( MODE_TAILCOLORL, COUNTUP, time/4, SPEED_ZERO, 0,0,0); //その場で停止
}

void Course::ajustment(int id){
	switch(id){
	case 0:
		//2点実ライントレース
		mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
		mLineTraceInfo->addData( MODE_COLORL,  DIS0_GE, 300, SPEED_LOW3,   50,  0, POS_AJST); //L0～L1: 直線
		mLineTraceInfo->addData( TEST_COLOR,   DIS_GE, 2000, SPEED_MAX, 50,0, 0);
		stopGarage(); //ガレージ停止
		break;
	case 1:
		//2点仮想直線トレース
		mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
		mLineTraceInfo->addData( MODE_COLORL,  DIS0_GE, 300, SPEED_LOW3,   50,  0, POS_AJST); //L0～L1: 直線
		mLineTraceInfo->addData( TEST_VLINE,   DIS_GE, 2000, SPEED_HIGH1, 0,10000, 0);
		stopGarage(); //ガレージ停止
		break;
	case 2:
		//3点仮想直線トレース
		mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
		mLineTraceInfo->addData( MODE_COLORL,  DIS0_GE, 300, SPEED_LOW3,   50,  0, POS_AJST); //L0～L1: 直線
		mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, SPEED_LOW1_5, 70, true, 0);
		mLineTraceInfo->addData( TEST_TAILVLINE,   DIS_GE, 2000, SPEED_MAX, 0,10000, 0);
		stopGarage(); //ガレージ停止
		break;
	case 3:
		//TODO: 3点実ライントレース？
		break;
	case 4:
		mLineTraceInfo->addData( TEST_MOTOR, BATTERY_LT, 8000, SPEED_MAX, SPEED_HIGH1, 10, 4000);
		break;
	case 5:
		mLineTraceInfo->addData( TEST_MOTOR, BATTERY_LT, 8000, SPEED_MAX, 10, 0, 0);
		break;
	default:
		break;
	}

	addScene(sceneLineTrace);
}


Course::ECource Course::getCourse(){
	return mCourse;
}


void Course::addScene(Scene* scene){
	scene->printClassName();

	mScenario.push_back(scene);
}

void Course::nextScene(){
	mScenario.front()->stop(); //現シーンの終了処理

	mScenario.pop_front(); //先頭シーンを削除

	if( mScenario.size() > 0 ){
		mScenario.front()->printClassName();
	}

}



void Course::run(){
	if( mScenario.size() > 0 ){
		//シーン走行
		if( mScenario.front()->run() == false ){
			nextScene(); //次のシーンへ
		}
	} else {
		//コース完走
		wup_tsk(MAIN_TASK);
	}

	//尻尾モータ制御
	mTailMotor->run();
}

void Course::testUturn(){
    //Uターンテスト
    const int Sw = 1;   /*テストケース切り替えスイッチ  */
    switch (Sw) {
    case 0: /* 180度Uturn   */
        mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT);
        mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, 15, 70, true, 0);
        mLineTraceInfo->addData( MODE_TAILVLINE, YPOS_GE, 30 , SPEED_LOW1, 0, 3000, 0);
        mLineTraceInfo->addData( MODE_TAILUP,  TAIL_ANGLE, 95, 15, 70, 0, 0);
        mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW1, 180,20,0);
        mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, 15, 70, false, 0);
        mLineTraceInfo->addData( MODE_TAILVLINE, YPOS_GE, 60 , SPEED_LOW1, 0, 3000, 0);
        mLineTraceInfo->addData( MODE_TAILUP,  TAIL_ANGLE, 95, 15, 70, 0, 0);
        mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW1, 180,20,0);
        mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, 15, 70, false, 0);
        mLineTraceInfo->addData( MODE_TAILVLINE, YPOS_GE, 90 , SPEED_LOW1, 0, 3000, 0);
        mLineTraceInfo->addData( MODE_TAILUP,  TAIL_ANGLE, 95, 15, 70, 0, 0);
        break;
    case 1:
        mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT);
        mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, 15, 70, true, 0);
        mLineTraceInfo->addData( MODE_TAILVLINE, YPOS_GE, 500 , SPEED_LOW1, 0, 3000, 0);
        mLineTraceInfo->addData( MODE_TAILUP,  TAIL_ANGLE, 95, 15, 70, 0, 0);
        mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW1, 185,20,0);
        mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, 15, 70, false, 0);
        mLineTraceInfo->addData( MODE_TAILVLINE, YPOS_LT, -100 , SPEED_LOW1, 0, 3000, 0);
        mLineTraceInfo->addData( MODE_TAILUP,  TAIL_ANGLE, 95, 15, 70, 0, 0);
        mLineTraceInfo->addData( MODE_SPIN3, NONE, 0, SPEED_LOW1, 185,20,0);
        mLineTraceInfo->addData( MODE_TAILDOWN,TAIL_ANGLE, 61, 15, 70, false, 0);
        mLineTraceInfo->addData( MODE_TAILVLINE, YPOS_GE, 500 , SPEED_LOW1, 0, 3000, 0);
        mLineTraceInfo->addData( MODE_TAILUP,  TAIL_ANGLE, 95, 15, 70, 0, 0);
        mLineTraceInfo->addData( MODE_TAILVLINE, NONE, 0 , 0, 0, 0, 0);
        break;
    default:
        break;
    }
    addScene(sceneLineTrace);
}

#if 0 //本番で使用しないためメンテ停止 2017.9.9 yamaguchi
void Course::basicL(){
	/* Lコースのシーンを追加 */
	sceneLineTrace->setShiftX(-400); //地図のx座標調整(-400mm)

	//高速走行へ要変更
	mLineTraceInfo->addData( MODE_STARTDASH, TAIL_ANGLE, 95, 0, 0, 0, POS_INIT); //スタートダッシュ
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 2000, SPEED_MAX,   50,  0, POS_AJST); //L0～L1: 直線
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 2150, SPEED_MAX,   50,  0, 0); //L0～L1: 直線
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 3776, SPEED_MAX,   42,  1, 0); //L1～L2:右カーブ
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 5987, SPEED_MAX,   71, -2, 0); //L2～L3:左カーブ
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 6800, SPEED_MAX,   71,  0, 0);
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 7231, SPEED_HIGH1, 78, -1, 0); //白よりS字
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 7500, SPEED_HIGH1, 78, -1, 0);
	mLineTraceInfo->addData( MODE_COLORL, DIS0_GE, 7700, SPEED_HIGH1, 45,  0, 0); //黒よりS字
	mLineTraceInfo->addData( MODE_COLORL, XPOS_GE, 2050, SPEED_HIGH1, 42,  0, POS_INIT); //黒よりS字
	mLineTraceInfo->addData( MODE_COLORL, DIS_GE,  1430, SPEED_MAX,   33,  0, POS_AJST); //GoStraightから移行
	mLineTraceInfo->addData( MODE_VLINE,  DIS_GE,   200, SPEED_HIGH2, 0,3000, 0);
	mLineTraceInfo->addData( MODE_COLORL, DIS_GE,   200, SPEED_LOW2,  42,  0, 0); //ライン復帰
	mLineTraceInfo->addData( MODE_COLORL, XPOS_LT, -450, SPEED_LOW3,  42,  0, POS_INIT); //GrayLineから移行
	mLineTraceInfo->addData( MODE_COLORL, DIS_GE,   210, SPEED_NORMAL1, 42,0, POS_AJST);
	mLineTraceInfo->addData( MODE_VLINE,  DIS_GE,   250, SPEED_HIGH2,  0,1000,0);
	mLineTraceInfo->addData( MODE_COLORL, DIS_GE,   100, SPEED_LOW2,  33,   0,0);
	//shortcutLと同じ階段～ガレージの走行区間情報を要追加

	addScene(sceneLineTrace);
}
#endif

#if 0
void Course::basicR(){
	/* Rコースのシーンを追加 */

	mLineTraceInfo->addColorData( DIS0_GE, 2000, SPEED_MAX,   50, 0, POS_AJST); //L0～L1: 直線
	mLineTraceInfo->addColorData( DIS0_GE, 2150, SPEED_MAX,   50, 0, 0); //R0～R1: 直線
	mLineTraceInfo->addColorData( DIS0_GE, 5362, SPEED_MAX,   41, 0, 0); //R1～R2:右カーブ
	mLineTraceInfo->addColorData( DIS0_GE, 6081, SPEED_MAX,   64, 0, 0); //R2～R3ちょっと前
	mLineTraceInfo->addColorData( DIS0_GE, 6181, SPEED_HIGH2, 64, 0, 0); //R3少し手前～R3:左カーブ
	mLineTraceInfo->addColorData( DIS0_GE, 7135, SPEED_HIGH2, 78, 0, 0); //R3～R4
	mLineTraceInfo->addColorData( XPOS_GE, 2450, SPEED_MAX,   41, 0, POS_INIT); //R4～R5
	mLineTraceInfo->addColorData( DIS_GE,  1450, SPEED_MAX,   33, 0, POS_AJST); //GoStraightから移行
	mLineTraceInfo->addColorData( DIS_GE,   100, SPEED_MAX,   33, 0, 0); //灰色の上で終了させること
	mLineTraceInfo->addVLineData( ON_LINE,   33, SPEED_HIGH2,  0, 3000, 0); //仮想直線の終了は黒線検知
	mLineTraceInfo->addColorData( DIS_GE,   100, SPEED_LOW1,  42,  0, 0); //ライン復帰
	mLineTraceInfo->addColorData( DIS_GE,   500, SPEED_LOW3,  42,  0, 0);

	addScene(sceneStartDash);
	addScene(sceneLineTrace);
	addScene(scenePassGate);
}
#endif
