﻿/*
 * LineTraceInfo.h
 *
 *  Created on: 2017/07/23
 *      Author: takeshi
 */

#ifndef SDK_WORKSPACE_ETRONO_TR2_APP_LINETRACEINFO_H_
#define SDK_WORKSPACE_ETRONO_TR2_APP_LINETRACEINFO_H_

#include <list>

#define PARAM_NUM 8

/*!
 * @class LineTraceInfo
 * @brief 走行情報データクラス
 */
class LineTraceInfo {
public:
	LineTraceInfo();
	virtual ~LineTraceInfo();

	/*!
	 * @brief 走行情報データを追加する
	 * @param[in] mode 走行モード
	 * @param[in] endTerm 終了条件
	 * @param[in] endValue 終了条件に用いる閾値
	 * @param[in] speed 速度
	 * @param[in] param1 走行モードに用いるパラメータ1
	 * @param[in] param2 走行モードに用いるパラメータ2
	 * @param[in] param3 走行モードに用いるパラメータ3
	 */
	void addData(int mode, int endTerm, int endValue, int speed, int param1, int param2, int param3);

	/*!
	 * @brief 走行情報データを追加する
	 * @param[in] mode 走行モード
	 * @param[in] endTerm 終了条件
	 * @param[in] endValue 終了条件に用いる閾値
	 * @param[in] speed 速度
	 * @param[in] param1 走行モードに用いるパラメータ1
	 * @param[in] param2 走行モードに用いるパラメータ2
	 * @param[in] param3 走行モードに用いるパラメータ3
	 * @param[in] A_R 倒立振子ライブラリに設定するA_R値（0:デフォルト値）※省略可能
	 *
	 * @par
	 * 走行モードに用いるパラメータ (param1,param2,param3)
	 *  - MODE_STARTDASH: なし
	 *  - MODE_COLORL:	反射光目標値(正規値)、バイアス(正：右方向、負：左方向)、位置補正(EPositionAjust）
	 *  - MODE_COLORL2:	同上
	 *  - MODE_COLORR:	同上
	 *  - MODE_COLORL1: 反射光目標値(正規値)、バイアス(正：右方向、負：左方向)、目標速度
	 *  - MODE_VLINE:	目標x座標、目標y座標、位置補正(EPositionAjust)
	 *  - MODE_VLINE2: 	目標x座標、目標y座標、減速後の速度
	 *  - MODE_VCIRCLE:	中心x座標、中心y座標、仮想円の半径(正：右回り、負：左回り）
	 *  - MODE_TURN:	setCommand()で指定する走行角度(iTurn)、(未使用)、位置補正(EPositionAjust）
	 *  - MODE_TURN3:	目標角度、setCommand()で指定する走行角度(iTurn)、(未使用)、位置補正(EPositionAjust）
	  *  - MODE_SPIN:	目標角度、setCommand()で指定する走行角度(iTurn)、各モーターの回転周期 (num * 4 ms)
	 *  - MODE_SPIN3:	目標角度、setCommand()で指定する走行角度(iTurn)、(未使用)
	 *  - MODE_GOAL:	目標y座標、ハイブリッド走行の終了x座標、(未使用)
	 *  - MODE_GRAY:	反射光目標値(正規値)、黒線未検知カウント、Kpに乗算する係数
	 *  - MODE_BACKLINE: 	(未使用)、(未使用)、位置補正(EPositionAjust）
	 *  - MODE_TAILRUN:		iTurn、（未使用）、位置補正(EPositionAjust)
	 *  - MODE_TAILVLINE:	目標x座標、目標y座標、位置補正(EPositionAjust)
	 *  - MODE_TAILUP:		待機カウント、(未使用)、(未使用)
	 *  - MODE_TAILDOWN:	待機カウント、(未使用)、(未使用)
	 *  - MODE_ARMSET:		目標角度、アーム動作速度、位置補正(EPositionAjust）
	 *  - TEST_MOTOR:		速度下限、計測速度間隔、一定期間を示すカウント数
	 *  - REDUSE_BATTERY:	バッテリ残量平均のN、(未使用)、(未使用)
	 */
	void addData(int mode, int endTerm, int endValue, int speed, int param1, int param2, int param3, float A_R);

	/*!
	 * @brief 走行情報データ数を返す
	 * @return 走行情報データ数
	 */
	int getDataSize();

	/*!
	 * @brief 走行モードを取得する
	 * @return 走行モード
	 */
	int getMode();


	/*
	 * @brief 走行パラメータ値を返す
	 * @param[in] id パラメータID(1-4)
	 * @return 走行パラメータ値
	 */
	int getParam(int id);

	/*!
	 * @brief 次のデータに切り替える
	 */
	bool nextData();

private:
	std::list<int> mData;

	bool init;

	int mParam[PARAM_NUM];

	bool setParam();


};

#endif /* SDK_WORKSPACE_ETRONO_TR2_APP_LINETRACEINFO_H_ */
