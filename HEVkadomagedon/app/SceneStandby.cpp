/** @file SceneStandby.cpp
*	@brief スタンバイシーン用クラス
*   @author yamaguchi
*   @date 2017.06.09
*/

#include "common.h"
#include "Scene.h"
#include "SceneStandby.h"
#include "TouchSensor.h"

using ev3api::TouchSensor;

SceneStandby::SceneStandby(TailMotor* tailMotor, LineMonitor* lineMonitor, BalancingWalker* balancingWalker, Vibration* vibration)
 : mTailMotor(tailMotor), mLineMonitor(lineMonitor), mBalancingWalker(balancingWalker), mVibration(vibration)
{
	mTouchSensor = new TouchSensor(PORT_1);
}

SceneStandby::~SceneStandby() {
	delete mTouchSensor;
}

bool SceneStandby::run(){
	static unsigned int ucStatus = 0;
	static int mCount = 0;
	ER ercd;
	int bt_cmd;

	switch(ucStatus) {
	case 0:
		//タッチセンサを押すまで待機
		//if( button_pressed() == 1 ) //省略 for HackEV by kidokoro
			ucStatus=4;//1,2,3を飛ばす for HackEV by kidokoro
		break;
#if 0
	case 1:
		//1秒間待機
		if( mCount++ > 250 )
			ucStatus=4;//2,3を飛ばす for HackEV by kidokoro
		break;
	case 2:
		/* 倒立ライブラリをリセット */
		//4msインターバルで、10回ずつ計測し、平均値が一致したら採用する
		if( mBalancingWalker->init() == true ){
			outBeep(1); //音を鳴らして完了通知
			ucStatus++;
		}
		break;
	case 3:
		//本体が持ち上げられ、揺れるまで待機
		mVibration->doMeasure();
//		if( mVibration->getAngler() > 20 )  // for HackEV by Kidokoro
			ucStatus++;
		break;
#endif
	case 4:
		/* Bluetooth通信タスクの起動 */
		ercd = act_tsk(BT_TASK);

		if( ercd != E_OK ){
			dprintf("ERR! act_tsk ercd=%d\n", ercd );
		}

		/* 完全停止用角度に設定    */
		mTailMotor->setStandby();

		ucStatus++;
		break;
	case 5:
		/* スタート待機 */
		if( button_pressed() == 1 ){
			/* タッチセンサが押されたら、シーン終了 */
			dprintf("touch sensor pressed.\n");
			ucStatus++; // for addData test by kidokoro
			//return false;// for addData test by kidokoro
		}

		/* BTタスクからデータ受信 */
		ercd = trcv_dtq(DT_QID, &bt_cmd, 0);

		if( ercd == E_OK ){
			dprintf("bt_cmd = %d\n", bt_cmd);

			switch(bt_cmd){
			case 1:
				/* スタンバイシーンを終了し、次のシーンへ */
				return false;
				break;
			case 2:
				/* 少し起きる */
				mTailMotor->standUp();
				break;
			case 3:
				/* 少し寝る */
				mTailMotor->standDown();
				break;
			case 4:
				/* 白のキャリブレーション */
				mLineMonitor->setWhite(mLineMonitor->getBrightness(10));
				break;
			case 5:
				/* 黒のキャリブレーション */
				mLineMonitor->setBlack(mLineMonitor->getBrightness(10));
				break;
			default:
				break;
			}
		}
		break;
	case 6://テスト用。ブロック並べの攻略手順からシナリオの算出を行う。Todo 本番は別タスクを想定。
		// for addData test by kidokoro
		//攻略手順を算出する

		//動作シナリオを算出する

		//LineTraceInfoに追加する

		return false;
		break;
	default:
		break;
	}

	return true;
}

//void SceneStandby::stop(){
//	/* Bluetooth通信タスクを終了 */
//原因不明だが、タスク終了するとモーター回転速度が下がるため終了させない(7/30)
//	ter_tsk(BT_TASK);
//	dprintf("ter_tsk(BT_TASK)\n");
//}


/* チャタリング対策用の関数 */
//参考URL: http://qiita.com/tkzw102/items/e2b148893067e33c9cc4
int SceneStandby::button_pressed(){
    /* ボタンの現在と直前の状態と関数の返り値を保持する変数 */
    static int now = 0;
    static int before = 0;
    int kekka = 0;
    static int flag = 0;

    /* ボタンの現在値を記録する */
    if( mTouchSensor->isPressed() == true )
    	now = 1;
    else
    	now = 0;

    /* ボタンを離した後に0が10回継続したときのみ1を返す */
    if(now == 0 && before == 1){
        flag = 1;
    } else if ( now == 0 && flag > 0 ){
    	flag++;
    	kekka = 0;
    	if(flag > 10)
    		kekka = 1;
    }else{
        kekka = 0;
        flag = 0;
    }

//    dprintf("now=%d,before=%d,kekka=%d\n", now,before,kekka);

    before = now;

    if(kekka == 1){
    	//初期化
    	now = 0;
    	before = 0;
    	flag = 0;
    }

    return kekka;
}



