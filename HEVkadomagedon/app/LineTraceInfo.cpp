/*
 * LineTraceInfo.cpp
 *
 *  Created on: 2017/07/23
 *      Author: takeshi
 */

#include "common.h"
#include "LineTraceInfo.h"

LineTraceInfo::LineTraceInfo() {
	init = true;
}

LineTraceInfo::~LineTraceInfo() {
}

void LineTraceInfo::addData(int mode, int endTerm, int endValue, int speed, int param1, int param2, int param3){
	addData(mode, endTerm, endValue, speed, param1, param2, param3, 0);
}

void LineTraceInfo::addData(int mode, int endTerm, int endValue, int speed, int param1, int param2, int param3, float A_R){
	mData.push_back(mode);
	mData.push_back(endTerm);
	mData.push_back(endValue);
	mData.push_back(speed);
	mData.push_back(param1);
	mData.push_back(param2);
	mData.push_back(param3);
	mData.push_back((int)(A_R * 1000));

	//dprintf("size:%d, %d\n", mData.size(), getDataSize());
}

int LineTraceInfo::getDataSize(){
	return mData.size()/PARAM_NUM;
}

int LineTraceInfo::getMode(){
	if( init ){
		setParam();
		init = false;
	}

	return mParam[0];
}


int LineTraceInfo::getParam(int id){
	if( init ){
		setParam();
		init = false;
	}

	if( id > PARAM_NUM-1 ){
		dprintf("id is invalid.(id=%d)\n", id);
		return -1;
	}

	return mParam[id];
}


bool LineTraceInfo::nextData(){
	return setParam();
}

bool LineTraceInfo::setParam(){
	if( getDataSize() > 0 ){
		for(int i = 0; i < PARAM_NUM; i++){
			mParam[i] = mData.front();
			mData.pop_front(); //先頭データを削除
		}
		return true;
	} else {
		dprintf("getDataSize = 0\n");
		return false;
	}
}



