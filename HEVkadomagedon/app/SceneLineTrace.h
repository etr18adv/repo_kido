/** @file SceneLineTrace.h
*	@brief ライントレース・倒立走行するシーン用クラス
*/


#ifndef SDK_WORKSPACE_ETRONO_TR2_SCENE_SCENELINETRACE_H_
#define SDK_WORKSPACE_ETRONO_TR2_SCENE_SCENELINETRACE_H_

#include "SelfPosition.h"
#include "LineTraceInfo.h"
#include "PidController.h"
#include "Course.h"
#include "Sonar.h"

/*!
 * @class SceneLineTrace
 * @brief 走行情報データを基に走行制御するクラス
 */
class SceneLineTrace : public Scene {
public:
	/*!
	 * @brief コンストラクタ
	 * @param[in] tailMotor 尻尾モーター
	 * @param[in] lineMonitor ラインモニター(2点走行)
	 * @param[in] lineMonitor3 ラインモニター(3点走行)
	 * @param[in] balancingWalker 倒立走行
	 * @param[in] freeWalker 3点走行
	 * @param[in] measure 自己位置推定
	 * @param[in] lineTraceInfo 走行情報データ
	 * @param[in] vibration 振動
	 * @param[in] sonar 超音波
	 */
	SceneLineTrace(TailMotor* tailMotor, LineMonitor* lineMonitor, LineMonitor* lineMonitor3, BalancingWalker* balancingWalker, FreeWalker* freeWalker, SelfPosition* measure, LineTraceInfo* lineTraceInfo, Vibration* vibration, Sonar* sonar, ArmControl* armControl);

	/*!
	 * @brief デストラクタ
	 */
	virtual ~SceneLineTrace();

	/*!
	 * @brief シーンを開始する
	 */
	bool run();

	/*!
	 * @brief x座標を調整する(Lコース向け)
	 * @param[in] x 調整距離(mm)
	 */
	void setShiftX(int x);


private:
	TailMotor*       mTailMotor;
	LineMonitor*     mLineMonitor;
	LineMonitor*     mLineMonitor3;
	BalancingWalker* mBalancingWalker;
	FreeWalker*      mFreeWalker;
	SelfPosition*    mMeasure;
	LineTraceInfo*   mLineTraceInfo;
	Vibration*       mVibration;

	PidController*   mPidController;
	Sonar*	mSonar;

	ArmControl* mArmControl;

	std::list<int> diffList; 	//保持する値のリスト ※実際の差分値を100倍した値
	int diffSum;				//保持した値の総和

	int shiftX;

	typedef struct {
		double x;
		double y;
	} Position;

	int lastStraght(double diff, int speed, int param1, int param2, int param3);

	/*
	 * @param[in] speed 速度
	 * @param[in] R 仮想円の半径
	 * @param[in] angle 目標角度
	 * @param[in] x0 開始位置x座標
	 * @param[in] y0 開始位置y座標
	 * @param[in] x  現在位置x座標
	 * @param[in] y  現在位置y座標
	 */
	bool backLine(int speed, int R, int angle, double x0, double y0, double x, double y);

	/*
	 * @param[in] speed 速度
	 * @param[in] turn 角度
	 * @param[in] dis1 黒線検知後、後退するときの移動距離
	 * @param[in] dis2 黒線検知後、前進するときの移動距離
	 */
	bool backLine2(int speed, int turn, int dis1, int dis2);

	/*
	 * @brief 尻尾を下げる方向に段階的変更
	 * @param[in] x 現在x座標
	 * @param[in] speed 走行速度
	 * @param[in] waitCount 待機カウント数
     * @param[in] ischange2to3  2点から3点移行チェック
	 */
	bool TailDown(double x, int speed, int waitCount, bool ischange2to3);

	/*
	 * @brief 尻尾を上げる方向に段階的変更
	 * @param[in] x 現在x座標
	 * @param[in] speed 走行速度
	 * @param[in] waitCount 待機カウント数
	 */
	bool TailUp(  double x, int speed, int waitCount);

	void initMovingDiff();
	double getMovingDiff(int speed, double fDiff);

	/*
	 * @brief ゴール前の直線でラインに徐々に復帰 ※回転で補正
	 * @param[in] x1 走行区間切替時のx座標
	 * @param[in] y1 走行区間切替時のy座標
	 * @param[in] param1 最後の直線y座標
	 * @param[in] dx 現在x座標
	 * @param[in] dy 現在y座標
	 * @param[in] speed 走行速度
	 * @param[in] remainDis 残りの走行距離
	 * @param[in] color カラー目標値
	 *
	 * @attention 以降の走行モードは座標軸が90度回転し、位置情報が初期化されているため注意
	 */
	void modeGoal(double x1, double y1, int param1, double dx, double dy, int speed, int remainDis, int color);

	bool modeSpin(int speed, int param1, int param2, int param3, double x0, double y0, double dx, double dy);

	/*
	 * @brief 倒立振子ライブラリに設定するA_R値を設定する
	 * @param[in] value A_R値
	 */
	void setA_R(int value);

	/*
	 * @brief 3点の座標を三角形にしたとき、現在座標の位置の内角を求める
	 *
	 * @param[in] x1 1点目のx座標
	 * @param[in] y1 1点目のy座標
	 * @param[in] x2 2点目のx座標
	 * @param[in] y2 2点目のy座標
	 * @param[in] dx  現在位置のx座標
	 * @param[in] dy  現在位置のy座標
	 */
	//double getCosTheta(double x1, double y1, double x2, double y2, double dx, double dy);

	/*!
	 * @brief 仮想円の円周と現在位置の誤差を求める
	 * @param[in] iCenterX 仮想円の中心座標x
	 * @param[in] iCenterY 仮想円の中心座標y
	 * @param[in] iRadius  仮想円の半径
	 * @param[in] ucRotation 回転方向(0:右回り, 1:左回り)
	 * @param[in] dx 現在x座標
	 * @param[in] dy 現在y座標
	 * @return 仮想円の円周と現在位置の誤差
	 */
    double getPositionErr(double iCenterX, double iCenterY, double iRadius, unsigned char ucRotation, double dx, double dy);

    /*!
     * @brief 1次関数の仮想直線からの誤差を返す
     * @param[in] x1 開始位置x座標
     * @param[in] y1 開始位置y座標
     * @param[in] x2 終了位置x座標
     * @param[in] y2 終了位置y座標
     * @param[in] dx 現在x座標
     * @param[in] dy 現在y座標
     * @return 仮想直線と現在位置の誤差
     */
    //double getLinePosErr(double x1, double y1, double x2, double y2, double dx, double dy);

    /*!
     * @brief 1次関数の仮想直線からの誤差を返す(リファクタイング後)
     * @param[in] x1 開始位置x座標
     * @param[in] y1 開始位置y座標
     * @param[in] x2 終了位置x座標
     * @param[in] y2 終了位置y座標
     * @param[in] dx 現在x座標
     * @param[in] dy 現在y座標
     * @return 仮想直線と現在位置の誤差
     */
    double getLinePosErr(double x1, double y1, double x2, double y2, double dx, double dy);
};

#endif /* SDK_WORKSPACE_ETRONO_TR2_SCENE_SCENELINETRACE_H_ */
