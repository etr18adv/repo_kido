/** @file SceneStandby.h
*	@brief スタンバイシーン用クラス
*   @author yamaguchi
*   @date 2017.06.09
*/

#ifndef SDK_WORKSPACE_ETRONO_TR_SCENE_SCENESTANDBY_H_
#define SDK_WORKSPACE_ETRONO_TR_SCENE_SCENESTANDBY_H_

#include "TouchSensor.h"

using ev3api::TouchSensor;

/*!
 * @class SceneStandby
 * @brief スタンバイシーンを制御するクラス
 */
class SceneStandby : public Scene {
public:
	/*!
	 * @brief コンストラクタ
	 * @param[in] tailMotor 尻尾モーター
	 * @param[in] lineMonitor ラインモニター
	 * @param[in] balancingWalker 倒立走行
	 * @param[in] vibration 振動
	 */
	SceneStandby(TailMotor* tailMotor, LineMonitor* lineMonitor, BalancingWalker* balancingWalker, Vibration* vibration);

	/*!
	 * @brief デストラクタ
	 */
	virtual ~SceneStandby();

	/*!
	 * @brief シーンを開始する
	 */
	bool run();

	/*!
	 * シーンを終了する
	 */
	//void stop();

private:
	TailMotor*       mTailMotor;
	LineMonitor*     mLineMonitor;
	BalancingWalker* mBalancingWalker;
	TouchSensor*     mTouchSensor;
	Vibration*       mVibration;

	/*
	 * @brief ボタン押下時のチャタリング対策用
	 */
	int button_pressed();
};



#endif /* SDK_WORKSPACE_ETRONO_TR_SCENE_SCENESTANDBY_H_ */
