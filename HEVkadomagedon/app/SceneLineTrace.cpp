/** @file SceneLineTrace.cpp
 *	@brief ライントレース・倒立走行するシーン用クラス
 */

#include <math.h>

#include "common.h"
#include "Scene.h"
#include "SceneLineTrace.h"
#include "PidController.h"
#include "Sonar.h"

#define abs(a) ((a) < 0 ? - (a) : (a))


SceneLineTrace::SceneLineTrace (TailMotor* tailMotor, LineMonitor* lineMonitor, LineMonitor* lineMonitor3, BalancingWalker* balancingWalker, FreeWalker* freeWalker, SelfPosition* measure, LineTraceInfo* lineTraceInfo, Vibration* vibration, Sonar* sonar, ArmControl* armControl)
: mTailMotor(tailMotor), mLineMonitor(lineMonitor), mLineMonitor3(lineMonitor3), mBalancingWalker(balancingWalker), mFreeWalker(freeWalker), mMeasure(measure), mLineTraceInfo(lineTraceInfo), mVibration(vibration), mSonar(sonar),mArmControl(armControl)
{
	mPidController = new PidController();
	shiftX = 0;
}

SceneLineTrace::~SceneLineTrace() {
	delete mPidController;
}


bool SceneLineTrace::run(){
	//共通パラメータ
	int mode = -1;
	int endTerm = -1;
	int endValue = 0;
	int speed = 0;
	//走行モード調整パラメータ
	int param1 = 0;
	int param2 = 0;
	int param3 = 0;
	int A_R = 0;
	int turn = 0;

	int distance = mMeasure->getDistance(); //走行距離
	int ajust = 0;
	bool endFlag = false;
	double fDiff = 0;
//	double ajustX = 0;
//	double ajustY = 0;

	static int distance0 = -1; //走行区間を切換える前の走行距離
	static Position position0;   //走行区間の切替前の座標
	static Position currentPos;  //現在の座標
	static int mCount = 0;
	static int grayDistance = 0; //最後に黒を検知した地点の距離
	static int sonarDistance = 0; //超音波で計測した距離の前回値
	static int whiteCount = 0; //turnが閾値以上を連続検知した回数（フェールセーフ用）

	static bool spinFlag = false;

	double tmp;

	int color;//by kidokoro for HackEV

#ifdef AJUSTMENT
	int battery = 0;
	static int L = 0;
	static int R = 0;
	static int speed1 = speed;
#endif

//	static double dis = 100000;
//	double tmp;

	/**** 初期化 ****/
	if( distance0 == -1 ){
		distance0 = distance;
	}

	/**** 走行モード調整パラメータ取得 ****/
	mode	 = mLineTraceInfo->getMode();
	endTerm  = mLineTraceInfo->getParam(1);
	endValue = mLineTraceInfo->getParam(2);
	speed    = mLineTraceInfo->getParam(3);
	param1   = mLineTraceInfo->getParam(4);
	param2   = mLineTraceInfo->getParam(5);
	param3   = mLineTraceInfo->getParam(6);
	A_R		 = mLineTraceInfo->getParam(7);

	/**** パラメータのx座標シフト ****/
	if( (endTerm == Course::XPOS_GE) || (endTerm == Course::XPOS_LT) ){
		endValue += shiftX;
	}

	/**** 倒立振子ライブラリのA_R値を設定 ****/
	setA_R(A_R);

	/************ 計測 ************/
	mMeasure->doMeasure();
//	mMeasure->printPosition();
	mMeasure->getPosition(&currentPos.x,&currentPos.y);

	//振動検知
	mVibration->doMeasure();

	if( endTerm == Course::SONAR_LT || endTerm == Course::SONAR_CLOSE){
		//超音波センサによる距離計測
		mSonar->doMeasure();
		//dprintf("XX sonar:%f\n", mSonar->getDistance());
	}

	/******** 走行モードによる動作 ********/
	switch(mode){
	case Course::MODE_STARTDASH:
		// スタートダッシュのための尻尾制御
		if(endTerm == Course::TAIL_ANGLE){
			mTailMotor->setAngle(endValue);
		}

		break;
	case Course::MODE_COLORL:
		ajust = param3;

		mLineMonitor->setTarget(param1);

		if( speed > 40 ){//４０以上の場合に変更　by kidokoro for HackEV
			mPidController->setKPID(PARAM_LINETRACE_KP_MAX, PARAM_LINETRACE_KI_MAX, PARAM_LINETRACE_KD_MAX);
		} else {
			mPidController->setKPID(PARAM_LINETRACE_KP, PARAM_LINETRACE_KI, PARAM_LINETRACE_KD);
		}

		fDiff = mLineMonitor->getDiff();
		turn = mPidController->calc_pid_value(fDiff) + param2;
		mBalancingWalker->setCommand(speed, turn);
		mBalancingWalker->run();

		if(turn > 20){
			whiteCount++;
		}else{
			whiteCount = 0;
		}

		//dprintf("[PID],%f,%d,%d,\n", fDiff, turn,whiteCount);
		break;
	case Course::MODE_COLORL1:
		//ajust = param3;

		mLineMonitor->setTarget(param1);

		if( speed > 100 ){
			mPidController->setKPID(PARAM_LINETRACE_KP_MAX, PARAM_LINETRACE_KI_MAX, PARAM_LINETRACE_KD_MAX);
		} else {
			mPidController->setKPID(PARAM_LINETRACE_KP, PARAM_LINETRACE_KI, PARAM_LINETRACE_KD);
		}

		//走行距離に応じた加速
		//speed = ((distance - distance0)*(param3-speed)/(endValue-param3)) + speed;
		speed = (distance - distance0)*(param3 - speed)/(endValue-distance0) + speed;

		if(speed > param3){
			speed = param3;
		}
		//dprintf("speed: %d distance=%d distance0=%d param3=%d endValue=%d\n", speed, distance, distance0, param3, endValue);

		fDiff = mLineMonitor->getDiff();
		turn = mPidController->calc_pid_value(fDiff) + param2;
		mBalancingWalker->setCommand(speed, turn);
		mBalancingWalker->run();

		break;
	case Course::MODE_COLORL2:
		ajust = param3;

		mLineMonitor->setTarget(param1);

		if( speed > 100 ){
			mPidController->setKPID(PARAM_LINETRACE_KP_MAX, PARAM_LINETRACE_KI_MAX, PARAM_LINETRACE_KD_MAX);
		} else {
			mPidController->setKPID(PARAM_LINETRACE_KP, PARAM_LINETRACE_KI, PARAM_LINETRACE_KD);
		}
		fDiff = mLineMonitor->getMovingDiff();
		turn = mPidController->calc_pid_value(fDiff) + param2;
		mBalancingWalker->setCommand(speed, turn);
		mBalancingWalker->run();

		break;
	case Course::MODE_COLORR:
		ajust = param3;

		mLineMonitor->setTarget(param1);

		mPidController->setKPID(PARAM_LINETRACE_KP, PARAM_LINETRACE_KI, PARAM_LINETRACE_KD);
		fDiff = mLineMonitor->getDiff();
		turn = -1*mPidController->calc_pid_value(fDiff) + param2;
		mBalancingWalker->setCommand(speed, turn);
		mBalancingWalker->run();

		break;
	case Course::MODE_VLINE:
		//仮想直線ライントレース
		param1 += shiftX; //x座標シフト

		ajust = param3;

		mPidController->setKPID(PARAM_VIRTUALLINE_KP, PARAM_VIRTUALLINE_KI, PARAM_VIRTUALLINE_KD);
		fDiff = getLinePosErr(position0.x, position0.y, param1, param2, currentPos.x, currentPos.y);
		fDiff = getMovingDiff(speed, fDiff); //移動平均値に変換
		turn = mPidController->calc_pid_value(fDiff);
		mBalancingWalker->setCommand(speed, turn);
		mBalancingWalker->run();
		break;
	case Course::MODE_VLINE2:
		//仮想直線ライントレース（減速機能付き）
		param1 += shiftX; //x座標シフト

		//ajust = param3;

		speed = ((distance - distance0)*(param3-speed)/(endValue-param3)) + speed;

		if(speed < param3){
			speed = param3;
		}

		//dprintf("XX speed:%d, dis:%d \n", speed, distance-distance0);

		mPidController->setKPID(PARAM_VIRTUALLINE_KP, PARAM_VIRTUALLINE_KI, PARAM_VIRTUALLINE_KD);
		fDiff = getLinePosErr(position0.x, position0.y, param1, param2, currentPos.x, currentPos.y);
		fDiff = getMovingDiff(speed, fDiff); //移動平均値に変換
		turn = mPidController->calc_pid_value(fDiff);
		mBalancingWalker->setCommand(speed, turn);
		mBalancingWalker->run();
		break;
	case Course::MODE_VCIRCLE:
		//仮想円ライントレース
		param1 += shiftX; //x座標シフト

		if( param3 > 0 ){
			fDiff = getPositionErr(param1, param2, param3, 0, currentPos.x, currentPos.y);
		} else {
			fDiff = getPositionErr(param1, param2, -1*param3, 1, currentPos.x, currentPos.y);
		}

		mPidController->setKPID(PARAM_VIRTUALCIRCLE_KP, PARAM_VIRTUALCIRCLE_KI, PARAM_VIRTUALCIRCLE_KD);
		fDiff = getMovingDiff(speed, fDiff); //移動平均値に変換
		turn = mPidController->calc_pid_value(fDiff);
		mBalancingWalker->setCommand(speed, turn);
		mBalancingWalker->run();
		break;
	case Course::MODE_TURN: //ライン無視走行
		ajust = param3;

		if(endTerm == Course::TAIL_ANGLE){
			mTailMotor->setAngle(endValue);
		}

		//mPidController->setKPID(PARAM_LINETRACE_KP, PARAM_LINETRACE_KI, PARAM_LINETRACE_KD);
		mBalancingWalker->setCommand(speed, param1);
		mBalancingWalker->run();

		break;
	case Course::MODE_SPIN: //2点スピン(独自版)
		mBalancingWalker->setCommand(speed, param2);
		if (mBalancingWalker->spin(param1, param3) == false ){
			endFlag = true;
		}
		break;
	case Course::MODE_SPIN2:
#if 0
		//新2点スピン(倒立振子ライブラリAPI仕様参考版)
		//バランスを崩して倒れるため一旦削除
		mBalancingWalker->setCommand(speed, param2);
		if (mBalancingWalker->spin(param1) == false ){
			endFlag = true;
		}
#else

		if( spinFlag == false ){
			if(speed > 0){
				mBalancingWalker->setCommand(speed, param2);
			}else{
				mBalancingWalker->setCommand(-speed, param2);
			}
			if (mBalancingWalker->spin(param1, param3) == false ){
				//endFlag = true;
				spinFlag = true;
				dprintf("XXXX\n");
				outBeep(5);
			}
		}else{
			//開始位置に位置調整
			if( speed > 0 ){ //xで補正
				if( endValue > 0 ){
					if( abs(currentPos.x - position0.x) < 10 ){
						endFlag = true;
						spinFlag = false;
						dprintf("XXXX\n");
					} else if(currentPos.x > position0.x){
						mBalancingWalker->setCommand(-SPEED_LOW1, 0);
						mBalancingWalker->run();
					} else {
						mBalancingWalker->setCommand(SPEED_LOW1, 0);
						mBalancingWalker->run();
					}
				} else {
					if( abs(currentPos.x - position0.x) < 10 ){
						endFlag = true;
						spinFlag = false;
						dprintf("XXXX\n");
					} else if(currentPos.x < position0.x){
						mBalancingWalker->setCommand(-SPEED_LOW1, 0);
						mBalancingWalker->run();;
					} else {
						mBalancingWalker->setCommand(SPEED_LOW1, 0);
						mBalancingWalker->run();
					}
				}
			} else { //yで補正
				if( endValue > 0 ){
					if( abs(currentPos.y - position0.y) < 10 ){
						endFlag = true;
						spinFlag = false;
						dprintf("XXXX\n");
					} else if(currentPos.y > position0.y){
						mBalancingWalker->setCommand(-SPEED_LOW1, 0);
						mBalancingWalker->run();
					} else {
						mBalancingWalker->setCommand(SPEED_LOW1, 0);
						mBalancingWalker->run();
					}
				} else {
					if( abs(currentPos.y - position0.y) < 10 ){
						endFlag = true;
						spinFlag = false;
						dprintf("XXXX\n");
					} else if(currentPos.y < position0.y){
						mBalancingWalker->setCommand(-SPEED_LOW1, 0);
						mBalancingWalker->run();
					} else {
						mBalancingWalker->setCommand(SPEED_LOW1, 0);
						mBalancingWalker->run();
					}
				}
			}
		}


		break;
#endif
		break;
	case Course::MODE_SPIN3: //3点スピン
		mFreeWalker->setCommand(speed, param2);
		if(endTerm==Course::SPIN_ANGLE){
		}
		else{
			endValue= 90;//現状、SPIN_ANGLEしかないが、他を検知したとき９０度にする
		}
        if (mFreeWalker->spin((double)endValue) == false ){
            endFlag = true;
        }
		break;
	case Course::MODE_TURN3: //ターンする。片方の車輪を止めて回る。by kidokoro for HackEV
		mFreeWalker->setCommand(speed, param2);
		if(endTerm==Course::TURN_ANGLE){
		}
		else{
			endValue= 90;//現状TURN_ANGLEしかないが、他を検知したとき９０度にする
		}
		if (mFreeWalker->turn((double)endValue) == false ){
            endFlag = true;
        }
		break;
	case Course::MODE_GOAL: //ゴール前直線の走行（幾野計算式）
		//仮想直線ライントレース
		//param1 += shiftX;
		param2 += shiftX; //x座標シフト

		modeGoal(position0.x, position0.y, param1, currentPos.x, currentPos.y, speed, param2-currentPos.x, param3);

		break;
	case Course::MODE_GRAY: //階段後の灰色クリア
		fDiff   = getLinePosErr(position0.x, position0.y, 0, 3000, currentPos.x, currentPos.y);

		grayDistance = lastStraght(fDiff, speed, param1, param2, param3);

		if((distance - distance0) > 450){
			//保険
			endFlag = true;
		}
		break;
	case Course::MODE_BACKLINE: //ライン復帰モード
		mLineMonitor->setTarget(endValue);
#if 1 //仮想円による探索
		endFlag = backLine(speed, param1, param2, position0.x, position0.y, currentPos.x, currentPos.y);
#else //旋回による探索
		endFlag = backLine2(speed, param1, param2, param3);
#endif

		break;
    case Course::MODE_TAILCOLORL:
		ajust = param3;
#if 1
		mLineMonitor3->setTarget(param1);

		if( speed > 100 ){
			mPidController->setKPID(PARAM_LINETRACE_KP_MAX, PARAM_LINETRACE_KI_MAX, PARAM_LINETRACE_KD_MAX);
		} else {
			mPidController->setKPID(PARAM_LINETRACE_KP, PARAM_LINETRACE_KI, PARAM_LINETRACE_KD);
//			mPidController->setKPID(0.84, PARAM_LINETRACE_KI, 0.01);
		}

		fDiff = mLineMonitor3->getDiff();
		turn = mPidController->calc_pid_value(fDiff) + param2;

		//dprintf("TAIL_COLORL:turn= %d,fdiff= %f\n",turn,fDiff);//by kidokoro
		mFreeWalker->setCommand(speed, 0.6*turn); //とりあえず0.5かける　by kidokoro
		mFreeWalker->run();
#else
		if( mLineMonitor3->isOnLine(param1) ){
			turn= 20;
			dprintf("ONLINE\n");
		}
		else{
			turn = -20;
			dprintf("OFFLINE\n");
		}
		mFreeWalker->setCommand(speed, turn);
		mFreeWalker->run();
#endif
		break;
	case Course::MODE_TAILRUN: //3点ライン無視走行
		ajust = param3;

		//mPidController->setKPID(PARAM_LINETRACE_KP, PARAM_LINETRACE_KI, PARAM_LINETRACE_KD);
		mFreeWalker->setCommand(speed, param2);
		mFreeWalker->run();

		break;
	case Course::MODE_TAILVLINE: //3点仮想直線
		//ajust = param3;

		fDiff   = getLinePosErr(position0.x, position0.y, param1, param2, currentPos.x, currentPos.y);
		//fDiff = getMovingDiff(fDiff); //高速走行しないため、移動平均値に変換しない

		if( param3 == 0 ){
			mPidController->setKPID(PARAM_VIRTUALLINE_KP, PARAM_VIRTUALLINE_KI, PARAM_VIRTUALLINE_KD);
		}else{
			mPidController->setKPID(PARAM_VIRTUALLINE_KP * param3, 0, 0);
		}

//		if (ajust == Course::POS_END) {
//            mFreeWalker->setCommand(speed, mPidController->calc_pid_value(fDiff) - 10);
//        }
//        else {
		tmp = mPidController->calc_pid_value(fDiff);
		mFreeWalker->setCommand(speed, tmp);
//        }
		mFreeWalker->run();

		//dprintf("[PID],%f,%f,\n", fDiff,tmp);

		break;
	case Course::MODE_TAILUP: //尻尾角度変更
		endFlag = TailUp(currentPos.x, speed, param1);

		ajust = param3;

		break;
	case Course::MODE_TAILDOWN: //尻尾角度変更
		endFlag = TailDown(currentPos.x, speed, param1, param2);

		break;
	case Course::MODE_ARMSET://アーム制御用 for HackEV by kidokoro
		ajust = param3;

		if(mArmControl->setArm(param1,param2)==true){
			endFlag = true;
		}
		dprintf("ARMSET endflag= %d\n",endFlag);
		break;
	case Course::CHECK_HSV://HSV値確認用　for HackEV by kidokoro
		rgb_raw_t rgb;
		int hsv_h,hsv_s,hsv_v;

		char ss[32];

		mLineMonitor->getRGB(&rgb);
		sprintf(ss,"RGB %d,%d,%d            \n",rgb.r, rgb.g, rgb.b);
		ev3_lcd_draw_string(ss,0,100);

		mLineMonitor->getHSV(&hsv_h,&hsv_s,&hsv_v);
		sprintf(ss,"HSV %d,%d,%d            \n",hsv_h, hsv_s, hsv_v);
		ev3_lcd_draw_string(ss,0,80);

		color = mLineMonitor->getColorHSV();
#if 0 //カラー判定のLCD出力はgetColorHSV側で
		if(color == HSV_BLACK){
			sprintf(ss,"HSV Black !!!!!!!!!!\n");
		}
		else if(color == HSV_GRAY){
			sprintf(ss,"HSV Gray !!!!!!!!!!\n");
		}
		else if(color == HSV_WHITE){
			sprintf(ss,"HSV WHITE !!!!!!!!!!\n");

		}
		else if(color == HSV_RED){
			sprintf(ss,"HSV RED !!!!!!!!!!\n");

		}
		else if(color == HSV_GREEN){
			sprintf(ss,"HSV GREEN !!!!!!!!!!\n");
		}
		else if(color == HSV_BLUE){
			sprintf(ss,"HSV BLUE !!!!!!!!!!\n");

		}
		else if(color == HSV_YELLOW){
			sprintf(ss,"HSV YELLOW !!!!!!!!!!\n");
		}
		else if(color == HSV_NONE){
			sprintf(ss,"HSV NONE !!!!!!!!!!\n");
		}
		else{
			sprintf(ss,"HSV ERROR !!!!!!!!!!\n");
		}
		ev3_lcd_draw_string(ss,0,100);
#endif


		break;

#ifdef AJUSTMENT
	case Course::TEST_COLOR: //実ラインのPIDパラメータ調整用
		mLineMonitor->setTarget(param1);

		mPidController->setKPID(0.24, 0,0);

		fDiff = mLineMonitor->getDiff();
		dprintf("[PID],%f,\n", fDiff);

		mBalancingWalker->setCommand(speed, mPidController->calc_pid_value(fDiff));
		mBalancingWalker->run();
		break;
	case Course::TEST_VLINE: //仮想直線のPIDパラメータ調整用
		mPidController->setKPID(0.24, 0,0);

		fDiff = getLinePosErr(position0.x, position0.y, param1, param2, currentPos.x, currentPos.y);
		dprintf("[PID],%f,\n", fDiff);

		mBalancingWalker->setCommand(speed, mPidController->calc_pid_value(fDiff));
		mBalancingWalker->run();
		break;
	case Course::TEST_TAILVLINE: //3点仮想直線のPIDパラメータ調整用
		mPidController->setKPID(0.24, 0,0);

		fDiff   = getLinePosErr(position0.x, position0.y, param1, param2, currentPos.x, currentPos.y);
		dprintf("[PID],%f,\n", fDiff);

		mFreeWalker->setCommand(speed, mPidController->calc_pid_value(fDiff));
		mFreeWalker->run();

		break;
	case Course::TEST_VCIRCLE: //2点仮想円のPIDパラメータ調整用
		mPidController->setKPID(0.24, 0,0);

		if( param3 > 0 ){
			fDiff = getPositionErr(param1, param2, param3, 0, currentPos.x, currentPos.y);
		} else {
			fDiff = getPositionErr(param1, param2, -1*param3, 1, currentPos.x, currentPos.y);
		}
		dprintf("[PID],%f,\n", fDiff);

		mBalancingWalker->setCommand(speed, mPidController->calc_pid_value(fDiff));
		mBalancingWalker->run();
		break;
	case Course::TEST_MOTOR:
		mFreeWalker->setCommand(speed1, 0);
		mFreeWalker->run();

		if(mCount++ > param3){ //一定時間、空走行
			dprintf("[MOTOR_L],%d,%d,%d\n", mFreeWalker->getLeftCount()-L, ev3_battery_voltage_mV(),speed1);
			dprintf("[MOTOR_R],%d,%d,%d\n", mFreeWalker->getRightCount()-R,ev3_battery_voltage_mV(),speed1);
			speed1 -= param2;

			if( speed1 < param1 ){
				speed1 = speed;
			}

			L = mFreeWalker->getLeftCount();
			R = mFreeWalker->getRightCount();
		}
		break;
	case Course::REDUSE_BATTERY:
		for(int i = 0; i < param1; i++){
			battery += ev3_battery_voltage_mV();
		}

		battery = battery/param1; //平均値

		mFreeWalker->setCommand(speed, 0);
		mFreeWalker->run();

		dprintf("battery: %d, speed: %d\n", battery, speed);
		break;
#endif
	default:
		dprintf("[ERR!] unknown mode:%d \n", mode);
		break;
	}

	/****** 前回値の保存 ******/
	sonarDistance = mSonar->getDistance();

	/****** 終了判定 ******/
	if( ((endTerm == Course::DIS0_GE) && (distance >= endValue )) ||
		((endTerm == Course::DIS0_LT) && (distance < endValue )) ||
		((endTerm == Course::DIS_GE) && (distance - distance0 >= endValue )) ||
		((endTerm == Course::DIS_LT) && (distance - distance0 < endValue )) ||
		((endTerm == Course::XPOS_GE) && (currentPos.x >= endValue)) ||
		((endTerm == Course::XPOS_LT) && (currentPos.x < endValue)) ||
		((endTerm == Course::YPOS_GE) && (currentPos.y >= endValue)) ||
		((endTerm == Course::YPOS_LT) && (currentPos.y < endValue)) ||
		((endTerm == Course::ON_LINE) && (mLineMonitor->isOnLine(endValue) == true)) ||
		((endTerm == Course::ON_LINESPIN) && (mLineMonitor->isOnLine(endValue) == true) && (endFlag == true)) ||
		((endTerm == Course::CROSSLINE) && (mLineMonitor->crossLine(endValue) == true)) ||
		((endTerm == Course::COUNTUP) && (mCount++ > endValue) ) ||
		((endTerm == Course::VIB_GE)  && (mVibration->getAngler() >= endValue) ) ||
		((endTerm == Course::SONAR_LT)  && (mSonar->getDistance() < endValue) ) ||
		((endTerm == Course::TAIL_ANGLE)  && (mTailMotor->checkStatus() == true) && (mode != Course::MODE_TAILUP) && (mode != Course::MODE_TAILDOWN) ) ||
		((endTerm == Course::GRAY_GE) && (grayDistance != 0) && ((distance - grayDistance) >= endValue)) ||
		((endTerm == Course::TURN_LT) && (turn < endValue)) ||
		((endTerm == Course::BATTERY_LT) && ( ev3_battery_voltage_mV() < endValue)) ||
		((mode == Course::MODE_TAILUP)  && (endFlag == true)) ||
		((mode == Course::MODE_TAILDOWN) && (endFlag == true)) ||
		((mode == Course::MODE_BACKLINE) && (endFlag == true)) ||
		((mode == Course::MODE_SPIN)  && (endTerm == Course::NONE) && (endFlag == true)) ||
		((mode == Course::MODE_SPIN2) && (endTerm == Course::NONE) && (endFlag == true)) ||
		((mode == Course::MODE_SPIN3) && (endTerm == Course::SPIN_ANGLE) && (endFlag == true)) ||
		((mode == Course::MODE_SPIN3) && (endTerm == Course::SONAR_CLOSE) && (mSonar->getDistance() <= endValue) && (sonarDistance - mSonar->getDistance() > 0) && (endFlag == true))||
		((mode == Course::MODE_TURN3) && (endTerm == Course::TURN_ANGLE) && (endFlag == true)) ||// for HackEV by kidokoro
		((mode == Course::MODE_ARMSET) && (endFlag == true))	||// for HackEV by kidokoro
		((endTerm== Course::HSV_COLOR)&&(endValue == mLineMonitor->getColorHSV()))// for HackEV by kidokoro
	){
		if(endTerm== Course::HSV_COLOR) {
			// by kidokoro for HackEV debug
			dprintf("end Value = %d\n",endValue);

		}

		// by kidokoro for HackEV
		// 仮想直線ライントレースの場合で、最初の数cmは除外
		static int pre_distance= -1;
		int now_distance;
		if((mode==Course::MODE_TAILVLINE)&&(endTerm==Course::ON_LINE)){
			if(pre_distance==-1){
				//最初の距離情報を覚えておく
				pre_distance = mMeasure->getDistance();
				return true;//ONLINE判定から除外
			}
			else{
				//距離を測定し、最初の数cmはONLINE判定から除外
				now_distance = mMeasure->getDistance();
				dprintf("ONLINE now:%d,pre:%d \n",now_distance,pre_distance);
				if(now_distance - pre_distance<=20 ){//最初の20mm
					dprintf("ONLINE ignore\n");

					return true;//ONLINE判定から除外
				}
				else{//ONLINEと判断。以降のモード終了処理に進む。
					pre_distance=-1;
				}
			}
		}


		if(mode == Course::MODE_STARTDASH){
			//スタートダッシュ終了時に尻尾角度変更
			mTailMotor->setWalk();
		}

		mMeasure->printPosition();

		position0.x = currentPos.x;
		position0.y = currentPos.y;

		distance0 = distance;

		//座標位置
		if( ajust == Course::POS_INIT ){
			//初期化
			mMeasure->init();
			position0.x = 0;
			position0.y = 0;

			if(mode != Course::MODE_STARTDASH){
				shiftX = 0;
			}
		} else if( ajust == Course::POS_AJST ){
			//座標補正値を計算
			mMeasure->ajustment();
		} else if( ajust == Course::POS_END ){
			//3点停止
			mFreeWalker->stop();
//		} else if( ajust == Course::POS_XAJST ){
//			//x軸補正
//			mMeasure->setPositionX(ajustX, ajustY);
//			//mMeasure->setPositionX(ajustX);
//		} else if( ajust == Course::POS_YAJST ){
//			//y軸補正
//			mMeasure->setPositionY(ajustX, ajustY);
//			//mMeasure->setPositionY(ajustY);
		}else if(ajust ==Course::POS_YINIT_BLOCK){
			//ブロック並べの入り口の赤ブロック置き場でY座標を-100にする
			//城所作成途中。
			mMeasure->setPositionY(0,-100);
		} else {
			//無視する
		}

#ifdef __DEBUG__
        if ((mode >= Course::MODE_STARTDASH) && (mode < Course::MODE_MAX) &&
            (endTerm >= Course::NONE) && (endTerm < Course::ENDTERM_MAX)) {
            dprintf("NextData. (pre: mode:%s, endTerm: %s, endValue: %d) %d (%f,%f) distance=%d\n",cModeStr[mode],cEndTermStr[endTerm], endValue, speed, currentPos.x, currentPos.y, distance);
        }
        else {
            dprintf("NextData Err\n");
        }
#endif  /* __DEBUG__    */

		outBeep(1);

		if( mLineTraceInfo->nextData() == false ){
			return false;
		} else {
			//走行モードが変わったときは前回値などを初期化する
			int next_mode = mLineTraceInfo->getMode();

			mCount = 0;
			whiteCount = 0;
			mBalancingWalker->spinInit();
			mFreeWalker->spinInit();

			if( mode != next_mode ){
				mPidController->init();
				mLineMonitor->init();
				initMovingDiff();
				mBalancingWalker->setA_R(DEFAULT_A_R);

#ifdef __DEBUG__
                if ((mode >= Course::MODE_STARTDASH) && (mode < Course::MODE_MAX) &&
                    (next_mode >= Course::MODE_STARTDASH) && (next_mode < Course::MODE_MAX)) {
                    dprintf("PID inited. (mode: %s -> %s)\n", cModeStr[mode], cModeStr[next_mode]);
                }
                else {
                    dprintf("PID inited. MODEERR\n");
                }
#endif  /* __DEBUG__    */
			}
		}
	}

	return true;
}


int SceneLineTrace::lastStraght(double fDiff, int speed, int param1, int param2, int param3){
	static unsigned int ucStatus = 0;
	unsigned int tmp = ucStatus;

	static int grayCount = 0;
	static int grayDistance = 0;

	switch(ucStatus){
	case 0:
		/* 仮想直線トレース 黒線部分 */
		mPidController->setKPID(PARAM_VIRTUALLINE_KP * param3, 0, 0); //左右にふる
		mBalancingWalker->setCommand(speed, mPidController->calc_pid_value(fDiff)); //左ライントレースのみ対応
		mBalancingWalker->run();

		mLineMonitor->setTarget(param1); //grayDistanceに値を入れるために変更

		if( mLineMonitor->isOnLine() == true ){
			grayCount = 0;
			grayDistance = mMeasure->getDistance(); //黒を検出した地点を覚えておく
		} else {
			grayCount++;
		}

		if( grayCount > param2 ){
			//一定期間、黒線検知しなかったら灰色にいると判断
			ucStatus++;
		}

		break;
	case 1:
		/* 仮想直線トレース 灰色部分 */

		//確実に黒をふむために若干、左右にふって前進
		mPidController->setKPID(PARAM_VIRTUALLINE_KP * param3, 0, 0);

		mBalancingWalker->setCommand(speed, mPidController->calc_pid_value(fDiff));
		mBalancingWalker->run();

//		if( mLineMonitor->isOnLine() == true ){
//			ucStatus++;
//		}
		break;
	default:
		/* ゆっくりライントレースしてライン復帰    */
//		mPidController->setKPID(PARAM_LINETRACE_KP, PARAM_LINETRACE_KI, PARAM_LINETRACE_KD);
//
//		mBalancingWalker->setCommand(speed, mPidController->calc_pid_value(mLineMonitor->getDiff()));
//		mBalancingWalker->run();

		break;
	}

	if( tmp != ucStatus ){
		//音を鳴らす
		outBeep(5);
		dprintf("XX %d -> %d \n", tmp, ucStatus);
	}

	return grayDistance;
}

bool SceneLineTrace::backLine(int speed, int R, int angle, double x0, double y0, double x, double y){
	static unsigned int status = 0;
	unsigned int preStatus = status;
	rgb_raw_t rgb;

	static signed int vector = -1; //-1:左から,1:右から

	bool endFlag = false;

	int distance = mMeasure->getDistance(); //走行距離
	static int iniDistance = distance;
	static int distance1; //戻るときの走行距離

	static int mCount = 0; //色検知から反射光検知までの切替には時間がかかる問題への対処
	const int countMAX = 250;

	double fDiff = 0;

	if(vector < 0){
		fDiff = getPositionErr(x0+R, y0, R, 1, x, y);
	} else{
		fDiff = getPositionErr(x0-R, y0, R, 0, x, y);
	}

	 mPidController->setKPID(PARAM_VIRTUALCIRCLE_KP, PARAM_VIRTUALCIRCLE_KI, PARAM_VIRTUALCIRCLE_KD);

//	 colorid_t colorNo = mLineMonitor->getColorNumber();
	 mLineMonitor->getRGB(&rgb);

	switch(status){
	case 0:
		mBalancingWalker->setCommand(speed, mPidController->calc_pid_value(fDiff));
		mBalancingWalker->run();

		//if(mLineMonitor->isOnLine() == true){
		//if( colorNo == COLOR_BLACK){
		if( mLineMonitor->checkColor(COLOR_BLACK) == true ){
			if( vector < 0){
				status = 3;
			} else {
				//右回り
				status = 2;
			}

			//mLineMonitor->init(); //test
		}

		//if((distance - iniDistance) > (M_PI * R * angle/360)){
		//if( colorNo == COLOR_GREEN){
		if( mLineMonitor->checkColor(COLOR_GREEN) == true ){
			status = 1;
			distance1 = distance - iniDistance;
		}

		break;
	case 1:
		//元の位置に戻る状態へ遷移
		mBalancingWalker->setCommand(-speed, -mPidController->calc_pid_value(fDiff));
		mBalancingWalker->run();

		if( iniDistance - distance > distance1 ){
			status = 0;
			vector *= -1;
		}

		break;
	case 2:
		//黒線発見後の後退
		if( mCount++ > countMAX ){
			mBalancingWalker->setCommand(-SPEED_SLOW, -5);

			if(iniDistance - distance > 30){ //距離で判断
			//if( (mLineMonitor->isOnLine() == true) || (iniDistance - distance > 20)){ //カラー目標値
				dprintf("XX %d - %d \n", iniDistance, distance);
				endFlag = true;
			}
		} else {
			mBalancingWalker->setCommand(0, 0);
		}

		mBalancingWalker->run();

		break;
	case 3:
		//黒線発見後の前進
		if( mCount++ > countMAX ){
			mBalancingWalker->setCommand(SPEED_SLOW, 0);

			if(distance - iniDistance > 5){ //距離で判断
				//if(mLineMonitor->crossLine() == true ){ //黒線クロス検知
				dprintf("XX %d - %d \n", iniDistance, distance);
				endFlag = true;
			}
		} else {
			mBalancingWalker->setCommand(0, 0);
		}

		mBalancingWalker->run();

		break;
	default:
		dprintf("[ERR!] case is invalid.\n");
		break;
	}

	//音を鳴らす
	if(status != preStatus){
		iniDistance = distance;
		outBeep(4);
		dprintf("status: %d -> %d \n", preStatus, status);
//		dprintf("XX Kp:%d \n", Kp);
//		dprintf("XX countMAX: %d \n", countMAX);
	}

	return endFlag;
}


bool SceneLineTrace::backLine2(int speed, int turn, int dis1, int dis2){
	static unsigned int status = 0;
	unsigned int preStatus = status;

	static unsigned int countMAX = 400;
	static signed int Kp = 1; //繰り返すときに乗算する係数(1:左から,-1:右から)
	static unsigned int mCount = 0;

	bool endFlag = false;

	int distance = mMeasure->getDistance(); //走行距離
	static int iniDistance = distance;

	turn *= Kp;

	mLineMonitor->setTarget(40);

//	colorid_t colorNo = mLineMonitor->getColorNumber();

	switch(status){
	case 0:
		mBalancingWalker->setCommand(speed, turn);
		mBalancingWalker->run();

		if(mLineMonitor->isOnLine() == true){
//		if( colorNo == COLOR_BLACK ){
			if( turn > 0){
				status = 2;
			} else {
				//右回り
				status = 3;
			}

			dprintf("XX turn:%d, Kp:%d status:%d\n", turn,Kp,status);
		}

//		mCount++;

//		if( colorNo == COLOR_GREEN ){
		if(mCount++ > countMAX){
			//逆方向を探しにいくために元の位置に戻る状態へ
			countMAX = mCount;
			mCount = 0;
			status = 1;
			countMAX *= 1.5;
		}

		break;
	case 1:
		mBalancingWalker->setCommand(-speed, -turn);
		mBalancingWalker->run();

		if(mCount++ > countMAX){
			//見つからなかったとき
			mCount = 0;
			status = 0;
			Kp *= -1;
			//countMAX *= 1.5;
		}

		break;
	case 2:
		//黒線発見後の後退
		mBalancingWalker->setCommand(-speed, turn);
		mBalancingWalker->run();

		if(iniDistance - distance > dis1){
			dprintf("XX %d - %d \n", iniDistance, distance);
			endFlag = true;
		}
		break;
	case 3:
		//黒線発見後の前進
		mBalancingWalker->setCommand(SPEED_SLOW, 0);
		mBalancingWalker->run();

		if(distance - iniDistance > dis2){
			dprintf("XX %d - %d \n", iniDistance, distance);
			endFlag = true;
		}
		break;
	default:

		break;
	}

	//音を鳴らす
	if(status != preStatus){
		iniDistance = distance;
		outBeep(4);
		dprintf("status: %d -> %d \n", preStatus, status);
		dprintf("XX Kp:%d \n", Kp);
		dprintf("XX countMAX: %d \n", countMAX);
	}

	return endFlag;
}

bool SceneLineTrace::TailDown(double x, int speed, int waitCount, bool ischange2to3){
	bool            canSetAngle = false;
	bool            isTailDown  = false;

	const unsigned int  WAIT_COUNT  = waitCount;

	/*! 尻尾状態    */
	static unsigned int uiTailSts   = 0;
	static unsigned int uiCount     = 0;
	static unsigned int uiCount2    = 0;

	if (uiTailSts == 0) {
        canSetAngle     = true;
	}
	else if (uiTailSts == 1) {
		/*! 若干前に出る */
		if ( uiCount < WAIT_COUNT ) {
			mPidController->setKPID(PARAM_VIRTUALLINE_KP, PARAM_VIRTUALLINE_KI, PARAM_VIRTUALLINE_KD);
			mFreeWalker->setCommand(speed, -1*mPidController->calc_pid_value(x));
			mFreeWalker->run();
			uiCount++;
		}
		else {
			canSetAngle = true;
		}
	}
	else {
		if ( uiCount < WAIT_COUNT ) {
			uiCount++;
		}
		else {
			canSetAngle = true;
		}
	}

	if (canSetAngle == true) {
		switch (uiTailSts) {
		case 0:
			mTailMotor->setAngle(80);
			break;
		case 1:
			mTailMotor->setAngle(75);
			break;
		case 2:
			mTailMotor->setAngle(70);
			break;
		case 3:
			mTailMotor->setAngle(64);
			break;
        default:
            dprintf("case Err%d\n", uiTailSts);
            break;
		}
		if( mTailMotor->checkStatus() != true ){
			if (uiTailSts == 0) {
                if (ischange2to3 == true) {
                    mBalancingWalker->setCommand(0, 0);
                    mBalancingWalker->run();
                }
                else {
                    mFreeWalker->setCommand(0, 0);
                    mFreeWalker->run();
                }
			}
			else {
				mFreeWalker->setCommand(0, 0);
				mFreeWalker->run();
			}
			if (uiCount2 == 175) {
                uiCount = 0;
                uiCount2 = 0;
                canSetAngle = false;
                uiTailSts++;
                if (uiTailSts == 4) {
                    uiTailSts    = 0;
                    isTailDown  = true;
                }
			}
			uiCount2++;
		}
		else {
			outBeep(4);
			uiCount = 0;
			uiCount2 = 0;
			canSetAngle = false;
			uiTailSts++;
			if (uiTailSts == 4) {
				uiTailSts    = 0;
				isTailDown  = true;
			}
		}
	}

	return isTailDown;
}

bool SceneLineTrace::TailUp(double x, int speed, int waitCount){
	bool            canSetAngle = false;
	bool            isTailUp    = false;

	/*! 尻尾状態    */
	static unsigned int uiTailSts   = 0;
	static unsigned int uiCount     = 0;
	static unsigned int uiCount2    = 0;
	static bool isWait              = true;
	const unsigned int  WAIT_COUNT  = waitCount;

	if (uiTailSts == 0) {
		canSetAngle = true;
	}
	else {
		if (isWait == true) {
			if ( uiCount < WAIT_COUNT ) {
				mPidController->setKPID(PARAM_VIRTUALLINE_KP, PARAM_VIRTUALLINE_KI, PARAM_VIRTUALLINE_KD);
				mFreeWalker->setCommand(0, 0);
				mFreeWalker->run();
				uiCount++;
			}
			else {
				canSetAngle = true;
			}
		}
	}
	if (canSetAngle == true) {
		switch (uiTailSts) {
		case 0:
			mTailMotor->setAngle(65);
			break;
		case 1:
			mTailMotor->setAngle(70);
			break;
		case 2:
			mTailMotor->setAngle(75);
			break;
		case 3:
			mTailMotor->setAngle(80);
			break;
			//            case 4:
				//                mTailMotor->setAngle(100);
				//                break;
			default:
				printf("case Err%u\n", uiTailSts);
				break;
		}
		if (mTailMotor->checkStatus() != true) {
			mPidController->setKPID(PARAM_VIRTUALLINE_KP, PARAM_VIRTUALLINE_KI, PARAM_VIRTUALLINE_KD);
			if (uiCount2 == 175) {
				mFreeWalker->setCommand(-speed, -1*mPidController->calc_pid_value(x));
				mFreeWalker->run();
			}
			else {
				mFreeWalker->setCommand(0, -1*mPidController->calc_pid_value(x));
				mFreeWalker->run();
				uiCount2++;
			}
		}
		else {
			outBeep(4);
			uiCount = 0;
			uiCount2 = 0;
			canSetAngle = false;
			uiTailSts++;
			if (uiTailSts == 4) {
				uiTailSts   = 0;
				isTailUp    = true;
			}
		}
	}

	return isTailUp;
}

void SceneLineTrace::initMovingDiff(){
	diffList = {};
	diffSum = 0;
	dprintf("MovingDiff inited.\n");
}

double SceneLineTrace::getMovingDiff(int speed, double fDiff){
	const int K = 100; //doubleを100倍してint型のリストに格納

	if( POSERRLIST_MAX < 2 ){
		return fDiff;
	}

	if( speed < SPEED_HIGH1 ){
		//低速走行の場合は移動平均値を使わない
		return fDiff;
	}

	fDiff *= K;

	if(diffList.size() < POSERRLIST_MAX){
		diffList.push_back((int)fDiff);
		diffSum += fDiff;
	}
	else{
		diffSum -= diffList.front();
		diffList.pop_front();
		diffList.push_back((int)fDiff);
		diffSum += fDiff;
	}

//	dprintf("XX diff:,%d, sum:,%d, num:,%d, \n", diff,colorSum,colorList.size());

	return (double)diffSum/diffList.size()/K; //移動平均
}



void SceneLineTrace::modeGoal(double x1, double y1, int param1, double dx, double dy, int speed, int remainDis, int color){
	static double lineY = y1;
	double fDiff;
	int turn;
	static bool init = true;
	static Position position0;
	//static int mInterval = 0;
	static int rotateCount = 0;

	static int remainDis0 = remainDis; //残距離の初期値
	double diffRate = ((double)remainDis) / remainDis0;

	mLineMonitor->setTarget(color);
	double colorDiff = mLineMonitor->getDiff(); //TODO:異常値？要再確認
//	double colorDiff = mLineMonitor->getNormalBrightness(1) - mLineMonitor->getTarget();

	double brightness = mLineMonitor->getNormalBrightness(1);

	int tmp;

	if(init == true){
		//y軸回転補正のための基準点
		position0.x = dx;
		position0.y = param1; //目標y座標

		mMeasure->setPositionY(position0.x, position0.y);
	}

	//黒線に近づくようにy座標ずれを元に仮想直線と実ラインのハイブリッド
	//fDiff = (diffRate * (dy - lineY)) + ((1.0 - diffRate) * colorDiff);
//	fDiff = (diffRate * (dy - param1)) + ((1.0 - diffRate) * colorDiff);

	int bias;
	const int colorRange = 20;

#if 0
	//仮想直線のPIDパラメータ使用で問題なし？おそらくPID制御に問題あり！
	mPidController->setKPID(PARAM_VIRTUALLINE_KP, PARAM_VIRTUALLINE_KI, PARAM_VIRTUALLINE_KD);
	turn = mPidController->calc_pid_value(fDiff);
#else

	//mPidController->setKPID(PARAM_LINETRACE_KP, PARAM_LINETRACE_KI, PARAM_LINETRACE_KD);

//	if( rotateCount >= 3 ){
		bias = 8-(rotateCount);
		if(bias < 1){
			bias = 1;
		}

#if 0
		if( dy > param1 ){
			fDiff = (diffRate * (dy - lineY)) + ((1.0 - diffRate) * colorDiff); //ハイブリッド
		} else {
			//fDiff = dy - param1;
			fDiff = (diffRate * (dy - lineY)) + ((1.0 - diffRate) * colorDiff * -1); //ハイブリッド
		}
		mPidController->setKPID(PARAM_LINETRACE_KP_MAX * bias, PARAM_LINETRACE_KI_MAX, PARAM_LINETRACE_KD_MAX);
#else
		if( (brightness > mLineMonitor->getTarget()-colorRange) && (brightness < mLineMonitor->getTarget()+colorRange) && (dy > param1)){
			//カラーでPID
			mPidController->setKPID(PARAM_LINETRACE_KP_MAX * bias, PARAM_LINETRACE_KI_MAX, PARAM_LINETRACE_KD_MAX);
			//mPidController->setKPID(PARAM_LINETRACE_KP_MAX, PARAM_LINETRACE_KI_MAX, PARAM_LINETRACE_KD_MAX);
			fDiff = colorDiff;
		}else{
			//位置ずれでPID
			mPidController->setKPID(PARAM_LINETRACE_KP_MAX * bias, PARAM_LINETRACE_KI_MAX, PARAM_LINETRACE_KD_MAX);
			fDiff = dy - param1;
		}
#endif

		//fDiff = (diffRate * (dy - param1)) + ((1.0 - diffRate) * colorDiff);

		turn = mPidController->calc_pid_value(fDiff);
//	}
//	else{
//		//mPidController->setKPID(PARAM_LINETRACE_KP_MAX/0.6, 0, 0);
//		bias = 6-(rotateCount*2);
//		if(bias < 1){
//			bias = 1;
//		}
//
//		mPidController->setKPID(PARAM_LINETRACE_KP_MAX * bias, PARAM_LINETRACE_KI_MAX, PARAM_LINETRACE_KD_MAX);
//		//fDiff = dy - lineY;
//		fDiff = dy - param1;
//		turn = mPidController->calc_pid_value(fDiff);
//		//turn = fDiff * 0.5;
//	}
#endif

	if( speed > SPEED_HIGH4 ){
		//dprintf("XX turn : %d < TURNMAX_SPEEDMAX \n", turn);
		//転倒防止のために旋回値の上限・下限設定
		turn = (turn>TURNMAX_SPEEDMAX)? TURNMAX_SPEEDMAX:((turn<-TURNMAX_SPEEDMAX)? -TURNMAX_SPEEDMAX:turn);
	}

//	dprintf("remainDis = %d, remainDis0=%d \n", remainDis, remainDis0);
	dprintf("XX,%d,%f,%f,%f,%f,%f\n", turn, fDiff, diffRate, colorDiff, dy, lineY);


	mBalancingWalker->setCommand(speed, turn);
	mBalancingWalker->run();

	if(init == true){
		init = false;
	} else {
		//if( mInterval++ > 10 ){ //GOALになってからrotateを許可するまでのインターバル
		//if( (dx - position0.x) > 30 ){
		if( (dx - position0.x) > 50 ){
			tmp = mLineMonitor->getNormalBrightness(1);

			if( abs(mLineMonitor->getTarget() - tmp) <= 1 ){
				//outBeep(3);
				lineY = dy;
				dprintf("XX lineY=%f \n", lineY);

				mMeasure->rotate(position0.x, position0.y);

				rotateCount++;
			}
		}else{
			dprintf("rotate skipped. x=%f (%f)\n", dx, position0.x);
		}
	}
}



void SceneLineTrace::setShiftX(int x){
	shiftX = x;
	dprintf("XX shiftX = %d \n", shiftX);
}


//double SceneLineTrace::getCosTheta(double x1, double y1, double x2, double y2, double dx, double dy){
//	// a^2=b^2+c^2-2bcosθ
//
//	double a2 = pow((x2-x1),2)+pow((y2-y1),2); //a^2
//	double b2 = pow((x2-dx),2)+pow((y2-dy),2); //b^2
//	double c2 = pow((x1-dx),2)+pow((y1-dy),2); //c^2
//
//	double cos_theta;
//
//	cos_theta = (b2+c2-a2)/(2*sqrt(b2));
//
//	return cos_theta;
//}


void SceneLineTrace::setA_R(int value){
	if(value == 0){
		mBalancingWalker->setA_R(DEFAULT_A_R); //デフォルト値
	} else {
		mBalancingWalker->setA_R((float)value/1000);
	}
}

double SceneLineTrace::getPositionErr(double iCenterX, double iCenterY, double iRadius, unsigned char ucRotation, double dx, double dy)
{
    double dPosErr;
    double dDistance;
    double dCenterX;
    double dCenterY;
    double dRadius;

    dCenterX    = (double)iCenterX;
    dCenterY    = (double)iCenterY;
    dRadius     = (double)iRadius;

    /* 仮想円の中心から現在位置までの距離を求める   */
    dDistance   = (dx - dCenterX) * (dx - dCenterX) +
                  (dy - dCenterY) * (dy - dCenterY);
    dDistance   = sqrt(dDistance);

    /* 目標地点と現在位置までの誤差を求める */
    if (ucRotation == 0) { //右回り
        dPosErr = dDistance - dRadius;
    }
    else if (ucRotation == 1) { //左回り
        dPosErr = dRadius - dDistance;
    }
    else {
        dPosErr = 0;
        dprintf("Select Rotation Err\n");
    }
    return dPosErr;
}

#if 0
double SceneLineTrace::getLinePosErr(double x1, double y1, double x2, double y2, double dx, double dy){
	//進行方向の計算
	int vector = 1;
	if(x2 > x1){
		if(y2 > y1){
			vector = -1;
		} else{
			vector = -1;
		}
	} else if(x2 < x1){
		if(y2 <= y1){
			vector = 1;
		}else{
			vector = -1;
		}
	} else { //x2==x1
		if(y2 > y1){
			vector = 1;
		} else{
			vector = 1;
		}
	}

	if( x2 != x1 ){
		// ax + by + c = 0の一次式でb=-1とする
		double a = (y2-y1)/(x2-x1);
		double c = y1 - (a * x1);

		return vector * (a*dx - dy +c)/sqrt(a*a+1);
//		return vector * abs(a*dx - dy +c)/sqrt(a*a+1);
	} else {
		if( y2 - y1 > 0 ){
			return -1 * (dx - x1);
		}else{
			return (dx - x1);
		}
	}
}
#else
double SceneLineTrace::getLinePosErr(double x1, double y1, double x2, double y2, double dx, double dy){
    double  a;
    double  c;
    double  dRtn;
    double  y;
    bool    isRtn = false;

    if ((x1 == x2) && (y1 == y2)) {
        dprintf("ParamErr\n");
    }
    else if (x1 == x2) {
        if( y2 - y1 > 0 ){
            dRtn = -1 * (dx - x1);
        }else{
            dRtn = (dx - x1);
        }
        isRtn = true;
    }
    else if (y1 == y2) {
        if (x2  - x1 > 0) {
            dRtn = (dy - y1);
        }
        else {
            dRtn = -1 * (dy - y1);
        }
        isRtn = true;
    }
    else {
        ;
    }
    if (isRtn == false) {
        a = (y2-y1)/(x2-x1);
        c = y1 - (a * x1);
        dRtn = abs(a*dx - dy +c)/sqrt(a*a+1);
        //進行方向の計算
        y = a * dx + c;
        if (x2 > x1) {
            if (dy < y) {
                dRtn *= -1;
            }
        }
        else {
            if (dy > y) {
                dRtn *= -1;
            }
        }
    }
    return dRtn;
}
#endif
