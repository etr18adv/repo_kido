/**
 ******************************************************************************
 ** ファイル名 : app.c
 **
 ** 概要 : 2輪倒立振子ライントレースロボットのTOPPERS/HRP2用Cサンプルプログラム
 **
 ** 注記 : sample_c4 (sample_c3にBluetooth通信リモートスタート機能を追加)
 ******************************************************************************
 **/

#include "ev3api.h"
#include "app.h"

#if defined(BUILD_MODULE)
	#include "module_cfg.h"
#else
	#include "kernel_cfg.h"
#endif

/*** 共通機能 *****************************************************/
/** センサー、モーター定義 */
static const sensor_port_t touch_sensor	= EV3_PORT_1;
static const sensor_port_t color_sensor	= EV3_PORT_2;
static const sensor_port_t sonar_sensor	= EV3_PORT_3;
static const sensor_port_t gyro_sensor	= EV3_PORT_4;

static const motor_port_t arm_motor   = EV3_PORT_A;
static const motor_port_t right_motor = EV3_PORT_B;
static const motor_port_t left_motor  = EV3_PORT_C;
static const motor_port_t tail_motor  = EV3_PORT_D;

void WheelControl( int front, float turn );
int PID_feedback( float x, float Kp, float Ki, float Kd, float dt );
void TailSet( void );

static int Speed   = 50;
static int Target  = 50;
static float Correct = 0.30F;
static float Kp = 0.80F;
static float Ki = 0.00F;
static float Kd = 0.06F;
static int TargetTailBuf = 0;
static int TargetTail = 0;
static int TailSetTime = 0;
static int ArmTarget = 10;

enum State{
	Wait,
	Ready,
	Run,
	Run2_Ready,
	Run2
}m_state = Wait;

#define LCD_Setup		0x0101
#define	LCD_Select1		0x0102
#define	LCD_Select2		0x0103
#define	LCD_Select3		0x0104
#define	LCD_Select4		0x0105
#define	LCD_Select5		0x0106
#define	LCD_Select6		0x0107
#define	LCD_Select7		0x0108
#define	LCD_Select8		0x0109
#define	LCD_Ready		0x010A
#define	LCD_Start		0x010B
void LCD_Set( int mEvt );

static enum {
	Dataset1,
	Dataset2,
	Dataset3,
	Dataset4,
	Dataset5,
	Dataset6,
	Dataset7,
	Dataset8
}m_enDataSetState = Dataset1;


/******************************************************************/
#define ButtonEvt_None	0
#define ButtonEvt_Trg	1
struct ButtonEvtAns{
	int Answer[ 4 ];
};

#define	BtUp		0
#define	BtDown		1
#define	BtLeft		2
#define	BtRight		3

void ButtonMainCycle( struct ButtonEvtAns *ans );

static FILE	 *bt = NULL;	 /* Bluetoothファイルハンドル */

/*** Function *******************************************************/
/* メインタスク */
void main_task( intptr_t unused )
{
	ev3_lcd_set_font( EV3_FONT_MEDIUM );

	LCD_Set( LCD_Setup );

	/* センサー入力ポートの設定 */
	ev3_sensor_config( sonar_sensor, ULTRASONIC_SENSOR );
	ev3_sensor_config( color_sensor, COLOR_SENSOR );
	ev3_sensor_config( touch_sensor, TOUCH_SENSOR );
	ev3_sensor_config( gyro_sensor, GYRO_SENSOR );

	/* モーター出力ポートの設定 */
	ev3_motor_config( left_motor,  LARGE_MOTOR );
	ev3_motor_config( right_motor, LARGE_MOTOR );
	ev3_motor_config( arm_motor,   LARGE_MOTOR );
	ev3_motor_config( tail_motor,  MEDIUM_MOTOR );

	/*  */
	ev3_motor_stop( left_motor, true );
	ev3_motor_stop( right_motor, true );	
	ev3_motor_stop( tail_motor, true );	
	ev3_motor_reset_counts( tail_motor );
	{
		unsigned short u16_x;

		for( u16_x = 0; u16_x < 150; u16_x++ ){
			ev3_motor_set_power( arm_motor, -10 );
			tslp_tsk( 10 );
		}

		ev3_motor_stop( arm_motor, true );
		for( u16_x = 0; u16_x < 10; u16_x++ ){
			tslp_tsk( 10 );
		}

		ev3_motor_reset_counts( arm_motor );
		for( u16_x = 0; u16_x < 10; u16_x++ ){
			tslp_tsk( 10 );
		}
	}
	LCD_Set( LCD_Select1 );

	bt = ev3_serial_open_file(EV3_SERIAL_BT);

	// 周期ハンドラ開始
	ev3_sta_cyc( EV3_CYC_TRACER );
	slp_tsk( ); 				 // バックボタンが押されるまで待つ

	// 周期ハンドラ停止
	ev3_stp_cyc( EV3_CYC_TRACER );

	ev3_motor_stop( left_motor, false );
	ev3_motor_stop( right_motor, false );
	ev3_motor_stop( arm_motor, false );
	ev3_motor_stop( tail_motor, false );

	ext_tsk( );
}

/* 4ms周期サイクル */
void ev3_cyc_tracer(intptr_t exinf) {
	static int Count = 0;
	static int State = 0;
	static int ReadyCount = (1500 / 4);
	
	rgb_raw_t rgbValue;

	/**********************************************************************************************/
	/* バックボタンで強制終了 *********************************************************************/
	/**********************************************************************************************/
	if( ev3_button_is_pressed( BACK_BUTTON ) ){
		wup_tsk( MAIN_TASK );  // バックボタン押下
		return;
	}

	struct ButtonEvtAns BtAns;
	ButtonMainCycle( &BtAns );	
	{
		static int WaitCount = 0;		/* 連続して押さないように */
		if( 0 == WaitCount ){
			if (ev3_touch_sensor_is_pressed(touch_sensor) == 1){
				/* タッチセンサが押された場合 */
				if( Wait == m_state ){
					/* スタート準備 */
					m_state = Ready;
					LCD_Set( LCD_Ready );
					ev3_motor_reset_counts( left_motor );		/* タイヤの回転量をリセット */
					ev3_motor_reset_counts( right_motor );
					ReadyCount = (1500 / 4);					/* 1.5s後にスタート */
				}
				if( Run == m_state ){
					/* 待機状態に戻る */
					m_state = Wait;
					m_enDataSetState = Dataset1;
					LCD_Set( LCD_Select1 );
					WaitCount = ( 1000 / 4 );	
				}
			}
		}else{
			WaitCount--;
		}
	}

	ev3_color_sensor_get_rgb_raw( color_sensor, &rgbValue );


	{
		static int RCount = 0;
		static float CG = 0, CB = 0;

		switch( m_state ){
			case Wait:
				/* パラメータセット */
				DataSet( BtAns );
				/* タイヤは停止 */
				ev3_motor_set_power(  left_motor, 0 );
				ev3_motor_set_power( right_motor, 0 );
				break;

			case Ready:
				{
					/* 設定時間経過後にスタート */
					ReadyCount--;
					//ev3_motor_set_power(tail_motor, 60);
					if( 0 == ReadyCount ){
						LCD_Set( LCD_Start );
						m_state = Run;
						RCount  = 0;
						ev3_motor_set_power(tail_motor, 0 );
					}
				}
				break;		

			case Run:
				/* 左右、尻尾モータに出力 */
				WheelControl( Speed, (float)( PID_feedback( (rgbValue.r - Target), Kp, Ki, Kd, 0.004F ) ) );
				if( ev3_bluetooth_is_connected( ) ){
					fprintf( bt, "%d,%d,%d,%d,%d,\r\n", ev3_motor_get_counts( left_motor ), ev3_motor_get_counts( right_motor ), rgbValue.r, rgbValue.g, rgbValue.b );
				}
				{
					static int GrayWaitCount = 0;
					if( RCount < 100 ){
						RCount++;
						if( 100 == RCount ){
							/* RGB補正 */
							CG = (float)rgbValue.r / rgbValue.g;
							CB = (float)rgbValue.r / rgbValue.b;
							GrayWaitCount = 0;
						}
					}else{
						if( ( 15 <= ( ( rgbValue.g * CG ) - rgbValue.r ) ) && 
							( 15 <= ( ( rgbValue.b * CB ) - rgbValue.r ) ) ){
							///* 灰色を検知した場合 */
							//GrayWaitCount++;
							//if( 20 <= GrayWaitCount ){
							//	m_state = Run2_Ready;
							//	RCount  = 0;
							//	///* 待機状態に戻る */
							//	//m_state = Wait;
							//	//m_enDataSetState = Dataset1;
							//	//LCD_Set( LCD_Select1 );
							//}
						}else{
							GrayWaitCount = 0;
						}
					}
				}
				break;
			
			case Run2_Ready:
				RCount++;
				if( ( 1500 / 4 ) <= RCount ){
					m_state = Run2;
				}
				if( ev3_bluetooth_is_connected( ) ){
					fprintf( bt, "%d,%d,%d,%d,%d,\r\n", ev3_motor_get_counts( left_motor ), ev3_motor_get_counts( right_motor ), rgbValue.r, rgbValue.g, rgbValue.b );
				}
				/* タイヤは停止 */
				ev3_motor_set_power(  left_motor, 0 );
				ev3_motor_set_power( right_motor, 0 );
				break;
	
			case Run2:
				/* 左右、尻尾モータに出力 */
				WheelControl( Speed, (float)( PID_feedback( (Target - rgbValue.r), Kp, Ki, Kd, 0.004F ) ) );	/* 反転 */
				if( ev3_bluetooth_is_connected( ) ){
					fprintf( bt, "%d,%d,%d,%d,%d,\r\n", ev3_motor_get_counts( left_motor ), ev3_motor_get_counts( right_motor ), rgbValue.r, rgbValue.g, rgbValue.b );
				}
				{	
					static int GreenWaitCount = 0;
					if( ( 15 <= ( ( rgbValue.g * CG ) - rgbValue.r ) ) && 
						( 15 <= ( ( rgbValue.g * CG ) - ( rgbValue.b * CB ) ) ) ){
						/* 緑色を検知した場合 */
						GreenWaitCount++;
						if( 20 <= GreenWaitCount ){
							/* 待機状態に戻る */
							m_state = Wait;
							m_enDataSetState = Dataset1;
							LCD_Set( LCD_Select1 );
						}
					}else{
						GreenWaitCount = 0;
					}
				}
				break;

			default:
				;
				break;
		}
	}
	
	int ArmPower = ( ArmTarget - ev3_motor_get_counts(arm_motor) ) * 2;
	ev3_motor_set_power( arm_motor, ArmPower );

	TailSet( );
}

/* タイヤ処理 ***************************/
int PID_feedback( float x, float Kp, float Ki, float Kd, float dt )
{
	static float p = 0, i = 0, d = 0, integral = 0;
	static int prev_x = 0;

	p         = Kp * x;
	integral += ( prev_x + x ) / 2.0F * dt;
	d         = Kd * ( x - prev_x ) / dt;
	i         = Ki * integral;

	prev_x = x;
	
	return((int)( p + i + d ));
}

void WheelControl( int front, float turn ){
	ev3_motor_set_power(  left_motor, (int)( front + ( turn * Correct ) ) );
	ev3_motor_set_power( right_motor, (int)( front - ( turn * Correct ) ) );
}

/* パラメータ設定処理 ****************************/
void DataSet( struct ButtonEvtAns m_BtAns ){
	switch( m_enDataSetState ){
		case Dataset1:
			if( ButtonEvt_Trg == m_BtAns.Answer[ BtUp ] ){
				LCD_Set( LCD_Select8 );
				m_enDataSetState = Dataset8;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtDown ] ){
				LCD_Set( LCD_Select2 );
				m_enDataSetState = Dataset2;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtRight ] ){
				Speed += 10;
				LCD_Set( LCD_Select1 );
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtLeft ] ){
				Speed -= 10;
				LCD_Set( LCD_Select1 );
			}else{
				;
			}		
			break;

		case Dataset2:		
			if( ButtonEvt_Trg == m_BtAns.Answer[ BtUp ] ){
				LCD_Set( LCD_Select1 );
				m_enDataSetState = Dataset1;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtDown ] ){
				LCD_Set( LCD_Select3 );
				m_enDataSetState = Dataset3;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtRight ] ){
				Target += 10;
				LCD_Set( LCD_Select2 );
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtLeft ] ){
				Target -= 10;
				LCD_Set( LCD_Select2 );
			}else{
				;
			}		
			break;

		case Dataset3:		
			if( ButtonEvt_Trg == m_BtAns.Answer[ BtUp ] ){
				LCD_Set( LCD_Select2 );
				m_enDataSetState = Dataset2;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtDown ] ){
				LCD_Set( LCD_Select4 );
				m_enDataSetState = Dataset4;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtRight ] ){
				Correct += 0.05;
				LCD_Set( LCD_Select3 );
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtLeft ] ){
				Correct -= 0.05;
				LCD_Set( LCD_Select3 );
			}else{
				;
			}					
			break;

		case Dataset4:		
			if( ButtonEvt_Trg == m_BtAns.Answer[ BtUp ] ){
				LCD_Set( LCD_Select3 );
				m_enDataSetState = Dataset3;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtDown ] ){
				LCD_Set( LCD_Select5 );
				m_enDataSetState = Dataset5;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtRight ] ){
				Kp += 0.01;
				LCD_Set( LCD_Select4 );
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtLeft ] ){
				Kp -= 0.01;
				LCD_Set( LCD_Select4 );
			}else{
				;
			}					
			break;

		case Dataset5:		
			if( ButtonEvt_Trg == m_BtAns.Answer[ BtUp ] ){
				LCD_Set( LCD_Select4 );
				m_enDataSetState = Dataset4;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtDown ] ){
				LCD_Set( LCD_Select6 );
				m_enDataSetState = Dataset6;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtRight ] ){
				Ki += 0.01;
				LCD_Set( LCD_Select5 );
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtLeft ] ){
				Ki -= 0.01;
				LCD_Set( LCD_Select5 );
			}else{
				;
			}					
			break;

		case Dataset6:		
			if( ButtonEvt_Trg == m_BtAns.Answer[ BtUp ] ){
				LCD_Set( LCD_Select5 );
				m_enDataSetState = Dataset5;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtDown ] ){
				LCD_Set( LCD_Select7 );
				m_enDataSetState = Dataset7;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtRight ] ){
				Kd += 0.01;
				LCD_Set( LCD_Select6 );
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtLeft ] ){
				Kd -= 0.01;
				LCD_Set( LCD_Select6 );
			}else{
				;
			}					
			break;

		case Dataset7:		
			if( ButtonEvt_Trg == m_BtAns.Answer[ BtUp ] ){
				LCD_Set( LCD_Select6 );
				m_enDataSetState = Dataset6;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtDown ] ){
				LCD_Set( LCD_Select8 );
				m_enDataSetState = Dataset8;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtRight ] ){
				TargetTailBuf += 360;
				TailSetTime = 1500 / 4;
				if( TargetTailBuf > 3960 ){
					TargetTailBuf = 3960;
				}
				LCD_Set( LCD_Select7 );
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtLeft ] ){
				TargetTailBuf -= 360;
				TailSetTime = 1500 / 4;
				if( TargetTailBuf < 0 ){
					TargetTailBuf = 0;
				}
				LCD_Set( LCD_Select7 );
			}else{
				;
			}					
			break;

		case Dataset8:		
			if( ButtonEvt_Trg == m_BtAns.Answer[ BtUp ] ){
				LCD_Set( LCD_Select7 );
				m_enDataSetState = Dataset7;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtDown ] ){
				LCD_Set( LCD_Select1 );
				m_enDataSetState = Dataset1;
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtRight ] ){
				ArmTarget += 5;
				LCD_Set( LCD_Select8 );
			}else if( ButtonEvt_Trg == m_BtAns.Answer[ BtLeft ] ){
				ArmTarget -= 5;
				LCD_Set( LCD_Select8 );
			}else{
				;
			}					
			break;

		default:
			;
			break;
	}
}

/* LCD表示処理 ************************************************************/
void LCD_Set( int mEvt ){
	char LcdStr[18];
	switch( mEvt ){
		case LCD_Select1:
			sprintf(LcdStr, "Setting 1 >>     " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, ">Speed  : %d    ", Speed );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, " Target : %d    ", Target );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			sprintf(LcdStr, " Corrent: %.2f  ", Correct );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );
			sprintf(LcdStr, " PID-P  : %.2f  ", Kp );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			sprintf(LcdStr, " PID-I  : %.2f  ", Ki );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			sprintf(LcdStr, " PID-D  : %.2f  ", Kd );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			break;

		case LCD_Select2:
			sprintf(LcdStr, "Setting 1 >>     " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, " Speed  : %d    ", Speed );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, ">Target : %d    ", Target );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			sprintf(LcdStr, " Corrent: %.2f  ", Correct );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );
			sprintf(LcdStr, " PID-P  : %.2f  ", Kp );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			sprintf(LcdStr, " PID-I  : %.2f  ", Ki );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			sprintf(LcdStr, " PID-D  : %.2f  ", Kd );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			break;

		case LCD_Select3:
			sprintf(LcdStr, "Setting 1 >>     " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, " Speed  : %d    ", Speed );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, " Target : %d    ", Target );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			sprintf(LcdStr, ">Corrent: %.2f  ", Correct );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );			
			sprintf(LcdStr, " PID-P  : %.2f  ", Kp );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			sprintf(LcdStr, " PID-I  : %.2f  ", Ki );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			sprintf(LcdStr, " PID-D  : %.2f  ", Kd );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			break;

		case LCD_Select4:
			sprintf(LcdStr, "Setting 1 >>     " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, " Speed  : %d    ", Speed );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, " Target : %d    ", Target );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			sprintf(LcdStr, " Corrent: %.2f  ", Correct );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );			
			sprintf(LcdStr, ">PID-P  : %.2f  ", Kp );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			sprintf(LcdStr, " PID-I  : %.2f  ", Ki );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			sprintf(LcdStr, " PID-D  : %.2f  ", Kd );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			break;

		case LCD_Select5:
			sprintf(LcdStr, "Setting 1 >>     " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, " Speed  : %d    ", Speed );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, " Target : %d    ", Target );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			sprintf(LcdStr, " Corrent: %.2f  ", Correct );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );			
			sprintf(LcdStr, " PID-P  : %.2f  ", Kp );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			sprintf(LcdStr, ">PID-I  : %.2f  ", Ki );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			sprintf(LcdStr, " PID-D  : %.2f  ", Kd );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			break;

		case LCD_Select6:
			sprintf(LcdStr, "Setting 1 >>     " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, " Speed  : %d    ", Speed );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, " Target : %d    ", Target );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			sprintf(LcdStr, " Corrent: %.2f  ", Correct );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );			
			sprintf(LcdStr, " PID-P  : %.2f  ", Kp );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			sprintf(LcdStr, " PID-I  : %.2f  ", Ki );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			sprintf(LcdStr, ">PID-D  : %.2f  ", Kd );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			break;

		case LCD_Select7:
			sprintf(LcdStr, "<< Setting 2     " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, ">Tail   : %d    ", TargetTailBuf );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, " Arm    : %d    ", ArmTarget );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			sprintf(LcdStr, "                 " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 7 );
			break;

		case LCD_Select8:
			sprintf(LcdStr, "<< Setting 2     " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, " Tail   : %d    ", TargetTailBuf );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, ">Arm    : %d    ", ArmTarget );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			sprintf(LcdStr, "                 " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 7 );
			break;

		case LCD_Ready:
			sprintf(LcdStr, "                 " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, " REDAY!!         " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, "                 " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 7 );
			break;

		case LCD_Start:
			sprintf(LcdStr, "                 " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, " GO!!            " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, "                 " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 7 );
			break;

		case LCD_Setup:
			sprintf(LcdStr, "                 " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 0 );
			sprintf(LcdStr, " Setup...        " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 1 );
			sprintf(LcdStr, " Battery: %dmV ", ev3_battery_voltage_mV() );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 2 );
			sprintf(LcdStr, "                 " );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 3 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 4 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 5 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 6 );
			ev3_lcd_draw_string( LcdStr, 7, 15 * 7 );
			break;

		default:

			break;
	}
}

void TailSet( void ){
	int m_TailCount = ev3_motor_get_counts( tail_motor );

	
	if( 0 == TailSetTime ){
		TargetTail = TargetTailBuf;
	}else{
		TailSetTime--;
	}

	if( TargetTail >= ( m_TailCount + 20 ) ){
		if( TargetTail >= ( m_TailCount + 20 ) ){
			ev3_motor_set_power( tail_motor, 70 );
		}else{
			ev3_motor_set_power( tail_motor, 20 );
		}
	}else if( TargetTail <= ( m_TailCount - 20 ) ){
		if( TargetTail <= ( m_TailCount - 20 ) ){
			ev3_motor_set_power( tail_motor, -70 );
		}else{
			ev3_motor_set_power( tail_motor, -20 );
		}
	}else{
		ev3_motor_set_power( tail_motor, 0 );
		ev3_motor_stop( tail_motor, true );
	}
}

/* 本体ボタン処理(チャタリング対応) ********************************************************************/
enum ButtonState{
	StateOff = 0,
	StateOn
};

static struct{
	enum ButtonState State;
	unsigned char    Buff;
	button_t    TargetButton;	/* ←typedefなのでenumいらない */
}ButtonData[ 4 ] =
{
	{ StateOff, 0x00, UP_BUTTON   },
	{ StateOff, 0x00, DOWN_BUTTON  },
	{ StateOff, 0x00, LEFT_BUTTON  },
	{ StateOff, 0x00, RIGHT_BUTTON  }
};

static int ButtonCount = 0;

void ButtonMainCycle( struct ButtonEvtAns *ans ){
	{
		int i;
		for( i = 0; i < 4; i++ ){
			ans->Answer[ i ] = ButtonEvt_None;
		}
	}

	ButtonCount++;
	if( ( 40 / 4 ) <= ButtonCount ){
		ButtonCount = 0;
		{
			int i;
			for( i = 0; i < 4; i++ ){
				ButtonData[ i ].Buff <<= 1;
				if( ev3_button_is_pressed( ButtonData[ i ].TargetButton ) ){
					ButtonData[ i ].Buff |= 0x01;
				}else{
					ButtonData[ i ].Buff &= ~0x01;					
				}

				switch( ButtonData[ i ].State ){
					case StateOff:
						if( 0x03 == ( ButtonData[ i ].Buff & 0x03 ) ){
							ButtonData[ i ].State = StateOn;
						}
						break;
					case StateOn:
						if( 0x00 == ( ButtonData[ i ].Buff & 0x03 ) ){
							ButtonData[ i ].State = StateOff;
							ans->Answer[ i ] = ButtonEvt_Trg;
						}						
						break;
					default:
						;	/* 異常 */
						break;
				}
			}
		}
	}
}

/*********************************************************************/
