/*
 * TailChange.cpp
 *
 *  Created on: 2018/05/06
 *      Author: Masahiro
 */

#include "TailChange.h"

TailChange::TailChange(ev3api::Motor& tailMotor)
		:mTailMotor(tailMotor),mIsInitialized(false) {
	// TODO 自動生成されたコンストラクター・スタブ
}


TailChange::~TailChange() {
	// TODO Auto-generated destructor stub
}

//run :デバッグ用メソッド
//引数は今は使っていない。
bool TailChange::run(int32_t arc){
	char s[32];
	static int st=0;
	//リセット
	if (mIsInitialized == false) {
		if(tailReset()==true){
			mIsInitialized = true;
			sprintf(s,"%s","TAIL Initialized");
			ev3_lcd_draw_string(s,0,60);
		}
    }
	//tailSet()の動作確認
	else{
		if(st==0){
			if(tailSet(4000)){
				st++;
				return false;
			}
			else{
				return false;
			}
		}

		else if(st==1){
			if( tailSet(2000) ){
				st++;
				return false;
			}
			else{
				return false;
			}
		}

		else if(st==2){
			if( tailSet(3000) ){
				st++;
				return false;
			}
			else{
				return false;
			}
		}
		else if(st==3){
			if( tailSet(1000) ){
				st++;
				return false;
			}
			else{
				return false;
			}
		}

		else{
			return true;
		}
	}
}

//尻尾のリセット
//モータをマイナス方向に回して押し付けてから（今のところ１秒）
//リセットする
bool TailChange::tailReset(){
	static int state=0;
	static int32_t count=250;//1000ms
	char s[32];

	if (state==0){
		sprintf(s,"TAIL count1 %d",mTailMotor.getCount());
		ev3_lcd_draw_string(s,0,20);

		mTailMotor.setPWM(-10);
		count--;
		if(count<=0){
			state++;
			mTailMotor.stop();
		}
		return false;
	}
	else{// モータエンコーダをリセットする
		mTailMotor.reset();
//		sprintf(s,"%s","TAILReset Done");
		sprintf(s,"TAIL count2 %d",mTailMotor.getCount());
		ev3_lcd_draw_string(s,0,40);
		return true;
	}
}

// 指定角度に尻尾をセットする。
// int arc:尻尾モータの回転角度
//尻尾を閉じた状態を０とし、正の値しか取らない。
// 指定角度まで尻尾を動かし終えたらtrueを返す
bool TailChange::tailSet(int32_t arc){
	int pwm=30;
	char s[32];
	int32_t count;	//現在角度

	//arcが0から4000の範囲以外はエラー終了
	if(arc<0||arc>4000){
		printf("ERROR:tailSet() arc<0\n");
		return true;
	}

	//現在角度を確認する。
	count = mTailMotor.getCount();

	//現在角度＜指定角度の場合→尻尾を開く
	if(count<arc){
		mTailMotor.setPWM(pwm);/*スピード３０で動かす*/

	}
	//現在角度＞指定角度の場合→尻尾を閉じる
	else{
		mTailMotor.setPWM(-pwm);/*スピード-３０で動かす*/
	}

	sprintf(s,"c %d: a %d",count,arc);
	ev3_lcd_draw_string(s,0,80);
//	printf("c %d: a %d\n",count,arc);// BTに出力

	if(count==arc){/*指定角度まできた？*/ //todo 動作が早すぎるとぴったりにならないかも。
		mTailMotor.stop();
		sprintf(s,"STOP count=%d:%d",count,arc);
		ev3_lcd_draw_string(s,0,100);
		return true;
	}
	else{
		return false;
	}

}


#if 0
bool TailChange::tailSet(int arc){
	int pwm=30;
	char s[32];
	/*角位置９０度まで動かしてみる*/
	mTailMotor.setPWM(pwm);/*スピード３０で動かす*/

	int count;
	count = mTailMotor.getCount();

	sprintf(s,"count=%d:%d",count,arc);
	ev3_lcd_draw_string(s,0,80);
	printf("count=%d:%d\n",count,arc);// BTに出力
	if(count>10 && count==arc){/*指定角度まできた？*/
		mTailMotor.stop();
		sprintf(s,"STOP count=%d:%d",count,arc);
		ev3_lcd_draw_string(s,0,100);
		return true;
	}
	else{
		return false;
	}

}
#endif
