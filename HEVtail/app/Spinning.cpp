/*
 * Spinning.cpp
 *
 *  Created on: 2018/04/08
 *      Author: Masahiro
 */

#include "Spinning.h"

Spinning::Spinning(const LineMonitor* lineMonitor,
		ev3api::Motor& leftWheel,
		ev3api::Motor& rightWheel,
		SimpleTimer* simpleTimer)
	: mLineMonitor(lineMonitor),
	  mLeftWheel(leftWheel),
	  mRightWheel(rightWheel),
	  mSimpleTimer(simpleTimer),
	  mIsInitialized(false) {
		// TODO 自動生成されたコンストラクター・スタブ

}

Spinning::~Spinning() {
	// TODO Auto-generated destructor stub
}

bool Spinning::run(int time){
	int pwm = 15;

	   // タイマーをLineTracerの中で実施する
	    if(mSimpleTimer->isStarted()){//タイマー開始した（開始時刻がゼロでない）
			mLeftWheel.setPWM(pwm);
			mRightWheel.setPWM(-pwm);
			if(mSimpleTimer->isTimedOut()){//タイマー終了確認
				mSimpleTimer->stop();			//【重要】タイマーストップ。開始時刻ゼロ。
				return true;
			}
			else{
				return false;
			}

	    }
	    	else{//タイマー開始していない（開始時刻がゼロ）
	    		mSimpleTimer->setTime(time);//指定時間をセット
	        mSimpleTimer->start();
	    		return false;
	    }
}
