﻿/******************************************************************************
 * app.cpp
 * 2018.4.21更新
 * Author: Masahiro Kidokoro
 *
 ＜ベースソフトの情報＞
 *  app.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Implementation of the Task main_task
 *  Author: Kazuhiro.Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "app.h"
#include "HackEV.h"

// デストラクタ問題の回避
// https://github.com/ETrobocon/etroboEV3/wiki/problem_and_coping
void *__dso_handle=0;

// using宣言
using ev3api::ColorSensor;
//using ev3api::GyroSensor;
using ev3api::TouchSensor;
using ev3api::Motor;
using ev3api::Clock;

// Device objects
// オブジェクトを静的に確保する
ColorSensor gColorSensor(PORT_2);/*HackEV*/
//GyroSensor  gGyroSensor(PORT_4);
TouchSensor gTouchSensor(PORT_1);
Motor       gLeftWheel(PORT_C);
Motor       gRightWheel(PORT_B);
Motor       gTailMotor(PORT_D,true,MEDIUM_MOTOR);
Clock       gClock;

// オブジェクトの定義
static LineMonitor     *gLineMonitor;
static Starter         *gStarter;
static SimpleTimer     *gSpinTimer;
static SimpleTimer     *gTraceTimer;
static LineTracer      *gLineTracer;
static Spinning  *gSpinning;
static HackEV    *gHackEV;
static TailChange *gTailChange;


/**
 * EV3システム生成
 */
static void user_system_create() {
    // [TODO] タッチセンサの初期化に2msのdelayがあるため、ここで待つ
    tslp_tsk(2);

    // オブジェクトの作成
    gStarter         = new Starter(gTouchSensor);
    gLineMonitor     = new LineMonitor(gColorSensor);
    gSpinTimer   = new SimpleTimer(gClock);
    gTraceTimer     = new SimpleTimer(gClock);
    gLineTracer      = new LineTracer(gLineMonitor, gLeftWheel ,gRightWheel,gTraceTimer);/*HackEV対応*/
    gSpinning  = new Spinning(gLineMonitor,gLeftWheel,gRightWheel,gSpinTimer);
    gTailChange = new TailChange(gTailMotor);/*尻尾追加*/
    gHackEV    = new HackEV(gLineTracer,
                                        gSpinning,
                                        gStarter,
                                        gTraceTimer,
										gTailChange);

    ev3_motor_config(EV3_PORT_D,MEDIUM_MOTOR);//尻尾モータの設定

    // 初期化完了通知
    ev3_led_set_color(LED_ORANGE);
}

/**
 * EV3システム破棄
 */
static void user_system_destroy() {
    gLeftWheel.reset();
    gRightWheel.reset();

    delete gHackEV;
    delete gSpinning;
    delete gLineTracer;
    delete gSpinTimer;
    delete gTraceTimer;
    delete gLineMonitor;
    delete gStarter;
    delete gTailChange;
}

/**
 * トレース実行タイミング
 */
void ev3_cyc_tracer(intptr_t exinf) {
    act_tsk(TRACER_TASK);
}

/**
 * メインタスク
 */
void main_task(intptr_t unused) {
    user_system_create();  // センサやモータの初期化処理

    /* Open Bluetooth file */
    gHackEV->bt = ev3_serial_open_file(EV3_SERIAL_BT);
    assert(gHackEV->bt != NULL);

    /* Bluetooth通信タスクの起動 */
    act_tsk(BT_TASK);
#if 0
    while(1){
		ev3_lcd_draw_string("WAITING BLUETOOTH",0,0);
		  tslp_tsk(10); /* 10msecウェイト (bt_taskに処理を渡すため)*/
    		if(gHackEV->bt_cmd==1){
    			break;
    		}
    }
#endif
    // 周期ハンドラ開始
    ev3_sta_cyc(EV3_CYC_TRACER);

    slp_tsk();  // バックボタンが押されるまで待つ

    // 周期ハンドラ停止
    ev3_stp_cyc(EV3_CYC_TRACER);

    user_system_destroy();  // 終了処理

    ext_tsk();
}

/**
 * ライントレースタスク
 */
void tracer_task(intptr_t exinf) {
    if (ev3_button_is_pressed(BACK_BUTTON)) {
        wup_tsk(MAIN_TASK);  // バックボタン押下
    } else {
        gHackEV->run();  // 倒立走行
    }

    ext_tsk();
}

//*****************************************************************************
// 関数名 : bt_task
// 引数 : unused
// 返り値 : なし
// 概要 : Bluetooth通信によるリモートスタート。 Tera Termなどのターミナルソフトから、
//       ASCIIコードで1を送信すると、リモートスタートする。
// 城所修正。HackEVクラスにBTコマンドとファイルを置いて、そちらを見ている。
//*****************************************************************************
void bt_task(intptr_t unused)
{
    while(1)
    {
        uint8_t c = fgetc(gHackEV->bt); /* 受信 */
        switch(c)
        {
        case '1':
            gHackEV->bt_cmd=1;
            break;
        case '2':
            gHackEV->bt_cmd=2;
            break;
        default:
            break;
        }
        fputc(c, gHackEV->bt); /* エコーバック */
    }
}
