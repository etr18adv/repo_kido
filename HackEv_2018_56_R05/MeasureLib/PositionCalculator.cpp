
/** Include file *************************************************************/
#include "PositionCalculator.h"
#include "BodyBase.h"

#include "math.h"

//static FILE	 *bt = NULL;
/* Function ******************************************************************/
PositionCalculator::PositionCalculator( 
	/** 引数 **/
		):
	/** 初期化 **/
		state( CalState::Def ),
		position( { 0.0F, 0.0F }),
		SumLocation( { 0, 0 } ),
		OldDistance( 0 ),
		CalAngle( 0 ),
		LeftSatrtCount( 0 ),
		RightStartCount( 0 ),
		WIDTH( (float)F_WIDTH / 10 ),
		DIAMETER( (float)F_DIAMETER / 10 ),
		I_START_DISTANCE( (float)F_I_START_DISTANCE / 10 ),
		I_CAL_DISTANCE( (float)F_I_CAL_DISTANCE / 10 )
/*****************************************************************************/
{
	//bt = ev3_serial_open_file(EV3_SERIAL_BT);

	this->aPosition[ 0 ].position.X = 0;
	this->aPosition[ 0 ].position.Y = 0;
	this->aPosition[ 0 ].startLocation.Distance = 0;
	this->aPosition[ 0 ].startLocation.Angle = 0;
}

/* Function ******************************************************************/
void PositionCalculator::Thread( )
/*****************************************************************************/
{
	/**** この計算では ****/
	/* 真直ぐがＹ＋方向   */
	/* 右がＸ＋方向       */
	/* 右周りが角度＋方向 */

	int32_t motor_ang_l = BodyBase::GetLeftWheelCount( ) - this->LeftSatrtCount;
	int32_t motor_ang_r = BodyBase::GetRightWheelCount( ) - this->RightStartCount;

	this->SumLocation.Angle    = ( ( (double)(  motor_ang_l - motor_ang_r ) * DIAMETER ) / ( WIDTH * 2 ) ) - this->CalAngle;	/* 向きの計算 */
	this->SumLocation.Distance = (int32_t)( ( motor_ang_r + motor_ang_l ) * 3.1415 * DIAMETER / ( 2  * 360 ) );			/* 距離の計算 */
	/* 角度は　総合移動距離÷タイヤ間を半径とした円弧　になる。片足が軸になるので。 */

	this->position.X += (sin( 3.1415 * this->SumLocation.Angle / 180 ) * ( this->SumLocation.Distance - this->OldDistance ) );
	this->position.Y += cos( 3.1415 * this->SumLocation.Angle / 180 ) * ( this->SumLocation.Distance - this->OldDistance );


	this->positionBuff[ this->Buffcount ] = this->position;
	this->SCount++;
	if( 2 <= this->SCount){
		this->SCount = 0;
		this->Buffcount++;
		if( 250 <= this->Buffcount ){
			this->Buffcount = 0;
		}
	}

	/* 相対値計算 */
	{
		int i;
		for( i = 0; i < this->AbsoluteId; i++ ){
			AbsoluteCal( i );
		}
	}

	switch( this->state ){
		/* 補正なしの走り */
		case CalState::Def:
			;	/* 特になし */
			break;

		/* 補正開始 */
		case CalState::Init:
			if( I_START_DISTANCE <= SumLocation.Distance ){
				/* 値を初期化して計算する */
				this->LeftSatrtCount  = BodyBase::GetLeftWheelCount( );
				this->RightStartCount = BodyBase::GetRightWheelCount( );
				this->position.X = 0.0F;
				this->position.Y = 0.0F;
				this->OldDistance = 0;
					
				this->state = CalState::InitCalculate;
			}
			break;

		/* 補正区間走行中 */
		case CalState::InitCalculate:
			if( I_CAL_DISTANCE <= SumLocation.Distance ){
				//this->CalAngle = (double)(atan( (double)this->position.X / this->position.Y ) * 180 / 3.1415);
				this->CalAngle = MCalculateAngle( this->position.X, this->position.Y, 0, 0 );
				this->position.X = 0;
				this->position.Y = I_CAL_DISTANCE;
				this->state = CalState::Calculate;
			}
			break;

		/* 補正完了 */
		case CalState::Calculate:
				
			break;
		default:
			;	/* 特になし */
			break;
	}

	this->OldDistance = SumLocation.Distance;
}


/* Function ******************************************************************/
struct PositionCalculator::Position PositionCalculator::GetPosition()
/*****************************************************************************/
{
	return( this->position );
}

/* Function ******************************************************************/
struct PositionCalculator::Position PositionCalculator::GetPosition( int id )
/*****************************************************************************/
{
	return( this->aPosition[ id ].position );
}

/* Function ******************************************************************/
int32_t PositionCalculator::GetDistance( )
/*****************************************************************************/
{
	return( this->SumLocation.Distance );
}

/* Function ******************************************************************/
int32_t PositionCalculator::GetDistance( int id )
/*****************************************************************************/
{
	return( this->SumLocation.Distance - this->aPosition[ id ].startLocation.Distance );
}

/* Function ******************************************************************/
double PositionCalculator::GetAngle( )
/*****************************************************************************/
{
	return( this->SumLocation.Angle );
}

/* Function ******************************************************************/
double PositionCalculator::GetAngle( int id )
/*****************************************************************************/
{
	return( this->SumLocation.Angle - this->aPosition[ id ].startLocation.Angle );
}

/* Function ******************************************************************/
void PositionCalculator::StartCalculate()
/*****************************************************************************/
{
	this->LeftSatrtCount  = BodyBase::GetLeftWheelCount( );
	this->RightStartCount = BodyBase::GetRightWheelCount( );

	this->position.X = 0.0F;
	this->position.Y = 0.0F;

	this->OldDistance = 0;

	this->state = CalState::Init;
}

/* Function ******************************************************************/
int PositionCalculator::CreateId( )
/*****************************************************************************/
{
	int ansId = this->AbsoluteId;
	this->aPosition[ ansId ].position.X = 0;
	this->aPosition[ ansId ].position.Y = 0;
	this->aPosition[ ansId ].startLocation = this->SumLocation;

	{
		int old_p = this->Buffcount - 200;
		if( old_p  < 0){
			old_p += 250;
		}
		int New_p = this->Buffcount - 100;
		if( New_p  < 0){
			New_p += 250;
		}
		double Angle = MCalculateAngle( 
						this->positionBuff[ New_p ].X,
						this->positionBuff[ New_p ].Y,
						this->positionBuff[ old_p ].X,
						this->positionBuff[ old_p ].Y );

		//fprintf( bt, "%f,%f,%f,%f,\r\n", this->positionBuff[ New_p ].X,
		//				this->positionBuff[ New_p ].Y,
		//				this->positionBuff[ old_p ].X,
		//				this->positionBuff[ old_p ].Y );

		/* 現在向きに合わせて、180度以内に計算する。 */
		while( 1 ){
			double dA = Angle - this->SumLocation.Angle;
			if( ( 180 >= dA ) && ( -180 <= dA ) ){
				break;
			}else{
				if( 0 > dA ){
					Angle += 360;
				}else{
					Angle -= 360;
				}
			}
		}	
		//ev3_speaker_play_tone(NOTE_G5,100);

		this->aPosition[ ansId ].startLocation.Angle = Angle;
	}

	this->AbsoluteId++;
	return ansId;
}

/* Function ******************************************************************/
void PositionCalculator::AbsoluteCal( int id )
/*****************************************************************************/
{
	int32_t Angle = this->SumLocation.Angle - this->aPosition[ id ].startLocation.Angle;

	this->aPosition[ id ].position.X += (sin( 3.1415 * Angle / 180 ) * ( this->SumLocation.Distance - this->OldDistance ) );
	this->aPosition[ id ].position.Y += cos( 3.1415 * Angle / 180 ) * ( this->SumLocation.Distance - this->OldDistance );
}

/* Function ******************************************************************/
double PositionCalculator::CalculateAngle( int tx, int ty )
/*****************************************************************************/
{
	int dx= tx - this->position.X;
	int dy= ty - this->position.Y;

	double Angle;

	if( ( ( 0 <= dy ) && ( 0 <= dx ) && ( dy >= dx ) ) || 
 	    ( ( 0 <= dy ) && ( 0 >= dx ) && ( dy >= -dx ) ) ){
		/* -45 ~ 45 */
		Angle = (double)(atan( (double)dx / dy ) * 180 / 3.1415);
	}else if( ( ( 0 <= dy ) && ( 0 <= dx ) && ( dy <= dx ) ) || 
 	          ( ( 0 >= dy ) && ( 0 <= dx ) && ( -dy <= dx ) ) ){
		/* 45 ~ 135 */
		Angle = (double)(atan( (double)( dy  * -1 ) / dx ) * 180 / 3.1415) + 90;
	}else if( ( ( 0 >= dy ) && ( 0 <= dx ) && ( -dy >= dx ) ) || 
 	          ( ( 0 >= dy ) && ( 0 >= dx ) && ( -dy >= -dx ) ) ){
		/* 135 ~ 225 */
		Angle = (double)(atan( (double)dx / dy ) * 180 / 3.1415) + 180;
	}else if( ( ( 0 >= dy ) && ( 0 >= dx ) && ( -dy <= -dx ) ) || 
 	          ( ( 0 <= dy ) && ( 0 >= dx ) && ( dy <= -dx ) ) ){
		/* -135 ~ -45 */
		Angle = (double)(atan( (double)( dy  * -1 ) / dx ) * 180 / 3.1415) - 90;
	}else{
		Angle = this->SumLocation.Angle;
	}

	/* -180 ~ 180 で返す */
	if( 180 < Angle ){
		Angle -= 360;
	}

	/* 現在向きに合わせて、180度以内に計算する。 */
	while( 1 ){
		double dA = Angle - this->SumLocation.Angle;
		if( ( 180 >= dA ) && ( -180 <= dA ) ){
			break;
		}else{
			if( 0 > dA ){
				Angle += 360;
			}else{
				Angle -= 360;
			}
		}
	}

	return( Angle );
}

/* Function ******************************************************************/
double PositionCalculator::CalculateAngle( int tx, int ty, int id )
/*****************************************************************************/
{
	int RelativeAngle = this->SumLocation.Angle - this->aPosition[ id ].startLocation.Angle;

	int dx= tx - this->aPosition[ id ].position.X;
	int dy= ty - this->aPosition[ id ].position.Y;

	double Angle;

	if( ( ( 0 <= dy ) && ( 0 <= dx ) && ( dy >= dx ) ) || 
 	    ( ( 0 <= dy ) && ( 0 >= dx ) && ( dy >= -dx ) ) ){
		/* -45 ~ 45 */
		Angle = (double)(atan( (double)dx / dy ) * 180 / 3.1415);
	}else if( ( ( 0 <= dy ) && ( 0 <= dx ) && ( dy <= dx ) ) || 
 	          ( ( 0 >= dy ) && ( 0 <= dx ) && ( -dy <= dx ) ) ){
		/* 45 ~ 135 */
		Angle = (double)(atan( (double)( dy  * -1 ) / dx ) * 180 / 3.1415) + 90;
	}else if( ( ( 0 >= dy ) && ( 0 <= dx ) && ( -dy >= dx ) ) || 
 	          ( ( 0 >= dy ) && ( 0 >= dx ) && ( -dy >= -dx ) ) ){
		/* 135 ~ 225 */
		Angle = (double)(atan( (double)dx / dy ) * 180 / 3.1415) + 180;
	}else if( ( ( 0 >= dy ) && ( 0 >= dx ) && ( -dy <= -dx ) ) || 
 	          ( ( 0 <= dy ) && ( 0 >= dx ) && ( dy <= -dx ) ) ){
		/* -135 ~ -45 */
		Angle = (double)(atan( (double)( dy  * -1 ) / dx ) * 180 / 3.1415) - 90;
	}else{
		Angle = RelativeAngle;
	}

	/* -180 ~ 180 で返す */
	if( 180 < Angle ){
		Angle -= 360;
	}

	/* 現在向きに合わせて、180度以内に計算する。 */
	while( 1 ){
		double dA = Angle - RelativeAngle;
		if( ( 180 >= dA ) && ( -180 <= dA ) ){
			break;
		}else{
			if( 0 > dA ){
				Angle += 360;
			}else{
				Angle -= 360;
			}
		}
	}

	return( Angle );
}


/* Function ******************************************************************/
double PositionCalculator::MCalculateAngle( int tx, int ty, int bx, int by )
/*****************************************************************************/
{
	int dx= tx - bx;
	int dy= ty - by;

	double Angle = 0;

	if( !( ( tx == 0 ) && ( ty == 0 ) && ( bx == 0 ) && ( by == 0 ) ) ){
		if( ( ( 0 <= dy ) && ( 0 <= dx ) && ( dy >= dx ) ) || 
	 	    ( ( 0 <= dy ) && ( 0 >= dx ) && ( dy >= -dx ) ) ){
			/* -45 ~ 45 */
			Angle = (double)(atan( (double)dx / dy ) * 180 / 3.1415);
		}else if( ( ( 0 <= dy ) && ( 0 <= dx ) && ( dy <= dx ) ) || 
	 	          ( ( 0 >= dy ) && ( 0 <= dx ) && ( -dy <= dx ) ) ){
			/* 45 ~ 135 */
			Angle = (double)(atan( (double)( dy  * -1 ) / dx ) * 180 / 3.1415) + 90;
		}else if( ( ( 0 >= dy ) && ( 0 <= dx ) && ( -dy >= dx ) ) || 
	 	          ( ( 0 >= dy ) && ( 0 >= dx ) && ( -dy >= -dx ) ) ){
			/* 135 ~ 225 */
			Angle = (double)(atan( (double)dx / dy ) * 180 / 3.1415) + 180;
		}else if( ( ( 0 >= dy ) && ( 0 >= dx ) && ( -dy <= -dx ) ) || 
	 	          ( ( 0 <= dy ) && ( 0 >= dx ) && ( dy <= -dx ) ) ){
			/* -135 ~ -45 */
			Angle = (double)(atan( (double)( dy  * -1 ) / dx ) * 180 / 3.1415) - 90;
		}else{
			Angle = 0;
		}
	}

	/* -180 ~ 180 で返す */
	if( 180 < Angle ){
		Angle -= 360;
	}
	return( Angle );
}

/* Function ******************************************************************/
bool PositionCalculator::RangeJuge( int tx, int ty, int range )
/*****************************************************************************/
{
	int dx= tx - this->position.X;
	int dy= ty - this->position.Y;

	if( ( dx <= range ) && ( dx >= -range ) && ( dy <= range ) && ( dy >= -range ) ){
		return true;
	}else{
		return false;
	}
}

/* Function ******************************************************************/
bool PositionCalculator::RangeJuge( int tx, int ty, int range, int id )
/*****************************************************************************/
{
	int dx= tx - this->aPosition[ id ].position.X;
	int dy= ty - this->aPosition[ id ].position.Y;

	if( ( dx <= range ) && ( dx >= -range ) && ( dy <= range ) && ( dy >= -range ) ){
		return true;
	}else{
		return false;
	}
}

