/** Include file *************************************************************/
#include "RootAnalysis.h"
#include "math.h"
#include "ev3api.h"

static FILE	 *bt = NULL;

/** using宣言 ****************************************************************/
#define R1	0
#define G1	1
#define B1	2
#define Y1	3
#define R2	4
#define G2	5
#define B2	6
#define Y2	7

#define H_Plus_Offset		2
#define V_Plus_Offset		3
#define H_Minus_Offset		0
#define V_Minus_Offset		1
const int MoveOffset[4] = {2,3,0,1};	/* 向き計算で使用 */

/* データベースと合わせている注意 */
#define Red			0
#define Green		1
#define Blue		2
#define Yellow		3

/* ルート組合せ　再帰だと時間がかかるので */
const std::vector<std::vector<int>> Kumiawase = {
	{0,1,2,3},{0,1,3,2},{0,2,1,3},{0,2,3,1},{0,3,1,2},{0,3,2,1},{1,0,2,3},{1,0,3,2},{1,2,0,3},{1,2,3,0},{1,3,0,2},{1,3,2,0},
	{2,0,1,3},{2,0,3,1},{2,1,0,3},{2,1,3,0},{2,3,0,1},{2,3,1,0},{3,0,1,2},{3,0,2,1},{3,1,0,2},{3,1,2,0},{3,2,0,1},{3,2,1,0}
};

/* 経路組合せ　再帰だと時間がかかるのでここで一発でとる */
const std::vector<std::vector<int>> NTN[4][4] =
{	{
		{ /*[0][0]*/ /* なし */},
		{ /*[0][1]*/ { 1 }},
		{ /*[0][2]*/ { 1, 1 }},
		{ /*[0][3]*/ { 1, 1, 1 }}
	},{
		{ /*[1][0]*/ { 0 }},
		{ /*[1][1]*/ { 0, 1 },{ 1, 0 }},
		{ /*[1][2]*/ { 0, 1, 1 },{ 1, 0, 1 },{ 1, 1, 0 }},
		{ /*[1][3]*/ { 0, 1, 1, 1 },{ 1, 0, 1, 1 },{ 1, 1, 0, 1 },{ 1, 1, 1, 0 }}
	},{
		{ /*[2][0]*/ { 0, 0 }},
		{ /*[2][1]*/ { 0, 0, 1 },{ 0, 1, 0 },{ 1, 0, 0 }},
		{ /*[2][2]*/ { 0, 0, 1, 1 },{ 0, 1, 0, 1 },{ 0, 1, 1, 0 },{ 1, 0, 0, 1 },{ 1, 0, 1, 0 },{ 1, 1, 0, 0 }},
		{ /*[2][3]*/ { 0, 0, 1, 1, 1 },{ 0, 1, 0, 1, 1 },{ 0, 1, 1, 0, 1 },{ 0, 1, 1, 1, 0 },{ 1, 0, 0, 1, 1 },
					 { 1, 0, 1, 0, 1 },{ 1, 0, 1, 1, 0 },{ 1, 1, 0, 0, 1 },{ 1, 1, 0, 1, 0 },{ 1, 1, 1, 0, 0 }}
	},{
		{ /*[3][0]*/ { 0, 0, 0 }},
		{ /*[3][1]*/ { 0, 0, 0, 1 },{ 0, 0, 1, 0 },{ 0, 1, 0, 0 },{ 1, 0, 0, 0 }},
		{ /*[3][2]*/ { 0, 0, 0, 1, 1 },{ 0, 0, 1, 0, 1 },{ 0, 0, 1, 1, 0 },{ 0, 1, 0, 0, 1 },{ 0, 1, 0, 1, 0 },
					 { 0, 1, 1, 0, 0 },{ 1, 0, 0, 0, 1 },{ 1, 0, 0, 1, 0 },{ 1, 0, 1, 0, 0 },{ 1, 1, 0, 0, 0 }},
		{ /*[3][3]*/ { 0, 0, 0, 1, 1, 1 },{ 0, 0, 1, 0, 1, 1 },{ 0, 0, 1, 1, 0, 1 },{ 0, 0, 1, 1, 1, 0 },
					 { 0, 1, 0, 0, 1, 1 },{ 0, 1, 0, 1, 0, 1 },{ 0, 1, 0, 1, 1, 0 },{ 0, 1, 1, 0, 0, 1 },
					 { 0, 1, 1, 0, 1, 0 },{ 0, 1, 1, 1, 0, 0 },{ 1, 0, 0, 0, 1, 1 },{ 1, 0, 0, 1, 0, 1 },
					 { 1, 0, 0, 1, 1, 0 },{ 1, 0, 1, 0, 0, 1 },{ 1, 0, 1, 0, 1, 0 },{ 1, 0, 1, 1, 0, 0 },
					 { 1, 1, 0, 0, 0, 1 },{ 1, 1, 0, 0, 1, 0 },{ 1, 1, 0, 1, 0, 0 },{ 1, 1, 1, 0, 0, 0 }}
	}
};

/* 位置コードとブロック座標の関係 */
const struct BlockPosition positionInfo[16] =
{
	{ 0, 0 },{ 1, 0 },{ 2, 0 },{ 3, 0 },
	{ 0, 1 },{ 1, 1 },{ 2, 1 },{ 3, 1 },
	{ 0, 2 },{ 1, 2 },{ 2, 2 },{ 3, 2 },
	{ 0, 3 },{ 1, 3 },{ 2, 3 },{ 3, 3 }
};

/* パワーコードとパワーブロック置き場の関係 */
const int CodeToPw[9][2] = {
	{ 0, 4 },		/* ダミー */
	{ 0, 4 },{ 1, 5 },{ 3, 7 },{ 4, 8 },
	{ 1, 3 },{ 2, 4 },{ 4, 6 },{ 5, 7 }
};

/* パワーブロック置き場とパワースポットの関係(RGBY順) */
const std::vector<std::vector<int>> PwRGBP = {
	{  0,  5,  4,  1 },{  6,  5,  2,  1 },{  6,  3,  2,  7 },
	{  8,  5,  4,  9 },{  6,  5, 10,  9 },{  6, 11, 10,  7 },
	{  8, 13, 12,  9 },{ 14, 13, 10,  9 },{ 14, 11, 10, 15 }
};

/* Function ******************************************************************/
RootAnalysis::RootAnalysis( BodyInfo* bodyInfo, BlockSortDataManager* mBlockSortDataManager, BluetoothDrv* mBluetoothDrv ):
	state(Init),
	body(bodyInfo),
	blockSortDataManager(mBlockSortDataManager),
	bluetoothDrv(mBluetoothDrv)
/*****************************************************************************/
{
	bt = ev3_serial_open_file(EV3_SERIAL_BT);
}

/* Function ******************************************************************/
RootAnalysis::~RootAnalysis( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void RootAnalysis::DirectStart( )
/*****************************************************************************/
{
	/* 通信せずに入力値で処理 */
	this->state = StartKaiseki;
}

/* Function ******************************************************************/
void RootAnalysis::Start( )
/*****************************************************************************/
{
	this->state = CheackStart;
}

/* Function ******************************************************************/
bool RootAnalysis::AnalysisFinish()
/*****************************************************************************/
{
	if( Complate == this->state){
		return true;
	}else{
		return false;
	}
}

/* Function ******************************************************************/
std::vector<std::vector<ControlOder>> RootAnalysis::GetOrder()
/*****************************************************************************/
{
	return(Result);
}

/* Function ******************************************************************/
void RootAnalysis::Run( )
/*****************************************************************************/
{
	switch(this->state){
		case Init:
			break;

		case CheackStart:
			{
				/* 通信処理 */
				bluetoothDrv->StartPcComera( );
		
				/* 受信待ち状態へ */
				this->state = ReceiveCheack;
			}
			break;

		case ReceiveCheack:
			{
				if(bluetoothDrv->PcComeraFinishJudge()){
					/* 通信完了したら解析へ */
					this->state = StartKaiseki;
				}
			}
			break;

		case StartKaiseki:
			{
				int mColorData[4] = {Red,Green,Yellow,Blue};
				BlockSortDataManager::ColorBlocks c = blockSortDataManager->GetColorBlocks();
				/* データベースと合わせている注意 */
				mColorData[0] = c.FirstColor;
				mColorData[1] = c.SecoundColor;
				mColorData[2] = c.ThurdColor;
				mColorData[3] = c.FourthColor;
				CreateInitData( blockSortDataManager->GetInitPositionCode(), mColorData );
				this->state = Kaiseki1;
			}
			break;

		case Kaiseki1:
			{
				static int MinOmomi = 30000;
				static int cNo1 = 0;
				static unsigned char cNo2 = 0;

				//fprintf( bt, "%d %d", cNo1, cNo2 );
				
				std::vector<int> k = Kumiawase[cNo1];
				for(int i = 0; i < 4; i++ ){
					if( 0 != (cNo2 & (0x01 << i)) ){
						k[i] = k[i] + 4;
					}
				}

				if( CheckTp( k ) ){
					int SouOmomi = 0;
					std::vector<std::vector<ControlOder>> *AnswerOrder = new std::vector<std::vector<ControlOder>>();
					CreateBpTp( k, AnswerOrder );	/* ターゲットの設定 */
					for(std::vector<ControlOder> ans : *AnswerOrder){
						for(ControlOder a : ans){
							SouOmomi += OmomiJudge( a );
						}
					}
					//fprintf( bt, ": %d \r\n", SouOmomi );
					if(SouOmomi < MinOmomi){
						Result = *AnswerOrder;
						ReKumiawase = k;
						MinOmomi = SouOmomi;
						//fprintf( bt, " MinRoot!Update \r\n" );
					}
					// delete AnswerOrder;  /* ←動作確認 */
				}else{
					//fprintf( bt, "\r\n" );
				}
				
				cNo2++;
				if( 16 <= cNo2 ){	/* ここは8ではなく16 */
					cNo2 = 0;
					cNo1++;
					if( (int)Kumiawase.size() <= cNo1){
						this->state = Seach;
						//fprintf( bt, "\r\n" );
					}
				}
			}
			break;

		case Seach:
			this->state = Complate;
			/* 採用した制御命令リストを表示(表示のみなので削除可) */
			for( std::vector<ControlOder> re : Result){
				for( ControlOder r : re){
					switch(r.order){
						case NodeTo:
							fprintf( bt, "NodeToNode: ");
							break;
						case BBring:
							fprintf( bt, "BlockHold : ");
							break;
						case BNodeTo:
							fprintf( bt, "BlockCart : ");
							break;
						case BSet:
							fprintf( bt, "BlockSet  : ");
							break;
					}
					switch(r.muki){
						case Front:
							fprintf( bt, "Front: ");
							break;
						case Left:
							fprintf( bt, "Left : ");
							break;
						case Back:
							fprintf( bt, "Back : ");
							break;
						case Right:
							fprintf( bt, "Right: ");
							break;
						case NoMove:
							fprintf( bt, "     : ");
							break;
						default:
							fprintf( bt, "%d  : ",r.muki);
							break;
					}
					switch(r.aboid){
						case true:
							fprintf( bt, "True\r\n");
							break;
						default:
							fprintf( bt, "False\r\n");
							break;
					}
					//fprintf( bt, "%d%d%d\r\n", r.order, r.muki, r.aboid );
				}
			}
			fprintf( bt, "End\r\n" );
			break;

		case Complate:
			break;

		default:
			break;
	}
}

/* Function ******************************************************************/
void RootAnalysis::CreateInitData(long InitCode, int *colorData) 
/*  */
/*****************************************************************************/
{
	int PowerCode;
	int position[5];
	for (int i = 0; i < 5; i++) {
		position[4 - i] = InitCode % 16;
		InitCode /= 16;
	}
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			if (i == colorData[j]) {
				this->Block_RGBY_Position[i] = position[j];
			}
		}
	}

	PowerCode = position[4];
	/* ここでパワースポットの設定 */
	PowerB1_RGBY_Position = PwRGBP[CodeToPw[PowerCode][0]];
	PowerB2_RGBY_Position = PwRGBP[CodeToPw[PowerCode][1]];
}

/* Function ******************************************************************/
bool RootAnalysis::CheckTp(std::vector<int> targetPw)
/*****************************************************************************/
{
	bool Ans = true;

	int Bp[4];
	int Tp[4];

	/* 判定用配列作成 */
	for (int i = 0; i < 4; i++) {
		switch (targetPw[i]) {
		case R1:
			Bp[i] = Block_RGBY_Position[Red];
			Tp[i] = PowerB1_RGBY_Position[Red];
			break;
		case R2:
			Bp[i] = Block_RGBY_Position[Red];
			Tp[i] = PowerB2_RGBY_Position[Red];
			break;
		case G1:
			Bp[i] = Block_RGBY_Position[Green];
			Tp[i] = PowerB1_RGBY_Position[Green];
			break;
		case G2:
			Bp[i] = Block_RGBY_Position[Green];
			Tp[i] = PowerB2_RGBY_Position[Green];
			break;
		case B1:
			Bp[i] = Block_RGBY_Position[Blue];
			Tp[i] = PowerB1_RGBY_Position[Blue];
			break;
		case B2:
			Bp[i] = Block_RGBY_Position[Blue];
			Tp[i] = PowerB2_RGBY_Position[Blue];
			break;
		case Y1:
			Bp[i] = Block_RGBY_Position[Yellow];
			Tp[i] = PowerB1_RGBY_Position[Yellow];
			break;
		case Y2:
			Bp[i] = Block_RGBY_Position[Yellow];
			Tp[i] = PowerB2_RGBY_Position[Yellow];
			break;
		default:
			break;
		}
	}

	for(int i = 0; i < 4; i++){
		//fprintf( bt, "%d ", Tp[i] );
	}
	//fprintf( bt, ": " );

	/* 移動ブロックの目的位置にブロックが置かれているルートは省く */
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			/* 判定 */
			if( Tp[i] == Bp[j] ){
				Ans = false;
			}
		}
		Bp[i] = Tp[i];	/* ブロック位置更新 */
	}

	return(Ans);
}

/* Function ******************************************************************/
void RootAnalysis::CreateBpTp(std::vector<int> targetPw, std::vector<std::vector<ControlOder>> *AnswerOrder)
/*****************************************************************************/
{
	int Bp[4];
	int Tp[4];

	/* 判定用配列作成 */
	for (int i = 0; i < 4; i++) {
		switch (targetPw[i]) {
		case R1:
			Bp[i] = Block_RGBY_Position[Red];
			Tp[i] = PowerB1_RGBY_Position[Red];
			break;
		case R2:
			Bp[i] = Block_RGBY_Position[Red];
			Tp[i] = PowerB2_RGBY_Position[Red];
			break;
		case G1:
			Bp[i] = Block_RGBY_Position[Green];
			Tp[i] = PowerB1_RGBY_Position[Green];
			break;
		case G2:
			Bp[i] = Block_RGBY_Position[Green];
			Tp[i] = PowerB2_RGBY_Position[Green];
			break;
		case B1:
			Bp[i] = Block_RGBY_Position[Blue];
			Tp[i] = PowerB1_RGBY_Position[Blue];
			break;
		case B2:
			Bp[i] = Block_RGBY_Position[Blue];
			Tp[i] = PowerB2_RGBY_Position[Blue];
			break;
		case Y1:
			Bp[i] = Block_RGBY_Position[Yellow];
			Tp[i] = PowerB1_RGBY_Position[Yellow];
			break;
		case Y2:
			Bp[i] = Block_RGBY_Position[Yellow];
			Tp[i] = PowerB2_RGBY_Position[Yellow];
			break;
		default:
			break;
		}
	}

	TargetList(*body, Bp, Tp, AnswerOrder );
}

/** ルート探索 **/
/* Function ******************************************************************/
void RootAnalysis::TargetList(BodyInfo bodyInfo, int *blp, int *tp, std::vector<std::vector<ControlOder>> *AnswerOrder) 
/*****************************************************************************/
{
	std::vector<BlockPosition> *ansl = new std::vector<BlockPosition>();
	for (int i = 0; i < 4; i++) {
		ansl->push_back(positionInfo[blp[i]]);
		ansl->push_back(positionInfo[tp[i]]);
	}
	bool BlockOn = false;

	int BMoveCount = 0;
	int EnzanyouBlockP[4];
	for (int i = 0; i < 4; i++) {
		EnzanyouBlockP[i] = blp[i];
	}

	BlockPosition mb = positionInfo[bodyInfo.PositionCode];
	int m_hougaku = bodyInfo.hougaku;
	std::vector<Enzanyou> EL;
	for (BlockPosition p : *ansl) {
		std::vector<std::vector<int>> ans = RootSearch(mb, p);

		if (0 < EL.size()) {
			EL.clear();
		}
		for (std::vector<int> an : ans) {
			Enzanyou e;
			e.omomi = 0;
			/* 1経路計算 */
			BlockPosition mmb = mb;
			int mm_hougaku = m_hougaku;
			int countNo = 0;
			for (int a : an) {
				/* 制御命令作成 */
				ControlOder c;
				if (!BlockOn) {
					c.order = NodeTo;
				}
				else {
					if (0 == countNo) {
						c.order = BBring;
					}
					else {
						c.order = BNodeTo;
					}
				}
				/* 向き = (走行体向き - 移動先オフセット + 4) % 4 */
				c.muki = (mm_hougaku - MoveOffset[a] + 4) % 4;
				c.aboid = false;
				for (int i = 0; i < 4; i++) {
					if (EnzanyouBlockP[i] == (mmb.H + (mmb.V * 4))) {
						c.aboid = true;
					}
				}

				e.col.push_back(c);	/* 命令追加 */
				e.omomi += OmomiJudge(c);

				/* ここは単純なマイナスではない */
				switch (a) {
				case H_Plus:
					mmb.H += 1;
					mm_hougaku = H_Minus;
					break;
				case V_Plus:
					mmb.V += 1;
					mm_hougaku = V_Minus;
					break;
				case H_Minus:
					mmb.H -= 1;
					mm_hougaku = H_Plus;
					break;
				case V_Minus:
					mmb.V -= 1;
					mm_hougaku = V_Plus;
					break;
				default:
					break;
				}
				e.bd = mm_hougaku;
				countNo++;
			}
			if (BlockOn) {
				ControlOder c;
				c.order = BSet;
				c.muki = NoMove;
				c.aboid = false;
				e.col.push_back(c);	/* 命令追加 */
			}

			EL.push_back(e);
		}
		
		/* 最小判定 */
		Enzanyou Min_e;
		Min_e.bd = m_hougaku;		/* 通らない場合があるので */
		int MinOmomi = 50000;
		bool fl = false;
		for (Enzanyou e : EL) {
			if (e.omomi < MinOmomi) {
				Min_e = e;
				MinOmomi = e.omomi;
			}
			fl = true;
		}
		if(fl){
			AnswerOrder->push_back(Min_e.col);
			m_hougaku = Min_e.bd;		/* ここは最小のときの終了向き */
		}else{
			m_hougaku = m_hougaku;		/* ここは最小のときの終了向き */
		}

		/* 次経路持ち越しデータ */
		mb = p;							/* これはそのまま */

		if (BlockOn) {
			/* 新カラーブロック位置 */
			EnzanyouBlockP[BMoveCount] = tp[BMoveCount];
			BMoveCount++;
			BlockOn = false;
		}
		else {
			BlockOn = true;
		}
	}
}

/* Function ******************************************************************/
std::vector<ControlOder> RootAnalysis::EndRoot()
/*****************************************************************************/
{
	std::vector<int> an;
	int h = body->PositionCode % 4;
	int v = body->PositionCode / 4;
	switch(h){
		case 0:
			an.push_back(H_Plus);
			an.push_back(H_Plus);
			an.push_back(H_Plus);
			break;
		case 1:
			an.push_back(H_Plus);
			an.push_back(H_Plus);	
			break;
		case 2:
			an.push_back(H_Plus);	
			break;
		case 3:
	
			break;
		default:
			break;
	}
	switch(v){
		case 0:
			an.push_back(V_Plus);	
			an.push_back(V_Plus);	
			break;
		case 1:
			an.push_back(V_Plus);	
			break;
		case 2:
			an.push_back(V_Minus);
			break;
		case 3:
			an.push_back(V_Minus);
			an.push_back(V_Minus);
			break;
		default:
			break;
	}

	int BlockP[4];
	for (int i = 0; i < 4; i++) {
		switch (ReKumiawase[i]) {
		case R1:
			BlockP[i] = PowerB1_RGBY_Position[Red];
			break;
		case R2:
			BlockP[i] = PowerB2_RGBY_Position[Red];
			break;
		case G1:
			BlockP[i] = PowerB1_RGBY_Position[Green];
			break;
		case G2:
			BlockP[i] = PowerB2_RGBY_Position[Green];
			break;
		case B1:
			BlockP[i] = PowerB1_RGBY_Position[Blue];
			break;
		case B2:
			BlockP[i] = PowerB2_RGBY_Position[Blue];
			break;
		case Y1:
			BlockP[i] = PowerB1_RGBY_Position[Yellow];
			break;
		case Y2:
			BlockP[i] = PowerB2_RGBY_Position[Yellow];
			break;
		default:
			break;
		}
	}

	std::vector<ControlOder> Answer;
	int m_hougaku = body->hougaku;
	int m_PositionCode = body->PositionCode;
	for(int a : an ){
		ControlOder c;
		c.order = NodeTo;		
		c.muki = (m_hougaku - MoveOffset[a] + 4) % 4;
		c.aboid = false;
		for (int i = 0; i < 4; i++) {
			if (BlockP[i] == m_PositionCode) {	/* ここの比較は現状ブロック位置にできると途中脱出もできる */
				c.aboid = true;
			}
		}
		Answer.push_back(c);

		switch (a) {
		case H_Plus:
			m_hougaku = H_Minus;
			break;
		case V_Plus:
			m_hougaku = V_Minus;
			break;
		case H_Minus:
			m_hougaku = H_Plus;
			break;
		case V_Minus:
			m_hougaku = V_Plus;
			break;
		default:
			break;
		}

		if( a != NoMove ){
			int h = m_PositionCode % 4;
			int v = m_PositionCode / 4;
			/* 次置場から見た位置なので反対の加減算 */
			switch( m_hougaku ){
				case H_Plus:
					h = h - 1;
					break;
	
				case V_Plus:
					v = v - 1;
					break;
	
				case H_Minus:
					h = h + 1;			
					break;
	
				case V_Minus:
					v = v + 1;
					break;
			}
			m_PositionCode = (v * 4) + h;
		}
	}

	{
		ControlOder c;
		c.order = BSet;
		c.muki = NoMove;
		c.aboid = false;
		Answer.push_back(c);	/* 命令追加 */
	}

	for( ControlOder r : Answer){
		switch(r.order){
			case NodeTo:
				fprintf( bt, "NodeToNode: ");
				break;
			case BBring:
				fprintf( bt, "BlockHold : ");
				break;
			case BNodeTo:
				fprintf( bt, "BlockCart : ");
				break;
			case BSet:
				fprintf( bt, "BlockSet  : ");
				break;
		}
		switch(r.muki){
			case Front:
				fprintf( bt, "Front: ");
				break;
			case Left:
				fprintf( bt, "Left : ");
				break;
			case Back:
				fprintf( bt, "Back : ");
				break;
			case Right:
				fprintf( bt, "Right: ");
				break;
			case NoMove:
				fprintf( bt, "     : ");
				break;
			default:
				fprintf( bt, "%d  : ",r.muki);
				break;
		}
		switch(r.aboid){
			case true:
				fprintf( bt, "True\r\n");
				break;
			default:
				fprintf( bt, "False\r\n");
				break;
		}
	}
	fprintf( bt, "End\r\n" );
	
	return Answer;
}

/* Function ******************************************************************/
int RootAnalysis::OmomiJudge(ControlOder co) 
/*****************************************************************************/
{
	int answer = 500;
	switch (co.order) {
	case NodeTo:
		if (co.aboid) {
			switch(co.muki){
				case Front:
					answer = 21;
					break;
				case Left:
					answer = 15;
					break;
				case Back:
					answer = 16;
					break;
				case Right:
					answer = 15;
					break;
				case NoMove:
					answer = 15;
					break;
				default:
					answer = 15;
					break;
			}
		}
		else {
			answer = 10;
		}
		break;

	case BBring:
		switch(co.muki){
			case Front:
				answer = 15;
				break;
			case Left:
				answer = 15;
				break;
			case Back:
				answer = 20;
				break;
			case Right:
				answer = 15;
				break;
			case NoMove:
				answer = 15;
				break;
			default:
				answer = 15;
				break;
		}
		break;

	case BNodeTo:
		if (co.aboid) {
			switch(co.muki){
				case Front:
					answer = 21;
					break;
				case Left:
					answer = 15;
					break;
				case Back:
					answer = 16;
					break;
				case Right:
					answer = 15;
					break;
				case NoMove:
					answer = 15;
					break;
				default:
					answer = 15;
					break;
			}
		}
		else {
			answer = 10;
		}
		break;

	case BSet:
		if (co.aboid) {
			answer = 15;
		}
		else {
			answer = 10;
		}
		break;

	default:
		answer = 10;
		break;
	}
	return answer;
}

/* Function ******************************************************************/
std::vector<std::vector<int>> RootAnalysis::RootSearch(BlockPosition NowPosition, BlockPosition TargetPosition ) 
/*****************************************************************************/
{
	int dh = TargetPosition.H - NowPosition.H;
	int dv = TargetPosition.V - NowPosition.V;

	int sh = (0 < dh) ? H_Plus : H_Minus;
	int sv = (0 < dv) ? V_Plus : V_Minus;

	/* 絶対値にして個数を調べる */
	dh = ( 0 <= dh) ? dh: (-1 * dh);
	dv = ( 0 <= dv) ? dv: (-1 * dv);

	std::vector<std::vector<int>> Answer = NTN[dh][dv];
	int i = 0, j = 0;
	for(std::vector<int> ans : Answer){
		for(int a : ans){
			Answer[i][j] = (a == 0) ? sh : sv;
			j++;
		}
		i++;
		j = 0;
	}
	//std::vector<int> d;		/* 計算用 組合せ前要素 */
	//std::vector<int> *b = new std::vector<int>();		/* 計算用 組合せ済要素 */
	//for (int i = 0; i < dh + dv; i++) {
	//	d.push_back(i);
	//}
	//func(b, d, dh, ans, sh, sv);
	//delete b;

	return Answer;
}

/* Function ******************************************************************/
void BodyInfo::Ref(int muki) 
/*****************************************************************************/
{
	switch( muki ){
		case Front:
			/*  */
			break;
		case Back:
			this->hougaku = (this->hougaku + 2 + 4) % 4;
			break;
		case Right:
			this->hougaku = (this->hougaku + 1 + 4) % 4;
			break;
		case Left:
			this->hougaku = (this->hougaku - 1 + 4) % 4;
			break;
		case NoMove:
			
			break;
	}

	if( muki != NoMove ){
		int h = this->PositionCode % 4;
		int v = this->PositionCode / 4;
		/* 次置場から見た位置なので反対の加減算 */
		switch( this->hougaku ){
			case H_Plus:
				h = h - 1;
				break;
	
			case V_Plus:
				v = v - 1;
				break;
	
			case H_Minus:
				h = h + 1;			
				break;
	
			case V_Minus:
				v = v + 1;
				break;
		}
		this->PositionCode = (v * 4) + h;
	}

	//fprintf( bt, "%d %d\r\n", this->PositionCode % 4, this->PositionCode / 4 );

}

/* Function ******************************************************************/
int BodyInfo::GetHougaku()
/*****************************************************************************/
{
	return this->hougaku;
}

