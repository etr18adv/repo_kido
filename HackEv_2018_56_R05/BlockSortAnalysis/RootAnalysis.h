#ifndef _ROOT_ANALYSIS_
#define _ROOT_ANALYSIS_

/** Include file *************************************************************/
#include "BlockSortInfo.h"
#include "BlockSortDataManager.h"
#include "BluetoothDrv.h"

#include <vector>

/* 向き */
#define	Front		0
#define Left		1
#define	Back		2
#define Right		3
#define NoMove		4

/* 方角 */
#define H_Plus		0
#define V_Plus		1
#define H_Minus		2
#define V_Minus		3

/** using宣言 ****************************************************************/
enum Order {
	NodeTo = 0,
	BBring = 1,
	BNodeTo = 2,
	BSet = 3
};

/* 制御命令 */
struct ControlOder{
	Order order;
	int muki;
	bool aboid;
};

struct Enzanyou {
	std::vector<ControlOder> col;
	int bd;	/* 方角 */
	int omomi;
};

/* Class ******************************************************************/
/* 走行体情報 */
class BodyInfo{
	public:
		int PositionCode;
		int hougaku;

		void Ref(int muki);
		int GetHougaku();
};

/*  */
//class{
//
//}

/* 1ブロック設置手順 */
class SingleProcedure{

};

/* ブロック設置手順 */
class RootProcedure{

};

/* 最短ルート探索 */
class RootAnalysis{
	private:
		enum RaState{
			Init,
			CheackStart,
			ReceiveCheack,
			StartKaiseki,
			Kaiseki1,
			Kaiseki2,
			Seach,
			Complate
		};

	public:
		RootAnalysis( BodyInfo* bodyInfo, BlockSortDataManager*, BluetoothDrv* );
		~RootAnalysis( );
		void Start( );
		void DirectStart( );
		bool AnalysisFinish( );
		void Run( );
		std::vector<std::vector<ControlOder>> GetOrder();

		BodyInfo* GetBody(){ return body; };
		std::vector<ControlOder> EndRoot();

	private:
		RaState state;

		BodyInfo *body;		/* 走行体情報 */
		BlockSortDataManager *blockSortDataManager;
		BluetoothDrv* bluetoothDrv;

		std::vector<std::vector<int>> TaisyouKumiawase;

		std::vector<std::vector<ControlOder>> Result;
		std::vector<int> ReKumiawase;

		int Block_RGBY_Position[4];			/* ブロックの位置(RGBY順) */
		std::vector<int> PowerB1_RGBY_Position;		/* パワースポット1の位置(RGBY順) */
		std::vector<int> PowerB2_RGBY_Position;		/* パワースポット2の位置(RGBY順) */

		void CreateInitData(long InitCode, int *colorData);

		void TargetList(BodyInfo bodyInfo, int *blp, int *tp, std::vector<std::vector<ControlOder>> *AnswerOrder);
		int OmomiJudge(ControlOder co);
		std::vector<std::vector<int>> RootSearch(BlockPosition NowPosition, BlockPosition TargetPosition );
		bool CheckTp(std::vector<int> targetPw);
		void CreateBpTp(std::vector<int> targetPw, std::vector<std::vector<ControlOder>> *AnswerOrder);
};

#endif 
