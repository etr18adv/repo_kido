#ifndef _BLOCK_SORT_INFO_H_
#define _BLOCK_SORT_INFO_H_

/** Include file *************************************************************/

/** using宣言 ****************************************************************/

/** 定義 *********************************************************************/
class BSI_Def{
public:
	/* 方角 */
	enum Direction{
		H_Plus,
		H_Minus,
		V_Plus,
		V_Minus
	};

	/* 色 */
	enum BlockSortColor{
		None, Red, Green, Blue, Yellow
	};
};
/* XY座標 */
struct XYPosition{
	int X;
	int Y;
};

/* ブロック座標 */
struct BlockPosition{
	int H;		/* 水平 */
	int V;		/* 垂直 */
};

/* ノード */
struct Node{
	struct XYPosition xyPosition;
};

/* Class ******************************************************************/
/*****************************************************************************************************/
/******** カラーブロック                                                                      ********/
/*****************************************************************************************************/
class ColorBlock
{
	public:
		ColorBlock( );
		~ColorBlock( );

	private:
		enum BSI_Def::BlockSortColor color;
		struct BlockPosition bPosition;
};



/*****************************************************************************************************/
/******** ライン                                                                              ********/
/*****************************************************************************************************/
class Line
{
	public:
		Line( );
		~Line( );

	private:

	protected:
		struct Node node;		
};



/*****************************************************************************************************/
/******** 水平ライン                                                                          ********/
/*****************************************************************************************************/
class HLine : public Line
{
	public:
		HLine( int x, int y );
		~HLine( );

	private:

};



/*****************************************************************************************************/
/******** 垂直ライン                                                                          ********/
/*****************************************************************************************************/
class VLine : public Line
{
	public:
		VLine( int x, int y );
		~VLine( );

	private:

};



/*****************************************************************************************************/
/******** 開始ライン                                                                          ********/
/*****************************************************************************************************/
class StartLine : public Line
{
	public:
		StartLine( int x, int y );
		~StartLine( );

	private:

};



/*****************************************************************************************************/
/******** カラーブロック置き場                                                                ********/
/*****************************************************************************************************/
class ColorBlockPlace
{
	public:
		ColorBlockPlace( int code, int x, int y, enum BSI_Def::BlockSortColor c );
		~ColorBlockPlace( );
		void SetLine( HLine* hp, HLine* hm, VLine* vp, VLine* vm );

	private:
		enum BSI_Def::BlockSortColor color;
		struct BlockPosition bPosition;
		struct XYPosition xyPosition;
		int positionCode;
		HLine* hLinePuls;
		HLine* hLineMinus;
		VLine* vLinePuls;
		VLine* vLineMinus;
		StartLine* sLine;
};



/*****************************************************************************************************/
/******** 有効パワーブロック置き場                                                            ********/
/*****************************************************************************************************/
class EffectivePowerBlockPlace
{
	public:
		EffectivePowerBlockPlace( );
		~EffectivePowerBlockPlace( );

	private:
		int powerBlockCode;
		ColorBlockPlace* pRedPowerSpot;
		ColorBlockPlace* pGreenPowerSpot;
		ColorBlockPlace* pBluePowerSpot;
		ColorBlockPlace* pYellowPowerSpot;
};



/*****************************************************************************************************/
/******** ブロック並べエリア情報                                                              ********/
/*****************************************************************************************************/
class BlockSortInfo
{
	public:
		BlockSortInfo( );
		~BlockSortInfo( );

	private:
		int initialPositionCode;

		/* 各ライン生成とノード座標定義 */
		VLine vLine[12] = {
			{ 600,4},{ 600,454},{ 600,904},{ 600,1354},
			{ 200,4},{ 200,454},{ 200,904},{ 200,1354},
			{-200,4},{-200,454},{-200,904},{-200,1354}
		};
		HLine hLine[12] = {
			{ 800,229},{ 800,679},{ 800,1129},
			{ 400,229},{ 400,679},{ 400,1129},
			{   0,229},{   0,679},{   0,1129},
			{-400,229},{-400,679},{-400,1129}
		};
		StartLine sLine = {0,-221};

		/* 各ブロック置き場定義 */
		ColorBlockPlace cbp[16] = {
			{  0,  800,    4, BSI_Def::Red },
			{  1,  800,  454, BSI_Def::Yellow },
			{  2,  800,  904, BSI_Def::Blue },
			{  3,  800, 1354, BSI_Def::Green },
			{  4,  400,    4, BSI_Def::Blue },
			{  5,  400,  454, BSI_Def::Green },
			{  6,  400,  904, BSI_Def::Red },
			{  7,  400, 1354, BSI_Def::Yellow },
			{  8,    0,    4, BSI_Def::Red },
			{  9,    0,  454, BSI_Def::Yellow },
			{ 10,    0,  904, BSI_Def::Blue },
			{ 11,    0, 1354, BSI_Def::Green },
			{ 12, -400,    4, BSI_Def::Blue },
			{ 13, -400,  454, BSI_Def::Green },
			{ 14, -400,  904, BSI_Def::Red },
			{ 15, -400, 1354, BSI_Def::Yellow }
		};
};

#endif 
