#ifndef _GUI_PANEL_BASE_H_
#define _GUI_PANEL_BASE_H_

/** Include file *************************************************************/
#include "ev3api.h"

#include "GuiButton.h"
#include <string>

/** using宣言 ****************************************************************/


/** Class ********************************************************************/
namespace GUI{
	class GuiPanelBase{
		public:
			/* 定期スレッド呼出し用 */
			virtual bool BtEvt( GuiButton* ){ return false; };
			/* コンストラクタ */
    		GuiPanelBase( ){ };
			virtual ~GuiPanelBase(){ };

			std::string Name = "";

			virtual void Refresh( ){ };		/* 画面更新処理 */

			void StartDisplay( ){ CursolFl = false; Load( ); };
			void StartOperation( ){ CursolFl = false; StartSet( ); };
			bool GetCursolRightReturn( ){ return CursolFl; };

		private:
			bool CursolFl;

			virtual void Load( ){ };		/* パネルが表示されたときにおこなう処理 */
			virtual void StartSet( ){ };	/* カーソルが移った時におこなう処理 */

		protected:
			static const int StartY = 37;
			static const int StrH = 17;
			static const int StrW = 10;

			void CursolRightReturn( ){ CursolFl = true; };
			void draw_line(int x1, int y1, int x2, int y2 ){ ev3_lcd_draw_line( x1, y1 + StartY, x2, y2 + StartY ); };
			void draw_string( const char *str, int32_t x, int32_t y ){ ev3_lcd_draw_string( str, x * StrW, ( y * StrH ) + StartY ); };
			void draw_stringXFree( const char *str, int32_t x, int32_t y ){ ev3_lcd_draw_string( str, x, ( y * StrH ) + StartY ); };
	};
}

#endif  // EV3_APP_LINETRACER_H_
