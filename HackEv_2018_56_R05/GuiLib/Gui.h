#ifndef _GUI_H_
#define _GUI_H_

/** Include file *************************************************************/
#include "ev3api.h"
#include "GuiButton.h"
#include "GuiPanelBase.h"
#include "ScenarioManager.h"
#include "BlockSortDataManager.h"

#include <vector>
#include <string>

/** using宣言 ****************************************************************/


/** Class ********************************************************************/
namespace GUI{
	class Gui{
		public:
			/* 定期スレッド呼出し用 */
			void Thread( );
			~Gui( );

			void Refresh( );

			void OpeningDisplay( );
			void SettingDisplay( );
			void ReadyDisplay( );
			bool GetSttingFixState();

			static void Create( ScenarioManager* sm, BlockSortDataManager* bdm ){ 
				if( gui == NULL ){
					gui = new Gui( sm, bdm );
				}
			};
			static Gui* GetGui(){ return gui; };

		private:
			static Gui* gui;

			/* コンストラクタ */
	    	Gui( ScenarioManager*, BlockSortDataManager* );

			bool CursolEnable;
			ScenarioManager* scenarioManager;
			BlockSortDataManager* blockSortDataManager;

			enum State{	LcdInit = 0, Opening, InitialSetting, Ready, Run };
			enum State state;
			bool SettingFixFl;

			int selectPanelNo;
			int selectScenarioNo;

			enum CurSol{ None = 0, SenarioSelect, PanelSelect };
			enum CurSol curSol;

			std::vector<GuiPanelBase*> panelList;
			GuiButton* guiButton;

			void OpeningPanelRefresh( );
			void RaadyPanelRefresh( );

			void SettingPanelEvt( );
			void SettingPanelRefresh( );
	};
}
#endif  // EV3_APP_LINETRACER_H_
