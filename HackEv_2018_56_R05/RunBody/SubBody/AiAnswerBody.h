#ifndef _AI_ANSWER_BODY_H_
#define _AI_ANSWER_BODY_H_

/** Include file *************************************************************/
#include "BodyBase.h"
#include "PositionCalculator.h"

/** using宣言 ****************************************************************/

/* Class ******************************************************************/
class AiAnswerBody : public BodyBase {
public:
	AiAnswerBody(PositionCalculator*);
	~AiAnswerBody();

	unsigned char GetDagitalData(){ return DagitalData; };

private:
	enum Evt {
		Evt_Wait = 0,
		Evt_LineTrace,
		Evt_GoToPosition,
		Evt_GotoAngleLeft,
		Evt_GotoAngleRight,
		Evt_RelativeCheck,
		Evt_DigitalDataScan
	};
	Evt evt;

	PositionCalculator* positionCalculator;

	unsigned char DagitalData = 0x00;

	void Start();
	void Run(Motor&, Motor&, ArmControl*, RGB_Checker*);

	void WheelControl(int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel);
	int PID_feedback( float x, float Kp, float Ki, float Kd, float dt );

/* イベント定義 **********************************************************/
	/* Stop ************************************************/
	public:
		void Stop();
	private:
	/*******************************************************/

	/* LineTrace *******************************************/
	public:
		void LineTrace();
	private:
	/*******************************************************/

	/* RelativeCheck ***************************************/
	public:
		void RelativeCheck();
	private:
	/*******************************************************/

	/* GoToPosition ****************************************/
	public:
		void GoToPosition( int speed, int x, int y, int id );
	private:
		int PositionSpeed = 50;
		int TargetPositionX = 0;
		int TargetPositionY = 0;
		int RelativeId;
	/*******************************************************/

	/* GoToAngle *******************************************/
	public:
		void GotoAngleLeft( int speed, double angle );
		void GotoAngleRight( int speed, double angle );
	private:
		int AngleSpeed = 20;
		int TargetAngle = 0;
	/*******************************************************/

	/* DigitalDataScan *************************************/
	public:
		void DigitalDataScan(int id);
	private:
		void digitalDataScanFuncetion(Motor& m_leftWheel, Motor& m_rightWheel, RGB_Checker* m_rgb_Checker, int id);
		int DScanState = 0;
	/*******************************************************/

};

#endif 
