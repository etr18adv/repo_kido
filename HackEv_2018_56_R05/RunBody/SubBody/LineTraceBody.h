#ifndef _LINETRACE_BODY_H_
#define _LINETRACE_BODY_H_

/** Include file *************************************************************/
#include "BodyBase.h"
#include "PositionCalculator.h"

/** using宣言 ****************************************************************/

/* Class ******************************************************************/

class LineTraceBody : public BodyBase {
public:
	LineTraceBody(PositionCalculator*);
	~LineTraceBody();

private:
	enum Evt {
		Evt_Wait = 0,
		Evt_LineTrace,
		Evt_LineTracePID,
		Evt_GoToPosition
	};
	Evt evt;

	PositionCalculator* positionCalculator;

	void Start();
	void Run(Motor&, Motor&, ArmControl*, RGB_Checker*);

/* 共通関数 */
	int PID_feedback(float x, float Kp, float Ki, float Kd, float dt);
	void WheelControl(int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel);


/* イベント定義 **********************************************************/
	/* Stop ************************************************/
	public:
		void Stop();
	private:
	/*******************************************************/

	/* LineTrace *******************************************/
	public:
		void LineTrace(int speed, int target );
		void LineTrace( int speed, int Target, float mp );
	private:
		int LineTraceSpeed = 50;
		int LineTarget = 50;
		float p = 0.45;
	/*******************************************************/

	/* GoToPosition ****************************************/
	public:
		void GoToPosition( int speed, int x, int y, int id );
	private:
		int PositionSpeed = 50;
		int TargetPositionX = 0;
		int TargetPositionY = 0;
		int ID = 0;
	/*******************************************************/

};

#endif 
