/** Include file *************************************************************/
#include "R_ParkingBody.h"

/** using宣言 ****************************************************************/

/* Function ******************************************************************/
R_ParkingBody::R_ParkingBody(
	/** 引数 **/
		PositionCalculator* mpositionCalculator ):
	/** 初期化 **/
		BodyBase( ),
		evt( Evt::Evt_Wait ),
		positionCalculator( mpositionCalculator )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
R_ParkingBody::~R_ParkingBody( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void R_ParkingBody::Start( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void R_ParkingBody::Run( Motor& m_leftWheel, Motor& m_rightWheel, ArmControl* m_armControl, RGB_Checker* m_rgb_Checker )
/*****************************************************************************/
{

	switch( this->evt ){
		/* 待機 */
		case Evt::Evt_Wait:
			m_leftWheel.setPWM( 0 );
			m_rightWheel.setPWM( 0 );			
			break;

		/* ライントレース */
		case Evt::Evt_LineTrace:
			int turn;
			if( this->RightFl ){
				//turn = PID_feedback( (60 - m_rgb_Checker->GetRefrectData( )), 0.70F, 0.00F, 0.07F, 0.004F );
				turn = PID_feedback( (m_rgb_Checker->GetRefrectData( ) - 50), 0.45F, 0.00F, 0.05F, 0.004F );
			}else{
				//turn = PID_feedback( (m_rgb_Checker->GetRefrectData( ) - 60), 0.70F, 0.00F, 0.07F, 0.004F );
				turn = PID_feedback( (m_rgb_Checker->GetRefrectData( ) - 50), 0.45F, 0.00F, 0.05F, 0.004F );
			}
			//if(15 < turn){
			//	turn = 15;
			//}
			//if(-15 > turn){
			//	turn = -15;
			//}
			WheelControl( 
				18,	(float)turn, m_leftWheel, m_rightWheel );
			break;

		/* 指定座標へ向かう */
		case Evt::Evt_GoToPosition:
			{
				double tA = positionCalculator->CalculateAngle( this->TargetPositionX, this->TargetPositionY );
				double dA = tA - positionCalculator->GetAngle( );
				if( 90.0 < dA ){
					dA = 90.0;
				}else if( -90.0 > dA ){
					dA = -90.0;
				}else{
					
				}
				WheelControl( this->PositionSpeed, dA, m_leftWheel, m_rightWheel );

				//PositionCalculator::Position position = positionCalculator->GetPosition( );
				//fprintf( bt, "%f,%f,%d,%d,\r\n", position.X, position.Y, tA, positionCalculator->GetAngle() );
			}
			break;

		/* 指定距離バックする */
		case Evt::Evt_Back:
			{
				int32_t NowDistance = positionCalculator->GetDistance( );
				if( ( this->StartDistance - this->BackDistance ) <= NowDistance ){
					m_leftWheel.setPWM( -this->BackSpeed );
					m_rightWheel.setPWM( -this->BackSpeed );
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
					this->Busy = false;
				}
			}
			break;

		/* 左足を中心に方向を変える */
		case Evt::Evt_TurnLeft:
			{
				int NowAngle = (int)positionCalculator->GetAngle( );
				if( ( this->TurnLeftAngle + this->LStartAngle )  <= NowAngle ){
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( this->TurnLeftSpeed );
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
					this->Busy = false;
				}
			}
			break;

		/* 右足を中心に方向を変える */
		case Evt::Evt_TurnRight:
			{
				int NowAngle = (int)positionCalculator->GetAngle( );
				if( ( this->TurnRightAngle + this->RStartAngle ) >= NowAngle ){
					m_leftWheel.setPWM( this->TurnRightspeed );
					m_rightWheel.setPWM( 0 );
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
					this->Busy = false;
				}
			}
			break;

		case Evt::Evt_KeepAngle:
			{
				double dA = this->Angle - positionCalculator->GetAngle( );
				WheelControl( this->Speed, dA, m_leftWheel, m_rightWheel );
			}
			break;

		/* フリー(検討用) */
		case Evt::Evt_Free:
			m_leftWheel.setPWM( this->LeftPower );
			m_rightWheel.setPWM( this->RightPower );
			break;

		default:
				
			break;
	}		
}

/* Function ******************************************************************/
int R_ParkingBody::PID_feedback( float x, float Kp, float Ki, float Kd, float dt )
/*****************************************************************************/
{
	float p = 0, i = 0, d = 0;

	p  = Kp * x;
	this->integral += ( this->prev_x + x ) / 2.0F * dt;
	d  = Kd * ( x - this->prev_x ) / dt;
	i  = Ki * this->integral;

	this->prev_x = x;
		
	return((int)( p + i + d ));
}

/* Function ******************************************************************/
void R_ParkingBody::WheelControl( int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel )
/*****************************************************************************/
{
	/* Turnが大きいほど右に曲がる */
	m_leftWheel.setPWM( (int)( front + ( turn * 0.50F ) ) );
	m_rightWheel.setPWM( (int)( front - ( turn * 0.50F ) ) );
}


/* Function ******************************************************************/
void R_ParkingBody::Stop( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Wait;
	this->Busy = false;
}

/* Function ******************************************************************/
void R_ParkingBody::LineTrace( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_LineTrace;
	this->integral = 0;
	this->prev_x = 0;
	this->RightFl = false;
}

/* Function ******************************************************************/
void R_ParkingBody::LineTraceRightOption( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_LineTrace;
	this->integral = 0;
	this->prev_x = 0;
	this->RightFl = true;
}

/* Function ******************************************************************/
void R_ParkingBody::GoToPosition( int speed, int x, int y )
/*****************************************************************************/
{
	this->evt = Evt::Evt_GoToPosition;
	this->PositionSpeed = speed;
	this->TargetPositionX = x;
	this->TargetPositionY = y;
}

/* Function ******************************************************************/
void R_ParkingBody::Back( int speed, int distance )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Back;
	this->BackSpeed = speed;
	this->BackDistance = distance;

	this->StartDistance = positionCalculator->GetDistance( );

	this->Busy = true;
}

/* Function ******************************************************************/
void R_ParkingBody::TurnLeft( int speed, int Angle )
/*****************************************************************************/
{
	this->evt = Evt::Evt_TurnLeft;
	this->TurnLeftSpeed = speed;
	this->TurnLeftAngle = Angle;

	this->LStartAngle = positionCalculator->GetAngle( );

	this->Busy = true;
}

/* Function ******************************************************************/
void R_ParkingBody::TurnRight( int speed, int Angle )
/*****************************************************************************/
{
	this->evt = Evt::Evt_TurnRight;
	this->TurnRightspeed = speed;
	this->TurnRightAngle = Angle;

	this->RStartAngle = positionCalculator->GetAngle( );

	this->Busy = true;
}

/* Function ******************************************************************/
void R_ParkingBody::KeepAngle( int speed, int angle )
/*****************************************************************************/
{
	this->evt = Evt::Evt_KeepAngle;
	this->Speed = speed;
	this->Angle = angle;
}

/* Function ******************************************************************/
void R_ParkingBody::Free( int mLeftPower, int mRightPower )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Free;
	this->LeftPower  = mLeftPower;
	this->RightPower = mRightPower;
}

