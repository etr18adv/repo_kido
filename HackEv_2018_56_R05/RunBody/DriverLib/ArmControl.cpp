
/** Include file *************************************************************/
#include "ArmControl.h"


/* Function ******************************************************************/
ArmControl::ArmControl( 
	/** 引数 **/
		Motor& mgArmMoter ):
	/** 初期化 **/
		state( State::Init ),
		TargetAngle( 25 ),
		gArmMoter( mgArmMoter )
/*****************************************************************************/
{
	;
}

/* Function ******************************************************************/
void ArmControl::Thread( )
/*****************************************************************************/
{
	switch( this->state ){
		case State::Init:
			if( InitAlignment( ) ){
				this->state = State::Play;
			}
			break;

		case State::Play:
			{
				int ArmPower = ( this->TargetAngle - gArmMoter.getCount( ));
				gArmMoter.setPWM( ArmPower );
			}
			break;

		default:
				
			break;
	}
}

/* Function ******************************************************************/
bool ArmControl::InitAlignment( )
/*****************************************************************************/
{
	bool ans = false;

	{
		static int SubStateCount = 0;
		static int TimeCount = 0;

		switch( SubStateCount ){
			case 0:
				TimeCount = 3000 / 4;				/* 3000ms待機 */
				this->gArmMoter.setPWM( -10 );		/* 逆回転で押さえつける(ギアの遊びをなくす) */
				SubStateCount = 1;
				break;

				case 1:
				TimeCount--;
				if( 0 == TimeCount ){
					this->gArmMoter.stop( );		/* 停止 */
					TimeCount = 200 / 4;			/* 200ms待機 */
					SubStateCount = 2;
				}
				break;

			case 2:
				TimeCount--;
				if( 0 == TimeCount ){
						this->gArmMoter.reset( );	/* リセット */
					TimeCount = 200 / 4;			/* 200ms待機 */
					SubStateCount = 3;
				}
				break;

			case 3:
				TimeCount--;
				if( 0 == TimeCount ){
					ans = true;
				}				
				break;

			default:
					
				break;
		}
	}
	return( ans );
}

/* Function ******************************************************************/
void ArmControl::SetTargetAngle( int target )
/*****************************************************************************/
{
	this->TargetAngle = target;
}

/* Function ******************************************************************/
bool ArmControl::IsAdjust( )
/*****************************************************************************/
{
	if( (this->state == State::Init) && 
		( ( ( this->TargetAngle - 1 ) <= gArmMoter.getCount( ) ) && 
			( ( this->TargetAngle + 1 ) >= gArmMoter.getCount( ) ) 
		)
		){
		return true;
	}else{
		return false;
	}
}

/* Function ******************************************************************/
bool ArmControl::IsInitial( )
/*****************************************************************************/
{
	return(this->state != State::Init);
}
