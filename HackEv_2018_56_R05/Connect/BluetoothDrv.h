#ifndef _BLUETOOTH_DRV_H_
#define _BLUETOOTH_DRV_H_

/** Include file *************************************************************/
#include "BlockSortDataManager.h"
#include "ev3api.h"

/** using宣言 ****************************************************************/


/** Class ********************************************************************/
class BluetoothDrv
{
	public:
		/* 定期スレッド呼出し用 */
		void Thread( );
		void ReceveTask( );
		/* コンストラクタ */
    	BluetoothDrv( BlockSortDataManager* );

		void StartPcComera();
		bool PcComeraFinishJudge();

	private:
		bool PcFinish;
		enum EnRxState{
			RWait = 0,
			RStart,
			RDataReceve,
			RFinCheackm
		};
		EnRxState RxState;
		int ReceveCount;
		bool ReceveFinish;
		uint8_t ReceveData[4];

		FILE	 *bt = NULL;

		/****************************************/
		enum EnKaState{
			KWait,
			KStart,
			KReceveWait
		};
		EnKaState KaState;
		uint8_t RxColorData[4];

		BlockSortDataManager* blockSortDataManager;
};

#endif  // EV3_APP_LINETRACER_H_
