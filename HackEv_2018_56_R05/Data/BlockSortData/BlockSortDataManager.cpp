
/** Include file *************************************************************/
#include "BlockSortDataManager.h"

/* Function ******************************************************************/
BlockSortDataManager::BlockSortDataManager( 
	/** 引数 **/
		):
	/** 初期化 **/
		InitPositionCode( 84722 ),
		colorBlocks( { None, None, None, None } )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void BlockSortDataManager::Thread(  )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
long BlockSortDataManager::GetInitPositionCode( ) const
/*****************************************************************************/
{
	return this->InitPositionCode;
}

/* Function ******************************************************************/
void BlockSortDataManager::SetInitPositionCode( long code )
/*****************************************************************************/
{
	this->InitPositionCode = code;
	//return true;
}

/* Function ******************************************************************/
struct BlockSortDataManager::ColorBlocks BlockSortDataManager::GetColorBlocks( ) const
/*****************************************************************************/
{
	return colorBlocks;
}

/* Function ******************************************************************/
void BlockSortDataManager::SetColorBlockColor( int id, enum BlockSortDataManager::ColorSet colorSet )
/*****************************************************************************/
{
	switch( id ){
		case 0:
			colorBlocks.FirstColor = colorSet;
			break;

		case 1:
			colorBlocks.SecoundColor = colorSet;
			break;

		case 2:
			colorBlocks.ThurdColor = colorSet;
			break;

		case 3:
			colorBlocks.FourthColor = colorSet;
			break;

		default:
			
			break;
	}
}
