/** Include file *************************************************************/
#include "LineTraceSceneR.h"

/** usingιΎ ****************************************************************/


/* Function ******************************************************************/
LineTraceSceneR::LineTraceSceneR( 
	LineTraceBody* mlineTraceBody,
	PositionCalculator* mPositionCalculator ):
	LineTraceScene( mlineTraceBody, mPositionCalculator )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
LineTraceSceneR::~LineTraceSceneR( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void LineTraceSceneR::Start( )
/*****************************************************************************/
{
	lineTraceBody->mStartRun( );
	const RGB_Checker* rgbC = lineTraceBody->GetRgb( );

	//RR[X
	//@ΕΜΌόTOcm
	lineTraceBody->LineTrace( 40, 60 );
	while(positionCalculator->GetDistance() < 50){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//AΕΜΌόγΌ
	lineTraceBody->LineTrace( 60, 60, 0.55 );
	while(positionCalculator->GetDistance() < 200){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//BΆJ[u
	lineTraceBody->LineTrace( 50, 60 );
	while(positionCalculator->GetDistance() < 380){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//CΌό
	lineTraceBody->LineTrace( 50, 60 );
	while(positionCalculator->GetDistance() < 435){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//CΌό
	lineTraceBody->LineTrace( 40, 60 );
	while(positionCalculator->GetDistance() < 450){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//DSOΌ
	lineTraceBody->LineTrace( 30, 60 );
	while(positionCalculator->GetDistance() < 490){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//ESγΌ
	lineTraceBody->LineTrace( 30, 30 );
	while(positionCalculator->GetDistance() < 560){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//FΌό
	lineTraceBody->LineTrace( 50, 60, 0.5 );
	while(positionCalculator->GetDistance() < 615){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//FΌό
	lineTraceBody->LineTrace( 40, 60 );
	while(positionCalculator->GetDistance() < 630){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//GEJ[u
	lineTraceBody->LineTrace( 30, 40 );
	while(positionCalculator->GetDistance() < 750){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//HΌό
	lineTraceBody->LineTrace( 60, 60, 0.55 );
	while(positionCalculator->GetDistance() < 920){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//HΌό
	lineTraceBody->LineTrace( 60, 60, 0.55 );
	while(positionCalculator->GetDistance() < 950){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//HΌό
	lineTraceBody->LineTrace( 50, 60, 0.55 );
	while(positionCalculator->GetDistance() < 970){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//10 Έ¬
	lineTraceBody->LineTrace( 30, 60 );
	while(positionCalculator->GetDistance() < 1020){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//11 ΆJ[u
	lineTraceBody->LineTrace( 15, 70 );
	while(positionCalculator->GetDistance() < 1070){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//12 Όό»Μγ
	lineTraceBody->LineTrace( 20, 70 );
	while(positionCalculator->GetDistance() < 1100){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//12 Όό»Μγ
	lineTraceBody->LineTrace( 30, 60 );
	while(positionCalculator->GetDistance() < 1200){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);


	lineTraceBody->Stop( );

	SetFinish( );
}

/* Function ******************************************************************/
void LineTraceSceneR::Run( )
/*****************************************************************************/
{
	SetFinish( );
}

