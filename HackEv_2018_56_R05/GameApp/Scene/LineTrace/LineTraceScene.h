#ifndef _LINETRACE_SCENE_
#define _LINETRACE_SCENE_

/** Include file *************************************************************/
#include "BaseScene.h"
#include "LineTraceBody.h"
#include "PositionCalculator.h"

/** using宣言 ****************************************************************/


/* Class ******************************************************************/
class LineTraceScene : public BaseScene
{
	public:
		LineTraceScene( LineTraceBody* mlineTraceBody, PositionCalculator* mPositionCalculator):BaseScene( ),lineTraceBody( mlineTraceBody ), positionCalculator( mPositionCalculator ){ };
		virtual ~LineTraceScene( ){ };

		virtual void Run( ) = 0 ;

	private:
		virtual void Start( ) = 0;
		
	protected:
		LineTraceBody* lineTraceBody;
		PositionCalculator* positionCalculator;

		//void GoToPosition( int Speed, int tx, int ty, int TargetRange ){
		//	lineTraceBody->GoToPosition( Speed, tx, ty );
		//	while( !positionCalculator->RangeJuge( tx, ty, TargetRange ) ){
		//		tslp_tsk( 4 );	/* 4ms待機 */
		//	}
		//};

		void GoToPosition( int Speed, int tx, int ty, int TargetRange, int id ){
			lineTraceBody->GoToPosition( Speed, tx, ty, id );
			while( !positionCalculator->RangeJuge( tx, ty, TargetRange, id ) ){
				tslp_tsk( 4 );	/* 4ms待機 */
			}
		};

};

#endif 
