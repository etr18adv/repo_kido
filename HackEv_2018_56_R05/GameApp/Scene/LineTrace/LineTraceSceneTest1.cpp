/** Include file *************************************************************/
#include "LineTraceSceneTest1.h"

/** using�錾 ****************************************************************/


/* Function ******************************************************************/
LineTraceSceneTest1::LineTraceSceneTest1( 
	LineTraceBody* mlineTraceBody,
	PositionCalculator* mPositionCalculator ):
	LineTraceScene( mlineTraceBody, mPositionCalculator )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
LineTraceSceneTest1::~LineTraceSceneTest1( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void LineTraceSceneTest1::Start( )
/*****************************************************************************/
{
	lineTraceBody->mStartRun( );
	//const RGB_Checker* rgbC = lineTraceBody->GetRgb( );

	lineTraceBody->LineTrace( 30, 60 );
	//positionCalculator->StartCalculate( );

	while(positionCalculator->GetDistance() < 60){
		tslp_tsk( 4 );
	}
	int id = positionCalculator->CreateId();
#if 0
	GoToPosition( 70,    0, 100 + 140, 5, id );
	GoToPosition( 50,  -50, 145 + 140, 10, id );
	GoToPosition( 70, -150, 160 + 140, 10, id );
	GoToPosition( 50, -280, 175 + 140, 10, id );
	GoToPosition( 50, -280, 285 + 140, 5, id );
	GoToPosition( 50, -220, 255 + 140, 5, id );
	GoToPosition( 70,   -5,  95 + 140, 5, id );
	GoToPosition( 50,   -5, -200 + 140, 0, id );
	//GoToPosition( 50, -150, 230, 5, id );
	//GoToPosition( 50,    0, 350, 5 );
	//GoToPosition( 50,    0, 390, 5 );
	//GoToPosition( 50,    0, 420, 5 );
#else
	GoToPosition( 70,    0,  40 + 140, 5, id );
	GoToPosition( 50,  -50,  70 + 140, 10, id );
	GoToPosition( 50, -100,  50 + 140, 5, id );

	GoToPosition( 70, -125,-185 + 140, 5, id );

	GoToPosition( 50,  -80,-210 + 140, 10, id );
	GoToPosition( 50,  -55,-190 + 140, 5, id );

	GoToPosition( 70,  -45, -90 + 140, 5, id );
	GoToPosition( 50,  -95, -70 + 140, 10, id );

	GoToPosition( 50, -110, 100 + 140, 5, id );

	GoToPosition( 50,   50, 100 + 140, 5, id );

//	GoToPosition( 50,  0,-220, 5, id );


//	GoToPosition( 50,  -70,-225, 5, id );
//	GoToPosition( 50, -100,-220, 5, id );
//	GoToPosition( 50, -100, 100, 5, id );

//	GoToPosition( 50, -150, 230, 5, id );
#endif
	lineTraceBody->Stop( );
}

/* Function ******************************************************************/
void LineTraceSceneTest1::Run( )
/*****************************************************************************/
{

}

