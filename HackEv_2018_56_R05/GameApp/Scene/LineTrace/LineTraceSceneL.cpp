/** Include file *************************************************************/
#include "LineTraceSceneL.h"
#include "RGB_Checker.h"

/** using宣言 ****************************************************************/

//static FILE	 *bt = NULL;
/* Function ******************************************************************/
LineTraceSceneL::LineTraceSceneL( 
	LineTraceBody* mlineTraceBody,
	PositionCalculator* mPositionCalculator ):
	LineTraceScene( mlineTraceBody, mPositionCalculator )
/*****************************************************************************/
{
	//bt = ev3_serial_open_file(EV3_SERIAL_BT);	/* ←本来はSceneの方も持っていて、戦略的な支持を出す。 */
}

/* Function ******************************************************************/
LineTraceSceneL::~LineTraceSceneL( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void LineTraceSceneL::Start( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void LineTraceSceneL::Run( )
/*****************************************************************************/
{
	SetFinish( );

}

