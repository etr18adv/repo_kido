#ifndef _AI_ANSWER_SCENE_
#define _AI_ANSWER_SCENE_

/** Include file *************************************************************/
#include "BaseScene.h"
#include "AiAnswerBody.h"
#include "PositionCalculator.h"

/** using宣言 ****************************************************************/


/* Class ******************************************************************/
class AiAnswerScene : public BaseScene
{
	public:
		AiAnswerScene( AiAnswerBody*, PositionCalculator* );
		~AiAnswerScene( );

		void Run( );

	private:
		AiAnswerBody* aiAnswerBody;
		PositionCalculator* positionCalculator;

		int RelativePotsitionId;

		void Start( );

		void GoToPosition( int Speed, int tx, int ty, int TargetRange, int id ){
			aiAnswerBody->GoToPosition( Speed, tx, ty, id );
			while( !positionCalculator->RangeJuge( tx, ty, TargetRange, id ) ){
				tslp_tsk( 4 );	/* 4ms待機 */
			}
		};

		void GoToAngleLeft( int Speed, int Angle ){
			aiAnswerBody->GotoAngleLeft( Speed, Angle );
			while( aiAnswerBody->GetBusyState( ) ){
				tslp_tsk( 4 );	/* 4ms待機 */
			}
		}

		void GoToAngleRight( int Speed, int Angle ){
			aiAnswerBody->GotoAngleRight( 20, 270 );
			while( aiAnswerBody->GetBusyState( ) ){
				tslp_tsk( 4 );	/* 4ms待機 */
			}
		}
};

#endif 
