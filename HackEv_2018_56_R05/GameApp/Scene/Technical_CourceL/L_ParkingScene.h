#ifndef _L_PARKING_SCENE_
#define _L_PARKING_SCENE_

/** Include file *************************************************************/
#include "BaseScene.h"

#include "L_ParkingBody.h"

/** using�錾 ****************************************************************/

/* Class ******************************************************************/
class L_ParkingScene : public BaseScene
{
	public:
		L_ParkingScene( L_ParkingBody* );
		~L_ParkingScene( );

		void Run( );

	private:
		L_ParkingBody* l_ParkingBody;

		void Start( );
};

#endif 
