#ifndef _BLOCK_SORT_SCENE_
#define _BLOCK_SORT_SCENE_

/** Include file *************************************************************/
#include "BaseScene.h"

#include "BlockSortBody.h"
#include "BlockSortDataManager.h"
#include "RootAnalysis.h"

/** using�錾 ****************************************************************/


/* Class ******************************************************************/
class BlockSortScene : public BaseScene
{
	public:
		BlockSortScene( BlockSortBody*, PositionCalculator*, const BlockSortDataManager*, RootAnalysis* );
		~BlockSortScene( );

		void Run( );

	private:
		BlockSortBody* blockSortBody;
		PositionCalculator* positionCalculator;

		int RelativePotsitionId;

		const BlockSortDataManager* blockSortDataManager;
		RootAnalysis* rootAnalysis;

		BodyInfo* bodyInfo;

		void Start( );

		void GoToPosition( int Speed, int tx, int ty, int TargetRange, int id ){
			blockSortBody->GoToPosition( Speed, tx, ty, id );
			while( !positionCalculator->RangeJuge( tx, ty, TargetRange, id ) ){
				tslp_tsk( 4 );	/* 4ms�ҋ@ */
			}
		};

		void Wait500ms( );

		void NT_Function( int muki, bool aboid );
		void B_HOLD_Function( int muki, bool aboid );
		void B_NT_Function( int muki, bool aboid );
		void B_SET_Function( int muki, bool aboid );
};

#endif 
