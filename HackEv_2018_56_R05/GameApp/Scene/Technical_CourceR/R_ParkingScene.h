#ifndef _R_PARKING_SCENE_
#define _R_PARKING_SCENE_

/** Include file *************************************************************/
#include "BaseScene.h"

#include "R_ParkingBody.h"

/** using�錾 ****************************************************************/


/* Class ******************************************************************/
class R_ParkingScene : public BaseScene
{
	public:
		R_ParkingScene( R_ParkingBody*, PositionCalculator* );
		~R_ParkingScene( );

		void Run( );

	private:
		R_ParkingBody* r_ParkingBody;
		PositionCalculator* positionCalculator;

		void Start( );

		void Wait500ms( );

};

#endif 
