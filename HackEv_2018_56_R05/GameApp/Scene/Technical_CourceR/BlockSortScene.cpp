/** Include file *************************************************************/
#include "BlockSortScene.h"

#include <vector>

static FILE	 *bt = NULL;
/** using宣言 ****************************************************************/


/* Function ******************************************************************/
BlockSortScene::BlockSortScene( 
	BlockSortBody* mBlockSortBody,
	PositionCalculator* mPositionCalculator,
	const BlockSortDataManager* mblockSortDataManager,
	RootAnalysis* mRootAnalysis ):
	BaseScene( ),
	blockSortBody( mBlockSortBody ),
	positionCalculator( mPositionCalculator ),
	blockSortDataManager( mblockSortDataManager ),
	rootAnalysis( mRootAnalysis )
/*****************************************************************************/
{
	bt = ev3_serial_open_file(EV3_SERIAL_BT);
}

/* Function ******************************************************************/
BlockSortScene::~BlockSortScene( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void BlockSortScene::Start( )
/*****************************************************************************/
{

	blockSortBody->mStartRun( );				/* 走行体をブロック並べモード */
	const RGB_Checker* rgbC = blockSortBody->GetRgb( );	/* RGBデータ取得用 */

	bodyInfo = rootAnalysis->GetBody();			/* 走行体位置情報を取得 */

	/* 通信エラーの場合どうするか **************************************************/
	while( 1 ){
		/* 解析完了まで待つ */
		if( rootAnalysis->AnalysisFinish() ){
			break;
		}
		tslp_tsk( 4 );	/* 4ms待機 */
	}
	/*******************************************************************************/

	/* 制御命令リスト取得 */
	std::vector<std::vector<ControlOder>> COrderList = rootAnalysis->GetOrder();

	/* 赤までライントレース */
	blockSortBody->LineTrace( );
	while( 1 ){
		RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
		//fprintf( bt, "%d:%d:%d\r\n", hsv.H, hsv.S, hsv.V );
		if(( 35 < hsv.V ) && ( 150 < hsv.S )){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}
	blockSortBody->Stop( );

	/* 相対座標の取得 */
	this->RelativePotsitionId = positionCalculator->CreateId();

	/* 開始ノードまでバック */
	blockSortBody->Free(-10, -10);
	while(1){
		int32_t d = positionCalculator->GetDistance( this->RelativePotsitionId );
		//fprintf( bt, "%d\r\n", d );
		if( d <= -11 ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}
	blockSortBody->Stop( );


	/* 制御命令リスト通りに実行 */
	for(std::vector<ControlOder> cOrderL:COrderList){
		for(ControlOder cOrder : cOrderL){
			/* 現置き場の座標取得 */
			/* 現向き取得 */
			/* 現走行体情報更新、目標ノード座標取得 */
			bodyInfo->Ref( cOrder.muki );
			switch( cOrder.order ){
				/* ブロック無 ノード間移動 */
				case NodeTo:
					NT_Function( cOrder.muki, cOrder.aboid );
					break;

				/* ブロックを取りノードへ */
				case BBring:
					B_HOLD_Function( cOrder.muki, cOrder.aboid );
					break;

				/* ブロック有 ノード間移動 */
				case BNodeTo:
					NT_Function( cOrder.muki, cOrder.aboid );
					break;

				/* ブロック設置、 */
				case BSet:
					B_SET_Function( cOrder.muki, cOrder.aboid );
					break;

				default:
					break;
			}
			ev3_speaker_play_tone(NOTE_A5,100);
		}
	}

	/* ブロック並べ脱出 */
	std::vector<ControlOder> EndCOrderList = rootAnalysis->EndRoot();	/* ルート作成 */
	for(ControlOder cOrder : EndCOrderList){
		/* 現置き場の座標取得 */
		/* 現向き取得 */
		/* 現走行体情報更新、目標ノード座標取得 */
		bodyInfo->Ref( cOrder.muki );
		switch( cOrder.order ){
			/* ブロック無 ノード間移動 */
			case NodeTo:
				NT_Function( cOrder.muki, cOrder.aboid );
				break;

			/* ブロック設置、 */
			case BSet:
				B_SET_Function( cOrder.muki, cOrder.aboid );
				break;

			default:
				break;
		}
	}

	/* ガレージの方を向く */
	{
		double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
		if(bodyInfo->hougaku == V_Plus){
			blockSortBody->Free(10, -10);
			while(1){
				double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
				if( ( BuffAngle - SAngle ) >= 90 ){ break; }
				tslp_tsk( 4 );	/* 4ms待機 */
			}
		}else{
			blockSortBody->Free(-10, 10);
			while(1){
				double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
				if( ( BuffAngle - SAngle ) <= -90 ){ break; }
				tslp_tsk( 4 );	/* 4ms待機 */
			}
		}
	}

	/* 停止 */
	blockSortBody->Stop( );

	SetFinish( );
}

/* Function ******************************************************************/
void BlockSortScene::Run( )
/*****************************************************************************/
{
	SetFinish( );
}

/* Function ******************************************************************/
void BlockSortScene::NT_Function( int muki, bool aboid )
/*****************************************************************************/
{
	double BuffAngle;
	const RGB_Checker* rgbC = blockSortBody->GetRgb( );	/* RGBデータ取得用 */

	switch(muki){
		/* 前進 *********************************************************/
		case Front:
			if( aboid ){
				/** 避ける **/
				/******************** 右除け等の対策も必要!! *************************/
				/* 1.左を向く */
				{
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->Free(-15, 15);
					while(1){
						BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
						if( ( BuffAngle - SAngle ) <= -35 ){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 2.角度維持で前進 */
				{
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->KeepAngle( 15, (int)BuffAngle, this->RelativePotsitionId );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						if( (Nd - Sd) >= 1 ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}
	
					//ev3_speaker_play_tone(NOTE_G5,100);
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
						if( (hsv.V < 20) || ( (Nd - Sd) >= 22 ) ){ break; }						/* ←ここに距離判定を追加(距離だけでやってみる？) */
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				//ev3_speaker_play_tone(NOTE_G5,200);
				/* 3.右を向く */
				{
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->Free(15, -15);
					while(1){
						BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
						if( ( BuffAngle - SAngle ) >= 35 ){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 4.角度維持で前進 */
				{
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->KeepAngle( 15, (int)BuffAngle, this->RelativePotsitionId );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						if( (Nd - Sd) >= 6 ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}

				/* 5.右を向く */
				{
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->Free(15, -15);
					while(1){
						BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
						if( ( BuffAngle - SAngle ) >= 42 ){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 6.角度維持で前進 */
				{
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->KeepAngle( 15, (int)BuffAngle, this->RelativePotsitionId );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						if( (Nd - Sd) >= 8 ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}

					blockSortBody->KeepAngle( 15, (int)BuffAngle - 20, this->RelativePotsitionId );
					while(1){
						RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
						if(hsv.V < 20){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();
	

				/* 7.左を向く */
				{
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->Free(-15, 15);
					while(1){
						BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
						if( ( BuffAngle - SAngle ) <= -22 ){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 8.ライントレース */
				{
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->LineTraceRightOption( );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						int hougaku = bodyInfo->GetHougaku();
						int le;
						if( (H_Plus == hougaku) || (H_Minus == hougaku)){
							//le = 12;		/* 0915 */
							le = 10;
						}else{
							//le = 8;		/*  */
							le = 6;
						}
						if( (Nd - Sd) >= le ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}			
				Wait500ms();

			}else{
				/** 避けない **/
				/* 1.ライントレース */
				{
					blockSortBody->LineTrace( );
					while( 1 ){
						RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
						if(( 35 < hsv.V ) && ( 160 < hsv.S )){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 2.ちょっと左を向く */
				{
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->Free(-15, 15);
					while(1){	
						BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
						if( ( BuffAngle - SAngle ) <= -8 ){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 3.角度維持で前進 */
				{
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->KeepAngle( 15, (int)BuffAngle, this->RelativePotsitionId );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						if( (Nd - Sd) >= 4 ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}

					//ev3_speaker_play_tone(NOTE_G5,100);
					while(1){
						RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
						if(( 35 < hsv.V ) && ( 160 < hsv.S )){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
	
					//ev3_speaker_play_tone(NOTE_G5,100);
					while(1){
						RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
						if(( 35 < hsv.V ) && ( 100 > hsv.S )){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 4.ライントレース */
				{
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->LineTrace( );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						int hougaku = bodyInfo->GetHougaku();
						int le;
						if( (H_Plus == hougaku) || (H_Minus == hougaku)){
							le = 21;
						}else{
							le = 17;
						}
						if( (Nd - Sd) >= le ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();
			}
			break;

		/* 左 ***********************************************************/
		case Left:
			if( aboid ){
				/** 避ける **/
				/* 1.左を向く */
				{
					//fprintf( bt, "L-1\r\n" );
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->Free(-15, 15);
					while(1){
						BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
						if( ( BuffAngle - SAngle ) <= -35 ){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 2.角度維持で前進 */
				{
					//fprintf( bt, "L-2\r\n" );
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->KeepAngle( 15, (int)BuffAngle, this->RelativePotsitionId );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						if( (Nd - Sd) >= 8 ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}

					while(1){
						RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
						if(hsv.V < 20){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 3.左を向く */
				{
					//fprintf( bt, "L-3\r\n" );
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->Free(-15, 15);
					while(1){
						BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
						if( ( BuffAngle - SAngle ) <= -45 ){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 4.ライントレース */
				{
					//fprintf( bt, "L-4\r\n" );
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->LineTrace( );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						int hougaku = bodyInfo->GetHougaku();
						int le;
						if( (H_Plus == hougaku) || (H_Minus == hougaku)){
							le = 13;
						}else{
							le = 7;
						}
						if( (Nd - Sd) >= le ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();
			}else{
				/** 避けない **/
				/* 1.ブロック置き場まで移動 */
				blockSortBody->LineTrace( );
				while( 1 ){
					RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
					//fprintf( bt, "%d:%d:%d\r\n", hsv.H, hsv.S, hsv.V );
					if(( 35 < hsv.V ) && ( 150 < hsv.S )){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				/* 2.左を向く */
				{
					//fprintf( bt, "L-1\r\n" );
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->Free(0, 15);
					while(1){
						BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
						if( ( BuffAngle - SAngle ) <= -105 ){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 3.ライントレース */
				{
					//fprintf( bt, "F-4\r\n" );
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->LineTrace( );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						int hougaku = bodyInfo->GetHougaku();
						int le;
						if( (H_Plus == hougaku) || (H_Minus == hougaku)){
							le = 19;
						}else{
							le = 15;
						}
						if( (Nd - Sd) >= le ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();	
	
			}
			break;

		/* 右 ***********************************************************/
		case Right:
			if( aboid ){
				/** 避ける **/
				/* 1.右を向く */
				{
					//fprintf( bt, "R-1\r\n" );
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->Free(15, -15);
					while(1){
						BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
						if( ( BuffAngle - SAngle ) >= 45 ){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 2.角度維持で前進 */
				{
					//fprintf( bt, "R-2\r\n" );
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->KeepAngle( 15, (int)BuffAngle, this->RelativePotsitionId );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						if( (Nd - Sd) >= 8 ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}

					while(1){
						RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
						if(hsv.V < 20){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}

					Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						if( (Nd - Sd) >= 2 ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 3.右を向く */
				{
					//fprintf( bt, "R-3\r\n" );
					blockSortBody->Free(10, -10);
					while(1){
						RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
						if(hsv.V < 20){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

				/* 4.ライントレース */
				{
					//fprintf( bt, "R-5\r\n" );
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->LineTraceRightOption( );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						int hougaku = bodyInfo->GetHougaku();
						int le;
						if( (H_Plus == hougaku) || (H_Minus == hougaku)){
							le = 13;
						}else{
							le = 9;
						}
						if( (Nd - Sd) >= le ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}			
				Wait500ms();
			}else{
				/** 避けない **/
				/* 1.ブロック置き場まで移動 */
				blockSortBody->LineTrace( );
				while( 1 ){
					RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
					//fprintf( bt, "%d:%d:%d\r\n", hsv.H, hsv.S, hsv.V );
					if(( 35 < hsv.V ) && ( 150 < hsv.S )){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				{
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->KeepAngle( 10, (int)SAngle, this->RelativePotsitionId );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						if( (Nd - Sd) >= 1 ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}

				/* 2.右を向く */
				{
					//fprintf( bt, "R-1\r\n" );
					double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					blockSortBody->Free(15, 0);
					while(1){
						BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
						if( ( BuffAngle - SAngle ) >= 75 ){ break; }
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();
			
				/* 3.ライントレース */
				{
					//fprintf( bt, "F-4\r\n" );
					int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
					blockSortBody->LineTrace( );
					while(1){
						int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
						int hougaku = bodyInfo->GetHougaku();
						int le;
						if( (H_Plus == hougaku) || (H_Minus == hougaku)){
							le = 19;
						}else{
							le = 15;
						}
						if( (Nd - Sd) >= le ){ break;}
						tslp_tsk( 4 );	/* 4ms待機 */
					}
				}
				Wait500ms();

			}
			break;

		/* 後退 *********************************************************/
		case Back:
			{
				//fprintf( bt, "B-1\r\n" );
				double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
				blockSortBody->Free(-20, 20);
				while(1){
					BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					if( ( BuffAngle - SAngle ) <= -140 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				blockSortBody->Free(-8, 8);
				while(1){
					RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
					if(hsv.V < 20){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
				Wait500ms();

			}

			/* ノードまでバック */
			{
				int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
				blockSortBody->Free(-10, -10);
				while(1){
					int32_t d = positionCalculator->GetDistance( this->RelativePotsitionId );
					if( (d - Sd) <= 0 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
			Wait500ms();
			break;

		default:
			
			break;
	}
}

/* Function ******************************************************************/
void BlockSortScene::B_HOLD_Function( int muki, bool aboid )
/*****************************************************************************/
{
	double BuffAngle;
	const RGB_Checker* rgbC = blockSortBody->GetRgb( );	/* RGBデータ取得用 */

	/* ブロック置き場まで移動 */
	blockSortBody->LineTrace( );
	while( 1 ){
		RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
		//fprintf( bt, "%d:%d:%d\r\n", hsv.H, hsv.S, hsv.V );
		if(( 35 < hsv.V ) && ( 150 < hsv.S )){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	switch(muki){
		/* 前進 *********************************************************/
		case Front:
			/* 1.ちょっと左を向く */
			{
				//fprintf( bt, "F-2\r\n" );
				double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
				blockSortBody->Free(-15, 15);
				while(1){
					BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					if( ( BuffAngle - SAngle ) <= -8 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
			Wait500ms();

			/* 2.角度維持で前進 */
			{
				//fprintf( bt, "F-3\r\n" );
				int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
				blockSortBody->KeepAngle( 15, (int)BuffAngle, this->RelativePotsitionId );
				while(1){
					int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
					if( (Nd - Sd) >= 4 ){ break;}
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				while(1){
					RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
					if(( 35 < hsv.V ) && ( 160 < hsv.S )){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				while(1){
					RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
					if(( 35 < hsv.V ) && ( 100 > hsv.S )){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
			Wait500ms();

			/* 3.ライントレース */
			{
				//fprintf( bt, "F-4\r\n" );
				int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
				blockSortBody->LineTrace( );
				while(1){
					int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
					int hougaku = bodyInfo->GetHougaku();
					int le;
					if( (H_Plus == hougaku) || (H_Minus == hougaku)){
						le = 23;
					}else{
						le = 19;
					}
					if( (Nd - Sd) >= le ){ break;}
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
			Wait500ms();			
			break;

		/* 左 ***********************************************************/
		case Left:
			/* 1.左を向く */
			{
				//fprintf( bt, "L-1\r\n" );
				double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
				blockSortBody->Free(0, 15);
				while(1){
					BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					if( ( BuffAngle - SAngle ) <= -105 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
			Wait500ms();

			/* 2.ライントレース */
			{
				//fprintf( bt, "F-4\r\n" );
				int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
				blockSortBody->LineTrace( );
				while(1){
					int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
					int hougaku = bodyInfo->GetHougaku();
					int le;
					if( (H_Plus == hougaku) || (H_Minus == hougaku)){
						le = 19;
					}else{
						le = 15;
					}
					if( (Nd - Sd) >= le ){ break;}
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
			Wait500ms();			
			break;

		/* 右 ***********************************************************/
		case Right:
			{
				int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
				double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
				blockSortBody->KeepAngle( 10, (int)SAngle, this->RelativePotsitionId );
				while(1){
					int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
					if( (Nd - Sd) >= 1 ){ break;}
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}

			/* 1.右を向く */
			{
				//fprintf( bt, "R-1\r\n" );
				double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
				blockSortBody->Free(15, 0);
				while(1){
					BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					if( ( BuffAngle - SAngle ) >= 75 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
			Wait500ms();
			
			/* 2.ライントレース */
			{
				//fprintf( bt, "F-4\r\n" );
				int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
				blockSortBody->LineTrace( );
				while(1){
					int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
					int hougaku = bodyInfo->GetHougaku();
					int le;
					if( (H_Plus == hougaku) || (H_Minus == hougaku)){
						le = 19;
					}else{
						le = 15;
					}
					if( (Nd - Sd) >= le ){ break;}
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
			Wait500ms();			
			break;

		/* 後退 *********************************************************/
		case Back:
			{
				double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
				int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
				blockSortBody->KeepAngle( 10, (int)SAngle, this->RelativePotsitionId );
				while(1){
					int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
					if( (Nd - Sd) >= 10 ){ break;}
					tslp_tsk( 4 );	/* 4ms待機 */
				}
				Wait500ms();
			}

			{
				//fprintf( bt, "B-1\r\n" );
				double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
				blockSortBody->Free(0, 20);
				while(1){
					BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					if( ( BuffAngle - SAngle ) <= -235 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				blockSortBody->Free(0, 8);
				while(1){
					BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					if( ( BuffAngle - SAngle ) <= -270 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
			Wait500ms();

			{
				int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
				blockSortBody->KeepAngle( 15, (int)BuffAngle, this->RelativePotsitionId );
				while(1){
					int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
					//if( (Nd - Sd) >= 5 ){ break;}
					if( (Nd - Sd) >= 6 ){ break;}
					tslp_tsk( 4 );	/* 4ms待機 */
				}
				Wait500ms();
			}

			{
				double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
				blockSortBody->Free(10, 0);
				while(1){
					BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
					if( ( BuffAngle - SAngle ) >= 80 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}

			/* 3.ライントレース */
			{
				//fprintf( bt, "F-4\r\n" );
				int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
				blockSortBody->LineTrace( );
				while(1){
					int32_t Nd = positionCalculator->GetDistance( this->RelativePotsitionId );
					//if( (Nd - Sd) >= 5 ){ break;}
					if( (Nd - Sd) >= 10 ){ break;}
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
			Wait500ms();			
			break;

		default:
			
			break;
	}
}

/* Function ******************************************************************/
void BlockSortScene::B_NT_Function( int muki, bool aboid )
/*****************************************************************************/
{
	switch(muki){
		/* 前進 *********************************************************/
		case Front:
			
			break;

		/* 左 ***********************************************************/
		case Left:
			
			break;

		/* 右 ***********************************************************/
		case Right:
			
			break;

		/* 後退 *********************************************************/
		case Back:
			
			break;

		default:
			
			break;
	}
}

/* Function ******************************************************************/
void BlockSortScene::B_SET_Function( int muki, bool aboid )
/*****************************************************************************/
{
	const RGB_Checker* rgbC = blockSortBody->GetRgb( );	/* RGBデータ取得用 */

	blockSortBody->LineTrace( );
	while( 1 ){
		RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
		if(( 35 < hsv.V ) && ( 150 < hsv.S )){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}
	Wait500ms();

	/* ノードまでバック */
	{
		int32_t Sd = positionCalculator->GetDistance( this->RelativePotsitionId );
		blockSortBody->Free(-10, -10);
		while(1){
			int32_t d = positionCalculator->GetDistance( this->RelativePotsitionId );
			if( (d - Sd) <= -11 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();
}

/* Function ******************************************************************/
void BlockSortScene::Wait500ms( )
/*****************************************************************************/
{
	int Time = 500 / 4;

	/* 停止 */
	blockSortBody->Stop( );
	while(1){
		Time--;
		if(0 == Time){ break; }
	}
}

