#ifndef _BLOCK_PRE_SCENE_H_
#define _BLOCK_PRE_SCENE_H_

/** Include file *************************************************************/
#include "BaseScene.h"
#include "RootAnalysis.h"

/** using�錾 ****************************************************************/


/* Class ******************************************************************/
class BlockPreScene : public BaseScene
{
	public:
		BlockPreScene( RootAnalysis* );
		~BlockPreScene( );

		void Run( );

	private:
		RootAnalysis* rootAnalysis;
		void Start( );

};

#endif 
