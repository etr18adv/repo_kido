#ifndef _BLOCK_PRE_TEST_SCENE_H_
#define _BLOCK_PRE_TEST_SCENE_H_

/** Include file *************************************************************/
#include "BaseScene.h"
#include "RootAnalysis.h"

/** using�錾 ****************************************************************/


/* Class ******************************************************************/
class BlockPreTestScene : public BaseScene
{
	public:
		BlockPreTestScene( RootAnalysis* );
		~BlockPreTestScene( );

		void Run( );

	private:
		RootAnalysis* rootAnalysis;
		void Start( );

};

#endif 
