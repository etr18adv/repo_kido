/** Include file *************************************************************/
#include "R_ParkingScene.h"

/** using宣言 ****************************************************************/

//static FILE	 *bt = NULL;


/* Function ******************************************************************/
R_ParkingScene::R_ParkingScene( 
	R_ParkingBody* mR_ParkingBody,
	PositionCalculator* mPositionCalculator ):
	BaseScene( ),
	r_ParkingBody( mR_ParkingBody ),
	positionCalculator( mPositionCalculator )
/*****************************************************************************/
{
	//bt = ev3_serial_open_file(EV3_SERIAL_BT);
}

/* Function ******************************************************************/
R_ParkingScene::~R_ParkingScene( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void R_ParkingScene::Start( )
/*****************************************************************************/
{
	r_ParkingBody->mStartRun( );
	const RGB_Checker* rgbC = r_ParkingBody->GetRgb( );	/* RGBデータ取得用 */

	{
		int WaitTime = 360 / 4;
		while( 1 ){
			if( 0 == WaitTime ){
				break;
			}
			WaitTime--;
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	/* ラインまで直進 */
	{
		int32_t Sd = positionCalculator->GetDistance( );
		double SAngle = positionCalculator->GetAngle( );
		r_ParkingBody->KeepAngle( 18, (int)SAngle + 30 );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 4 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(hsv.V < 20){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		Wait500ms();
	}

	/* 左を向く */
	{
		double SAngle = positionCalculator->GetAngle( );
		r_ParkingBody->Free(-15, 15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( );
			if( ( BuffAngle - SAngle ) <= -115 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	/* ライントレース */
	ev3_speaker_play_tone(NOTE_A5,100);
	{
		r_ParkingBody->LineTrace( );
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 20 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	{
		/* 向きを固定して直進 */
		int32_t StartD = positionCalculator->GetDistance( );
		double SAngle = positionCalculator->GetAngle( );
		r_ParkingBody->KeepAngle( 15, (int)SAngle );
		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			//fprintf( bt, "A_%d:%d:%d\r\n", hsv.H, hsv.S, hsv.V );
			if(hsv.V > 100){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		/*  */
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			//fprintf( bt, "B_%d:%d:%d\r\n", hsv.H, hsv.S, hsv.V );
			//if( ( (Nd - Sd) >= 30 ) && ( (Nd - StartD) >= 45 ) ){ break;}
			if( ( (Nd - Sd) >= 22 ) && ( (Nd - StartD) >= 45 ) ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	r_ParkingBody->Stop( );
}

/* Function ******************************************************************/
void R_ParkingScene::Run( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void R_ParkingScene::Wait500ms( )
/*****************************************************************************/
{
	int Time = 500 / 4;

	/* 停止 */
	r_ParkingBody->Stop( );
	while(1){
		Time--;
		if(0 == Time){ break; }
	}
}

