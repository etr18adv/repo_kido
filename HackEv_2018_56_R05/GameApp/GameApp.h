#ifndef _GAME_APP_
#define _GAME_APP_

/** Include file *************************************************************/
#include "ScenarioManager.h"

/** using宣言 ****************************************************************/

/* Class ******************************************************************/
class GameApp
{
	public:
		/* 定期スレッド呼出し用 */
		void Thread( );

		GameApp( ScenarioManager* );
		~GameApp( );

		void GameMain( );

	private:
		ScenarioManager* scenarioManager;

};

#endif 
