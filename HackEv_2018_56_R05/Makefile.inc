mkfile_path := $(dir $(lastword $(MAKEFILE_LIST)))

APPL_CXXOBJS += \
	GameApp.o \
	RGB_Checker.o \
	PositionCalculator.o \
	ArmControl.o \
	Gui.o \
	GuiButton.o \
	GuiPanelBlockSortSetting.o \
	ScenarioManager.o \
	InitSetingScene.o \
	ReadyScene.o \
	LineTraceSceneL.o \
	LineTraceSceneR.o \
	BlockSortScene.o \
	R_ParkingScene.o \
	Test1Scene.o \
	BlockPreScene.o \
	BlockPreTestScene.o \
	LineTraceSceneTest1.o \
	BlockSortDataManager.o \
	BodyBase.o \
	WaitBody.o \
	LineTraceBody.o \
	BlockSortBody.o \
	R_ParkingBody.o \
	BlockSortInfo.o \
	RootAnalysis.o \
	BluetoothDrv.o

SRCLANG := c++

ifdef CONFIG_EV3RT_APPLICATION

# Include libraries
include $(EV3RT_SDK_LIB_DIR)/libcpp-ev3/Makefile

endif

APPL_DIR += \
	$(mkfile_path)GameApp \
	$(mkfile_path)GameApp/Scene \
	$(mkfile_path)GameApp/Scene/Initial \
	$(mkfile_path)GameApp/Scene/LineTrace \
	$(mkfile_path)GameApp/Scene/Technical_CourceL \
	$(mkfile_path)GameApp/Scene/Technical_CourceR \
	$(mkfile_path)RunBody \
	$(mkfile_path)RunBody/SubBody \
	$(mkfile_path)RunBody/DriverLib \
	$(mkfile_path)MeasureLib \
	$(mkfile_path)GuiLib \
	$(mkfile_path)GuiLib/GuiSettinPanel \
	$(mkfile_path)Data/BlockSortData \
	$(mkfile_path)BlockSortAnalysis \
	$(mkfile_path)Connect

INCLUDES += \
	-I$(mkfile_path)GameApp \
	-I$(mkfile_path)GameApp/Scene \
	-I$(mkfile_path)GameApp/Scene/Initial \
	-I$(mkfile_path)GameApp/Scene/LineTrace \
	-I$(mkfile_path)GameApp/Scene/Technical_CourceL \
	-I$(mkfile_path)GameApp/Scene/Technical_CourceR \
	-I$(mkfile_path)RunBody \
	-I$(mkfile_path)RunBody/SubBody \
	-I$(mkfile_path)RunBody/DriverLib \
	-I$(mkfile_path)MeasureLib \
	-I$(mkfile_path)GuiLib \
	-I$(mkfile_path)GuiLib/GuiSettinPanel \
	-I$(mkfile_path)Data/BlockSortData \
	-I$(mkfile_path)BlockSortAnalysis \
	-I$(mkfile_path)Connect

COPTS += -fno-use-cxa-atexit

