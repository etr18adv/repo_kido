#ifndef _BLOCKSORT_DATA_MANAGER_H_
#define _BLOCKSORT_DATA_MANAGER_H_

/** Include file *************************************************************/


/** using宣言 ****************************************************************/


/** Class ********************************************************************/
class BlockSortDataManager
{
	public:
		/* 定期スレッド呼出し用 */
		void Thread( );
		/* コンストラクタ */
    	BlockSortDataManager( );

		long GetInitPositionCode( ) const;
		void SetInitPositionCode( long );

		/* 解析データベースと合わせている注意 */
		enum ColorSet{ Red = 0, Green = 1, Blue = 2, Yellow = 3, None = 4 };
		struct ColorBlocks{
			enum ColorSet FirstColor;
			enum ColorSet SecoundColor;
			enum ColorSet ThurdColor;
			enum ColorSet FourthColor;		
		};
		struct ColorBlocks GetColorBlocks( ) const;
		void SetColorBlockColor( int, enum ColorSet );

		int GetAiAnalogData( ) const{ return AiAnalogData; };
		void SetAiAnalogData( int adata ){ AiAnalogData = adata; };

	private:
		long InitPositionCode;
		struct ColorBlocks colorBlocks;

		int AiAnalogData = 0;
};

#endif  // EV3_APP_LINETRACER_H_
