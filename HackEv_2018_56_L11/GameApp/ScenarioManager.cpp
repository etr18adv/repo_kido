
/** Include file *************************************************************/
#include "ScenarioManager.h"

#include "InitSetingScene.h"
#include "ReadyScene.h"
#include "LineTraceSceneL.h"
//#include "LineTraceSceneR.h"
#include "AiAnswerScene.h"
//#include "BlockSortScene.h"
#include "L_ParkingScene.h"
//#include "R_ParkingScene.h"
#include "Test1Scene.h"
#include "LineTraceSceneTest1.h"

//#include "BlockPreScene.h"
//#include "BlockPreTestScene.h"

/** using宣言 ****************************************************************/
using ev3api::TouchSensor;

std::vector<std::string> ScenarioManager::ScenarioNameList;

/* Function ******************************************************************/
ScenarioManager::ScenarioManager( 
	const BlockSortDataManager* blockSortDataManager,
	WaitBody* WaitBody,
	LineTraceBody* lineTraceBody,
	AiAnswerBody* aiAnswerBody,
	L_ParkingBody* l_ParkingBody,
	R_ParkingBody* r_ParkingBody,
	PositionCalculator* positionCalculator,
	BluetoothDrv* bluetoothDrv
	 ):
	nowScenarioId( 0 )
/*****************************************************************************/
{
	/* スタートシナリオ作成 */
	InitScenario = new Scenario("InitScenario");
	InitScenario->AddScene( new InitSetingScene( ) );
	InitScenario->AddScene( new ReadyScene( ) );

	/* Lコースシナリオ作成 */
	Scenario *l_Cource = new Scenario("L-Cource");
	l_Cource->AddScene( new LineTraceSceneL( lineTraceBody, positionCalculator ) );
	l_Cource->AddScene( new AiAnswerScene( aiAnswerBody, positionCalculator, blockSortDataManager, bluetoothDrv ) );
	l_Cource->AddScene( new L_ParkingScene( r_ParkingBody, positionCalculator ) );
	scenarioList.push_back( l_Cource );

	/* テスト3シナリオ作成 */
	Scenario *t3_Cource = new Scenario("TestScenario3");
	t3_Cource->AddScene( new AiAnswerScene( aiAnswerBody, positionCalculator, blockSortDataManager, bluetoothDrv ) );
	t3_Cource->AddScene( new L_ParkingScene( r_ParkingBody, positionCalculator ) );
	scenarioList.push_back( t3_Cource );

	/* Lコースシナリオ作成 */
	Scenario *l_Line = new Scenario("L-LineTrace");
	l_Line->AddScene( new LineTraceSceneL( lineTraceBody, positionCalculator ) );
	scenarioList.push_back( l_Line );

	{
		int i;
		for( i = 0; i < (int)scenarioList.size( ); i++ ){
			ScenarioNameList.push_back( scenarioList[ i ]->Name );
		}
	}
}

/* Function ******************************************************************/
std::vector<std::string> ScenarioManager::GetScenarioNameList()
/*****************************************************************************/
{
	return ScenarioNameList;
}

/* Function ******************************************************************/
void ScenarioManager::InitScenarioSatrt( )
/*****************************************************************************/
{
	int sceneNo = 0;

	while( sceneNo < (int)InitScenario->SceneList.size( ) ){
		SceneStart( InitScenario->SceneList[ sceneNo ] );
		sceneNo++;
	}
}

/* Function ******************************************************************/
void ScenarioManager::ScenarioSatrt( )
/*****************************************************************************/
{
	/* シナリオ開始 */
	nowSceneNo = 0;

	while( nowSceneNo < (int)scenarioList[ nowScenarioId ]->SceneList.size( ) ){
		SceneStart( scenarioList[ nowScenarioId ]->SceneList[ nowSceneNo ] );
		nowSceneNo++;
	}
}

/* Function ******************************************************************/
void ScenarioManager::SceneStart( BaseScene *scene )
/*****************************************************************************/
{
	nowScene = scene;

	/* シーン開始 */
	nowScene->SceneStart( );

	/* 完了待ち */
	while( !nowScene->Finished( ) ){
		tslp_tsk( 4 );	/* 4ms待機 */
	}
}

/* Function ******************************************************************/
void ScenarioManager::Thread( )
/*****************************************************************************/
{
	if( NULL != nowScene ){
		/* シーン定期処理 */
		nowScene->Run( );
	}
}

