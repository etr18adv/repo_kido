#ifndef _SCENARIO_MANAGER_
#define _SCENARIO_MANAGER_

/** Include file *************************************************************/
#include "TouchSensor.h"

#include "BaseScene.h"
#include "BlockSortDataManager.h"
#include "WaitBody.h"
#include "LineTraceBody.h"
#include "AiAnswerBody.h"
#include "L_ParkingBody.h"
#include "R_ParkingBody.h"
#include "PositionCalculator.h"
#include "BluetoothDrv.h"

//#include "RootAnalysis.h"

#include <vector>
#include <string>

/** using宣言 ****************************************************************/
using ev3api::TouchSensor;

/* Class ******************************************************************/
class ScenarioManager
{
	public:
		/* 定期スレッド呼出し用 */
		void Thread( );

		/* コンストラクタ */
		ScenarioManager( const BlockSortDataManager*, 
						 WaitBody*, 
						 LineTraceBody*, 
						 AiAnswerBody*, 
						 L_ParkingBody*, 
						 R_ParkingBody*,
						 PositionCalculator*,
						 BluetoothDrv* );

		/* シナリオクラス定義 */
		class Scenario{
			public:
				std::string Name;
				std::vector<BaseScene*> SceneList;

				/* コンストラクタ */
				Scenario( std::string name ):Name( name ){ };
				/* シーン追加 */
				void AddScene( BaseScene* scene ){ SceneList.push_back( scene ); };
			private:
		};

		/* 選択シナリオ設定用 */
		static std::vector<std::string> GetScenarioNameList();
		void SetScenario( std::string s ){
				int i;
				for( i = 0; i < (int)scenarioList.size(); i++ ){
					if( s == scenarioList[ i ]->Name ){
						nowScenarioId = i;
					}
				}
			};
		void InitScenarioSatrt( );	/* 初期シナリオ　スタート */
		void ScenarioSatrt( );		/* 選択シナリオ　スタート */

	private:
		BaseScene* nowScene;		/* 再生シーン保持（タスク用） */
		int nowScenarioId;			/* 選択シナリオID */
		int nowSceneNo;				/* シ−ン再生番号 */

		void SceneStart( BaseScene *scene );	/* シーン再生 */

		Scenario* InitScenario;								/* 初期シーンリスト */
		std::vector<Scenario*> scenarioList;				/* 選択シナリオリスト */
		static std::vector<std::string> ScenarioNameList;	/* 選択シナリオ名一覧 */
};



#endif 
