/** Include file *************************************************************/
#include "LineTraceSceneL.h"
#include "RGB_Checker.h"

/** using宣言 ****************************************************************/

//static FILE	 *bt = NULL;
/* Function ******************************************************************/
LineTraceSceneL::LineTraceSceneL( 
	LineTraceBody* mlineTraceBody,
	PositionCalculator* mPositionCalculator ):
	LineTraceScene( mlineTraceBody, mPositionCalculator )
/*****************************************************************************/
{
	//bt = ev3_serial_open_file(EV3_SERIAL_BT);	/* ←本来はSceneの方も持っていて、戦略的な支持を出す。 */
}

/* Function ******************************************************************/
LineTraceSceneL::~LineTraceSceneL( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void LineTraceSceneL::Start( )
/*****************************************************************************/
{
	lineTraceBody->mStartRun( );
	const RGB_Checker* rgbC = lineTraceBody->GetRgb( );

	//Lコース
	//�@最初の直線70cm
	//fprintf( bt, "Part 1,\r\n" );
	lineTraceBody->LineTrace( 40, 60, 0.45, 0.00, 0.06 );
	while(positionCalculator->GetDistance() < 70){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//�A最初の直線後半
	//fprintf( bt, "Part 2,\r\n" );
	lineTraceBody->LineTrace( 60, 60, 0.55, 0.00, 0.09 );
	while(positionCalculator->GetDistance() < 240){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//�B左カーブ
	//fprintf( bt, "Part 3,\r\n" );
	lineTraceBody->LineTrace( 50, 60, 0.45, 0.00, 0.06 );
	while(positionCalculator->GetDistance() < 405){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//�C直線
	//fprintf( bt, "Part 4,\r\n" );
	lineTraceBody->LineTrace( 60, 60, 0.55, 0.00, 0.09 );
	while(positionCalculator->GetDistance() < 540){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//�C直線
	//fprintf( bt, "Part 4-2,\r\n" );
	lineTraceBody->LineTrace( 45, 60, 0.55, 0.00, 0.06 );
	while(positionCalculator->GetDistance() < 570){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//�D右カーブ
	//fprintf( bt, "Part 5,\r\n" );
	lineTraceBody->LineTrace( 30, 30, 0.45, 0.00, 0.06 );
	while(positionCalculator->GetDistance() < 650){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//�E直線
	//fprintf( bt, "Part 6,\r\n" );
	lineTraceBody->LineTrace( 50, 60, 0.45, 0.00, 0.05 );
	while(positionCalculator->GetDistance() < 680){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//�E直線
	//fprintf( bt, "Part 6,\r\n" );
	lineTraceBody->LineTrace( 40, 60, 0.45, 0.00, 0.05 );
	while(positionCalculator->GetDistance() < 700){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//�F右カーブ
	//fprintf( bt, "Part 7,\r\n" );
	lineTraceBody->LineTrace( 30, 30, 0.45, 0.00, 0.06 );
	while(positionCalculator->GetDistance() < 800){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//�G直線
	//fprintf( bt, "Part 8,\r\n" );
	lineTraceBody->LineTrace( 60, 60, 0.6, 0.00, 0.09 );
	while(positionCalculator->GetDistance() < 1000){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//�H直線　減速
	//fprintf( bt, "Part 9,\r\n" );
	lineTraceBody->LineTrace( 30, 60, 0.45, 0.00, 0.06 );
	while(positionCalculator->GetDistance() < 1020){
		tslp_tsk( 4 );
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	//10　左カーブ
	//fprintf( bt, "Part10,\r\n" );
	lineTraceBody->LineTrace( 15, 70, 0.45, 0.00, 0.06 );
	while(positionCalculator->GetDistance() < 1100){
		tslp_tsk( 4 );
	}

	lineTraceBody->Stop( );

	SetFinish( );
}

/* Function ******************************************************************/
void LineTraceSceneL::Run( )
/*****************************************************************************/
{
	SetFinish( );
}

