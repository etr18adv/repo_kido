#ifndef _LINETRACE_L_SCENE_
#define _LINETRACE_L_SCENE_

/** Include file *************************************************************/
#include "LineTraceScene.h"
#include "LineTraceBody.h"

/** using�錾 ****************************************************************/


/* Class ******************************************************************/
class LineTraceSceneL : public LineTraceScene
{
	public:
		LineTraceSceneL( LineTraceBody*, PositionCalculator* );
		~LineTraceSceneL( );

		void Run( );

	private:
		void Start( );
};

#endif 
