/** Include file *************************************************************/
#include "L_ParkingScene.h"

/** using宣言 ****************************************************************/


/* Function ******************************************************************/
L_ParkingScene::L_ParkingScene( 
	R_ParkingBody* mR_ParkingBody,
	PositionCalculator* mPositionCalculator ):
	BaseScene( ),
	r_ParkingBody( mR_ParkingBody ),
	positionCalculator( mPositionCalculator )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
L_ParkingScene::~L_ParkingScene( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void L_ParkingScene::Start( )
/*****************************************************************************/
{
	r_ParkingBody->mStartRun( );
	const RGB_Checker* rgbC = r_ParkingBody->GetRgb( );	/* RGBデータ取得用 */
	ArmControl* armD = r_ParkingBody->GetArm( );

	/* 左を向く */
	{
		double SAngle = positionCalculator->GetAngle( );
		r_ParkingBody->Free(-15, 15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( );
			if( ( BuffAngle - SAngle ) <= -45 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	Wait500ms();

	int32_t Sd = positionCalculator->GetDistance( );
	double SAngle = positionCalculator->GetAngle( );
	r_ParkingBody->KeepAngle( 15, (int)SAngle );

	armD->SetTargetAngle( 80 );
	while(1){
		int32_t Nd = positionCalculator->GetDistance( );
		if( (Nd - Sd) >= 15 ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	armD->SetTargetAngle( 30 );
	while(1){
		int32_t Nd = positionCalculator->GetDistance( );
		if( (Nd - Sd) >= 20 ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	armD->SetTargetAngle( 80 );
	while(1){
		int32_t Nd = positionCalculator->GetDistance( );
		if( (Nd - Sd) >= 25 ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	armD->SetTargetAngle( 30 );
	while(1){
		int32_t Nd = positionCalculator->GetDistance( );
		if( (Nd - Sd) >= 30 ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	armD->SetTargetAngle( 80 );
	while(1){
		int32_t Nd = positionCalculator->GetDistance( );
		if( (Nd - Sd) >= 35 ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	armD->SetTargetAngle( 30 );
	while(1){
		int32_t Nd = positionCalculator->GetDistance( );
		if( (Nd - Sd) >= 40 ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}


	armD->SetTargetAngle( 80 );
	while(1){
		int32_t Nd = positionCalculator->GetDistance( );
		if( (Nd - Sd) >= 45 ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}


	armD->SetTargetAngle( 30 );
	while(1){
		int32_t Nd = positionCalculator->GetDistance( );
		if( (Nd - Sd) >= 50 ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	armD->SetTargetAngle( 80 );
	while(1){
		int32_t Nd = positionCalculator->GetDistance( );
		if( (Nd - Sd) >= 55 ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}
	ev3_speaker_play_tone(NOTE_A5,100);

	/* 右を向く */
	{
		double SAngle = positionCalculator->GetAngle( );
		r_ParkingBody->Free(15, 0);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( );
			if( ( BuffAngle - SAngle ) >= 80 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	r_ParkingBody->Stop( );
}

/* Function ******************************************************************/
void L_ParkingScene::Run()
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void L_ParkingScene::Wait500ms( )
/*****************************************************************************/
{
	int Time = 500 / 4;

	/* 停止 */
	r_ParkingBody->Stop( );
	while(1){
		Time--;
		if(0 == Time){ break; }
	}
}
