#ifndef _L_PARKING_SCENE_
#define _L_PARKING_SCENE_

/** Include file *************************************************************/
#include "BaseScene.h"

#include "R_ParkingBody.h"
#include "PositionCalculator.h"

/** using�錾 ****************************************************************/

/* Class ******************************************************************/
class L_ParkingScene : public BaseScene
{
	public:
		L_ParkingScene( R_ParkingBody*, PositionCalculator* );
		~L_ParkingScene( );

		void Run( );

	private:
		R_ParkingBody* r_ParkingBody;
		PositionCalculator* positionCalculator;

		void Start( );
		void Wait500ms( );
};

#endif 
