/** Include file *************************************************************/
#include "AiAnswerScene.h"

/** using宣言 ****************************************************************/
//static FILE	 *bt = NULL;

/* Function ******************************************************************/
AiAnswerScene::AiAnswerScene(
	AiAnswerBody* mAiAnswerBody,
	PositionCalculator* mPositionCalculator,
	const BlockSortDataManager* mBlockSortDataManager,
	BluetoothDrv* mBluetoothDrv):
	BaseScene( ),
	aiAnswerBody( mAiAnswerBody ),
	positionCalculator( mPositionCalculator ),
	blockSortDataManager( mBlockSortDataManager ),
	bluetoothDrv( mBluetoothDrv )
/*****************************************************************************/
{
	//bt = ev3_serial_open_file(EV3_SERIAL_BT);
}

/* Function ******************************************************************/
AiAnswerScene::~AiAnswerScene( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void AiAnswerScene::Start( )
/*****************************************************************************/
{
	/* AIアンサー用走行　許可 */
	aiAnswerBody->mStartRun( );

	const RGB_Checker* rgbC = aiAnswerBody->GetRgb( );

	ev3_speaker_play_tone(NOTE_A5,100);

#if 1
	aiAnswerBody->LineTrace( );
	while(1){
		RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
		if((hsv.V > 40)&&(hsv.S > 130)&&((60 < hsv.H)&&(hsv.H < 240))){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	int D_data = DegitalDataRun( );
#else
	int D_data = 0;
#endif

	AnalogDataRun( );	/* アナログをおこなう場合 */

	ev3_speaker_play_tone(NOTE_A5,100);

	int A_data = 0;
	int LimitTime = 3000 / 4;
	while(1){
		if(bluetoothDrv->PcComeraFinishJudge()){
			A_data = blockSortDataManager->GetAiAnalogData();
			break;
		}
		LimitTime--;
		if(0 >= LimitTime){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	AnswerDousa( D_data, A_data );

	/* 停止 */
	aiAnswerBody->Stop( );

	SetFinish( );
}

/* Function ******************************************************************/
void AiAnswerScene::Run( )
/*****************************************************************************/
{
	SetFinish( );
}

/* Function ******************************************************************/
int AiAnswerScene::DegitalDataRun( )
/*****************************************************************************/
{
	const RGB_Checker* rgbC = aiAnswerBody->GetRgb( );
	int SData[4] = { 0, 0, 0, 0 };
	const int ToValue[16] = {
		1, 0, 0, 0,
		7, 0, 3, 2,
		0, 0, 4, 0,
		0, 0, 5, 6
	};

	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->KeepAngle(15, SAngle + 5);
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 2 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(hsv.V > 100){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	/* 左を向く */
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->Free(-15, 15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( );
			if( ( BuffAngle - SAngle ) <= -84 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	{
		aiAnswerBody->LineTraceBoad( );
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 12 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(hsv.S > 135){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	{
		aiAnswerBody->Free( -15, -15 );
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) <= -5 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		aiAnswerBody->Free( -7, -7 );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) <= -9 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	/* 右を向く */
	{
		double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
		aiAnswerBody->Free(15, -15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) >= 65 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		aiAnswerBody->Free(7, -7);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) >= 89 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	/*** 横 ***/
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->KeepAngle(15, SAngle);
		int32_t Sd = positionCalculator->GetDistance( );

		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if( (Nd - Sd) >= 2 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if( (Nd - Sd) >= 14 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while(1){
			/* 判定1 */
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(hsv.V < 100){ SData[0] = 1;	}
			if( (Nd - Sd) >= 26 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if( (Nd - Sd) >= 27 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);
	}
	Wait500ms();

	{
		aiAnswerBody->Free( -15, -15 );
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) <= -8 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		aiAnswerBody->Free( -8, -8 );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) <= -11 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	/* 右を向く */
	{
		double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
		aiAnswerBody->Free(15, -15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) >= 65 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		aiAnswerBody->Free(8, -8);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			//if( ( BuffAngle - SAngle ) >= 90 ){ break; }
			if( ( BuffAngle - SAngle ) >= 95 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	/*** 縦 ***/
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->KeepAngle(15, SAngle);
		int32_t Sd = positionCalculator->GetDistance( );

		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if( (Nd - Sd) >= 2 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while(1){
			/* 判定2 */
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(hsv.V < 100){ SData[1] = 1;	}
			if( (Nd - Sd) >= 12 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while(1){
			/* 判定3 */
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(hsv.V < 100){ SData[2] = 1;	}
			if( (Nd - Sd) >= 23 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if( (Nd - Sd) >= 25 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);
	}
	Wait500ms();

	{
		aiAnswerBody->Free( -15, -15 );
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) <= -8 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		aiAnswerBody->Free( -8, -8 );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) <= -11 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	/* 左を向く */
	{
		double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
		aiAnswerBody->Free(-15, 15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) <= -65 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		aiAnswerBody->Free(-8, 8);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) <= -90 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	/*** 横 ***/
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->KeepAngle(15, SAngle);
		int32_t Sd = positionCalculator->GetDistance( );

		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if( (Nd - Sd) >= 2 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while(1){
			/* 判定4 */
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(hsv.V < 100){ SData[3] = 1;	}
			if( (Nd - Sd) >= 10 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if( (Nd - Sd) >= 12 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);
	}
	Wait500ms();

	int Answer = 0;
	if(SData[0] == 1){ Answer = Answer + 1; }
	if(SData[1] == 1){ Answer = Answer + 2; }
	if(SData[2] == 1){ Answer = Answer + 4; }
	if(SData[3] == 1){ Answer = Answer + 8; }

	return ToValue[Answer];
}

/* Function ******************************************************************/
void AiAnswerScene::AnalogDataRun( )
/*****************************************************************************/
{
	const RGB_Checker* rgbC = aiAnswerBody->GetRgb( );

	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->KeepAngle(15, SAngle);

		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if((hsv.V > 40)&&(hsv.S > 100)){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}


		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 2 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(hsv.V > 100){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	/* 左を向く */
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->Free(-15, 15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( );
			if( ( BuffAngle - SAngle ) <= -84 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	{
		aiAnswerBody->LineTraceBoad( );
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 12 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(hsv.S > 135){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	/* 右を向く */
	{
		double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
		aiAnswerBody->Free(15, -15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) >= 55 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		aiAnswerBody->Free(8, -8);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) >= 75 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		aiAnswerBody->Free(4, -4);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) >= 90 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->KeepAngle(15, SAngle + 5);
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 14 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	/* 右を向く */
	{
		double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
		aiAnswerBody->Free(10, -10);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) >= 65 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		aiAnswerBody->Free(5, -5);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) >= 85 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	/* 少し後ろへ */
	{
		aiAnswerBody->Free( -7, -7 );
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			//RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			//if( (hsv.V > 50)&&(hsv.S > 135) ){ break; }
			if( (Nd - Sd) <= -7 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if( (hsv.V > 40)&&(hsv.S > 120) ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
	Wait500ms();

	{
		RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
		//if( (rgbC->GetRefrectData() <= 118) || (hsv.S >= 80) ){
		{
			ev3_speaker_play_tone(NOTE_C5,100);
			aiAnswerBody->Free( 5, 5 );

			while(1){
				hsv = rgbC->GetHSV();
				if((hsv.V > 120)&&(hsv.S < 130) ){ break; }//kidokoro 90->130
				tslp_tsk( 4 );	/* 4ms待機 */
			}

			int32_t Sd = positionCalculator->GetDistance( );
			while(1){
				int32_t Nd = positionCalculator->GetDistance( );
				if((Nd - Sd) >= 3){ break; }
				tslp_tsk( 4 );	/* 4ms待機 */
			}
		}
	}
	Wait500ms();

	aiAnswerBody->AnalogDataScan( this->RelativePotsitionId );

	while( 1 ){
		if( false == aiAnswerBody->GetBusyState() ){ break; }
		tslp_tsk( 4 );	/* 4ms待機 */
	}

	//while(1){
	//	tslp_tsk( 4 );	/* 4ms待機 */
	//}
}

/* Function ******************************************************************/
void AiAnswerScene::NoAnalog( )
/*****************************************************************************/
{
	/* 少し前へ */
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->KeepAngle(15, SAngle);
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 8 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	/* 左を向く */
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->Free(-15, 15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( );
			if( ( BuffAngle - SAngle ) <= -90 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	/* 前へ */
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->KeepAngle(15, SAngle);
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 2 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}
}

/* Function ******************************************************************/
void AiAnswerScene::AnswerDousa( int a, int b )
/*****************************************************************************/
{
	int AnswerA[3] = { 0, 0, 0 };	/* 0から回答順。上位ビットから回答 */
	int AnswerB[3] = { 0, 0, 0 };

	for( int i = 0; i < 3; i++ ){
		if(0 == (a & (0x01 << i))){
			AnswerA[2 - i] = 0;
		}else{
			AnswerA[2 - i] = 1;
		}
		if(0 == (b & (0x01 << i))){
			AnswerB[2 - i] = 0;
		}else{
			AnswerB[2 - i] = 1;
		}
	}

	/* RGB取得用ドライバ取得 */
	const RGB_Checker* rgbC = aiAnswerBody->GetRgb( );
	ArmControl* armD = aiAnswerBody->GetArm( );


	/* 解答動作 ****************************************************************/
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->KeepAngle(15, SAngle);
		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if((hsv.V > 80)&&(hsv.S<120)){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 4 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}

		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(hsv.V < 20){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	/* 左を向く */
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->Free(-15, 15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( );
			if( ( BuffAngle - SAngle ) <= -85 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	{
		aiAnswerBody->LineTrace( 0.20 );
		while( 1 ){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if(( 15 < hsv.V ) && ( 130 < hsv.S )){ break; }//kidokoro 35->15
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	/* 右を向く */
	{
		double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
		aiAnswerBody->Free(15, -15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) >= 90 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->KeepAngle(15, SAngle);
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 1 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	/* 右を向く */
	{
		double SAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
		aiAnswerBody->Free(15, -15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( this->RelativePotsitionId );
			if( ( BuffAngle - SAngle ) >= 70 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	{
		aiAnswerBody->LineTrace( 0.20 );
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			int32_t Nd = positionCalculator->GetDistance( );
			if( (Nd - Sd) >= 20 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);

		while( 1 ){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			if( 10 > hsv.V ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	/* 左を向く */
	{
		double SAngle = positionCalculator->GetAngle( );
		aiAnswerBody->Free(-15, 15);
		while(1){
			double BuffAngle = positionCalculator->GetAngle( );
			if( ( BuffAngle - SAngle ) <= -85 ){ break; }
			tslp_tsk( 4 );	/* 4ms待機 */
		}
	}

	for(int i = 0; i < 3; i++){
		{
			aiAnswerBody->LineTrace( 0.20 );
			int32_t Sd = positionCalculator->GetDistance( );
			while(1){
				RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
				int32_t Nd = positionCalculator->GetDistance( );
				if( (Nd - Sd) >= 15 ){ break;}
				tslp_tsk( 4 );	/* 4ms待機 */
			}
			ev3_speaker_play_tone(NOTE_A5,100);

			while( 1 ){
				RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
				if( 10 > hsv.V ){ break; }
				tslp_tsk( 4 );	/* 4ms待機 */
			}
		}

		/* デジタルの方 *****************************************/
		if(AnswerA[ i ]){
			/* 1の動き */
			{
				double SAngle = positionCalculator->GetAngle( );
				aiAnswerBody->Free(0, 20);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) <= -120 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				aiAnswerBody->Free(0, -20);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) >= -30 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				aiAnswerBody->Free(0, -15);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) >= 0 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
		}else{
			/* 0の動き */
			{
				double SAngle = positionCalculator->GetAngle( );
				aiAnswerBody->Free(0, -20);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) >= 150 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				aiAnswerBody->Free(0, 20);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) <= 30 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				aiAnswerBody->Free(0, 15);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) <= 0 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
		}

		/* アナログの方 *****************************************/
		if(AnswerB[ i ]){
			/* 1の動き */
			{
				armD->SetTargetAngle( 40 );
				double SAngle = positionCalculator->GetAngle( );
				aiAnswerBody->Free(20, 0);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) >= 140 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				armD->SetTargetAngle( 25 );
				aiAnswerBody->Free(-20, 0);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) <= 30 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				aiAnswerBody->Free(-15, 0);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) <= 0 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
		}else{
			/* 0の動き */
			{
				double SAngle = positionCalculator->GetAngle( );
				aiAnswerBody->Free(-20, 0);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) <= -140 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				aiAnswerBody->Free(20, 0);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) >= -30 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}

				aiAnswerBody->Free(15, 0);
				while(1){
					double BuffAngle = positionCalculator->GetAngle( );
					if( ( BuffAngle - SAngle ) >= 0 ){ break; }
					tslp_tsk( 4 );	/* 4ms待機 */
				}
			}
		}
	}

	{
		aiAnswerBody->LineTrace( 0.20 );
		int32_t Sd = positionCalculator->GetDistance( );
		while(1){
			RGB_Checker::ST_HSV hsv = rgbC->GetHSV();
			int32_t Nd = positionCalculator->GetDistance( );
			//if( (Nd - Sd) >= 15 ){ break;}
			if( (Nd - Sd) >= 40 ){ break;}
			tslp_tsk( 4 );	/* 4ms待機 */
		}
		ev3_speaker_play_tone(NOTE_A5,100);
	}
	/**************************************************************************/
}

/* Function ******************************************************************/
void AiAnswerScene::Wait500ms( )
/*****************************************************************************/
{
	int Time = 500 / 4;

	/* 停止 */
	aiAnswerBody->Stop( );
	while(1){
		Time--;
		if(0 == Time){ break; }
	}
}

