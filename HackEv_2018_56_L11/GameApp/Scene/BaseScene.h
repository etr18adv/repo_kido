#ifndef _BASE_SCENE_
#define _BASE_SCENE_

/** Include file *************************************************************/

/** using�錾 ****************************************************************/

/* Class ******************************************************************/
class BaseScene
{
	public:
		BaseScene( ):fl( false ){ };
		virtual ~BaseScene( ){ };

		void SceneStart( ){ fl = false; Start( ); };
		virtual void Run( ) = 0;

		bool Finished( ){ return( fl ); };

	private:
		virtual void Start( ) = 0;
		bool fl;

	protected:
		void SetFinish( ){ fl = true; };

};

#endif 
