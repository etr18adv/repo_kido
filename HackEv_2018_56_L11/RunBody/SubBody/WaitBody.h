#ifndef _WAIT_BODY_H_
#define _WAIT_BODY_H_

/** Include file *************************************************************/
#include "BodyBase.h"

/** using�錾 ****************************************************************/

/* Class ******************************************************************/

class WaitBody : public BodyBase {
public:
	WaitBody();
	~WaitBody();

private:
	void Start();
	void Run(Motor&, Motor&, ArmControl*, RGB_Checker*);
};

#endif 
