#ifndef _AI_ANSWER_BODY_H_
#define _AI_ANSWER_BODY_H_

/** Include file *************************************************************/
#include "BodyBase.h"
#include "PositionCalculator.h"

#define X_MAX	30		/*  */
#define Y_MAX	40		/*  */

/** using宣言 ****************************************************************/

/* Class ******************************************************************/
class AiAnswerBody : public BodyBase {
public:
	AiAnswerBody(PositionCalculator*);
	~AiAnswerBody();

	unsigned char GetDagitalData(){ return DagitalData; };

private:
	enum Evt {
		Evt_Wait = 0,
		Evt_LineTrace,
		Evt_LineTracePid,
		Evt_LineTraceBoad,
		Evt_GoToPosition,
		Evt_GotoAngleLeft,
		Evt_GotoAngleRight,
		Evt_RelativeCheck,
		//Evt_DigitalDataScan,
		Evt_AnalogDataScan,
		Evt_KeepAngle,
		Evt_Free
	};
	Evt evt;

	static int TxData[X_MAX][Y_MAX];
	PositionCalculator* positionCalculator;

	unsigned char DagitalData = 0x00;
	int Speed;
	int Angle;
	int Distance;

	void Start();
	void Run(Motor&, Motor&, ArmControl*, RGB_Checker*);

	void WheelControl(int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel);
	int PID_feedback( float x, float Kp, float Ki, float Kd, float dt );

/* イベント定義 **********************************************************/
	/* Stop ************************************************/
	public:
		void Stop();
	private:
	/*******************************************************/

	/* LineTrace *******************************************/
	public:
		void LineTrace();
		void LineTrace(float p);
		void LineTraceBoad();
	private:
		float p;
	/*******************************************************/

	/* RelativeCheck ***************************************/
	public:
		void RelativeCheck();
	private:
	/*******************************************************/

	/* GoToPosition ****************************************/
	public:
		void GoToPosition( int speed, int x, int y, int id );
	private:
		int PositionSpeed = 50;
		int TargetPositionX = 0;
		int TargetPositionY = 0;
		int RelativeId;
	/*******************************************************/

	/* GoToAngle *******************************************/
	public:
		void GotoAngleLeft( int speed, double angle );
		void GotoAngleRight( int speed, double angle );
	private:
		int AngleSpeed = 20;
		int TargetAngle = 0;
	/*******************************************************/

	/* AnalogDataScan *************************************/
	public:
		void AnalogDataScan(int id);
	private:
		void analogDataScanFuncetion(Motor& m_leftWheel, Motor& m_rightWheel, RGB_Checker* m_rgb_Checker, int id);
		int AScanState = 0;
	/*******************************************************/

	/* 角度を保って直進 */
	public:
		void KeepAngle( int speed, int angle );

	/* Freet *フリー(検討用)********************************/
	public:
		void Free( int mLeftPower, int mRightPower );
	private:
		int LeftPower = 0;
		int RightPower = 0;
	/*******************************************************/

};

#endif 
