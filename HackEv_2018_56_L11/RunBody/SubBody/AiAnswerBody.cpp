/** Include file *************************************************************/
#include "AiAnswerBody.h"

/** using宣言 ****************************************************************/

static FILE	 *bt = NULL;
int AiAnswerBody::TxData[X_MAX][Y_MAX];

/* Function ******************************************************************/
AiAnswerBody::AiAnswerBody(
	/** 引数 **/
		PositionCalculator* mpositionCalculator ):
	/** 初期化 **/
		BodyBase( ),
		evt( Evt::Evt_Wait ),
		positionCalculator( mpositionCalculator )
/*****************************************************************************/
{
	bt = ev3_serial_open_file(EV3_SERIAL_BT);	/* ←本来はSceneの方も持っていて、戦略的な支持を出す。 */
	for( int i = 0; i < X_MAX; i++ ){
		for( int j = 0; j < Y_MAX; j++){
			TxData[i][j] = 255;
		}
	}
}

/* Function ******************************************************************/
AiAnswerBody::~AiAnswerBody( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void AiAnswerBody::Start( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void AiAnswerBody::Run( Motor& m_leftWheel, Motor& m_rightWheel, ArmControl* m_armControl, RGB_Checker* m_rgb_Checker )
/*****************************************************************************/
{
	switch( this->evt ){
		/* 待機 */
		case Evt::Evt_Wait:
			m_leftWheel.setPWM( 0 );
			m_rightWheel.setPWM( 0 );
			break;

		/* ライントレース */
		case Evt::Evt_LineTrace:
			WheelControl(
				15,
				(float)( PID_feedback( (m_rgb_Checker->GetRefrectData( ) - 60), 0.45F, 0.00F, 0.04F, 0.004F ) ),
				m_leftWheel, m_rightWheel );
			break;

		/* ライントレース */
		case Evt::Evt_LineTracePid:
			WheelControl(
				15,
				(float)( PID_feedback( (m_rgb_Checker->GetRefrectData( ) - 60), p, 0.00F, 0.04F, 0.004F ) ),
				m_leftWheel, m_rightWheel );
			break;

		/* ライントレース */
		case Evt::Evt_LineTraceBoad:
			WheelControl(
				10,
				//(float)( PID_feedback( (60 - m_rgb_Checker->GetRefrectData( )), 0.45F, 0.00F, 0.04F, 0.004F ) ),
				(float)( PID_feedback( (60 - m_rgb_Checker->GetRefrectData( )), 0.45F, 0.00F, 0.02F, 0.004F ) ),
				m_leftWheel, m_rightWheel );
			break;

		/* 指定座標へ移動 */
		case Evt_GoToPosition:
			{
				double tA = positionCalculator->CalculateAngle( this->TargetPositionX, this->TargetPositionY, this->RelativeId );
				double dA = tA - positionCalculator->GetAngle( this->RelativeId );
				if( 90.0 < dA ){
					dA = 90.0;
				}else if( -90.0 > dA ){
					dA = -90.0;
				}else{

				}
				WheelControl( this->PositionSpeed, dA, m_leftWheel, m_rightWheel );
			}
			break;

		/* 指定角度まで右回り方向転換 */
		case Evt_GotoAngleRight:
			{
				if( true == this->Busy ){
					double angle = positionCalculator->GetAngle( this->RelativeId );
					if( this->TargetAngle > angle ){
						WheelControl( 0, this->AngleSpeed, m_leftWheel, m_rightWheel );
					}else{
						this->Busy = false;
						m_leftWheel.setPWM( 0 );
						m_rightWheel.setPWM( 0 );
					}
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
				}
			}
			break;

		/* 指定角度まで左回り方向転換 */
		case Evt_GotoAngleLeft:
			{
				if( true == this->Busy ){
					double angle = positionCalculator->GetAngle( this->RelativeId );
					if( this->TargetAngle < angle ){
						WheelControl( 0, -this->AngleSpeed, m_leftWheel, m_rightWheel );
					}else{
						this->Busy = false;
						m_leftWheel.setPWM( 0 );
						m_rightWheel.setPWM( 0 );
					}
				}else{
					m_leftWheel.setPWM( 0 );
					m_rightWheel.setPWM( 0 );
				}
			}
			break;

		/* バック */
		case Evt_RelativeCheck:
			WheelControl(
				-10,
				(float)( PID_feedback( -(m_rgb_Checker->GetRefrectData( ) - 60), 0.70F, 0.00F, 0.07F, 0.004F ) ),
				m_leftWheel, m_rightWheel );
			break;

		case Evt_AnalogDataScan:
			analogDataScanFuncetion( m_leftWheel, m_rightWheel, m_rgb_Checker, this->RelativeId);
			break;

		case Evt::Evt_KeepAngle:
			{
				double dA = this->Angle - positionCalculator->GetAngle( );
				WheelControl( this->Speed, dA, m_leftWheel, m_rightWheel );
			}
			break;

		/* フリー(検討用) */
		case Evt::Evt_Free:
			m_leftWheel.setPWM( this->LeftPower );
			m_rightWheel.setPWM( this->RightPower );
			break;

		default:

			break;
	}

}

/* Function ******************************************************************/
int AiAnswerBody::PID_feedback( float x, float Kp, float Ki, float Kd, float dt )
/*****************************************************************************/
{
	static float p = 0, i = 0, d = 0, integral = 0;
	static int prev_x = 0;

	p         = Kp * x;
	integral += ( prev_x + x ) / 2.0F * dt;
	d         = Kd * ( x - prev_x ) / dt;
	i         = Ki * integral;

	prev_x = x;

	return((int)( p + i + d ));
}

/* Function ******************************************************************/
void AiAnswerBody::WheelControl( int front, float turn, Motor& m_leftWheel, Motor& m_rightWheel )
/*****************************************************************************/
{
	/* Turnが大きいほど右に曲がる */
	m_leftWheel.setPWM( (int)( front + ( turn * 0.50F ) ) );
	m_rightWheel.setPWM( (int)( front - ( turn * 0.50F ) ) );
}


/* Function ******************************************************************/
void AiAnswerBody::Stop( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Wait;
}

/* Function ******************************************************************/
void AiAnswerBody::LineTrace( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_LineTrace;
}

/* Function ******************************************************************/
void AiAnswerBody::LineTrace( float mp)
/*****************************************************************************/
{
	this->evt = Evt::Evt_LineTracePid;
	this->p = mp;
}

/* Function ******************************************************************/
void AiAnswerBody::LineTraceBoad()
/*****************************************************************************/
{
	this->evt = Evt::Evt_LineTraceBoad;
}

/* Function ******************************************************************/
void AiAnswerBody::RelativeCheck( )
/*****************************************************************************/
{
	this->evt = Evt::Evt_RelativeCheck;
}

/* Function ******************************************************************/
void AiAnswerBody::GotoAngleLeft( int speed, double angle )
/*****************************************************************************/
{
	this->evt = Evt::Evt_GotoAngleLeft;
	this->AngleSpeed  = speed;
	this->TargetAngle = angle;
	this->Busy = true;
}

/* Function ******************************************************************/
void AiAnswerBody::GotoAngleRight( int speed, double angle )
/*****************************************************************************/
{
	this->evt = Evt::Evt_GotoAngleRight;
	this->AngleSpeed  = speed;
	this->TargetAngle = angle;
	this->Busy = true;
}

/* Function ******************************************************************/
void AiAnswerBody::GoToPosition( int speed, int x, int y, int id )
/*****************************************************************************/
{
	this->evt = Evt::Evt_GoToPosition;
	this->PositionSpeed = speed;
	this->TargetPositionX = x;
	this->TargetPositionY = y;
	this->RelativeId = id;
}

/* Function ******************************************************************/
void AiAnswerBody::AnalogDataScan(int id)
/*****************************************************************************/
{
	this->evt = Evt::Evt_AnalogDataScan;
	this->AScanState = 0;
	this->RelativeId = id;
	this->Busy = true;
}

/* Function ******************************************************************/
void AiAnswerBody::KeepAngle( int speed, int angle )
/*****************************************************************************/
{
	this->evt = Evt::Evt_KeepAngle;
	this->Speed = speed;
	this->Angle = angle;
}

/* Function ******************************************************************/
void AiAnswerBody::Free( int mLeftPower, int mRightPower )
/*****************************************************************************/
{
	this->evt = Evt::Evt_Free;
	this->LeftPower  = mLeftPower;
	this->RightPower = mRightPower;
}

/* Function ******************************************************************/
void AiAnswerBody::analogDataScanFuncetion(Motor& m_leftWheel, Motor& m_rightWheel, RGB_Checker* m_rgb_Checker, int id)
/*****************************************************************************/
{
	static int AiAnalogState = 0;
	static int32_t StartAnalogDistance = 0;
	static int AnalogScanId = 0;
	static int StartId = 0;

	RGB_Checker::ST_HSV hsv = m_rgb_Checker->GetHSV( );
	int LineData = m_rgb_Checker->GetRefrectData( );

	switch( AiAnalogState ){
		case 0:
			m_leftWheel.setPWM( 5 );
			m_rightWheel.setPWM( 5 );
			if( LineData <= 95 ){
				StartAnalogDistance = positionCalculator->GetDistance( );
				AiAnalogState = 1;
				AnalogScanId = positionCalculator->CreateIdDirect();
			}
			break;

		case 1:
			//半分読取りでいのではないか？
			//読取り開始してから(最小のy)がプラス25を一度でも、かつ向きが90を超えたとき
			{
				static int m_state = 0;
				static double StartAngle = 0;
				switch( m_state ){
					case 0:
						{
							//int turn = (float)( PID_feedback( (90 - LineData), 0.07, 0, 0.5, 0.004F ) );
							int turn = (82 - LineData) * 1.0;
							if( turn < -15 ){ turn = -15; }
							if( 15 < turn ){ turn = 15; }
							WheelControl( 10, turn, m_leftWheel, m_rightWheel );
							if( LineData <  65 ){
								StartAngle = positionCalculator->GetAngle( id );
								m_state = 10;
							}
							//if( 115 < LineData ){
							if( 100 < LineData ){
								StartAngle = positionCalculator->GetAngle( id );
								m_state = 20;
							}
						}
						break;

					case 10:
						{
							double angle = positionCalculator->GetAngle( id );
							//if( 20 < ( angle - StartAngle ) ){
								if( 75 <= LineData ){
									m_state = 0;
								}
							//}
							m_leftWheel.setPWM( 10 );
							m_rightWheel.setPWM( -15 );
						}
						break;

					case 20:
						{
							double angle = positionCalculator->GetAngle( id );
							//if( -20 > ( angle - StartAngle ) ){
								if( 93 >= LineData ){
									m_state = 0;
								}
							//}
							m_leftWheel.setPWM( -15 );
							m_rightWheel.setPWM( 10 );
						}
						break;

					default:

						break;
				}
				{
					int32_t analogDistance = positionCalculator->GetDistance( );
					if( 45 < ( analogDistance - StartAnalogDistance ) ){
						AiAnalogState = 2;
					}
				}
				{
					static int TestState = 0;
					int32_t analogDistance = positionCalculator->GetDistance( );
					if( TestState < ( ( analogDistance - StartAnalogDistance ) / 5 ) ){
						ev3_speaker_play_tone(NOTE_A5,100);
						TestState++;
					}
				}

				{
					struct PositionCalculator::Position pos = positionCalculator->GetPosition( AnalogScanId );
					int px = (int)pos.X + 15;
					int py = (int)pos.Y + 4;
					if( ( px >= 0 ) && ( px < X_MAX ) && ( py >= 0 ) && ( py < Y_MAX )){
						if( LineData < TxData[ px ][ py ] ){
							TxData[ px ][ py ] = LineData;
						}
					}
				}
			}
			break;

		case 2:
			{
				static int mTxState = 0;
				static int tx_x = 0;		/* 送信座標 X */
				static int tx_y = 0;		/* 送信座標 Y */
				switch(mTxState){
					case 0:
						fprintf( bt, "Start,\r\n" );
						mTxState = 1;
						break;

					case 1:
						fprintf(
							bt,
							"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,\r\n",
							TxData[ tx_x ][ tx_y + 0 ],
							TxData[ tx_x ][ tx_y + 1 ],
							TxData[ tx_x ][ tx_y + 2 ],
							TxData[ tx_x ][ tx_y + 3 ],
							TxData[ tx_x ][ tx_y + 4 ],
							TxData[ tx_x ][ tx_y + 5 ],
							TxData[ tx_x ][ tx_y + 6 ],
							TxData[ tx_x ][ tx_y + 7 ],
							TxData[ tx_x ][ tx_y + 8 ],
							TxData[ tx_x ][ tx_y + 9 ]
							);
						tx_y += 10;
						if( Y_MAX <= tx_y ){
							tx_y = 0;
							tx_x++;
							if( X_MAX <= tx_x ){
								tx_x = 0;
								mTxState = 2;
							}
						}
						break;

					case 2:
						fprintf( bt, "End,\r\n" );
						ev3_speaker_play_tone(NOTE_C5,100);
						mTxState = 3;
						AiAnalogState = 4;
						break;

					default:

						break;
				}
			}
			m_leftWheel.setPWM( 0 );
			m_rightWheel.setPWM( 0 );
			break;

		case 3:
			{
				double tA = positionCalculator->CalculateAngle( -2, -1, AnalogScanId );
				double dA = tA - positionCalculator->GetAngle( AnalogScanId );
				if( 90.0 < dA ){
					dA = 90.0;
				}else if( -90.0 > dA ){
					dA = -90.0;
				}else{

				}
				WheelControl( 10, dA, m_leftWheel, m_rightWheel );

				if(positionCalculator->RangeJuge( -2, -1, 2, AnalogScanId )){
					ev3_speaker_play_tone(NOTE_A5,100);
					AiAnalogState = 4;
				}
			}
			break;

		case 4:
			{
				double tA = positionCalculator->CalculateAngle( -1, -12, AnalogScanId );
				double dA = tA - positionCalculator->GetAngle( AnalogScanId );
				if( 90.0 < dA ){
					dA = 90.0;
				}else if( -90.0 > dA ){
					dA = -90.0;
				}else{

				}
				WheelControl( 10, dA, m_leftWheel, m_rightWheel );

				if(positionCalculator->RangeJuge( -1, -12, 2, AnalogScanId )){
					ev3_speaker_play_tone(NOTE_A5,100);
					AiAnalogState = 5;
					this->Busy = false;
				}
			}
			break;



		default:
			m_leftWheel.setPWM( 0 );
			m_rightWheel.setPWM( 0 );
			break;
	}

}

