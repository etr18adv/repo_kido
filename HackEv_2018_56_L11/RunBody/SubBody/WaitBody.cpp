/** Include file *************************************************************/
#include "WaitBody.h"

/** using宣言 ****************************************************************/

/* Function ******************************************************************/
WaitBody::WaitBody(
	/** 引数 **/
		):
	/** 初期化 **/
		BodyBase( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
WaitBody::~WaitBody( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void WaitBody::Start( )
/*****************************************************************************/
{

}

/* Function ******************************************************************/
void WaitBody::Run(Motor& m_leftWheel, Motor& m_rightWheel, ArmControl* m_armControl, RGB_Checker* m_rgb_Checker)
	/*****************************************************************************/
{

}
