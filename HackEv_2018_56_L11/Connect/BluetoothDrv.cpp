
/** Include file *************************************************************/
#include "BluetoothDrv.h"

/* Function ******************************************************************/
BluetoothDrv::BluetoothDrv( BlockSortDataManager* mBlockSortDataManager ):
	PcFinish(false),
	RxState(BluetoothDrv::RWait),
	ReceveCount(0),
	ReceveFinish(false),
	KaState(BluetoothDrv::KWait),
	blockSortDataManager(mBlockSortDataManager)
/*****************************************************************************/
{
	this->bt = NULL;
}

/* Function ******************************************************************/
void BluetoothDrv::Thread(  )
/*****************************************************************************/
{
	if( ReceveFinish ){
		blockSortDataManager->SetAiAnalogData( RxColorData[3] - '0' );
		ReceveFinish = false;
		PcFinish = true;
	}
}

/* Function ******************************************************************/
void BluetoothDrv::ReceveTask(  )
/*****************************************************************************/
{
	static bool f = false;

	/* AcRGBYE */
	while(1){
		if( ev3_bluetooth_is_connected( ) ){	/* 追加 */
			if( NULL == this->bt ){
				this->bt = ev3_serial_open_file( EV3_SERIAL_BT );
				f = true;
			}
		}else{
			this->bt = NULL;
			if( f ){
				ev3_led_set_color( LED_RED );
			}
		}
		
		if( NULL != this->bt ){
			uint8_t mu8_RxData = fgetc(this->bt);

			if( mu8_RxData == 'A' ){
				RxState = BluetoothDrv::RStart;
				ev3_speaker_play_tone(NOTE_G5,100);	
			}else{
				switch(RxState){
					case BluetoothDrv::RWait:
						/* 特になし */
						break;
		
					case BluetoothDrv::RStart:
						if( mu8_RxData == 'c' ){
							ev3_speaker_play_tone(NOTE_G5,100);	
							RxState = BluetoothDrv::RDataReceve;
							ReceveFinish = false;
							ReceveCount = 0;
						}
						break;
		
					case BluetoothDrv::RDataReceve:
						if(( mu8_RxData >= '0' )&&( mu8_RxData <= '9' )){
							ev3_speaker_play_tone(NOTE_G5,100);	
							ReceveData[ ReceveCount ] = mu8_RxData;

							ReceveCount++;
							if(4 <= ReceveCount){
								RxState = BluetoothDrv::RFinCheackm;
							}
						}else{
							RxState = BluetoothDrv::RWait;						
						}
						break;
		
					case BluetoothDrv::RFinCheackm:
						if( mu8_RxData == 'E' ){
							ev3_speaker_play_tone(NOTE_G5,100);	
							for(int i = 0; i < 4; i++){
								RxColorData[i] = ReceveData[i];
							}
							ReceveFinish = true;
						}
						RxState = BluetoothDrv::RWait;						
						break;
		
					default:
						RxState = BluetoothDrv::RWait;
						break;
				}
			}
		}
		tslp_tsk( 10 );
	}
}

/* Function ******************************************************************/
void BluetoothDrv::StartPcComera(  )
/*****************************************************************************/
{
	PcFinish = false;
	KaState = BluetoothDrv::KStart;
}

/* Function ******************************************************************/
bool BluetoothDrv::PcComeraFinishJudge(  )
/*****************************************************************************/
{
	return( PcFinish );
}

