#ifndef _GUI_PANEL_TEST_2_H_
#define _GUI_PANEL_TEST_2_H_

/** Include file *************************************************************/
#include "GuiPanelBase.h"
#include "GuiButton.h"
#include <string>
/** using宣言 ****************************************************************/


/** Class ********************************************************************/
namespace GUI{
	class GuiPanelTest2 : public GuiPanelBase{
		public:
			/* 定期スレッド呼出し用 */
			bool BtEvt( GuiButton* );
			/* コンストラクタ */
    		GuiPanelTest2( );

			void Load( );
			void StartSet( );
			void Refresh( );

		private:

	};
}

#endif  // EV3_APP_LINETRACER_H_
