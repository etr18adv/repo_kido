
/** Include file *************************************************************/
#include "ev3api.h"
#include "Gui.h"

//#include "GuiPanelTest1.h"
//#include "GuiPanelTest2.h"
#include "GuiPanelBlockSortSetting.h"

#include "ScenarioManager.h"

namespace GUI{
	Gui* Gui::gui = NULL;

	/* Function ******************************************************************/
	Gui::Gui( 
		/** 引数 **/
			ScenarioManager* mScenarioManager,
			BlockSortDataManager* mblockSortDataManager):
		/** 初期化 **/
			CursolEnable( true ),
			scenarioManager( mScenarioManager ),
			blockSortDataManager( mblockSortDataManager ),
			state( State::LcdInit ),
			SettingFixFl( false ),
			selectPanelNo( 0 ),
			selectScenarioNo( 0 ),
			curSol( CurSol::SenarioSelect )
	/*****************************************************************************/
	{
		ev3_lcd_set_font( EV3_FONT_MEDIUM );

		guiButton = new GuiButton();

		panelList.push_back( new GuiPanelBlockSortSetting( mblockSortDataManager ) );
		//panelList.push_back( new GuiPanelTest1() );
		//panelList.push_back( new GuiPanelTest2() );
	}

	/* Function ******************************************************************/
	Gui::~Gui( )
	/*****************************************************************************/
	{
		delete guiButton;

		{
			int i;
			for( i = 0; i < (int)panelList.size(); i++ ){
				delete panelList[ i ];
			}
		}
	}

	/* Function ******************************************************************/
	void Gui::Thread( )
	/*****************************************************************************/
	{
		guiButton->Thread( );

		/* パネルイベント処理 */
		switch( state ){
			case State::LcdInit:
				
				break;

			case State::InitialSetting:
				SettingPanelEvt( );
				break;

			default:

				break;
		}
	}

	/* Function ******************************************************************/
	void Gui::Refresh( )
	/*****************************************************************************/
	{
		/* サイズは(0,0)〜(177,127) */
		//ev3_speaker_play_tone(NOTE_A5,100);
		ev3_lcd_fill_rect(0, 0, 178, 127, EV3_LCD_WHITE);

		switch( state ){
			case State::LcdInit:
				
				break;

			case State::Opening:
				OpeningPanelRefresh( );
				break;

			case State::InitialSetting:
				SettingPanelRefresh( );
				break;

			case State::Ready:
				RaadyPanelRefresh( );
				break;

			default:

				break;
		}
	}

	/* Function ******************************************************************/
	void Gui::OpeningDisplay( )
	/*****************************************************************************/
	{
		state = State::Opening;

		/* 表示更新 */
		this->Refresh( );
	}

	/* Function ******************************************************************/
	void Gui::SettingDisplay( )
	/*****************************************************************************/
	{
		state = State::InitialSetting;

		/* 表示更新 */
		this->Refresh( );
	}

	/* Function ******************************************************************/
	void Gui::ReadyDisplay( )
	/*****************************************************************************/
	{
		state = State::Ready;

		/* 表示更新 */
		this->Refresh( );
	}

	/* Function ******************************************************************/
	bool Gui::GetSttingFixState()
	/*****************************************************************************/
	{
		return( SettingFixFl );
	}

	/* Function ******************************************************************/
	void Gui::OpeningPanelRefresh( )
	/*****************************************************************************/
	{
		std::string title1 = "product  by";
		std::string title2 = "[PROJECT-K]";
		ev3_lcd_draw_string( title1.c_str(), 30, 18 * 4 + 2 );
		ev3_lcd_draw_string( title2.c_str(), 30, 18 * 5 + 2 );

	}

	/* Function ******************************************************************/
	void Gui::RaadyPanelRefresh()
	/*****************************************************************************/
	{
		std::string title = "Touch";
		ev3_lcd_draw_string( title.c_str(), 30, 18 * 0 + 2 );
	}

	/* Function ******************************************************************/
	void Gui::SettingPanelEvt()
	/*****************************************************************************/
	{
		if( CursolEnable ){
			{
				GuiButton::ButtonSelect evtBt = guiButton->GetButtonEvt( );
				switch( curSol ){
					case CurSol::PanelSelect:
						switch(evtBt){
							case GuiButton::ButtonSelect::BtRight:
								if( ( (int)panelList.size() - 1 ) == selectPanelNo ){
									selectPanelNo = 0;
								}else{
									++selectPanelNo;
								}
								panelList[ selectPanelNo ]->StartDisplay( );
								Refresh( );
								break;

							case GuiButton::ButtonSelect::BtLeft:
								if( 0 == selectPanelNo ){
									selectPanelNo = ( (int)panelList.size() - 1 );
								}else{
										--selectPanelNo;
								}
								panelList[ selectPanelNo ]->StartDisplay( );
								Refresh( );
								break;

							case GuiButton::ButtonSelect::BtUp:
								curSol = CurSol::SenarioSelect;
								Refresh( );
								break;

							case GuiButton::ButtonSelect::BtDown:
								CursolEnable = false;
								panelList[ selectPanelNo ]->StartOperation( );
								Refresh( );
								break;

							default:
								
								break;
						}
						break;
		
					case CurSol::SenarioSelect:
						{
							int scenarioMax = (int)ScenarioManager::GetScenarioNameList( ).size();
							//int scenarioMax = 3;
							switch(evtBt){
								case GuiButton::ButtonSelect::BtRight:
									if( ( scenarioMax - 1 ) == selectScenarioNo ){
										selectScenarioNo = 0;
									}else{
										++selectScenarioNo;
									}
									Refresh( );
									break;

								case GuiButton::ButtonSelect::BtLeft:
									if( 0 == selectScenarioNo ){
										selectScenarioNo = ( scenarioMax - 1 );
									}else{
										--selectScenarioNo;
									}
									Refresh( );
									break;

								case GuiButton::ButtonSelect::BtUp:
									//CursolEnable = false;
									//panelList[ selectPanelNo ]->StartOperation( );
									//Refresh( );
									break;

								case GuiButton::ButtonSelect::BtDown:
									curSol = CurSol::PanelSelect;
									Refresh( );
									break;

								case GuiButton::ButtonSelect::BtEnter:
									{
										std::vector<std::string> sm_NameList = ScenarioManager::GetScenarioNameList( );
										scenarioManager->SetScenario( sm_NameList[ selectScenarioNo ] );
										SettingFixFl = true;
									}
									break;

								default:
									
									break;
							}
						}
						break;

					default:

						break;
				}
			}
		}else{
			/* パネルにカーソル権を渡す */
			if( panelList[ selectPanelNo ]->BtEvt( guiButton ) ){
				if( panelList[ selectPanelNo ]->GetCursolRightReturn( ) ){
					CursolEnable = true;
				}
				Refresh( );
			}
		}
	}

	/* Function ******************************************************************/
	void Gui::SettingPanelRefresh()
	/*****************************************************************************/
	{
		/* ベース表示 */
		ev3_lcd_draw_line(0, 0, 0, 36);
		ev3_lcd_draw_line(0, 0, 177, 0);
		ev3_lcd_draw_line(177, 0, 177, 36);
		ev3_lcd_draw_line(0, 18, 177, 18);
		ev3_lcd_draw_line(0, 36, 177, 36);

		if( CursolEnable ){
			switch( curSol ){
				case CurSol::SenarioSelect:
					{
						std::string moji = "<<             >>";
						ev3_lcd_draw_string( moji.c_str(), 2, 18 * 0 + 2 );
					}
					break;
				case CurSol::PanelSelect:
					{
						std::string moji = "<<             >>";
						ev3_lcd_draw_string( moji.c_str(), 2, 18 * 1 + 2 );
					}
					break;
				default:
					;
					break;
			}
		}

		/*  */
		{
			std::vector<std::string> sm_NameList = ScenarioManager::GetScenarioNameList( );
			ev3_lcd_draw_string( sm_NameList[ selectScenarioNo ].c_str(), 30, 18 * 0 + 2 );
		}
		ev3_lcd_draw_string( panelList[ selectPanelNo ]->Name.c_str(), 30, 18 * 1 + 2 );

		/* パネル表示 */
		panelList[ selectPanelNo ]->Refresh();
	}
}
