/******************************************************************************
 *  LineTracer.h (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Definition of the Class LineTracer
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#ifndef EV3_APP_LINETRACER_H_
#define EV3_APP_LINETRACER_H_

#include "LineMonitor.h"
#include "Motor.h"

#include "SimpleTimer.h"


#define SPEED_LOW 30
#define SPEED_NORMAL 50
#define SPEED_HIGH 70

class LineTracer {
public:
    LineTracer(const LineMonitor* lineMonitor,
    		ev3api::Motor& leftWheel, /*HackEV用に修正*/
            ev3api::Motor& rightWheel,/*HackEV用に修正*/
			 SimpleTimer* simpleTimer); /*タイマを内部で持つ用に修正*/

    bool run(int time);

private:
    const LineMonitor* mLineMonitor;
    bool mIsInitialized;
    /*HackEV用に修正*/
    ev3api::Motor& mLeftWheel;
    ev3api::Motor& mRightWheel;
    int mForward;
    int mTurn;

    	/*タイマーを中で持つ場合の宣言*/
    SimpleTimer* mSimpleTimer;

    int calcDirection(bool isOnLine);
};

#endif  // EV3_APP_LINETRACER_H_
