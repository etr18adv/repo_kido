/*
 * HackEV.cpp
 *
 *  Created on: 2018/04/08
 *      Author: masah
 */

#include <stdlib.h>
#include "Clock.h"

#include "HackEV.h"

HackEV::HackEV(LineTracer* lineTracer,
        Spinning* spinning,
        const Starter* starter,
        SimpleTimer* simpleTimer,
		TailChange* tailChange)
		: mLineTracer(lineTracer),
	      mSpinning(spinning),
	      mStarter(starter),
	      mSimpleTimer(simpleTimer),
		  mTailChange(tailChange),
	      mState(UNDEFINED)  {
	// TODO 自動生成されたコンストラクター・スタブ

	ev3api::Clock* clock = new ev3api::Clock();
    srand(clock->now());  // 乱数をリセットする
	delete clock;

}

HackEV::~HackEV() {
	// TODO Auto-generated destructor stub
}

/*デモ走行する*/
void HackEV::run() {
    switch (mState) {
    case UNDEFINED:
        execUndefined();
        break;
    case WAITING_FOR_START:
        execWaitingForStart();
        break;
    case LINE_TRACING:
        execLineTracing();
        break;
    case SPINNING:
        execSpinning();
        break;
    case TAIL_CHANGING:
       char s[32];
    	   sprintf(s,"%s","TAIL CHANGING 1RUNNN");
    	   ev3_lcd_draw_string(s,0,0);

    	   mTailChange->run();//尻尾テスト
    	    sprintf(s,"%s","TAIL CHANGING 3");
    	    ev3_lcd_draw_string(s,0,100);
    		break;
    default:
        break;
    }
}

/**
 * 状態を切り替える
 */
void HackEV::modeChangeAction(State st) {
	char s[32];

	mState = st;

	/* 状態をLCDに表示する */
	ev3_lcd_set_font(EV3_FONT_MEDIUM);	//フォントサイズ設定
	if(st == LINE_TRACING)
			sprintf(s,"%s", "LINE_TRACING         ");
	else if(st==SPINNING)
			sprintf(s,"%s", "SPINNING                 ");
	else if(st==UNDEFINED)
			sprintf(s,"%s", "UNDEFINNED            ");
	else if(st==WAITING_FOR_START)
			sprintf(s,"%s", "WAITING FOR START");
	else if(st==TAIL_CHANGING)
		sprintf(s,"%s","TAIL CHANGING");
    ev3_lcd_draw_string(s,0,0);

    /*尻尾テスト*/

    /*アームテスト*/

}

/**
 * 未定義状態の処理
 */
void HackEV::execUndefined() {
    modeChangeAction(WAITING_FOR_START);
}

/**
 * 開始待ち状態の処理
 */
void HackEV::execWaitingForStart() {
//	tslp_tsk(10); /* 10msecウェイト →不要。main_taskはsleep中、tracer_taskが処理を終われば勝手にbt_taskになる*/
    if (this->bt_cmd==1) {
        modeChangeAction(LINE_TRACING);
    }
    else if(mStarter->isPushed() || this->bt_cmd==2){	// 尻尾動作テスト
    		modeChangeAction(TAIL_CHANGING);
    }
}

/**
 * ライントレース状態の処理
 */
void HackEV::execLineTracing() {
    if(mLineTracer->run(11500)){
        modeChangeAction(SPINNING);
    }
}

/**
 * スピン中の処理
 */
void HackEV::execSpinning() {
    if(mSpinning->run(1300)){
        modeChangeAction(LINE_TRACING);
    }
}
