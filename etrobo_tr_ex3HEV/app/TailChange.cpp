/*
 * TailChange.cpp
 *
 *  Created on: 2018/05/06
 *      Author: Masahiro
 */

#include "TailChange.h"

TailChange::TailChange(ev3api::Motor& tailMotor)
		:mTailMotor(tailMotor),mIsInitialized(false) {
	// TODO 自動生成されたコンストラクター・スタブ
}


TailChange::~TailChange() {
	// TODO Auto-generated destructor stub
}

void TailChange::run(){
	char s[32];

	sprintf(s,"%s","TAIL CHANGING 2-1");
	ev3_lcd_draw_string(s,0,20);
//	ev3_motor_set_power(EV3_PORT_D,30);
	this->mTailMotor.setPWM(30);
//	mTailMotor.setPWM(30);

#if 0
	if (mIsInitialized == false) {
        // モータエンコーダをリセットする
        //mTailMotor.reset();
        mIsInitialized = true;

        sprintf(s,"%s","TAIL CHANGING 2-2");
    		ev3_lcd_draw_string(s,0,40);
    }
#endif
	sprintf(s,"%s","TAIL CHANGING 2-3");
	ev3_lcd_draw_string(s,0,60);


	/*角位置９０度まで動かしてみる*/
	//mTailMotor.setPWM(30);/*スピード３０で動かす*/
	//ev3_motor_set_power(EV3_PORT_D,30);
	ev3_motor_rotate(EV3_PORT_D,90,30,true);

	sprintf(s,"%s","TAIL CHANGING 2-4");
	ev3_lcd_draw_string(s,0,80);

	#if 0
	if(mTailMotor.getCount()==90){/*90度まできた？*/
		mTailMotor.stop();
	}
#endif

}
