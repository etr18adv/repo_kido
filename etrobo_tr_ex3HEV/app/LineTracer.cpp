/******************************************************************************
 *  LineTracer.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/26
 *  Implementation of the Class LineTracer
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "LineTracer.h"

/* HackEV用に、BalancingWalker,Balancerを削除。
 * さらに、GyroSensorもつなげていない。*/
/**
 * コンストラクタ
 * @param lineMonitor     ライン判定
 */
LineTracer::LineTracer(const LineMonitor* lineMonitor,
				ev3api::Motor& leftWheel,
				ev3api::Motor& rightWheel,
				SimpleTimer* simpleTimer)
    : mLineMonitor(lineMonitor),
      mLeftWheel(leftWheel),
      mRightWheel(rightWheel),
	  mSimpleTimer(simpleTimer),
      mIsInitialized(false) {

}

/**
 * ライントレースする
 * 一定時間終了すればtrue,それまではfalse
 */
bool LineTracer::run(int time) {
    if (mIsInitialized == false) {
        // モータエンコーダをリセットする
        mLeftWheel.reset();
        mRightWheel.reset();

        mIsInitialized = true;
    }
    // タイマーをLineTracerの中で実施する
    if(mSimpleTimer->isStarted()){	//タイマー開始した（開始時刻がゼロでない）
		/*HackEV対応*/
		//mForward = SPEED_LOW;
    		mForward = 20;
#if 0
		bool isOnLine = mLineMonitor->isOnLine();

		// 走行体の向きを計算する
		mTurn = calcDirection(isOnLine);
#else
		float Kp = 0.83;
		int diff;
		diff = mLineMonitor->getBrightness();
		diff-= 20;//20は目標値
		mTurn= (int)(Kp*diff);
#endif
		mLeftWheel.setPWM(mForward + mTurn);
		mRightWheel.setPWM(mForward - mTurn);

		if(mSimpleTimer->isTimedOut()){//タイマー終了確認
			mSimpleTimer->stop();			//【重要】タイマーストップ。開始時刻ゼロ。
			return true;
		}
		else{
			return false;
		}

    }
    else{//タイマー開始していない（開始時刻がゼロ）
    		mSimpleTimer->setTime(time);//指定時間をセット
        mSimpleTimer->start();
    		return false;
    }
}

/**
 * 走行体の向きを計算する
 * @param isOnLine true:ライン上/false:ライン外
 * @retval 30  ライン上にある場合(右旋回指示)
 * @retval -30 ライン外にある場合(左旋回指示)
 */
int LineTracer::calcDirection(bool isOnLine) {
    if (isOnLine) {
        // ライン上にある場合
        return SPEED_LOW;
    } else {
        // ライン外にある場合
        return -SPEED_LOW;
    }
}
