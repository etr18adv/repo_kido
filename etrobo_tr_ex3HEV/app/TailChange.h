/*
 * TailChange.h
 *
 *  Created on: 2018/05/06
 *      Author: Masahiro
 */

#ifndef MODELING_ETROBO_TR_EX3HEV_APP_TAILCHANGE_H_
#define MODELING_ETROBO_TR_EX3HEV_APP_TAILCHANGE_H_

#include "Motor.h"

class TailChange {
public:
	TailChange(ev3api::Motor& tailMotor);
	virtual ~TailChange();

	void run();

private:
	ev3api::Motor& mTailMotor;
	bool mIsInitialized;
};

#endif /* MODELING_ETROBO_TR_EX3HEV_APP_TAILCHANGE_H_ */
