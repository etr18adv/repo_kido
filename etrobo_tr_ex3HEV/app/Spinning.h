/*
 * Spinning.h
 *
 *  Created on: 2018/04/08
 *      Author: Masahiro
 */

#ifndef SDK_MODELING_ETROBO_TR_EX3HEV_APP_SPINNING_H_
#define SDK_MODELING_ETROBO_TR_EX3HEV_APP_SPINNING_H_

#include "LineMonitor.h"
#include "Motor.h"

#include "SimpleTimer.h"

class Spinning {
public:
	Spinning(const LineMonitor* lineMonitor,
    		ev3api::Motor& leftWheel, /*HackEV用に修正*/
        ev3api::Motor& rightWheel,/*HackEV用に修正*/
		 SimpleTimer* simpleTimer); /*タイマを内部で持つ用に修正*/

	virtual ~Spinning();

	bool run(int time);

private:
    const LineMonitor* mLineMonitor;
    bool mIsInitialized;
    /*HackEV用に修正*/
    ev3api::Motor& mLeftWheel;
    ev3api::Motor& mRightWheel;
    int mForward;
    int mTurn;

	/*タイマーを中で持つ場合の宣言*/
    SimpleTimer* mSimpleTimer;
};

#endif /* SDK_MODELING_ETROBO_TR_EX3HEV_APP_SPINNING_H_ */
